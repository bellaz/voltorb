%% A1 System setup for CW
cd('/home/sesp0915/CMTB')
path(path,'/home/sesp0915/LLRF_matlab_library')
%% (0) Adjust Output Rotation Matrix to align SP and RBV for VS 

%% (1) set ANC and activate
cav_all = {'C1','C2','C3','C4','C5','C6','C7','C8'}
color_cav_plot = {'k','b','r','m','c','y','g',[0.8 0.8 0.8]}
ANC_channel_all = 1:4;

ANC_channel = 1;
ave = 1;

cav_ANC_main = 'C5' 

figure(10)
clf
for k_cav = 1:length(cav_all)
    cav = cav_all{k_cav};
    A = doocsread(['TTF.RF/LLRF.CONTROLLER/',cav,'.M1.CMTB/PROBE.PHASE']);

    A_mat{k_cav} = zeros(ave,length(A.data.d_spect_array_val));
    A_mat{k_cav}(end,:) = A.data.d_spect_array_val;
end
tic 
for k_cav = 1:length(cav_all)
    cav = cav_all{k_cav};
    for k_pulse = 1:ave
        pause(1)
        A_mat{k_cav}(1:end-1,:) = A_mat{k_cav}(2:end,:);
        A = doocsread(['TTF.RF/LLRF.CONTROLLER/',cav,'.M1.CMTB/PROBE.PHASE']);
        A_mat{k_cav}(end,:) = A.data.d_spect_array_val;
        disp(['Collect Pulse ',num2str(k_pulse),' out of ',num2str(ave)])
    end
end
while(1)
   pause(1-toc) 
    tic
    for k_cav = 1:length(cav_all)
        cav = cav_all{k_cav};
        A_mat{k_cav}(1:end-1,:) = A_mat{k_cav}(2:end,:); 
        A = doocsread(['TTF.RF/LLRF.CONTROLLER/',cav,'.M1.CMTB/PROBE.PHASE']);
        A_mat{k_cav}(end,:) = A.data.d_spect_array_val; 
    end
    for k = 1:length(ANC_channel_all)
        A11{k} = doocsread(['TTF.RF/LLRF.PIEZO/',cav_ANC_main,'.M1.CMTB/ANC_FREQ',num2str(k)]);
    end
    
    figure(1)
    hold off
    for k_cav = 1:length(cav_all)
        fun_amplitude_spectrum(A_mat{k_cav}.',cav,'plotdb','hann',1/262e3,1,0,color_cav_plot{k_cav});
        hold on, grid on
    end
    xlim([10 1000])
    legend(cav_all)
    title(['ANC cavity is ',cav_ANC_main,' (Others for comparision)'])
    yy = ylim
    for k = 1:length(ANC_channel_all)   
        plot([A11{k}.data A11{k}.data],[yy],'r--','LineWidth',2)
    end
    drawnow
    for k = 1:length(ANC_channel_all) 
        A1{k} = doocsread(['TTF.RF/LLRF.PIEZO/',cav_ANC_main,'.M1.CMTB/ANC_DELAY',num2str(k)]);
    end
    A2 = doocsread(['TTF.RF/LLRF.CONTROLLER/',cav_ANC_main,'.M1.CMTB/VREFL.PHASE']);
    figure(10)
    for k = 1:length(ANC_channel_all) 
    subplot(length(ANC_channel_all),1,k)
    hold on, grid on  
    plot(A1{k}.data,max(A2.data.d_spect_array_val)-min(A2.data.d_spect_array_val),'ko','MarkerFaceColor','r')
    end
end

return





%% Additional
% can you find an optimal I gain for piezo of C1...C8 such that couplings
% (big oscillations) are reduced?