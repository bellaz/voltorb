clear
tic
clc
cav = 'C1'
ave = 10;
A = doocsread(['TTF.RF/LLRF.CONTROLLER/',cav,'.M1.CMTB/PROBE.PHASE']);
A_mat = zeros(ave,length(A.data.d_spect_array_val));
A_mat(end,:) = A.data.d_spect_array_val;

for k_pulse = 1:ave
    pause(1)
    A_mat(1:end-1,:) = A_mat(2:end,:); 
    A = doocsread(['TTF.RF/LLRF.CONTROLLER/',cav,'.M1.CMTB/PROBE.AMPL']);
    A_mat(end,:) = A.data.d_spect_array_val; 
    k_pulse
end

%%
while(1)
   pause(1-toc) 
    tic
    A_mat(1:end-1,:) = A_mat(2:end,:); 
    A = doocsread(['TTF.RF/LLRF.CONTROLLER/',cav,'.M1.CMTB/PROBE.AMPL']);
    A_mat(end,:) = A.data.d_spect_array_val;    
    fun_amplitude_spectrum(A_mat.',cav,'plotdb','hann',1/262e3);
    grid on
    drawnow
end