%% Pulse mode tools
main_path = 'TTF.RF/LLRF.CONTROLLER/'
CAV_all = {'C1','C2','C3','C4','C5','C6','C7','C8'};

gradient_min = 0.5;         % minimal gradient along the RF pulse for calibration etc.

%% Find cavities in VS ...
CAV_in_VS = {}
for k = 1:length(CAV_all)
    temp = doocsread([main_path,CAV_all{k},'.M1.CMTB/PROBE.PVS_ENA']);
    if(temp.data)
        CAV_in_VS = [CAV_in_VS CAV_all{k}]
    end
end

clc
CAV_in_VS
%% (0a) Roughly Tune Cavities to see a Signal
disp('Manual Step: (0) Roughly Tune Cavities to see a Signal...')
disp('Do it by hand')

temp = doocsread([main_path,'MAIN.CMTB/PULSE_DELAY_PERCENT']);
PULSE_DELAY_PERCENT = temp.data
temp = doocsread([main_path,'MAIN.CMTB/PULSE_FILLING_PERCENT']);
PULSE_FILLING_PERCENT = temp.data
temp = doocsread([main_path,'MAIN.CMTB/PULSE_FLATTOP_PERCENT']);
PULSE_FLATTOP_PERCENT = temp.data
%% rough tuning - not working
dw = 1e6;
signdw_old = 1;

for k = 3:length(CAV_in_VS)
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.AMPL']);
    PA = (temp.data.d_spect_array_val);
    PA_end = length(PA)*(PULSE_DELAY_PERCENT+PULSE_FILLING_PERCENT+PULSE_FLATTOP_PERCENT)/100;
    dk = round(PA_end+500:PA_end+5000);
    dw = 1e6;
    k_loop = 1;
    corr_size_init = 1;
    if(max(PA)>0.25)
        while(abs(dw)>1e4)
            clc
            if(k_loop && abs(dw)>1e4)
                temp = doocsread(['TTF.RF/LLRF.PIEZO/',CAV_in_VS{k},'.M1.CMTB/DAC_OFFSET_SP'])
                U_DAC = temp.data;
                temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.PHASE']);
                PA = (temp.data.d_spect_array_val);
                pp = fit(dk.',unwrap(PA(dk)/180*pi),'poly1');
                dw = pp(1).*(dk(end)-dk(1))
                U_DAC_new = U_DAC + corr_size_init;
                temp = doocswrite(['TTF.RF/LLRF.PIEZO/',CAV_in_VS{k},'.M1.CMTB/DAC_OFFSET_SP'],U_DAC_new)
                pause(4)
                temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.PHASE']);
                PA = (temp.data.d_spect_array_val);
                pp = fit(dk.',unwrap(PA(dk)/180*pi),'poly1');
                dw_new = pp(1).*(dk(end)-dk(1))
                k_gain = sign(dw/dw_new/4)
                k_loop = 0;
            end
            temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.PHASE']);
            PA = (temp.data.d_spect_array_val);
            pp = fit(dk.',unwrap(PA(dk)/180*pi),'poly1');
            CAV_in_VS{k}
            dw = pp(1).*(dk(end)-dk(1))
            if(signdw_old ~= sign(dw) && ~k_loop)
                corr_size_init = corr_size_init/2
            end
            signdw_old = sign(dw);
            U_DAC = U_DAC - corr_size_init*k_gain*sign(dw);
            temp = doocswrite(['TTF.RF/LLRF.PIEZO/',CAV_in_VS{k},'.M1.CMTB/DAC_OFFSET_SP'],U_DAC);
            corr_size_init
            figure(111)
            clf
            plot(PA,'k')
            hold on, grid on
            plot(dk,unwrap(PA(dk)/180*pi)*180/pi,'r','LineWidth',2)
            % dw = unwrap(PA(dk)/180*pi).*(dk(end)-dk(1));
            pause(1)
        end
    end
    pause(1)
end
% piezo_limit = 50;
% for k = 1:length(CAV_in_VS)
%    temp = doocsread(['TTF.RF/LLRF.PIEZO/',CAV_in_VS{k},'.M1.CMTB/DAC_OFFSET_SP'])
%    temp0 = doocsread(['TTF.RF/CPL.LV/',CAV_in_VS{k},'.MTS/E.ACC'])
%    A_meas(:,k) = temp0.data;
%    
% end
%% (0b) Set QL to desired value


%% (1) Match estimated probe amplitude with RBV
for k = 1:length(CAV_in_VS)
    temp0 = doocsread(['TTF.RF/CPL.LV/',CAV_in_VS{k},'.MTS/E.ACC'])
    A_meas = temp0.data
    temp1 = doocsread(['TTF.RF/CPL.LV/',CAV_in_VS{k},'.MTS/P.IN']);
    QL = doocsread(['TTF.RF/LLRF.DIAGNOSTICS/',CAV_in_VS{k},'.M1.CMTB/CAV_QL']);
    A_est = sqrt(temp1.data*1e3*4*1023*QL.data)/1e6
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.AMPL']);
    PA = max(temp.data.d_spect_array_val);
     if(PA>gradient_min)
        temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.CAL_SCA'])
        A_new = A_est/PA * temp.data;
        A = doocswrite(temp.channel,A_new);
     end    
end
%% (2) Do cavity phasing to get all phases equal

%% (3a) Calibrate FORW and REFL signal wrt PROBE signal
clear PA
for k = 1:length(CAV_in_VS)
    BUFF_curr = doocsread([main_path,'MAIN.CMTB/CURRENT_BUFFER']);
    BUF = mod(BUFF_curr.data-2,16);
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.AMPL'],[BUF]);
    PA(:,k) = temp.data.d_spect_array_val;
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.PHASE'],[BUF]);
    PP(:,k) = temp.data.d_spect_array_val;
    
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VFORW.AMPL'],[BUF]);
    FA(:,k) = temp.data.d_spect_array_val;
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VFORW.PHASE'],[BUF]);
    FP(:,k) = temp.data.d_spect_array_val;
    
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VREFL.AMPL'],[BUF]);
    RA(:,k) = temp.data.d_spect_array_val;
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VREFL.PHASE'],[BUF]);
    RP(:,k) = temp.data.d_spect_array_val;
end
%
for k = 1:length(CAV_in_VS)
    Pc(:,k) = PA(:,k).*exp(1i*PP(:,k)/180*pi);
    Fc(:,k) = FA(:,k).*exp(1i*FP(:,k)/180*pi);
    Rc(:,k) = RA(:,k).*exp(1i*RP(:,k)/180*pi);
    A = [Fc(:,k) Rc(:,k)];
    B = Pc(:,k);
    coeff1(:,k) = inv(A.'*A)*A.'*B;
    
end
coeff1
pause(3)
%% (3b) Apply correction to A/P scaling/rotation of FORW and REFL
for k = 1:length(CAV_in_VS)
    if(max(PA(:,k))>gradient_min)
        temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VFORW.CAL_SCA'])
        A_new = abs(coeff1(1,k)) * temp.data;
        if(A_new>2)
            A_new = 2;
            warning('Reduced to maximum of 2')
        end
        A = doocswrite(temp.channel,A_new);
        temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VREFL.CAL_SCA'])
        if(A_new>2)
            A_new = 2;
            warning('Reduced to maximum of 2')
        end
        A_new = abs(coeff1(2,k)) * temp.data;
        A = doocswrite(temp.channel,A_new);
        
        temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VFORW.CAL_ROT'])
        A_new = 180/pi*angle(coeff1(1,k)) + temp.data;
        while(abs(A_new)>180)
           A_new = A_new - sign(A_new)*360; 
        end
        A = doocswrite(temp.channel,A_new);        
        temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VREFL.CAL_ROT'])
        A_new = 180/pi*angle(coeff1(2,k)) + temp.data;
        while(abs(A_new)>180)
           A_new = A_new - sign(A_new)*360; 
        end
        A = doocswrite(temp.channel,A_new);
    end
end

%% (4) Check calibration
for k = 1:length(CAV_in_VS)
    BUFF_curr = doocsread([main_path,'MAIN.CMTB/CURRENT_BUFFER']);
    BUF = mod(BUFF_curr.data-2,16);
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.AMPL'],[BUF]);
    PA(:,k) = temp.data.d_spect_array_val;
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/PROBE.PHASE'],[BUF]);
    PP(:,k) = temp.data.d_spect_array_val;
    
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VFORW.AMPL'],[BUF]);
    FA(:,k) = temp.data.d_spect_array_val;
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VFORW.PHASE'],[BUF]);
    FP(:,k) = temp.data.d_spect_array_val;
    
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VREFL.AMPL'],[BUF]);
    RA(:,k) = temp.data.d_spect_array_val;
    temp = doocsread([main_path,CAV_in_VS{k},'.M1.CMTB/VREFL.PHASE'],[BUF]);
    RP(:,k) = temp.data.d_spect_array_val;
    Pc(:,k) = PA(:,k).*exp(1i*PP(:,k)./180*pi);
    Fc(:,k) = FA(:,k).*exp(1i*FP(:,k)/180*pi);
    Rc(:,k) = RA(:,k).*exp(1i*RP(:,k)/180*pi);
    
    figure(100+k)
    clf
    subplot(211)
    plot(abs(Pc(:,k)),'b','LineWidth',2)
    hold on, grid on
    plot(abs(Fc(:,k)),'k','LineWidth',2)
    plot(abs(Rc(:,k)),'r','LineWidth',2)
    plot(abs(Fc(:,k) + Rc(:,k)),'c--','LineWidth',2)
    title(['Calibration CMTB for ',CAV_in_VS{k}])
    ylabel('Amplitude [MV/m]')
    xlabel('Samples')
    subplot(212)
    plot(180/pi*angle(Pc(:,k)),'b','LineWidth',2)
    hold on, grid on
    plot(180/pi*angle(Fc(:,k)),'k','LineWidth',2)
    plot(180/pi*angle(Rc(:,k)),'r','LineWidth',2)
    plot(180/pi*angle(Fc(:,k)+Rc(:,k)),'c--','LineWidth',2)
    title(['Calibration CMTB for ',CAV_in_VS{k}])
    ylabel('Phase [deg]')
    xlabel('Samples')    
end

%% (5) Check and understand VS_BIT_SHIFT 



%% Go to CW mode