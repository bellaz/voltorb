function Value = read_fw(device,module,register)
try
  Value = device.read(module, register);
catch exception
  fprintf(['Cannot find ',module,'.',register,' in device\n Trying ',register,'\n']);
  try
    Value = device.read('', register);
  catch
      Value = [];
    fprintf('! Cannot find register... !\n');
    return;
  end
end