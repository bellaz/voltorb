clc
local_dev = 'UTC4';
server_loc = '/export/doocs/server/llrfCtrl_server';
mtca4u.setDMapFilePath([server_loc,'/devMapFile.dmap']);
device = mtca4u(local_dev);

module = 'APP0';
registers = {'WORD_MIMO_A11','WORD_MIMO_A12','WORD_MIMO_A21','WORD_MIMO_A22',....
   'WORD_MIMO_B11','WORD_MIMO_B12','WORD_MIMO_B21','WORD_MIMO_B22',...
   'WORD_MIMO_COEF_VALID','WORD_MIMO_ENA'};

for prop = 1:length(registers)
    value = read_fw(device,module,registers{prop});
    disp([registers{prop},' is: ', num2str(value)])
end

%% 

load('/home/sesp0915/CW_Tests/C1_CMTB.mat')
path('/home/sesp0915/LLRF_matlab_library',path)
%%
Ts = C1.Ts;
z = tf('z',Ts);

clc
rho = 1/30    % 7000 1100  % paper: 52
Tc  = 1/600    % 1200 250   % paper: 135

Tfinal_step = 10000e-6;

f1 = 1/Tc;
f2 = 1/(rho*Tc);

C = 0.5*(1/rho * ((Ts+2*rho*Tc) + (Ts-2*rho*Tc)*z^-1) /((Ts+2*Tc) + (Ts-2*Tc)*z^-1));
dcgain(C)

[num, den] = tfdata(C);

if(den{1}(1)~=1)
    num{1} = num{1}/den{1}(1);
    den{1} = den{1}/den{1}(1);
    num{1}
    den{1}
end

C1 = tf(num{1},den{1},Ts,'Variable','z^-1');


figure(1)
clf
fun_myBo(C,'k');

subplot(212)
ylim([-180 180])
xlim([1 inv(C1.Ts)])

Ts = C1.Ts
Nr_bit = 18;
C_phi = 0;

sca = 2^(Nr_bit-1);
C2 = C1.*[cosd(C_phi) , sind(C_phi) ; -sind(C_phi) cosd(C_phi)] ;
[num, den] = tfdata(C2);
den_int{1}(1:3) = zeros(1,3);
den_int{2}(1:3) = zeros(1,3);
den_int{3}(1:3) = zeros(1,3);
den_int{4}(1:3) = zeros(1,3);

num_int{1}(1:3) = zeros(1,3);
num_int{2}(1:3) = zeros(1,3);
num_int{3}(1:3) = zeros(1,3);
num_int{4}(1:3) = zeros(1,3);
%
num_int{1}(1:length(num{1})) =round(num{1}*sca/2);
num_int{2}(1:length(num{2})) =round(num{2}*sca/2);
num_int{3}(1:length(num{3})) =round(num{3}*sca/2);
num_int{4}(1:length(num{4})) =round(num{4}*sca/2);

den_int{1}(1:length(den{1})) =round(den{1}.*sca/2);
den_int{2}(1:length(den{2})) =round(den{2}.*sca/2);
den_int{3}(1:length(den{3})) =round(den{3}.*sca/2);
den_int{4}(1:length(den{4})) =round(den{4}.*sca/2);

C_int_II = tf(num_int{1},den_int{1},Ts);
C_int_QI = tf(num_int{2},den_int{2},Ts);
C_int_IQ = tf(num_int{3},den_int{3},Ts);
C_int_QQ = tf(num_int{4},den_int{4},Ts);

disp(['Controller with integer values (',num2str(Nr_bit),' bit): '])
C_int = [C_int_II C_int_IQ ; C_int_QI C_int_QQ]
%
device.write(module, 'WORD_MIMO_ENA',0)

device.write(module, 'WORD_MIMO_A11',den_int{1}(2:end))
device.write(module, 'WORD_MIMO_A12',den_int{2}(2:end))
device.write(module, 'WORD_MIMO_A21',den_int{3}(2:end))
device.write(module, 'WORD_MIMO_A22',den_int{4}(2:end))

device.write(module, 'WORD_MIMO_B11',num_int{1})
device.write(module, 'WORD_MIMO_B12',num_int{2})
device.write(module, 'WORD_MIMO_B21',num_int{3})
device.write(module, 'WORD_MIMO_B22',num_int{4})
pause(0.2)
device.write(module, 'WORD_MIMO_COEF_VALID',1)
pause(0.2)
device.write(module, 'WORD_MIMO_ENA',1)


%%
% Do FFT on flashlxuser1 to set ANC
tic
A = doocsread('TTF.RF/LLRF.CONTROLLER/C3.M1.CMTB/PROBE.PHASE');

while(1)
   pause(1-toc) 
    tic
    A = doocsread('TTF.RF/LLRF.CONTROLLER/C3.M1.CMTB/PROBE.PHASE');
    fun_amplitude_spectrum(A.data.d_spect_array_val,'test','plotdb','off',1/262e3);
end




