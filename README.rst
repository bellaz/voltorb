=======
voltorb
=======

.. image:: docs/voltorb.gif
    :width: 200

Scripts for accelerating SRF CW 

Description
===========

A generic set of script to characterize/measure/calibrate/drive SRF CW 
accelerating modules built on MSK-LLRF/doocs infrastructure


Requirements
============

- python3  
  - pydoocs_
- doocs_ 

Installation
============

install:
.. code-block:: bash
    python3 setup.py install

test:
.. code-block:: bash
    python3 setup.py test

Scripts usage
=============

Data file interfaces 
====================

To ease data sharing among different programs, this program support a minimal
IO interface to write/read dictionaries to/from different file formats.
The following formats are supported:

- HDF5 (.h5) : Portable binary scientific format suitable for large datafiles
- MatFile (.mat) : Matlab® data file format
- Python Pickle (.pickle) : Python native serialization format
- JSON (.json) : JavaScript Object Notation file format
- YAML (.yaml) : human-readable data serialization language
- Excel2010 (.xlsx) : Office Open XML spreadsheet format

Example of the use of the dictionary file interface:

.. literalinclude:: examples/read_write_dictfile.py
    :language: python3

Note the following:

1) If a filename terminates with one of the recognized file format extensions 
    the use of such a filetype parser is enforced regardless of the additional 
    parameters passed to the I/O functions

2) The array-like elements are converted to numpy arrays after the deserialization

Note
====

This project has been set up using PyScaffold 3.1. For details and usage
information on PyScaffold see https://pyscaffold.org/.

.. _pydoocs: https://ttfinfo.desy.de/DOOCSWiki/Wiki.jsp?page=PythonClientInterface
.. _doocs: http://tesla.desy.de/doocs/doocs.html
