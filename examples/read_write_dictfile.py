'''
    example of the use of dictfile interface to read/write from/to multiple file
    formats
'''
import numpy as np
import voltorb.dictfile as dictfile

#
# Create the dictionary values to be written. the array can be described by numpy
# types or native iterable types
#
values = {}
values['STRING'] = 'A string'
values['INT'] = 42
values['ARRAY'] = [1, 2, 3, 4, 5, 6]
values['2DARRAY'] = np.array([[1+2j, 3+5j], [-7-10j, -2j]])

# write a dictionary using the file extension to determine the file type
dictfile.write('test.h5', values)
#dictfile.write('test.json', values)
#dictfile.write('test.yml', values)
#dictfile.write('test.mat', values)
#dictfile.write('test.pickle', values)
#dictfile.write('test.xlsx', values)

# read a dictionary file
values = dictfile.read('test.h5')
# values = dictfile.read('test.json')
# values = dictfile.read('test.yml')
# values = dictfile.read('test.mat')
# values = dictfile.read('test.pickle')
# values = dictfile.read('test.xlsx')

# write a dictionary file giving explicitly the file type to use:
# dictfile.write('unknown_extension', values, 'HDF5')
# dictfile.write('unknown_extension', values, 'JSON')
# dictfile.write('unknown_extension', values, 'YAML')
# dictfile.write('unknown_extension', values, 'MATFILE')
# dictfile.write('unknown_extension', values, 'PICKLE')
# dictfile.write('unknown_extension', values, 'YAML')
