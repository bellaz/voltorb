'''
    Test dictfile backends
'''

import io
import numpy as np
import voltorb.dictfile as dictfile
from tests.test_types import SANITIZABLE_VALUES, SANITIZED_VALUES

def compare_dicts_numpy(dict1, dict2):
    '''
        compare two dicts (string keyed) with ndarray variables
    '''
    assert len(dict1) == len(dict2)

    for key in dict1:
        if isinstance(dict1[key], np.ndarray):
            assert np.array_equal(dict1[key], dict2[key])
        else:
            assert dict1[key] == dict2[key]

def test_hdf5():
    '''
        test HDF5 read/write capabilities
    '''
    testfile = io.BytesIO()
    executor = dictfile.DICTFILE_HDF5.manager(testfile)
    executor.write(SANITIZABLE_VALUES)
    compare_dicts_numpy(SANITIZED_VALUES, executor.read())

def test_json():
    '''
        test JSON read/write capabilities
    '''
    testfile = io.StringIO()
    executor = dictfile.DICTFILE_JSON.manager(testfile)
    executor.write(SANITIZABLE_VALUES)
    testfile.seek(0)
    compare_dicts_numpy(SANITIZED_VALUES, executor.read())

def test_yaml():
    '''
        test YAML read/write capabilities
    '''
    testfile = io.StringIO()
    executor = dictfile.DICTFILE_YAML.manager(testfile)
    executor.write(SANITIZABLE_VALUES)
    testfile.seek(0)
    compare_dicts_numpy(SANITIZED_VALUES, executor.read())

def test_matfile():
    '''
        test MATFILE read/write capabilities
    '''
    testfile = io.BytesIO()
    executor = dictfile.DICTFILE_MATFILE.manager(testfile)
    executor.write(SANITIZABLE_VALUES)
    testfile.seek(0)
    compare_dicts_numpy(SANITIZED_VALUES, executor.read())

def test_pickle():
    '''
        test PICKLE read/write capabilities
    '''
    testfile = io.BytesIO()
    executor = dictfile.DICTFILE_PICKLE.manager(testfile)
    executor.write(SANITIZABLE_VALUES)
    testfile.seek(0)
    compare_dicts_numpy(SANITIZED_VALUES, executor.read())

def test_excel2010():
    '''
        test EXCEL2010 read/write capabilities
    '''
    testfile = io.BytesIO()
    executor = dictfile.DICTFILE_EXCEL2010.manager(testfile)
    executor.write(SANITIZABLE_VALUES)
    testfile.seek(0)
    compare_dicts_numpy(SANITIZED_VALUES, executor.read())
