'''
    Test of sanitize_data
'''
import pytest
import numpy as np
from voltorb.dictfile_backend.sanitize_data import (sanitize_value, sanitize_numpy,
                                                    sanitize_native)

from tests.test_types import (SANITIZED_VALUES, SANITIZED_VALUES_LIST, UNSANITIZABLE_VALUES,
                              SANITIZABLE_VALUES)

def test_sanitize_value():
    '''
        Test sanitize value function
    '''
    for key in SANITIZABLE_VALUES:
        sanitized = sanitize_value(SANITIZABLE_VALUES[key])
        assert np.array_equal(sanitized, SANITIZED_VALUES[key])

def test_sanitize_numpy():
    '''
        Test sanitize numpy
    '''
    sanitized_dict = sanitize_numpy(SANITIZABLE_VALUES)
    for key in SANITIZABLE_VALUES:
        if isinstance(SANITIZED_VALUES[key], np.ndarray):
            assert np.array_equal(sanitized_dict[key], SANITIZED_VALUES[key])
        else:
            assert sanitized_dict[key] == SANITIZED_VALUES[key]


def test_sanitize_fail():
    '''
        Test exception of sanitize value
    '''
    for key in UNSANITIZABLE_VALUES:
        with pytest.raises(TypeError):
            sanitize_value(UNSANITIZABLE_VALUES[key])

def test_sanitize_native():
    '''
        Test sanitize native
    '''
    sanitized_dict = sanitize_native(SANITIZABLE_VALUES)
    for key in SANITIZED_VALUES_LIST:
        assert sanitized_dict[key] == SANITIZED_VALUES_LIST[key]
