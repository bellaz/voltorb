'''
    Test encode spreadsheet
'''
from voltorb.dictfile_backend.sanitize_data import sanitize_native
from voltorb.dictfile_backend.encode_spreadsheet import encode, decode, __reduce_col

TEST_STRING = ('STR', 'STR')
TEST_FLOAT = ('FLOAT', 1.33)
TEST_INT = ('INT', 433)
TEST_VECTOR = ('VEC', [1, 2])
TEST_MATRIX = ('MAT', [[1, 3], [2, 4]])

INPUT_ENCODE1 = dict([TEST_STRING, TEST_VECTOR, TEST_MATRIX])
INPUT_ENCODE2 = dict([TEST_STRING, TEST_FLOAT, TEST_INT])
INPUT_ENCODE3 = dict([TEST_MATRIX])
INPUT_ENCODE4 = dict([TEST_VECTOR])
INPUT_ENCODE5 = dict([TEST_VECTOR, TEST_MATRIX])

OUTPUT_ENCODE1 = [['STR', 'STR', None, 'VEC', None, 'MAT', 'MAT'],
                  [None, None, None, 1, None, 1, 2],
                  [None, None, None, 2, None, 3, 4]]

OUTPUT_ENCODE3 = [[None, None, None, None, 'MAT', 'MAT'],
                  [None, None, None, None, 1, 2],
                  [None, None, None, None, 3, 4]]

OUTPUT_ENCODE4 = [[None, None, None, 'VEC', None],
                  [None, None, None, 1, None],
                  [None, None, None, 2, None]]

OUTPUT_ENCODE5 = [[None, None, None, 'VEC', None, 'MAT', 'MAT'],
                  [None, None, None, 1, None, 1, 2],
                  [None, None, None, 2, None, 3, 4]]

OUTPUT_ENCODE6 = [['STR', 'STR', None, 'VEC', None, 'MAT', 'MAT', None, 1],
                  [None, None, None, 1, None, 1, 2, None, 2],
                  [None, None, None, 2, None, 3, 4, None, None],
                  [None, None, None, None, None, None, None, None, None],
                  ['Grinch', 'Awful', None, 2, None, 3, 4, None, None]]

def test___reduce_col():
    '''
        Test reduce column function
    '''
    assert [] == __reduce_col([])
    assert [] == __reduce_col([None])
    assert [] == __reduce_col([None, None])
    assert [] == __reduce_col([None, None, 1])
    assert [] == __reduce_col([None, None, 2, None])
    assert [1] == __reduce_col([1])
    assert [1] == __reduce_col([1, None])
    assert [1] == __reduce_col([1, None, 2])
    assert ['a', 'b', 'c'] == __reduce_col(['a', 'b', 'c'])
    assert ['a', 'b', 'c'] == __reduce_col(['a', 'b', 'c', None])

def test_encode1():
    '''
        test encode on dataset1
    '''
    assert OUTPUT_ENCODE1 == encode(INPUT_ENCODE1)

def test_encode2():
    '''
        test encode on dataset1
    '''
    encoded = list(zip(*encode(INPUT_ENCODE2)))
    assert 'STR' in encoded[0]
    assert 'INT' in encoded[0]
    assert 'FLOAT' in encoded[0]
    assert 'STR' in encoded[1]
    assert 433 in encoded[1]
    assert 1.33 in encoded[1]
    assert encoded[2] == (None, None, None)

def test_encode3():
    '''
        test encode on dataset3
    '''
    assert OUTPUT_ENCODE3 == encode(INPUT_ENCODE3)

def test_encode4():
    '''
        test encode on dataset4
    '''
    assert OUTPUT_ENCODE4 == encode(INPUT_ENCODE4)

def test_encode5():
    '''
        test encode on dataset5
    '''
    assert OUTPUT_ENCODE5 == encode(INPUT_ENCODE5)

def test_decode_1():
    '''
        test decode on dataset1
    '''
    assert INPUT_ENCODE1 == sanitize_native(decode(OUTPUT_ENCODE1))

def test_decode_2():
    '''
        test decode on dataset1
    '''
    assert INPUT_ENCODE2 == sanitize_native(decode([['STR', 'STR'],
                                                    ['FLOAT', 1.33],
                                                    ['INT', 433]]))

def test_decode_3():
    '''
        test decode on dataset3
    '''
    assert INPUT_ENCODE3 == sanitize_native(decode(OUTPUT_ENCODE3))

def test_decode_4():
    '''
        test decode on dataset4
    '''
    assert INPUT_ENCODE4 == sanitize_native(decode(OUTPUT_ENCODE4))

def test_decode_5():
    '''
        test decode on dataset5
    '''
    assert INPUT_ENCODE5 == sanitize_native(decode(OUTPUT_ENCODE5))

def test_decode_6():
    '''
        test decode on dataset6
    '''
    assert INPUT_ENCODE1 == sanitize_native(decode(OUTPUT_ENCODE6))
