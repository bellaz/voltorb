'''
    Test dictfile
'''
import tempfile
import pytest
import voltorb.dictfile as dictfile

def test_search_by_name():
    '''
        test search by name
    '''
    for dictype in dictfile.DICTFILE_TYPES:
        assert dictype == dictfile.search_type_by_name(dictype.name)

    with pytest.raises(RuntimeError):
        dictfile.search_type_by_name('UNKNOWN')

def test_search_by_ext():
    '''
        test search by ext
    '''
    for dictype in dictfile.DICTFILE_TYPES:
        assert dictype == dictfile.search_type_by_ext(dictype.ext)

def test_guess_by_name():
    '''
        test guess by name
    '''
    for dictype in dictfile.DICTFILE_TYPES:
        mpfile = tempfile.NamedTemporaryFile(suffix='.' + dictype.ext)
        assert dictype == dictfile.guess_type_by_filename(mpfile.name)

    with pytest.raises(RuntimeError):
        dictfile.guess_type_by_filename('not.')

    with pytest.raises(RuntimeError):
        dictfile.guess_type_by_filename('Ava.3lid')

    with pytest.raises(RuntimeError):
        dictfile.guess_type_by_filename('exten38*on.')
