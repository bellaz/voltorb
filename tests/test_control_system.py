'''
    Control system interface tests
'''
import pytest
import voltorb.controlsys as controlsys

def test_search_by_scheme():
    '''
        test search by scheme
    '''
    for controltype in controlsys.CONTROLSYS_TYPES:
        assert controltype == controlsys.search_type_by_scheme(controltype.scheme)
