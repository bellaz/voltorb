'''
    Useful types to test IO functions
'''

import numpy as np

INTEGER = 42
FLOAT = 3.1415
LONG = 65536*65536
BOOL = True
STR = 'gesundheit'
COMPLEX = 3.1415 + 33j
LIST1DARRAY = [1.0, 100.0]
LIST2DARRAY = [[10, 3], [-11, -7]]
LISTCOMPLEX = [1.0 + 0.1j, 100.0 + 1000.0j]
NP1DARRAY = np.array(LIST1DARRAY)
NP2DARRAY = np.array(LIST2DARRAY)
NPCOMPLEXARRAY = np.array(LISTCOMPLEX)

SANITIZABLE_VALUES = {
        'INTEGER'        : INTEGER,
        'FLOAT'          : FLOAT,
        'LONG'           : LONG,
        'BOOL'           : BOOL,
        'STR'            : STR,
        'COMPLEX'        : COMPLEX,
        'LIST1DARRAY'    : LIST1DARRAY,
        'LIST2DARRAY'    : LIST2DARRAY,
        'LISTCOMPLEX'    : LISTCOMPLEX,
        'NP1DARRAY'      : NP1DARRAY,
        'NP2DARRAY'      : NP2DARRAY,
        'NPCOMPLEXARRAY' : NPCOMPLEXARRAY}

SANITIZED_VALUES = {
        'INTEGER'        : INTEGER,
        'FLOAT'          : FLOAT,
        'LONG'           : LONG,
        'BOOL'           : BOOL,
        'STR'            : STR,
        'COMPLEX'        : COMPLEX,
        'LIST1DARRAY'    : NP1DARRAY,
        'LIST2DARRAY'    : NP2DARRAY,
        'LISTCOMPLEX'    : NPCOMPLEXARRAY,
        'NP1DARRAY'      : NP1DARRAY,
        'NP2DARRAY'      : NP2DARRAY,
        'NPCOMPLEXARRAY' : NPCOMPLEXARRAY}

SANITIZED_VALUES_LIST = {
        'INTEGER'        : INTEGER,
        'FLOAT'          : FLOAT,
        'LONG'           : LONG,
        'BOOL'           : BOOL,
        'STR'            : STR,
        'COMPLEX'        : COMPLEX,
        'LIST1DARRAY'    : LIST1DARRAY,
        'LIST2DARRAY'    : LIST2DARRAY,
        'LISTCOMPLEX'    : LISTCOMPLEX,
        'NP1DARRAY'      : LIST1DARRAY,
        'NP2DARRAY'      : LIST2DARRAY,
        'NPCOMPLEXARRAY' : LISTCOMPLEX}

LIST3DARRAY = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]
NP3DARRAY = np.array(LIST3DARRAY)
NONE = None
OBJECT = ([2, 3], 3)
DICT = {'a' : 3, 'b' : 4}

UNSANITIZABLE_VALUES = {
        'LIST3DARRAY': LIST3DARRAY,
        'NP3DARRAY'  : NP3DARRAY,
        'NONE' : NONE,
        'OBJECT' : OBJECT,
        'VOIDDICT' :  DICT}
