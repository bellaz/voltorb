
# CW\_util\_scripts

---
## Scope

Create a generic set of script to characterize/measure/calibrate/drive SRF CW accelerating modules
built on MSK-LLRF/doocs infrastructure

---
## Requirements

- python3
    - numpy
    - scipy
    - h5py
    - toml
    - [pydoocs](https://ttfinfo.desy.de/DOOCSWiki/Wiki.jsp?page=PythonClientInterface)
- [doocs](http://tesla.desy.de/doocs/doocs.html) 

---
## Installation

```bash
    python3 setup.py install
```

---

