#!/usr/bin/env python
'''
    Show dictfile values
'''

from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    input_file = params['ifile']
    dictfile = dfile.read(input_file)
    justification = max(*[len(el) for el in dictfile])

    for k, v in dictfile.items():
        if type(v).__name__ == 'ndarray':
            print(k.ljust(justification), ': ndarray', v.shape)
        else:
            print(k.ljust(justification), ':', type(v).__name__)

if __name__ == '__main__':
    main.run()
