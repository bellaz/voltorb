#!/usr/bin/env python
'''
    Convert a dictfile in an other type
'''

from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    input_file = params['ifile']
    output_file = params['ofile']

    dfile.write(output_file, dfile.read(input_file))

if __name__ == '__main__':
    main.run()
