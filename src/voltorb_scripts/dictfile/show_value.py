#!/usr/bin/env python
'''
    Show dictfile values
'''

from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    input_file = params['ifile']
    dictfile = dfile.read(input_file)
    print(dictfile[params['name']])

if __name__ == '__main__':
    main.run()
