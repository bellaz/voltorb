#!/usr/bin/env python
'''
    Long term stability using piezos
'''

import numpy as np
from numpy.fft import rfft
from scipy.signal import detrend
import voltorb.dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ifile = params['ifile']
    trace_time = params.get('trace_time', 1)
    dictfile = dfile.read(ifile)
    timestamps = dictfile['TIMESTAMPS']
    labels = dictfile['LABELS']
    max_freq = params['max_freq']

    trace=None

    for label in labels:
        plt.figure()
        trace = dictfile[label]
        fft = np.transpose(np.log(np.abs(rfft(detrend(trace, type='constant')))))

        print(fft.shape)

        plt.title(label)
        plt.imshow(np.flip(fft, axis=0), interpolation='bilinear', extent=(0.0,
                                                          timestamps[-1]/3600,
                                                          0.0,
                                                          10000),
                                                  aspect='auto')
        plt.ylim(1, max_freq)
        plt.xlabel('Time [h]')
        plt.ylabel('Spectrum component [Hz]')
        plt.draw()

    plt.figure()
    plt.plot(np.linspace(0, 10000, len(trace[0])/2+1), np.abs(rfft(detrend(trace[0], type='constant'))))
    plt.draw()
    
    input('Press Enter..')
