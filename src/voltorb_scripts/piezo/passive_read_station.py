#!/usr/bin/env python
'''
    Set an excitation to a cavity piezo actuator and read the piezo sensor
'''

import time
import numpy as np
import voltorb.controlsys as csys
import voltorb.dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    output_file = params['ofile']
    delay = params['delay']
    measurements = params['measurements']

    dictfile = {}

    piezo_sensor_addr = params['template_piezo_sensor_addr']

    addresses = []
    labels = []
    for cav in range(1, 9):
        for module in range(1, 5):
            addresses.append(piezo_sensor_addr.format(cav, module))
            labels.append('C' + str(cav) + '.M' + str(module))

    dictfile['LABELS'] = labels

    datalen = len(csys.read(piezo_sensor_addr.format(1, 1)))

    piezo_trace_avg = np.zeros((32, datalen))

    print('Waiting {} seconds'.format(delay*10))
    time.sleep(delay*10)

    for i in range(measurements):
        time.sleep(delay)
        data = np.array(csys.read(*addresses))
        piezo_trace_avg += data

        print('Done', i+1, '/', measurements)

    piezo_trace_avg /= measurements
    dictfile['PIEZO_TRACE_AVG'] = piezo_trace_avg
    dfile.write(output_file, dictfile)


if __name__ == "__main__":
    main.run()
