#!/usr/bin/env python
'''
    Show the fft of a measured piezo module trace
'''

import numpy as np
from numpy.fft import rfft
from scipy.signal import detrend
import voltorb.dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params): 
    '''
        Script main entry point
    '''
    only_module = params.get('only_module', None)
    range_hz = params.get('range_hz', None)
    normalization = params.get('normalization', 1)
    input_file = params['ifile']
    dictfile = dfile.read(input_file)
    labels = dictfile['LABELS']
    cavities = len(labels) 
    sample_freq = params.get('sample_freq', 20000) 
    range_freq = params.get('range_freq', None)

    plt.figure(1)
    if range_freq is not None:
        plt.xlim(0, range_freq)
    
    #plt.title('Cavity amplitudes')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Piezo sensor amplitude [UA]')
    
    plt.figure(2)
    if range_freq is not None:
        plt.xlim(0, range_freq)

    #plt.title('Cavity phases')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Piezo sensor phase [deg]')


    traces = detrend(dictfile['PIEZO_TRACE_AVG'], type = 'constant')
    
    ffts = rfft(traces)/normalization
    frequencies = np.linspace(0, sample_freq/2, len(ffts[0]))
   
    plt.figure(1)

    if normalization != 1:
        plt.ylim(10E-6, 1)

    if range_freq is not None:
        plt.xlim(0, range_freq) 

    
    for j in range(cavities):
        if only_module is None:
            plt.semilogy(frequencies, np.abs(ffts[j]), label=labels[j])
        else:
            if labels[j].endswith(str(only_module)):
                plt.semilogy(frequencies, np.abs(ffts[j]), label='Cavity ' + labels[j][1])
    
    plt.legend()
    plt.draw()
   
    plt.figure(2)
    for j in range(cavities):
        if only_module is None:
            plt.semilogy(frequencies, np.angle(ffts[j], deg=True), label=labels[j])
        else:
            if labels[j].endswith(str(only_module)):
                plt.semilogy(frequencies, np.angle(ffts[j], deg=True), label='Cavity ' + labels[j][1])
    
    plt.legend()
    plt.draw()

    input('Press enter')

if __name__ == '__main__':
    main.run()
