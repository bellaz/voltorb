#!/usr/bin/env python
'''
    Set an excitation to a cavity piezo actuator and read the piezo sensor
'''

import time
import numpy as np
import voltorb.controlsys as csys
import voltorb.dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    output_file = params['ofile']
    delay = params['delay']
    piezo_volts = params['piezo_volts']
    piezo_pulses = params['piezo_pulses']
    piezo_freq = params['piezo_freq']
    piezo_delay = params['piezo_delay']
    measurements = params['measurements']
    module = params['module']

    dictfile = {}
    dictfile['PIEZO_VOLTS'] = piezo_volts
    dictfile['PIEZO_PULSES'] = piezo_pulses
    dictfile['PIEZO_FREQ'] = piezo_freq

    piezo_sensor_addr = params['template_piezo_sensor_addr']

    addresses = []
    for cav in range(1, 9):
        addresses.append(piezo_sensor_addr.format(cav, module))

    datalen = len(csys.read(piezo_sensor_addr.format(1, module)))
    
    for cav in range(1, 9):
        piezo_enable_addr = params['template_piezo_enable_addr'].format(cav, module)
        dac_enable_addr = params['template_dac_enable_addr'].format(cav, module)
        piezo_ac_voltage_addr = params['template_piezo_ac_voltage_addr'].format(cav, module)
        piezo_pulses_addr = params['template_piezo_pulses_addr'].format(cav, module)
        piezo_freq_addr = params['template_piezo_freq_addr'].format(cav, module)
        piezo_delay_addr = params['template_piezo_delay_addr'].format(cav, module)

        old_volts = csys.read(piezo_ac_voltage_addr)
        old_pulses = csys.read(piezo_pulses_addr)
        old_freq = csys.read(piezo_freq_addr)
        old_delay = csys.read(piezo_delay_addr)

        csys.write(piezo_ac_voltage_addr, piezo_volts)
        csys.write(piezo_pulses_addr, piezo_pulses)
        csys.write(piezo_freq_addr, piezo_freq)
        csys.write(piezo_delay_addr, piezo_delay)
        csys.write(piezo_enable_addr, 2)
        #csys.write(dac_enable_addr, 1)

        piezo_trace_avg = np.zeros((8, datalen))
        
        print('Waiting {} seconds'.format(delay*10))
        time.sleep(delay*10)

        for i in range(measurements):
            time.sleep(delay)
            data = np.array(csys.read(*addresses))
            piezo_trace_avg += data
            print('Cavity', cav, 'done', i+1, '/', measurements)

        piezo_trace_avg /= measurements
        dictfile['PIEZO_TRACE_AVG_C{}'.format(cav)] = piezo_trace_avg

        #csys.write(dac_enable_addr, 0)
        csys.write(piezo_enable_addr, 0)
        csys.write(piezo_ac_voltage_addr, old_volts)
        csys.write(piezo_pulses_addr, old_pulses)
        csys.write(piezo_freq_addr, old_freq)
        csys.write(piezo_delay_addr, old_delay)

    dfile.write(output_file, dictfile)


if __name__ == "__main__":
    main.run()
