#!/usr/bin/env python
'''
    Set an excitation to a cavity piezo actuator and read the piezo sensor
'''

import time
import numpy as np
import voltorb.controlsys as csys
import voltorb.dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    output_file = params['ofile']
    delay = params['delay']
    measurements = params['measurements']

    dictfile = {}

    piezo_sensor_addr = params['template_piezo_sensor_addr']

    addresses = []
    labels = []

    for cav in range(1, 9):
        addresses.append(piezo_sensor_addr.format(cav))
        labels.append('C' + str(cav))
        dictfile[labels[-1]] = []

    dictfile['LABELS'] = labels

    datalen = len(csys.read(piezo_sensor_addr.format(1, 1)))

    timestamps = []
    initial_time = time.time()

    for i in range(measurements):
        time.sleep(delay) 
        for s in range(8):
            dictfile[labels[s]].append(csys.read(addresses[s]))

        timestamps.append(time.time() - initial_time)
        print('Done', i+1, '/', measurements)

    dictfile['TIMESTAMPS'] = timestamps
    dictfile['LABELS'] = labels
    dfile.write(output_file, dictfile)


if __name__ == "__main__":
    main.run()
