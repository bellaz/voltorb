#!/usr/bin/env python
'''
    Set an excitation to a cavity piezo actuator and read the piezo sensor
'''

import time
import numpy as np
import voltorb.controlsys as csys
import voltorb.dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    output_file = params['ofile']
    delay = params['delay']
    piezo_volts = params['piezo_volts']
    piezo_pulses = params['piezo_pulses']
    piezo_freq = params['piezo_freq']
    template_piezo_ff_table_addr = params['template_piezo_ff_table_addr']
    trace_time = params.get('trace_time', 1)
    measurements = params['measurements']

    dictfile = {}
    dictfile['PIEZO_VOLTS'] = piezo_volts
    dictfile['PIEZO_PULSES'] = piezo_pulses
    dictfile['PIEZO_FREQ'] = piezo_freq

    template_piezo_sensor_addr = params['template_piezo_sensor_addr']

    addresses = []
    for cav in range(1, 9):
        addresses.append(template_piezo_sensor_addr.format(cav))

    datalen = len(csys.read(template_piezo_ff_table_addr.format(1)))

    for cav in range(1, 9):
        csys.write(template_piezo_ff_table_addr.format(cav), list(np.zeros(datalen)))

    print('Datalen:', datalen)

    for cav in range(1, 9):
        bin_len = int(datalen*piezo_pulses/(trace_time*piezo_freq))
        trace_piezo = piezo_volts * np.sin(np.linspace(0, 2*np.pi*piezo_pulses, bin_len))
        print(datalen - bin_len)
        trace_piezo = np.concatenate((trace_piezo, np.zeros(datalen-bin_len)))
        csys.write(template_piezo_ff_table_addr.format(cav), list(trace_piezo))

        piezo_trace_avg = np.zeros((8, len(csys.read(addresses[0]))))

        print('Waiting {} seconds'.format(delay*10))
        time.sleep(delay*10)

        for i in range(measurements):
            time.sleep(delay)
            data = []
            for address in addresses:
                data.append(csys.read(address))

            piezo_trace_avg += np.array(data)
            print('Cavity', cav, 'done', i+1, '/', measurements)

        piezo_trace_avg /= measurements
        dictfile['PIEZO_TRACE_AVG_C{}'.format(cav)] = piezo_trace_avg
        csys.write(template_piezo_ff_table_addr.format(cav), list(np.zeros(datalen)))

    dfile.write(output_file, dictfile)


if __name__ == "__main__":
    main.run()
