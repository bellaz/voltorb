#!/usr/bin/env python
'''
    Show the fft of a measured piezo module trace
'''

import numpy as np
from numpy.fft import rfft
from scipy.signal import detrend
import voltorb.dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap

@parser_default(__doc__)
def main(params): 
    '''
        Script main entry point
    '''
    show_cross = params.get('show_cross', False)
    input_file = params['ifile']
    dictfile = dfile.read(input_file)
    cavities = params.get('cavities', 8)
    sample_freq = params.get('sample_freq', 20000) 
    freq_excitation = params.get('freq_excitation', None)
    periods = params.get('periods', 1)
    normalization = params.get('normalization', 1)
    range_freq = params.get('range_freq', None)

    plt.figure(90)
    if range_freq is not None:
        plt.xlim(0, range_freq)
    
    if normalization != 1:
        plt.ylim(0, 1)

    plt.title('Cavity amplitudes')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Piezo sensor amplitude [UA]')
    
    plt.figure(100)
    if range_freq is not None:
        plt.xlim(0, range_freq)

    plt.title('Cavity phases')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Piezo sensor phase [deg]')
    plt.yticks([-180, -90, 0, 90, 180])

    plt.figure(200)
    if range_freq is not None:
        plt.xlim(0, range_freq)

    plt.title('Cavity cumulative noise')
    plt.xlabel('Frequency [Hz]')
    plt.ylabel('Cumulative noise [Hz]')

    fft_ex = None
    fft_angle = None
    
    if freq_excitation != None:
        len_excitation = int(sample_freq*periods/freq_excitation)
        traces = detrend(dictfile['PIEZO_TRACE_AVG_C1'], type = 'constant')
        excitation = np.sin(np.linspace(0, 2*np.pi*periods, len_excitation))
        excitation = np.concatenate((excitation, [0]*(len(traces[0]) - len_excitation)))
        plt.figure(110)
        plt.plot(excitation)
        fft_ex = np.abs(rfft(excitation))
        fft_angle = np.angle(rfft(excitation), deg=True)
        plt.figure(120)
        frequencies = np.linspace(0, sample_freq/2, len(fft_ex))
        plt.plot(frequencies, fft_ex)
        plt.xlim(0, range_freq)
        
        plt.figure(200)
        plt.plot(frequencies, np.sqrt(np.cumsum(fft_ex**2)))
        
    for i in range(8):
        traces = detrend(dictfile['PIEZO_TRACE_AVG_C{}'.format(i+1)], type = 'constant')
        ffts = rfft(traces)
        frequencies = np.linspace(0, sample_freq/2, len(ffts[i]))

        if show_cross:
            plt.figure(i)
            plt.title('Cavity '+ str(i+1) + ' cross correlation')
            if range_freq is not None:
                plt.xlim(0, range_freq)
        
            plt.xlabel('Frequency [Hz]')
            plt.ylabel('Piezo sensor amplitude [UA]')
            
            for j in range(8):
                plt.plot(frequencies, np.abs(ffts[j]), label='Cavity {}'.format(j+1))
            
            plt.legend()
            plt.draw()
            
        plt.figure(90)
        plt.plot(frequencies, np.abs(ffts[i])/fft_ex/normalization, label='Cavity {}'.format(i+1))
        #plt.plot(frequencies, np.abs(ffts[i])/normalization, label='Cavity {}'.format(i+1))
        plt.ylim(0, 300)

        plt.figure(100)
        plt.plot(frequencies, phase_wrap(np.angle(ffts[i], deg=True) - fft_angle), label='Cavity {}'.format(i+1))


    plt.legend()
    plt.draw()
    plt.figure(90)
    plt.legend()
    plt.draw()
    plt.figure(200)
    plt.legend()
    plt.draw()

    input('Press enter')

if __name__ == '__main__':
    main.run()
