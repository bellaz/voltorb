'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap
import voltorb.dictfile as dfile

def lin_interp(x, y, i, half):
    '''
        linear interpolation
    '''
    return x[i] + (x[i+1] - x[i]) * ((half - y[i]) / (y[i+1] - y[i]))

def half_max_x(x, y):
    '''
        FWHM
    '''
    half = max(y)/np.sqrt(2.0)
    signs = np.sign(np.add(y, -half))
    zero_crossings = (signs[0:-2] != signs[1:-1])
    zero_crossings_i = np.where(zero_crossings)[0]
    return [lin_interp(x, y, zero_crossings_i[0], half),
            lin_interp(x, y, zero_crossings_i[1], half)]

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    print('This script will bring the cavities to resonance')

    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    cavity_cal_amp_addr = params['cavity_cal_amp_addr']

    cav_ql_addr = params.get('cav_ql_addr')
    piezo_dc_addr = params['piezo_dc_addr']

    frequency = params.get('frequency', 1.3E9)
    ql = params.get('ql', csys.read(cav_ql_addr))
    measurements = params.get('measurements', 100)
    delay = params.get('delay', 2)
    volt_range = params.get('volt_range', [-10,10])
    ofile = params.get('ofile', None)

    reset_piezo = params.get('reset_piezo', False)
    recalibrate = params.get('recalibrate', False)

    probe_amps = []
    probe_phas = []
    cal_probe_amps = []
    piezo_volts = np.linspace(volt_range[0], volt_range[1], measurements)
    piezo_volts = np.concatenate((piezo_volts, np.flip(piezo_volts)))

    csys.write(piezo_dc_addr, piezo_volts[0])
    time.sleep(delay)
    for volt in piezo_volts:
        csys.write(piezo_dc_addr, volt)
        print('Piezo V', volt)
        time.sleep(delay)
        probe_amps.append(np.mean(csys.read(probe_amp_addr)))
        probe_phas.append(np.mean(csys.read(probe_pha_addr)))
        cal_probe_amps.append(csys.read(cavity_cal_amp_addr))

    [left_amp, right_amp] = np.split(np.array(probe_amps), 2)
    [left_volt, right_volt] = np.split(np.array(piezo_volts), 2)

    [lVlow, lVhigh] = half_max_x(left_volt, left_amp)
    [rVhigh, rVlow] = half_max_x(right_volt, right_amp)

    #dvolt1 = left_volt[int(lidxhigh)]-left_volt[int(lidxlow)]
    #dvolt2 = right_volt[int(ridxhigh)]-right_volt[int(ridxlow)]
 
    dvolt1 = lVhigh - lVlow
    dvolt2 = rVhigh - rVlow 
   
    dvoltavg = np.mean([dvolt1, dvolt2])
    bw = frequency/ql
    sens = bw/dvoltavg

    print('dvolt1',dvolt1,'V')
    print('dvolt2',dvolt2,'V')
    print('dvoltavg',dvoltavg,'V')
    print('sens', sens,'Hz/V')

    if ofile is not None:
        dictfile = {}
        dictfile['FREQUENCY'] = frequency
        dictfile['QL'] = ql
        dictfile['PROBE_AMPS'] = probe_amps
        dictfile['PROBE_PHAS'] = probe_phas
        dictfile['PIEZO_VOLTAGE'] = piezo_volts
        dictfile['DVOLT1'] = dvolt1
        dictfile['DVOLT2'] = dvolt2
        dictfile['DVOLTAVG'] = dvoltavg
        dictfile['SENS'] = sens

        dfile.write(ofile, dictfile)

    if reset_piezo:
        csys.write(piezo_dc_addr, piezo_volts[np.argmax(probe_amps)])

    if recalibrate:
        amp_scale_addr = params['probe_amp_scale_addr']
        pha_scale_addr = params['probe_pha_rotation_addr']

        idxmax = np.argmax(probe_amps)
        
        csys.write(amp_scale_addr, 
                   csys.read(amp_scale_addr)*cal_probe_amps[idxmax]/probe_amps[idxmax]/1e6)
        csys.write(pha_scale_addr,
                   phase_wrap(csys.read(pha_scale_addr)-probe_phas[idxmax]))

if __name__ == '__main__':
    main.run()
