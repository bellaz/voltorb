#!/usr/bin/env python3
'''
    Check the output of the VM
'''

import numpy as np
from time import time, sleep
from datetime import datetime
from scipy.signal import savgol_filter
from voltorb import dictfile as dfile
from voltorb import controlsys as csys
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    time_s = params.get('time', None)
    delay = params.get('delay', 1)
    sample_freq = params.get('sample_freq', 100)
    keep_samples = params.get('keep_samples', 30000)
    ofile = params.get('ofile', None)

    fig = plt.figure()
    fig.autofmt_xdate()
    ax_amp = plt.subplot(2, 1, 1)
    ax_pha = plt.subplot(2, 1, 2)

    ax_amp.set_ylabel('Amplitude [AU]')
    ax_pha.set_ylabel('Phase [deg]')
    ax_pha.set_xlabel('Time')

    interp_amps = []
    interp_phas = []
    interp_ampin_amps = []
    interp_ampin_phas = []
    interp_ampout_amps = []
    interp_ampout_phas = []

    times = []
    interp_times =[]

    vm_output_amp_addr = params['vm_output_amp_addr']
    vm_output_pha_addr = params['vm_output_pha_addr']
    amplifier_out_amp_addr = params['amplifier_out_amp_addr'] 
    amplifier_out_pha_addr = params['amplifier_out_pha_addr'] 
    amplifier_in_amp_addr = params['amplifier_in_amp_addr'] 
    amplifier_in_pha_addr = params['amplifier_in_pha_addr'] 

    times.append(time())
    
    while True:
        sleep(delay) 
        amp = csys.read(vm_output_amp_addr)[skip:-skip_back-1]
        pha = csys.read(vm_output_pha_addr)[skip:-skip_back-1]
        ampin_amp = csys.read(amplifier_in_amp_addr)[skip:-skip_back-1]
        ampin_pha = csys.read(amplifier_in_pha_addr)[skip:-skip_back-1]
        ampout_amp = csys.read(amplifier_out_amp_addr)[skip:-skip_back-1]
        ampout_pha = csys.read(amplifier_out_pha_addr)[skip:-skip_back-1]

        times.append(time())

        if (time_s is not None) and ((times[-1] - times[0]) > time_s):
            break

        orig_time = np.linspace(times[-2], times[-1], len(amp))
        #amp = savgol_filter(amp, int(len(amp)/sample_freq), 1)
        #pha = savgol_filter(pha, int(len(pha)/sample_freq), 1)
        #ampin_amp  = savgol_filter(ampin_amp, int(len(ampin_amp)/sample_freq), 1)
        #ampin_pha  = savgol_filter(ampin_pha, int(len(ampin_pha)/sample_freq), 1)
        #ampout_amp = savgol_filter(ampout_amp, int(len(ampout_amp)/sample_freq), 1)
        #ampout_pha = savgol_filter(ampout_pha, int(len(ampout_pha)/sample_freq), 1)

        interp_time = np.linspace(times[-2], times[-1], sample_freq)
        interp_times.extend(list(interp_time))
        interp_amps.extend(list(np.interp(interp_time, orig_time, amp)))
        interp_phas.extend(list(np.interp(interp_time, orig_time, pha)))
        interp_ampin_amps.extend(list(np.interp(interp_time, orig_time, ampin_amp)))
        interp_ampin_phas.extend(list(np.interp(interp_time, orig_time, ampin_pha)))
        interp_ampout_amps.extend(list(np.interp(interp_time, orig_time, ampout_amp)))
        interp_ampout_phas.extend(list(np.interp(interp_time, orig_time, ampout_pha)))

        draw_times = [datetime.fromtimestamp(int_time) for int_time in interp_times]
        draw_amps = interp_amps
        draw_phas = interp_phas
        
        if len(draw_times) > keep_samples:
            draw_times = draw_times[-keep_samples:]
            draw_amps = draw_amps[-keep_samples:]
            draw_phas = draw_phas[-keep_samples:]

        ax_amp.clear()
        ax_pha.clear()
        ax_amp.plot(draw_times, draw_amps)
        ax_pha.plot(draw_times, draw_phas)
        ax_amp.relim()
        ax_pha.relim()
        ax_amp.autoscale_view() 
        ax_pha.autoscale_view() 
        fig.canvas.flush_events() 

        if ofile is not None:
            dictfile = {}
            dictfile['TIME'] = interp_times
            dictfile['AMP'] = interp_amps
            dictfile['PHA'] = interp_phas
            
            dictfile['AMPIN_AMP'] = interp_ampin_amps
            dictfile['AMPIN_PHA'] = interp_ampin_phas
            dictfile['AMPOUT_AMP'] = interp_ampout_amps
            dictfile['AMPOUT_PHA'] = interp_ampout_phas

            dfile.write(ofile, dictfile)



