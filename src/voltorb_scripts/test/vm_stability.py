#!/usr/bin/env python3
'''
    Analyze the stability of the VM output
'''
from datetime import datetime
import matplotlib.dates as mdates
import numpy as np
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
    Script main entry point
    '''
    xformatter = mdates.DateFormatter('%H:%M')
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    ifile = params['ifile']
    dictfile = dfile.read(ifile)
    downsample = params.get('downsample', 1)
    time_trace = dictfile['TIME'][skip:-skip_back-1][::downsample]
    amp_trace = dictfile['AMP'][skip:-skip_back-1][::downsample]
    pha_trace = dictfile['PHA'][skip:-skip_back-1][::downsample]

    time_trace = [datetime.fromtimestamp(timestamp) for timestamp in time_trace]

    fig, ax = plt.subplots()
    ax.set_title('Amplitude')
    ax.set_ylabel('Amplitude [AU]')
    ax.set_xlabel('Time')
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d:%H'))
    ax.plot(time_trace, amp_trace)
    plt.gcf().axes[0].xaxis.set_major_formatter(xformatter)

    fig, ax = plt.subplots()
    ax.set_title('Phase')
    ax.set_ylabel('Phase [Deg]')
    ax.set_xlabel('Time')
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d:%H'))
    ax.plot(time_trace, pha_trace)
    plt.gcf().axes[0].xaxis.set_major_formatter(xformatter)

    print('vm amp pkpk %', 100*np.ptp(amp_trace)/np.mean(amp_trace))
    print('vm pha pkpk deg', np.ptp(pha_trace))

    if 'AMPIN_AMP' in dictfile:
        ampin_amp_trace = dictfile['AMPIN_AMP'][skip:-skip_back-1][::downsample]
        ampin_pha_trace = dictfile['AMPIN_PHA'][skip:-skip_back-1][::downsample]
        ampout_amp_trace = dictfile['AMPOUT_AMP'][skip:-skip_back-1][::downsample]
        ampout_pha_trace = dictfile['AMPOUT_PHA'][skip:-skip_back-1][::downsample]

        fig, ax = plt.subplots()
        ax.set_title('Preamp output amplitude')
        ax.set_ylabel('Amplitude [AU]')
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d:%H'))
        ax.plot(time_trace, ampin_amp_trace)
        plt.gcf().axes[0].xaxis.set_major_formatter(xformatter)

        fig, ax = plt.subplots()
        ax.set_title('Preamp output phase')
        ax.set_ylabel('Phase [Deg]')
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d:%H'))
        ax.plot(time_trace, ampin_pha_trace)
        plt.gcf().axes[0].xaxis.set_major_formatter(xformatter)

        fig, ax = plt.subplots()
        ax.set_title('Amp output amplitude')
        ax.set_ylabel('Amplitude [AU]')
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d:%H'))
        ax.plot(time_trace, ampout_amp_trace)
        plt.gcf().axes[0].xaxis.set_major_formatter(xformatter)

        fig, ax = plt.subplots()
        ax.set_title('Amp output phase')
        ax.set_ylabel('Phase [Deg]')
        ax.set_xlabel('Time')
        ax.xaxis.set_major_formatter(mdates.DateFormatter('%d:%H'))
        ax.plot(time_trace, ampout_pha_trace)
        plt.gcf().axes[0].xaxis.set_major_formatter(xformatter)

        print('preamp amp pkpk %', 100*np.ptp(ampin_amp_trace)/np.mean(ampin_amp_trace))
        print('preamp pha pkpk deg', np.ptp(ampin_pha_trace))
        print('amp amp pkpk %', 100*np.ptp(ampout_amp_trace)/np.mean(ampout_amp_trace))
        print('amp pha pkpk deg', np.ptp(ampout_pha_trace))

    input('Press Return..')
