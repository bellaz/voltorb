#!/usr/bin/env python
'''
    Set a correction table that produces a sawtooth output sweep.
'''
import numpy as np
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    max_val = params['max_val']
    corr_table_i_addr = params['corr_table_I_addr']
    corr_table_q_addr = params['corr_table_Q_addr']
    corr_table_enable_addr = params['corr_enable_addr']

    table_len = len(csys.read(corr_table_i_addr))

    ramp = np.linspace(0, max_val, table_len-skip-skip_back)
    
    skip_arr = np.zeros(skip)
    skip_back_arr = np.zeros(skip_back)
    ramp = np.concatenate((skip_arr, ramp, skip_back_arr))
    csys.write(corr_table_i_addr, list(ramp.astype(int)))
    csys.write(corr_table_q_addr, [0]*table_len)
    #csys.write(corr_table_enable_addr, 1)

if __name__ == "__main__":
    main.run()
