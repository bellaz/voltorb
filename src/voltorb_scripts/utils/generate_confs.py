'''
    Generate confs files in the current directory
'''

import os
from voltorb.utils.generate_DESY_confs import generate_DESY_confs
from voltorb.parse_cmdline import parser_none

@parser_none(__doc__)
def main():
    '''
        Script main entry point
    '''
    generate_DESY_confs(os.getcwd())

if __name__ == '__main__':
    main.run()
