#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity. Use a multisine amplitude excitation
'''

import numpy as np
from numpy.fft import fft
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.bwdet import bwdet
from voltorb.matplotlib import pyplot as plt 
from voltorb.math import ap_to_cplx, butter_lowpass_filter


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ifile = params.get('ifile', 'out.h5')
    trace_time = params.get('trace_time', 1.0)
    ql = params.get('ql', 1.0e7)
    cutoff = cutoff = params.get('cutoff', 1000)
    dictfile = dfile.read(ifile)

    hz_range = params.get('hz_range', 500)

    probe_amp = np.mean(dictfile['PROBE_AMP'], axis=0)
    probe_pha = np.mean(dictfile['PROBE_PHA'], axis=0)
    forw_amp  = np.mean(dictfile['VFORW_AMP'], axis=0)
    forw_pha  = np.mean(dictfile['VFORW_PHA'], axis=0)

    probe_amp = butter_lowpass_filter(probe_amp, cutoff, fs=len(probe_amp)/trace_time)
    probe_pha = butter_lowpass_filter(probe_pha, cutoff, fs=len(probe_pha)/trace_time)
    forw_amp  = butter_lowpass_filter(forw_amp , cutoff, fs=len(forw_amp )/trace_time)
    forw_pha  = butter_lowpass_filter(forw_pha , cutoff, fs=len(forw_pha )/trace_time)

    time = np.linspace(0.0, 1.0, len(probe_amp))

    plt.figure()
    plt.plot(time, probe_amp)
    plt.title("Probe amplitude")
    plt.xlabel("Time [s]")
    plt.ylabel("Amp [MV/m]")

    plt.figure()
    plt.plot(time, probe_pha)
    plt.title("Probe phase")
    plt.xlabel("Time [s]")
    plt.ylabel("Phase [deg]")

    plt.figure()
    plt.plot(time, forw_amp)
    plt.title("Forward amplitude")
    plt.xlabel("Time [s]")
    plt.ylabel("Amp [MV/m]")

    plt.figure()
    plt.plot(time, forw_pha)
    plt.title("Forward phase")
    plt.xlabel("Time [s]")
    plt.ylabel("Phase [deg]")

    probe_cplx = ap_to_cplx(probe_amp, probe_pha)
    forw_cplx = ap_to_cplx(forw_amp, forw_pha)

    (bw, det) = bwdet(probe_cplx, forw_cplx, fs=len(probe_amp)/trace_time, ql=ql)

    plt.figure()
    plt.plot(time, det)
    plt.title("Detuning")
    plt.xlabel("Time [s]")
    plt.ylabel("Detuning [Hz]")

    fftdet = fft(det - np.mean(det))[:len(probe_amp)// 2] / len(probe_amp)
    fftamp = fft(probe_amp**2 - np.mean(probe_amp**2))[:len(probe_amp) // 2] / len(probe_amp)

    frequencies = np.linspace(0.0, len(probe_amp) * 0.5 / trace_time, len(probe_amp) // 2)

    plt.figure()
    plt.plot(frequencies, abs(fftdet))
    plt.title("Detuning spectrum amp")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Detuning [Hz]")

    plt.figure()
    plt.plot(frequencies, np.angle(fftdet, deg=True))
    plt.title("Detuning spectrum phase")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Phase [deg]")

    plt.figure()
    plt.plot(frequencies, abs(fftamp))
    plt.title("Amplitude2 spectrum amp")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Amplitude2 [(MV/m)**2]")

    plt.figure()
    plt.plot(frequencies, np.angle(fftamp, deg=True))
    plt.title("Amplitude2 spectrum phase")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Phase [deg]")

    plt.figure()
    plt.plot(frequencies, abs(fftdet/fftamp))
    plt.title("Amp-Det Transfer function")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Transfer function [Hz/(MV/m)**2]")

    plt.figure()
    plt.plot(frequencies, np.angle(fftdet/fftamp, deg=True))
    plt.title("Amp-Det Transfer function")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Phase [deg]")

    fig, (ax_amp, ax_pha) = plt.subplots(2, 1, sharex=True)
    fig.suptitle('Amp-Det transfer function')
    ax_amp.plot(frequencies, abs(fftdet/fftamp))
    ax_pha.plot(frequencies, np.angle(fftdet/fftamp, deg=True))
    ax_pha.set_xlabel("Frequency [Hz]")
    ax_amp.set_ylabel("Transfer function [Hz/(MV/m)**2]")
    ax_pha.set_ylabel("Phase [deg]")

    freq = len(probe_amp) / trace_time
    ax_amp.set_xlim(0, hz_range)
    ax_amp.set_ylim(0, max(abs(fftdet/fftamp)[:int(hz_range/freq*len(probe_amp))]))
    ax_pha.set_xlim(0, hz_range)

    dictfile['AMP2_DET_TF_AMP'] = abs(fftdet/fftamp)
    dictfile['AMP2_DET_TF_PHA'] = np.angle(fftdet/fftamp, deg=True)
    dictfile['AMP2_DET_TF_FREQ'] = frequencies
    dfile.write(ifile, dictfile)

    plt.show()

    input("Press return...")

if __name__ == "__main__":
    main.run()
