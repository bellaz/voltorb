#!/usr/bin/env python
'''
    Grab piezo sensor from a cavity. Use a multisine amplitude excitation
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.multisine import multisine
from voltorb.math import butter_lowpass_filter

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ofile = params.get('ofile', 'out.h5')
    delay = params.get('delay', 1)
    measurements = params.get('measurements', 256)
    amplitude = params.get('amplitude', 0)

    fmax = params.get('fmax', None)

    cavity_name = params['cavity_name'] 
    piezo_enable_addr = params['piezo_ff_table_enable_addr']
    piezo_table_addr = params['piezo_ff_table_addr']
    piezo_sensor_addr = params['piezo_sensor_addr']
    trace_time = params.get("trace_time", 1.0)

    dictfile = {}

    dictfile['CAVITY_NAME'] = cavity_name
    dictfile['TIME'] = []
    dictfile['TIMESTAMP'] = []
    dictfile['PIEZO_SENSOR'] = []
    dictfile['TIME_NO_EXC'] = []
    dictfile['TIMESTAMP_NO_EXC'] = []
    dictfile['PIEZO_SENSOR_NO_EXC'] = []
    dictfile['TIME_TEST'] = []
    dictfile['TIMESTAMP_TEST'] = []
    dictfile['PIEZO_SENSOR_TEST'] = []

    csys.write(piezo_enable_addr, 0)
    time.sleep(delay*10)
    time0 = time.time()

    for i in range(measurements):
    
        print('Measurement', i+1, '/', measurements)
    
        time.sleep(delay)
    
        try:
            dictfile['TIME_NO_EXC'].append(time.time()-time0)
            dictfile['TIMESTAMP_NO_EXC'].append(time.time())
            dictfile['PIEZO_SENSOR_NO_EXC'].append(csys.read(piezo_sensor_addr))
    
        except Exception as e:
            print(e)

    table_len = len(csys.read(piezo_table_addr))
    excitation = multisine(table_len, table_len/trace_time, fmax) * amplitude
    csys.write(piezo_table_addr, list(excitation))

    dictfile['PIEZO_EXCITATION'] = excitation
    dictfile['EXCITATION'] = excitation

    csys.write(piezo_enable_addr, 1)

    time.sleep(delay*10)
    time0 = time.time()

    for i in range(measurements):

        print('Measurement', i+1, '/', measurements)

        time.sleep(delay)

        try:
            dictfile['TIME'].append(time.time()-time0)
            dictfile['TIMESTAMP'].append(time.time())
            dictfile['PIEZO_SENSOR'].append(csys.read(piezo_sensor_addr))

        except Exception as e:
            print(e)


    excitation = butter_lowpass_filter(np.random.random(table_len) - 0.5, fmax, table_len/trace_time) 
    excitation = excitation * (amplitude/max(max(excitation), -min(excitation)))
    csys.write(piezo_table_addr, list(excitation))

    dictfile['PIEZO_EXCITATION_TEST'] = excitation

    time.sleep(delay*10)
    time0 = time.time()

    for i in range(measurements):

        print('Measurement', i+1, '/', measurements)

        time.sleep(delay)

        try:
            dictfile['TIME_TEST'].append(time.time()-time0)
            dictfile['TIMESTAMP_TEST'].append(time.time())
            dictfile['PIEZO_SENSOR_TEST'].append(csys.read(piezo_sensor_addr))

        except Exception as e:
            print(e)

    dfile.write(ofile, dictfile)
    csys.write(piezo_table_addr, list(np.zeros(table_len)))
    csys.write(piezo_enable_addr, 0)


if __name__ == "__main__":
    main.run()
