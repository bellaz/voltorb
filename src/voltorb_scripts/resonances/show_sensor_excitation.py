#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity. Use a multisine amplitude excitation
'''

import numpy as np
from numpy.fft import fft
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.bwdet import bwdet
from voltorb.matplotlib import pyplot as plt 
from voltorb.math import ap_to_cplx

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ifile = params.get('ifile', 'out.h5')
    ampfile = params.get('ampfile', None)
    trace_time = params.get('trace_time', 1.0)
    ql = params.get('ql', 1.0e7)
    cutoff = cutoff = params.get('cutoff', 1000)
    dictfile = dfile.read(ifile)
    dictfile_amp = {}
    
    if ampfile is not None:
        dictfile_amp = dfile.read(ampfile)

    hz_range = params.get('hz_range', 500)

    piezo_excitation = dictfile['PIEZO_EXCITATION']
    piezo_excitation_test = dictfile['PIEZO_EXCITATION_TEST']

    piezo_sensor_no_exc = dictfile['PIEZO_SENSOR_NO_EXC']
    piezo_sensor        = np.mean(dictfile['PIEZO_SENSOR'], axis=0)
    piezo_sensor_test   = dictfile['PIEZO_SENSOR_TEST']

    piezo_excitation      = np.interp(np.linspace(0.0, 1.0, len(piezo_sensor)),
                                      np.linspace(0.0, 1.0, len(piezo_excitation)),
                                      piezo_excitation)

    piezo_excitation_test = np.interp(np.linspace(0.0, 1.0, len(piezo_sensor)),
                                      np.linspace(0.0, 1.0, len(piezo_excitation)),
                                      piezo_excitation_test)


    fft_len = int(hz_range * trace_time)

    fft_excitation      = np.fft.fft(piezo_excitation)[:, fft_len]
    fft_excitation_test = np.fft.fft(piezo_excitation_test)[:, fft_len]

    fft_sensor_no_exc_amp = np.mean(np.abs(np.fft.fft(piezo_sensor_no_exc, axis=1)[:, fft_len]), axis=0)
    fft_sensor_test = np.fft.fft(piezo_sensor_test, axis=1)[:, fft_len]
    fft_sensor_test_amp = np.mean(np.abs(fft_sensor_test), axis=0)
    fft_sensor = np.fft.fft(piezo_sensor)[:, fft_len]

    frequencies = np.linspace(0.0, hz_range, len(fft_excitation))

    plt.figure()
    plt.plot(frequencies, abs(fft_excitation))
    plt.title("Piezo excitation")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[DAC values]")

    plt.figure()
    plt.plot(frequencies, abs(fft_excitation_test))
    plt.title("Piezo test excitation")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[DAC values]")

    plt.figure()
    plt.plot(frequencies, abs(fft_sensor))
    plt.title("Sensor w. excitation")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[ADC values]")

    plt.figure()
    plt.plot(frequencies, fft_sensor_no_exc_amp)
    plt.title("Sensor w/o excitation")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[ADC values]")

    plt.figure()
    plt.plot(frequencies, fft_sensor_test_amp)
    plt.title("Sensor w. test excitation")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[ADC values]")

    tf = fft_sensor / fft_excitation
    
    fig, (ax_tf_amp, ax_tf_pha) = plt.subplots(2, 1, sharex=True)
    ax_tf_amp.plot(frequencies, abs(tf))
    ax_tf_pha.plot(frequencies, np.angle(tf, deg=True))
    ax_tf_amp.set_ylabel('Transfer function [ADC/DAC counts]')
    ax_tf_deg.set_ylabel('Angle [deg]')
    ax_tf_deg.set_xlabel('Frequency [Hz]')
    
    fft_excitation_test_corr = tf * fft_excitation_test
    fft_sensor_test_corr = fft_sensor_test - fft_excitation_test_corr
    fft_sensor_test_corr_amp = np.mean(np.abs(fft_sensor_test_corr), axis=0)

    plt.figure()
    plt.plot(frequencies, fft_sensor_test_corr_amp)
    plt.title("Sensor w. test excitation corrected")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[ADC values]")

    plt.figure()
    plt.plot(frequencies, fft_sensor_test_corr_amp, title='Corrected')
    plt.plot(frequencies, fft_sensor_no_exc_amp, title='No excitation')

    plt.legend()
    plt.title("Noise spectrum comparison")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[ADC values]")

    plt.show()


if __name__ == "__main__":
    main.run()
