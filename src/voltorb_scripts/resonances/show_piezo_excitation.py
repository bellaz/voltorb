#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity. Use a multisine amplitude excitation
'''

import numpy as np
from numpy.fft import fft
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.bwdet import bwdet
from voltorb.matplotlib import pyplot as plt 
from voltorb.math import ap_to_cplx, butter_lowpass_filter


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ifile = params.get('ifile', 'out.h5')
    ampfile = params.get('ampfile', None)
    trace_time = params.get('trace_time', 1.0)
    ql = params.get('ql', 1.0e7)
    cutoff = cutoff = params.get('cutoff', 1000)
    dictfile = dfile.read(ifile)
    dictfile_amp = {}
    
    if ampfile is not None:
        dictfile_amp = dfile.read(ampfile)

    hz_range = params.get('hz_range', 500)

    probe_amp = np.mean(dictfile['PROBE_AMP'], axis=0)
    probe_pha = np.mean(dictfile['PROBE_PHA'], axis=0)
    forw_amp  = np.mean(dictfile['VFORW_AMP'], axis=0)
    forw_pha  = np.mean(dictfile['VFORW_PHA'], axis=0)
    piezo_excitation = dictfile['PIEZO_EXCITATION']


    probe_amp = butter_lowpass_filter(probe_amp, cutoff, fs=len(probe_amp)/trace_time)
    probe_pha = butter_lowpass_filter(probe_pha, cutoff, fs=len(probe_pha)/trace_time)
    forw_amp  = butter_lowpass_filter(forw_amp , cutoff, fs=len(forw_amp )/trace_time)
    forw_pha  = butter_lowpass_filter(forw_pha , cutoff, fs=len(forw_pha )/trace_time)
    piezo_excitation = np.interp(np.linspace(0.0, 1.0, len(probe_amp)), 
                                 np.linspace(0.0, 1.0, len(piezo_excitation)), 
                                 piezo_excitation)
    piezo_excitation = butter_lowpass_filter(piezo_excitation, cutoff, fs=len(piezo_excitation)/trace_time) 

    time = np.linspace(0.0, 1.0, len(probe_amp))

    tf_amp2 = None
    tf_amp2_freq = None

    if 'AMP2_DET_TF_AMP' in dictfile_amp:
        tf_amp2 = ap_to_cplx(dictfile_amp['AMP2_DET_TF_AMP'], 
                             dictfile_amp['AMP2_DET_TF_PHA'])
        tf_amp2_freq = dictfile_amp['AMP2_DET_TF_FREQ']

    plt.figure()
    plt.plot(time, probe_amp)
    plt.title("Probe amplitude")
    plt.xlabel("Time [s]")
    plt.ylabel("Amp [MV/m]")

    plt.figure()
    plt.plot(time, probe_pha)
    plt.title("Probe phase")
    plt.xlabel("Time [s]")
    plt.ylabel("Phase [deg]")

    plt.figure()
    plt.plot(time, forw_amp)
    plt.title("Forward amplitude")
    plt.xlabel("Time [s]")
    plt.ylabel("Amp [MV/m]")

    plt.figure()
    plt.plot(time, forw_pha)
    plt.title("Forward phase")
    plt.xlabel("Time [s]")
    plt.ylabel("Phase [deg]")

    plt.figure()
    plt.plot(time, piezo_excitation)
    plt.title("Piezo excitation")
    plt.xlabel("Time [s]")
    plt.ylabel("Amplitude [DAC value]")

    probe_cplx = ap_to_cplx(probe_amp, probe_pha)
    forw_cplx = ap_to_cplx(forw_amp, forw_pha)

    (bw, det) = bwdet(probe_cplx, forw_cplx, fs=len(probe_amp)/trace_time, ql=ql)

    plt.figure()
    plt.plot(time, det)
    plt.title("Detuning")
    plt.xlabel("Time [s]")
    plt.ylabel("Detuning [Hz]")

    fftdet = fft(det - np.mean(det))[:len(probe_amp)// 2] / len(probe_amp)
    fftamp = fft(probe_amp**2 - np.mean(probe_amp**2))[:len(probe_amp) // 2] / len(probe_amp)
    fftpiezo = fft(piezo_excitation - np.mean(piezo_excitation))[:len(probe_amp) // 2] / len(probe_amp)

    frequencies = np.linspace(0.0, len(probe_amp) * 0.5 / trace_time, len(probe_amp) // 2)

    plt.figure()
    plt.plot(frequencies, abs(fftdet))
    plt.title("Detuning spectrum amp")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Detuning [Hz]")

    plt.figure()
    plt.plot(frequencies, np.angle(fftdet, deg=True))
    plt.title("Detuning spectrum phase")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Phase [deg]")

    plt.figure()
    plt.plot(frequencies, abs(fftamp))
    plt.title("Amplitude2 spectrum amp")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Amplitude2 [(MV/m)**2]")

    plt.figure()
    plt.plot(frequencies, np.angle(fftamp, deg=True))
    plt.title("Amplitude2 spectrum phase")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Phase [deg]")

    plt.figure()
    plt.plot(frequencies, abs(fftpiezo))
    plt.title("Piezo spectrum")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("[DAC values]")

    plt.figure()
    plt.plot(frequencies, np.angle(fftpiezo, deg=True))
    plt.title("Piezo spectrum")
    plt.xlabel("Frequency [Hz]")
    plt.ylabel("Phase [deg]")


    fig, (ax_amp, ax_pha) = plt.subplots(2, 1, sharex=True)
    fig.suptitle('Piezo-Det transfer function (raw)')
    ax_amp.plot(frequencies, abs(fftdet/fftpiezo))
    ax_pha.plot(frequencies, np.angle(fftdet/fftpiezo, deg=True))
    ax_pha.set_xlabel("Frequency [Hz]")
    ax_amp.set_ylabel("Transfer function [Hz/DAC values]")
    ax_pha.set_ylabel("Phase [deg]")

    freq = len(probe_amp) / trace_time
    ax_amp.set_xlim(0, hz_range)
    ax_amp.set_ylim(0, max(abs(fftdet/fftpiezo)[:int(hz_range/freq*len(probe_amp))]))
    ax_pha.set_xlim(0, hz_range)

    if tf_amp2 is not None:

        tf_amp2 = np.interp(frequencies, tf_amp2_freq, tf_amp2)

        fig, (ax_amp, ax_pha) = plt.subplots(2, 1, sharex=True)
        fig.suptitle('Piezo-Det transfer function (raw)')
        ax_amp.plot(frequencies, abs((fftdet - (tf_amp2 * fftamp))/fftpiezo))
        ax_pha.plot(frequencies, np.angle(fftdet - (tf_amp2 * fftamp)/fftpiezo, deg=True))
        ax_pha.set_xlabel("Frequency [Hz]")
        ax_amp.set_ylabel("Transfer function [Hz/DAC values]")
        ax_pha.set_ylabel("Phase [deg]")
    
        freq = len(probe_amp) / trace_time
        ax_amp.set_xlim(0, hz_range)
        ax_amp.set_ylim(0, max(abs(fftdet/fftamp)[:int(hz_range/freq*len(probe_amp))]))
        ax_pha.set_xlim(0, hz_range)



    plt.show()

    input("Press return...")

if __name__ == "__main__":
    main.run()
