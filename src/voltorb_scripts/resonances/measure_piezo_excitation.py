#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity. Use a multisine amplitude excitation
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.multisine import multisine

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ofile = params.get('ofile', 'out.h5')
    delay = params.get('delay', 1)
    measurements = params.get('measurements', 256)
    amplitude = params.get('amplitude', 0)

    fmax = params.get('fmax', None)

    cavity_name = params['cavity_name'] 
    cavity_cal_amp_addr = params['cavity_cal_amp_addr']
    vforw_power_addr = params['vforw_power_addr']
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    vm_output_amp_addr = params['vm_output_amp_addr']
    vm_output_pha_addr = params['vm_output_pha_addr'] 
    piezo_enable_addr = params['piezo_ff_table_enable_addr']
    piezo_table_addr = params['piezo_ff_table_addr']
    trace_time = params.get("trace_time", 1.0)

    dictfile = {}
    dictfile['TIME'] = []
    dictfile['TIMESTAMP'] = []
    dictfile['CAVITY_NAME'] = cavity_name
    dictfile['PROBE_AMP'] = []
    dictfile['PROBE_PHA'] = []
    dictfile['VFORW_AMP'] = []
    dictfile['VFORW_PHA'] = []
    dictfile['VREFL_AMP'] = []
    dictfile['VREFL_PHA'] = []

    table_len = len(csys.read(piezo_table_addr))
    excitation = multisine(table_len, table_len/trace_time, fmax) * amplitude
    assert table_len == len(excitation)
    csys.write(piezo_table_addr, list(excitation))

    dictfile['PIEZO_EXCITATION'] = excitation

    time.sleep(delay)

    dictfile['EXCITATION'] = excitation

    csys.write(piezo_enable_addr, 1)
    time0 = time.time()

    for i in range(measurements):

        print('Measurement', i+1, '/', measurements)

        time.sleep(delay)

        try:
            [probe_amp, probe_pha, vforw_amp, 
             vforw_pha, vrefl_amp, vrefl_pha] = csys.read(probe_amp_addr, 
                                                          probe_pha_addr,
                                                          vforw_amp_addr, 
                                                          vforw_pha_addr,
                                                          vrefl_amp_addr, 
                                                          vrefl_pha_addr)


            dictfile['TIME'].append(time.time()-time0)
            dictfile['TIMESTAMP'].append(time.time())
            dictfile['PROBE_AMP'].append(probe_amp)
            dictfile['PROBE_PHA'].append(probe_pha)
            dictfile['VFORW_AMP'].append(vforw_amp)
            dictfile['VFORW_PHA'].append(vforw_pha)
            dictfile['VREFL_AMP'].append(vrefl_amp)
            dictfile['VREFL_PHA'].append(vrefl_pha)

            dfile.write(ofile, dictfile)
        except Exception as e:
            print(e)

    csys.write(piezo_table_addr, list(np.zeros(table_len)))
    csys.write(piezo_enable_addr, 0)

if __name__ == "__main__":
    main.run()
