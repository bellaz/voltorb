#!/usr/bin/env python
'''
    Show the measured traces at different duty cycles
'''

import numpy as np
from voltorb import dictfile as dfile
from voltorb import matplotlib
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    FILENAME_INPUT = params['ifile']
    MEASUREMENTS = params['measurements']
    SKIP = params['skip']
    MEASUREMENT_FRACTIONS = np.linspace(1.0, 1.0/MEASUREMENTS, MEASUREMENTS)
    DICTFILE = dfile.read(FILENAME_INPUT)

    DICTFILE = dfile.read(FILENAME_INPUT)

    f1, ax1 = plt.subplots()
    f2, ax2 = plt.subplots()
    f3, ax3 = plt.subplots()

    ax1.set_ylabel('IOT output power [kW]')
    ax1.set_xlabel('time [s]')
    ax1.set_title('IOT output power vs time')

    ax2.set_ylabel('IOT output phase [deg]')
    ax2.set_xlabel('time [s]')
    ax2.set_title('IOT output phase vs time')

    ax3.set_ylabel('gain [dB]')
    ax3.set_xlabel('time [s]')
    ax3.set_title('IOT output gain (dB) vs time')

    for frac in MEASUREMENT_FRACTIONS:
        watt = DICTFILE['DUTY_{}_PERMIL_IOT_OUT_W'.format(int(frac*1000))]
        phase = DICTFILE['DUTY_{}_PERMIL_PHASE_DIFF'.format(int(frac*1000))]
        gain = DICTFILE['DUTY_{}_PERMIL_GAIN_DB'.format(int(frac*1000))]

        frac_el = int(len(watt)*frac)
        time = np.linspace(0, 1, len(watt))
        time_red = np.linspace(0, frac, frac_el)

        ax1.plot(time, watt/1000, label='duty:{} %'.format(int(frac*100)))
        ax2.plot(time_red, phase[:frac_el], label='duty:{} %'.format(int(frac*100)))
        ax3.plot(time_red, gain[:frac_el], label='duty:{} %'.format(int(frac*100)))

    ax1.legend(framealpha=1.0)
    ax2.legend(framealpha=1.0)
    ax3.legend(framealpha=1.0)
    plt.draw()
    input('Insert something')

if __name__ == "__main__":
    main.run()

