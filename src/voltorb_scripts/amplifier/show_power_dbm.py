#!/usr/bin/env python
'''
    Show measured amplifier traces (dbm)
'''

import numpy as np
from voltorb import dictfile as dfile
from voltorb import matplotlib
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ifile = params['ifile']

    power_max_in_dbm = params.get('power_max_in_dbm', 2.42)
    dictfile = dfile.read(ifile)
    
    vm_output_I = dictfile['VM_OUTPUT_I']
    vm_output_I -= vm_output_I[0]
    vm_output_I = vm_output_I[1:]
    iot_out_watt = dictfile['CW_IOT_OUT_W'][1:]
    amplifier_output_phase = dictfile['AMPLIFIER_OUTPUT_PHASE'][1:]

    x20loga = power_max_in_dbm - 20*np.log10(vm_output_I[-1])
    RF_chain_in_dbm = 20*np.log10(vm_output_I) + x20loga
    iot_out_dbm = 10*np.log10(iot_out_watt*1000)

    plt.figure()
    plt.title('IOT output power')
    plt.xlabel('Chain input [dBm]')
    plt.ylabel('IOT output power [dBm]')
    plt.plot(RF_chain_in_dbm, iot_out_dbm)
    

    plt.figure()
    plt.title('IOT output phase')
    plt.xlabel('Chain input [dBm]')
    plt.ylabel('IOT output phase [deg]')
    plt.plot(RF_chain_in_dbm, amplifier_output_phase)


    input('Press Enter')

if __name__ == "__main__":
    main.run()

