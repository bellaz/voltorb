#!/usr/bin/env python
'''
    Measures the amplifier characteristics applying different duty cycles
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script entry point
    '''
    delay = params['delay']
    filename_output = params['ofile']
    measurements = params['measurements']
    sp_amplitude_addr = params['sp_amp_addr']
    ff_table_i_addr = params['ff_table_I_addr']
    corr_enable_addr = params['corr_enable_addr']
    corr_table_i_addr = params['corr_table_I_addr']
    vm_output_amplitude_addr = params['vm_output_amp_addr']
    vm_output_phase_addr = params['vm_output_pha_addr']
    iot_input_amplitude_addr = params['amplifier_in_amp_addr']
    iot_input_phase_addr = params['amplifier_in_pha_addr']
    iot_output_amplitude_addr = params['amplifier_out_amp_addr']
    iot_output_phase_addr = params['amplifier_out_pha_addr']
    iot_input_power_addr = params['iot_input_power_addr']
    iot_output_power_addr = params['iot_output_power_addr']

    measurement_fractions = np.linspace(1, 1/measurements, measurements)

    print('Waiting ', delay, 'seconds to warm up the IOT')

    ff_table_i = csys.read(ff_table_i_addr)

    csys.write(corr_table_i_addr, [0]*len(ff_table_i))
    print('FF table height: ', np.mean(ff_table_i))

    cw_iot_input_power = csys.read(iot_input_power_addr)
    cw_iot_output_power = csys.read(iot_output_power_addr)
    cw_iot_input_amplitude = np.mean(csys.read(iot_input_amplitude_addr))
    cw_iot_output_amplitude = np.mean(csys.read(iot_output_amplitude_addr))

    print('IOT in  power: ', cw_iot_input_power, ' W')
    print('IOT out power: ', cw_iot_output_power, ' W')
    print('IOT in  ampli: ', cw_iot_input_amplitude, ' W')
    print('IOT out ampli: ', cw_iot_output_amplitude, ' W')

    dictfile = {}
    dictfile['CW_IOT_IN_W'] = cw_iot_input_power
    dictfile['CW_IOT_OUT_W'] = cw_iot_output_power

    calib_in = cw_iot_input_power/(cw_iot_input_amplitude**2)
    print(cw_iot_output_amplitude, cw_iot_output_power)
    calib_out = cw_iot_output_power/(cw_iot_output_amplitude**2)

    print('CALIB_IN ', calib_in, ' W_in/(amp_in)^2')
    print('CALIB_OUT ', calib_out, ' W_out/(amp_out)^2')

    dictfile['CALIB_IN_WATT_O_BIN_2'] = calib_in
    dictfile['CALIB_OUT_WATT_O_BIN_2'] = calib_out
    dictfile['TIME_SECONDS'] = np.linspace(0, 1, len(ff_table_i))

    initial_amp = csys.read(sp_amplitude_addr)
    csys.write(sp_amplitude_addr, 0)
    csys.write(corr_enable_addr, 1)

    for frac in measurement_fractions:
        print('Measuring with ', '%.3f' % (frac*100), '% duty cycle')
        table_new = list(ff_table_i[:int(len(ff_table_i)*frac)])
        table_new += [0]*(len(ff_table_i) - len(table_new))
        csys.write(corr_table_i_addr, table_new)
        time.sleep(delay)

        dictfile['DUTY_{}_PERMIL_AMP_VM'.format(int(frac*1000))] = csys.read(vm_output_amplitude_addr)
        dictfile['DUTY_{}_PERMIL_PHASE_VM'.format(int(frac*1000))] = csys.read(vm_output_phase_addr)
        dictfile['DUTY_{}_PERMIL_AMP_IN'.format(int(frac*1000))] = amp_in = csys.read(iot_input_amplitude_addr)
        dictfile['DUTY_{}_PERMIL_PHASE_IN'.format(int(frac*1000))] = phase_in = csys.read(iot_input_phase_addr)
        dictfile['DUTY_{}_PERMIL_AMP_OUT'.format(int(frac*1000))] = amp_out = csys.read(iot_output_amplitude_addr)
        dictfile['DUTY_{}_PERMIL_PHASE_OUT'.format(int(frac*1000))] = phase_out = csys.read(iot_output_phase_addr)
        dictfile['DUTY_{}_PERMIL_IOT_IN_W'.format(int(frac*1000))] = p_in = calib_in*(amp_in**2)
        dictfile['DUTY_{}_PERMIL_IOT_OUT_W'.format(int(frac*1000))] = p_out = calib_out*(amp_out**2)
        dictfile['DUTY_{}_PERMIL_GAIN_LIN'.format(int(frac*1000))] = gain_lin = np.nan_to_num(p_out/p_in)
        dictfile['DUTY_{}_PERMIL_GAIN_DB'.format(int(frac*1000))] = np.nan_to_num(10*np.log10(gain_lin))
        dictfile['DUTY_{}_PERMIL_PHASE_DIFF'.format(int(frac*1000))] = phase_out - phase_in

    dfile.write(filename_output, dictfile)
    csys.write(corr_enable_addr, 0)
    csys.write(corr_table_i_addr, [0]*len(ff_table_i))
    csys.write(sp_amplitude_addr, initial_amp)

if __name__ == "__main__":
    main.run()
