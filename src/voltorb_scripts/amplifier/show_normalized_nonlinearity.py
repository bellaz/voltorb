#!/usr/bin/env python
'''
    Show measured amplifier traces (dbm)
'''

import numpy as np
from voltorb import dictfile as dfile
from voltorb import matplotlib
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    
    i = 0
    
    requested = []
    amplitudes = []
    phases = []
    #titles = []


    while True:
        i += 1

        filei = params.get('ifile' + str(i), None)
        
        skip = params.get('skip', 0)

        if filei is None:
            break
        #titles.append(params.get('title' + str(i), None))
        dictfile = dfile.read(filei)

        vm_output_I_len = len(np.trim_zeros(dictfile['VM_OUTPUT_I'][skip:]))
        amplitude = dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'][skip:]
        idxmax = np.argmax(amplitude)
        amplitudes.append(amplitude[idxmax-vm_output_I_len:idxmax])
        phases.append(dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'][skip:][idxmax-vm_output_I_len:idxmax])

    plt.figure()
    plt.xlabel('Requested output amplitude [AU]')
    plt.ylabel('Obtained output amplitude [AU]')
    plt.plot([0, 1], [0, 1])

    for amplitude in amplitudes:
        plt.plot(np.linspace(0, 1, len(amplitude)), amplitude)

    plt.figure()
    plt.xlabel('Requested output amplitude [AU]')
    plt.ylabel('Output phase rotation [deg]')
    plt.plot([0, 1], [0, 0])

    for phase in phases:
        plt.plot(np.linspace(0, 1, len(phase)), phase)

    input('Press Enter')

if __name__ == "__main__":
    main.run() 
