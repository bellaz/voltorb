#!/usr/bin/env python

'''
   Perform ILC on piezo 
'''

import numpy as np
from scipy import signal
import time
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    winlen = float(params.get("winlen", 1.0))
    lenmul = 1.0/winlen
    pulse_start = float(params.get("pulse_start", 0.0)) * lenmul
    pulse_end = float(params.get("pulse_end", 1.0)) * lenmul
    gain = float(params.get("gain", 0))
    setpoint = float(params.get("setpoint", 0))
    stable = float(params.get("stable", 0.0)) * lenmul


    piezo_ff_table_addr = params["piezo_ff_table_addr"]
    probe_pha_addr = params["probe_pha_addr"]
    vforw_pha_addr = params["vforw_pha_addr"]

    while True:
        print("Adapt")
        probe_pha = csys.read(probe_pha_addr)
        vforw_pha = csys.read(vforw_pha_addr)
        piezo_ff_table = csys.read(piezo_ff_table_addr)

        err = probe_pha - vforw_pha - setpoint
        err = signal.resample(err, len(piezo_ff_table))
        idx = np.linspace(0.0, 1.0, len(err))
        
        err_start = err[int(pulse_start*len(err))] 
        err_end = err[int(pulse_end*len(err))] 
        err[idx < pulse_start] = err[int(pulse_start*len(err))] 
        err[int((pulse_end)*len(err)):int((pulse_end+stable)*len(err))] = err_end
        err[int((pulse_end+stable)*len(err)):(len(err)-1)] = np.linspace(err_end, 
                                                                         err_start, 
                                                                         int(len(err) * 
                                                                         (1.0 - pulse_end-stable)))
        err[len(err)-1] = err_start

        piezo_ff_table = piezo_ff_table + (gain * err)
        #piezo_ff_table[:] = 0.0
        csys.write(piezo_ff_table_addr, list(piezo_ff_table))

        time.sleep(1.0)

