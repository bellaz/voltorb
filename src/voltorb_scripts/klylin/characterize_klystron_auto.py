#!/usr/bin/env python
'''
    Automatically measure klystron nonlinearity
'''
import time
import numpy as np
from voltorb import controlsys as csys
import voltorb.dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    output_file = params['ofile']
    skip = params.get('skip', 0) # 300 us = 2700
    sample_freq_mhz = params.get('sample_freq_mhz', 9)
    pulse_samples = int(params.get('pulse_us', 300) * sample_freq_mhz)
    corr_table_i_addr = params['corr_table_I_addr']
    corr_table_q_addr = params['corr_table_Q_addr']
    corr_table_enable_addr = params['corr_enable_addr']
    ff_table_i_addr = params['ff_table_I_addr']
    ff_table_q_addr = params['ff_table_Q_addr']
    amplifier_out_amp_addr = params['amplifier_out_amp_addr']
    amplifier_out_pha_addr = params['amplifier_out_pha_addr']
    sp_amp_addr = params['sp_amp_addr']
    vs_amp_addr = params['vs_amp_addr']
    vm_output_I_addr = params['vm_output_I_addr']
    vm_output_Q_addr = params['vm_output_Q_addr']
    measurements = params.get('measurements', 256)
    steps = params.get('steps', 40)
    delay = params.get('delay', 0.5)
    vs_max_multiplier = params.get('vs_max_multiplier', 0.5)
    vm_start = params.get('vm_start', 0)
    vm_stop = params.get('vm_stop', 1)

    ovc_amp_scale_addr = params['ovc_amp_scale_addr']
    ovc_pha_rotation_addr = params['ovc_pha_rotation_addr']
    amp_out_scale_addr = params['amp_out_scale_addr']
    pha_out_rotation_addr = params['pha_out_rotation_addr']

    vm_dac_offset_I_addr = params['vm_dac_offset_I_addr']
    vm_dac_offset_Q_addr = params['vm_dac_offset_Q_addr'] 

    ## read chain scale/rotation
    ovc_amp_scale = csys.read(ovc_amp_scale_addr)    
    ovc_pha_rotation = csys.read(ovc_pha_rotation_addr)
    amp_out_scale = csys.read(amp_out_scale_addr)
    pha_out_rotation = csys.read(pha_out_rotation_addr)

    total_scale = ovc_amp_scale * amp_out_scale
    total_rotation = ovc_pha_rotation + pha_out_rotation 

    ## read ADC offsets
    vm_dac_offset_I = csys.read(vm_dac_offset_I_addr)
    vm_dac_offset_Q = csys.read(vm_dac_offset_Q_addr)

    ## read initial values of VS and VM
    sp_amp_old = csys.read(sp_amp_addr)
    vm_I_value = csys.read(vm_output_I_addr) - vm_dac_offset_I
    vm_Q_value = csys.read(vm_output_Q_addr) - vm_dac_offset_Q
    max_vm_value = int(np.max(np.sqrt((vm_I_value**2) + (vm_Q_value**2))))
    max_vs_value = np.max(csys.read(vs_amp_addr))

    ## amplitude to zero, correction tables enabled and zeroed
    table_len = len(csys.read(corr_table_i_addr))
    
    skip_arr = np.zeros(skip)
    skip_back_arr = np.zeros(table_len-skip-pulse_samples)
    ramp = None

    print('Starting increasing vm value')
    print('Open loop max VM value', max_vm_value)
    print('Open loop max VS value', max_vs_value)
    print('Threshold VS value', vs_max_multiplier * max_vs_value)
    print('Steps', steps)
    print('Initial VM peak', vm_start * max_vm_value)
    print('End VM peak', vm_stop * max_vm_value)
    print('Amplitude correction scale', total_scale)
    print('Phase correction rotation', total_rotation)
    print('Dac offset I', vm_dac_offset_I)
    print('Dac offset Q', vm_dac_offset_Q)

    choice = input('Start? [y/n]')


    if choice != 'y':
        exit(0)
        
    csys.write(sp_amp_addr, 0)
    csys.write(corr_table_i_addr, [0]*table_len)
    csys.write(corr_table_q_addr, [0]*table_len)
    csys.write(corr_table_enable_addr, 1)


    for vm_amp in np.linspace(vm_start * max_vm_value/total_scale, 
                              vm_stop * max_vm_value/total_scale, 
                              steps):

        ramp = np.linspace(0, vm_amp, pulse_samples)
        ramp_full = np.concatenate((skip_arr, ramp, skip_back_arr))
        csys.write(corr_table_i_addr, list(ramp_full.astype(int)))
        time.sleep(delay)

        vs_value = np.max(csys.read(vs_amp_addr))
        if vs_value > (vs_max_multiplier * max_vs_value):
            print('VS threshold reached')
            break

        print('Actual vs:', vs_value,
              'fraction:', vs_value/max_vs_value,
              'Actual vm:', vm_amp,
              'fraction:', vm_amp/max_vm_value)

    print('Measuring nonlinearity')

    measurement_amplitudes = []
    measurement_phases = []

    for j in range(measurements):
        print('Measure', str(j+1) + '/' + str(measurements))
        time.sleep(delay)
        measurement_amplitudes.append(csys.read(amplifier_out_amp_addr))
        measurement_phases.append(csys.read(amplifier_out_pha_addr))


    dictfile = {}
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'] = np.array(measurement_amplitudes)
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'] = np.mean(dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'],
                                                         axis=0)
    dictfile['AMPLIFIER_OUTPUT_PHASE'] = np.array(measurement_phases)
    dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'] = np.mean(dictfile['AMPLIFIER_OUTPUT_PHASE'], axis=0)
    dictfile['VM_RAMP'] = ramp

    dictfile['FF_TABLE_I'] = csys.read(ff_table_i_addr) 
    dictfile['FF_TABLE_Q'] = csys.read(ff_table_q_addr) 
    dictfile['CORR_TABLE_I'] = csys.read(corr_table_i_addr)
    dictfile['CORR_TABLE_Q'] = csys.read(corr_table_q_addr)
    dictfile['VM_OUTPUT_I'] = csys.read(vm_output_I_addr)
    dictfile['VM_OUTPUT_Q'] = csys.read(vm_output_Q_addr)
    dictfile['OVC_AMP_SCALE'] = ovc_amp_scale    
    dictfile['OVC_PHA_ROTATION'] = ovc_pha_rotation
    dictfile['AMP_OUT_SCALE'] = amp_out_scale   
    dictfile['PHA_OUT_ROTATION'] = pha_out_rotation                 
    dictfile['TOTAL_SCALE'] = total_scale     
    dictfile['TOTAL_ROTATION'] = total_rotation  
    dictfile['VM_DAC_OFFSET_I'] = vm_dac_offset_I 
    dictfile['VM_DAC_OFFSET_Q'] = vm_dac_offset_Q 

    csys.write(corr_table_enable_addr, 0)
    csys.write(corr_table_i_addr, [0]*table_len)
    csys.write(corr_table_q_addr, [0]*table_len)

    dfile.write(output_file, dictfile)

if __name__ == "__main__":
    main.run()
