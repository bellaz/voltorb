#!/usr/bin/env python
'''
    Show klylin effects on amplifier linearity
'''

import numpy as np
from scipy.signal import detrend
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.matplotlib import pyplot as plt

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
   
    plt.minorticks_off()
    axis_range = params.get('axis_range', 1)

    #fig_amp, plot_amp = plt.subplots(1, 1)
    #fig_pha, plot_pha = plt.subplots(1, 1)
    fig_im, (plot_amp, plot_pha) = plt.subplots(2, 1)
    fig_amp_det, plot_amp_det = plt.subplots(1, 1)
    plot_amp_det.set_title('Amplifier output amplitude deviation')
    fig_pha_det, plot_pha_det = plt.subplots(1, 1)
    plot_pha_det.set_title('Amplifier output phase deviation')
    #fig_amp_err, plot_amp_err = plt.subplots(1, 1)
    #fig_pha_err, plot_pha_err = plt.subplots(1, 1) 
    fig_err, (plot_amp_err, plot_pha_err) = plt.subplots(2, 1)
    
    fig_abs, (plot_amp_abs, plot_pha_abs) = plt.subplots(2, 1, sharex=True)
    fig_abs2, plot_amp_abs2 = plt.subplots(1, 1, sharex=True)

    fig_amp_noise, plot_amp_noise = plt.subplots(1, 1)
    plot_amp_noise.set_title('Amplitude noise [a.u.]')
    fig_pha_noise, plot_pha_noise = plt.subplots(1, 1)
    plot_pha_noise.set_title('Phase noise')
    plot_amp.set_xlabel('Requested amplitude [a.u.]')
    plot_pha.set_xlabel('Requested amplitude [a.u.]')
    
    plot_pha_abs.set_xlabel('Requested amplitude [a.u.]')
    plot_amp_abs.set_xlabel('Requested amplitude [a.u.]')
    plot_amp_abs2.set_xlabel('Requested amplitude [a.u.]')

    plot_amp_det.set_xlabel('Requested amplitude [a.u.]')
    plot_pha_det.set_xlabel('Requested amplitude [a.u.]')
    plot_amp_err.set_xlabel('Requested amplitude [a.u.]')
    plot_pha_err.set_xlabel('Requested amplitude [a.u.]')
    plot_amp_noise.set_xlabel('Requested amplitude [a.u.]')
    plot_pha_noise.set_xlabel('Requested amplitude [a.u.]')

    plot_amp_abs.set_ylabel('Amplitude [a.u.]')
    plot_amp_abs2.set_ylabel('Amplitude [a.u.]')
    plot_amp_abs2.set_xlim([0, axis_range])
    plot_amp_abs2.set_ylim([0, axis_range])

    plot_pha_abs.set_ylabel('Phase [deg]')

    plot_amp_abs2.set_ylabel('Amplitude [a.u.]')

    plot_amp.set_ylabel('Amplitude [a.u.]')
    plot_pha.set_ylabel('Phase [deg]')
    plot_amp_det.set_ylabel('Amplitude linear deviation [%]')
    plot_pha_det.set_ylabel('Phase error [deg]')
    plot_amp_err.set_ylabel('Amplitude error %')
    plot_pha_err.set_ylabel('Phase error [deg]')
    plot_amp_noise.set_ylabel('Amplitude [a.u.]')
    plot_pha_noise.set_ylabel('Phase [deg]')

    titles = params['titles']

    skip_trace_percent = params.get('skip_percent', 0)
    skip_back_trace_percent = params.get('skip_back_percent', 0)

    skip = params.get('skip_unused', 0)
    skip_back = params.get('skip_back_unused', 0)

    xlim = params.get('xlim', None)
    ylim = params.get('ylim', None)

    phase_range = params.get('phase_range', None)

    idx = 0
    
    scale_unset = True
    scale_amp = 0
    scale_req = 0

    while True:
        idx += 1
        if 'ifile{}'.format(idx) in params:
           
            dictfile = dfile.read(params['ifile{}'.format(idx)])


            corr_table = dictfile['CORR_TABLE_I'][skip:-skip_back-1]
            amp_avg = dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'][skip:-skip_back-1]
            pha_avg = dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'][skip:-skip_back-1]

            corr_table = corr_table[np.nonzero(corr_table)]
            vm_len = len(corr_table)
            index_max = np.argmax(amp_avg)
            print(vm_len)
            print(index_max)
            amp_avg = amp_avg[index_max-vm_len:index_max]
            pha_avg = pha_avg[index_max-vm_len:index_max]

            skip_trace = params.get('skip', 0)
            skip_back_trace = params.get('skip_back', 0)

            table_len = len(corr_table)

            skip_trace      += int(skip_trace_percent*table_len/100.0)
            skip_back_trace += int(skip_back_trace_percent*table_len/100.0)

            corr_table = corr_table[skip_trace:-skip_back_trace-1]
            amp_avg = amp_avg[skip_trace:-skip_back_trace-1]
            pha_avg = pha_avg[skip_trace:-skip_back_trace-1]

            corr_table = corr_table/corr_table[-1]
            amp_avg = amp_avg/amp_avg[-1]

            plot_amp.plot(corr_table, amp_avg, '-', ms=6, label=titles[idx-1], lw=3)
            plot_pha.plot(corr_table, detrend(pha_avg, type='constant'), '-', ms=6, lw=3, label=titles[idx-1])

            amp_avg_det = detrend(amp_avg)/amp_avg[-1]*100
            pha_avg_det = detrend(pha_avg, type='constant')

            plot_amp_det.plot(corr_table, amp_avg_det, '.', ms=6, label=titles[idx-1])
            plot_pha_det.plot(corr_table, pha_avg_det, '.', ms=6, label=titles[idx-1])

            plot_amp_err.semilogy(corr_table, np.abs(amp_avg_det), '.', ms=6, label=titles[idx-1])
            plot_pha_err.semilogy(corr_table, np.abs(pha_avg_det), '.', ms=6, label=titles[idx-1])
         
            if scale_unset:
                scale_unset=True
                scale_amp = amp_avg[-1]
                scale_req = corr_table[-1]

            plot_amp_abs.plot(corr_table/scale_req, amp_avg/scale_amp, '.', ms=6, label=titles[idx-1])
            plot_amp_abs2.plot(corr_table/scale_req, amp_avg/scale_amp, '.', ms=6, label=titles[idx-1])
            plot_pha_abs.plot(corr_table/scale_req, pha_avg, '.', ms=6, label=titles[idx-1])
            
            print(titles[idx-1], 'average amp error', np.mean(np.abs(amp_avg_det)))
            print(titles[idx-1], 'average pha error', np.mean(np.abs(pha_avg_det)))
            print(titles[idx-1], 'maximum amp error', np.max(np.abs(amp_avg_det)))
            print(titles[idx-1], 'maximum pha error', np.max(np.abs(pha_avg_det)))

            noise_amp = detrend(amp_avg[1:] - amp_avg[:-1])
            noise_pha = detrend(pha_avg[1:] - pha_avg[:-1])
            plot_amp_noise.plot(corr_table[:-1], noise_amp, '.', ms=6, label=titles[idx-1])
            plot_pha_noise.plot(corr_table[:-1], noise_pha, '.', ms=6, label=titles[idx-1])

            print(titles[idx-1], 'noise STD [a.u.] :', np.std(noise_amp))
            print(titles[idx-1], 'noise STD [deg]:', np.std(noise_pha))

        else:
            break

    if xlim != None: 
        plot_amp.set_xlim(0, xlim)
        plot_pha.set_xlim(0, xlim)
        plot_amp_det.set_xlim(0, xlim)
        plot_pha_det.set_xlim(0, xlim)
        #plot_amp_err.
        plot_pha_err.set_xlim(0, xlim)
        plot_amp_noise.set_xlim(0, xlim)
        plot_pha_noise.set_xlim(0, xlim)
        plot_amp_abs.set_xlim(0, xlim)
        plot_amp_abs2.set_xlim(0, xlim)

    if ylim != None:
        plot_amp.set_ylim(0, ylim)
    #plot_pha.set_ylim(0, ylim)
    #plot_amp_det.set_ylim(0, ylim)
    #plot_pha_det.set_ylim(0, ylim)
    #plot_amp_err.
    #plot_pha_err.set_ylim(0, ylim)
    #plot_amp_noise.set_ylim(0, ylim)
    #plot_pha_noise.set_ylim(0, ylim)
    #plot_amp_abs.set_ylim(0, ylim)
    #plot_amp_abs2.set_ylim(0, ylim)

    if phase_range != None:
        plot_pha.set_ylim(-phase_range, phase_range)

    plot_amp.legend(markerscale=1.5, framealpha=1.0)
    plot_amp_det.legend(markerscale=1.5, framealpha=1.0)
    plot_pha_det.legend(markerscale=1.5, framealpha=1.0)
    #plot_amp_err.legend(markerscale=1.5)
    plot_pha_err.legend(markerscale=1.5, framealpha=1.0)
    plot_amp_noise.legend(markerscale=1.5, framealpha=1.0)
    plot_pha_noise.legend(markerscale=1.5, framealpha=1.0)
    plot_amp_abs.legend(markerscale=1.5, framealpha=1.0)
    plot_amp_abs2.legend(loc='lower right')

    #fig_abs.canvas.draw_idle()
    #fig_amp.canvas.draw_idle()
    #fig_pha.canvas.draw_idle()
    #fig_amp_det.canvas.draw_idle()
    #fig_pha_det.canvas.draw_idle()
    #fig_amp_err.canvas.draw_idle()
    #fig_pha_err.canvas.draw_idle()
    #fig_amp_noise.canvas.draw_idle()
    #fig_pha_noise.canvas.draw_idle()


    input('Enter something')
