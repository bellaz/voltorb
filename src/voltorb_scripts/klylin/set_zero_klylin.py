#!/usr/bin/python3
'''
    Set klylin tables to zero
'''
from voltorb.parse_cmdline import parser_default
try:
    import mtca4u
except:
    mtca4u = None

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    mtca4u.set_dmap_location(params['dmapfile'])

    dev = mtca4u.Device('TCK70')

    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x1)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', [0]*1024)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x2)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', [0]*1024)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x4)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', [0]*1024)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x8)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', [0]*1024)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x0)

if __name__ == "__main__":
    main.run()
