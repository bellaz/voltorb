#!/usr/bin/env python
'''
    Measures the VM -> Amplifier output characteristics using a sawtooth
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    delay = params['delay']
    filename_output = params['ofile']
    measurements = params['measurements']
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)

    sp_amplitude_addr = params['sp_amp_addr']
    corr_enable_addr = params['corr_enable_addr']
    ff_table_i_addr = params['ff_table_I_addr']
    ff_table_q_addr = params['ff_table_Q_addr']
    corr_table_i_addr = params['corr_table_I_addr']
    corr_table_q_addr = params['corr_table_Q_addr']
    vm_output_i_addr = params['vm_output_I_addr']
    vm_output_q_addr = params['vm_output_Q_addr']
    amplifier_out_amp_addr = params['amplifier_out_amp_addr']
    amplifier_out_pha_addr = params['amplifier_out_pha_addr']

    print('waiting ', delay, 'seconds to warm up the amplifier')

    ff_table_i = csys.read(ff_table_i_addr)
    ff_table_len = len(ff_table_i)
    ff_table_max = np.max(ff_table_i)

    skip_arr = np.zeros(skip)
    skip_back_arr = np.zeros(skip_back)
    ramp = np.linspace(0, ff_table_max, ff_table_len-skip-skip_back)

    table_new = np.concatenate((skip_arr, ramp, skip_back_arr)).astype(int)

    csys.write(corr_table_i_addr, [0]*ff_table_len)
    csys.write(sp_amplitude_addr, 0)
    csys.write(corr_enable_addr, 1)
    csys.write(corr_table_i_addr, list(table_new))

    time.sleep(delay*10)
    print('Measuring')

    measurement_amplitudes = []
    measurement_phases = []

    for j in range(measurements):
        print('Measure', str(j+1) + '/' + str(measurements))
        time.sleep(delay)
        measurement_amplitudes.append(csys.read(amplifier_out_amp_addr))
        measurement_phases.append(csys.read(amplifier_out_pha_addr))

    dictfile = {}
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'] = np.array(measurement_amplitudes)
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'] = np.mean(dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'],
                                                         axis=0)
    dictfile['AMPLIFIER_OUTPUT_PHASE'] = np.array(measurement_phases)
    dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'] = np.mean(dictfile['AMPLIFIER_OUTPUT_PHASE'], axis=0)
    dictfile['VM_RAMP'] = ramp.astype(int)

    dictfile['FF_TABLE_I'] = csys.read(ff_table_i_addr)
    dictfile['FF_TABLE_Q'] = csys.read(ff_table_q_addr)
    dictfile['CORR_TABLE_I'] = csys.read(corr_table_i_addr)
    dictfile['CORR_TABLE_Q'] = csys.read(corr_table_q_addr)
    dictfile['VM_OUTPUT_I'] = csys.read(vm_output_i_addr)
    dictfile['VM_OUTPUT_Q'] = csys.read(vm_output_q_addr)

    dfile.write(filename_output, dictfile)
    #csys.write(ADDR_CORR_ENABLE, 0)
    #csys.write(ADDR_CORR_TABLE_I, [0]*FF_TABLE_LEN)
    #csys.write(ADDR_SP_AMPLITUDE, INITIAL_AMP)

if __name__ == "__main__":
    main.run()
