#!/usr/bin/python3
'''
    Set klylin given an input measurement curve file
'''

import numpy as np
from scipy.signal import savgol_filter
from voltorb import dictfile as dfile
from voltorb.klylin_table_generator import KlylinGenerator
from voltorb.parse_cmdline import parser_default

try:
    import mtca4u
except:
    mtca4u = None

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    shift = params.get('shift', 0)

    rev_phase = params.get('rev_phase', False)
    shift_phase = params.get('shift_phase', 0)

    filename_input = params['ifile']
    dictfile = dfile.read(filename_input)

    iq_bit_size = params['iq_bit_len']
    a2_bit_size = params['a2_bit_len']
    unity_bit_len = params['unity_bit_len']
    table_bit_len = params['table_bit_len']

    smooth_win = params.get('smooth_win', 0)
    smooth_order = params.get('smooth_order', 2)

    scale_vm = params.get('scale_vm', None)

    vm_values = dictfile['CORR_TABLE_I'][skip:-skip_back-1]

    if scale_vm is not None:
        vm_values *= dictfile['TOTAL_SCALE'] / scale_vm

    vm_values = vm_values[np.nonzero(vm_values)]
    vm_values = np.insert(vm_values, 0, 0)

    amp = dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'][skip:-skip_back-1]
    pha = np.pi/180.0*(dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'][skip:-skip_back-1] + shift_phase)

    if rev_phase:
        pha = -pha

    vm_len = len(vm_values)
    index_max = np.argmax(amp) + shift

    amp = amp[index_max-vm_len:index_max]
    pha = pha[index_max-vm_len:index_max]

    if smooth_win > 0:
        amp = savgol_filter(amp, smooth_win, smooth_order)
        pha = savgol_filter(pha, smooth_win, smooth_order)

    amp -= amp[0]

    kgen = KlylinGenerator(unity_bit=unity_bit_len,
                           iq_bit_size=iq_bit_size,
                           a2_bit_size=a2_bit_size,
                           table_bit_len=table_bit_len)

    klylin = kgen.generate_klylin_AP(amp, pha, vm_values)

    mtca4u.set_dmap_location(params['dmapfile'])

    dev = mtca4u.Device('TCK70')

    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x1)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', klylin.base_I_tables)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x2)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', klylin.deriv_I_tables)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x4)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', klylin.base_Q_tables)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x8)
    dev.write('KLYLIN/0/', 'AREA_KLYLIN_MEM', klylin.deriv_Q_tables)
    dev.write('KLYLIN/0/', 'WORD_KLYLIN_MEM_SEL', 0x0)

if __name__ == "__main__":
    main.run()
