'''
    Simulate different interpolation functions with a fake nonlinear curve
'''

import numpy as np
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.klylin_table_generator import KlylinGenerator
from voltorb.parse_cmdline import parser_default
from scipy.signal import savgol_filter

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    interpolations = ['linear', 'quadratic', 'lms_linear', 'lms_quadratic']
    skip = params.get("skip", 0)

    iq_bit_size = params['iq_bit_len']
    a2_bit_size = params['a2_bit_len']
    unity_bit_len = params['unity_bit_len']
    table_bit_len = params['table_bit_len']

    vm_values = np.linspace(0, 2**iq_bit_size, 2**iq_bit_size/8)
    amp = (vm_values**2)/((2**iq_bit_size)**2)
    pha = 2*np.pi*(vm_values - (2**(iq_bit_size-1)))**2/(2**iq_bit_size)**2

    derived_x = np.linspace(0, vm_values[-1], vm_values[-1]+1)

    out_amps = []
    out_phas = []

    for interpolation in interpolations:

        kgen = KlylinGenerator(unity_bit=unity_bit_len, iq_bit_size=iq_bit_size,
                               a2_bit_size=a2_bit_size, table_bit_len=table_bit_len,
                               interpolation=interpolation)

        klylin = kgen.generate_klylin_AP(amp, pha, vm_values)

        (new_i, new_q) = klylin.correct_IQ(derived_x)
        phase = np.arctan2(new_q, new_i)
        ampl_vm = np.sqrt(new_i**2 + new_q**2)
        out_amp = np.interp(ampl_vm, vm_values, amp)
        out_pha = np.interp(ampl_vm, vm_values, pha) + phase

        out_amps.append(out_amp)
        out_phas.append(out_pha)

    plt.figure()
    plt.title('Amplitude')

    for idx, interpolation in enumerate(interpolations):
        plt.plot(derived_x[skip:], out_amps[idx][skip:], label=interpolation)

    plt.ylabel('Amplitude [AU]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Phase')
    for idx, interpolation in enumerate(interpolations):
        plt.plot(derived_x[skip:], 180/np.pi*out_phas[idx][skip:], label=interpolation)

    plt.ylabel('Phase [deg]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Amplitude error [abs]')

    for idx, interpolation in enumerate(interpolations):
        plt.semilogy(derived_x[skip:],
                     np.abs(out_amps[idx][skip:] - derived_x[skip:]/(2**iq_bit_size)), '.',
                     label=interpolation)

    plt.ylabel('Amplitude [AU]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Phase error [deg]')
    for idx, interpolation in enumerate(interpolations):
        plt.semilogy(derived_x[skip:], np.abs(180/np.pi*out_phas[idx][skip:]), '.', label=interpolation)

    plt.ylabel('Phase [deg]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Amplitude cumsum error')

    for idx, interpolation in enumerate(interpolations):
        plt.semilogy(np.flip(derived_x[skip:]),
                     np.sqrt(np.cumsum(np.flip(out_amps[idx][skip:]-derived_x[skip:]/(2**iq_bit_size))**2)),
                     label=interpolation)

    plt.ylabel('Amplitude [AU]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Phase cumsum error [deg]')
    for idx, interpolation in enumerate(interpolations):
        plt.semilogy(np.flip(derived_x[skip:]),
                     np.sqrt(np.cumsum(np.flip(180/np.pi*out_phas[idx][skip:])**2)),
                     label=interpolation)

    plt.ylabel('Phase [deg]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Step noise amplitude')

    for idx, interpolation in enumerate(interpolations):
        lin_amp = out_amps[idx][skip:]-derived_x[skip:]/(2**iq_bit_size)
        plt.plot(derived_x[skip:-1],
                 lin_amp[:-1] - lin_amp[1:],
                 label=interpolation)

    plt.ylabel('Amplitude [AU]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Step noise phase  [deg]')
    for idx, interpolation in enumerate(interpolations):
        lin_pha = 180/np.pi*out_phas[idx][skip:]
        plt.plot(derived_x[skip:-1],
                 lin_pha[:-1] - lin_pha[1:],
                 label=interpolation)

    plt.ylabel('Phase [deg]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()


    input('press enter')

if __name__ == '__main__':
    main.run()
