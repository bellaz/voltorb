#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.matplotlib import pyplot as plt

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    phase_correction = params.get('phase_correction', 0)
    shift_correction = params.get('shift_correction', 0)
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)

    filename_input = params['ifile']
    
    dictfile = dfile.read(filename_input)

    ff_table_amp = np.sqrt(dictfile['FF_TABLE_I']**2 + 
                           dictfile['FF_TABLE_Q']**2)[skip:-skip_back-1]
    corr_table_amp = np.sqrt(dictfile['CORR_TABLE_I']**2 + 
                             dictfile['CORR_TABLE_Q']**2)[skip:-skip_back-1]
    vm_output_amp = np.sqrt(dictfile['VM_OUTPUT_I']**2 + 
                            dictfile['VM_OUTPUT_Q']**2)[skip:-skip_back-1]

    amplifier_output_amp = dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'][skip:-skip_back-1]
    amplifier_output_phase = (dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'][skip:-skip_back-1] + 
                              phase_correction)
    
    ff_table_amp = np.roll(ff_table_amp, shift_correction)
    corr_table_amp = np.roll(ff_table_amp, shift_correction)
    vm_output_amp = np.roll(vm_output_amp, shift_correction)

    plt.figure()
    plt.title('Table binary values (amplitude sqrt(I^2+Q^2))')
    plt.plot(ff_table_amp, label="FF table")
    plt.plot(corr_table_amp, label="Correction table")
    plt.legend()
    plt.draw() 

    plt.figure()
    plt.title('Amplitude comparison (normalized)')
    plt.plot(ff_table_amp/np.max(ff_table_amp), label="FF table")
    plt.plot(corr_table_amp/np.max(corr_table_amp), label="Correction table")
    plt.plot(vm_output_amp/np.max(vm_output_amp), label="VM output")
    plt.plot(amplifier_output_amp/np.max(amplifier_output_amp), label="Amplifier output")
    plt.legend()
    plt.draw()
 
    plt.figure()
    plt.title('Amplifier phase')
    plt.plot(amplifier_output_phase)
    plt.draw() 

    input('Press Enter')

if __name__ == "__main__":
    main.run()
