#!/usr/bin/env python
'''
    Set a correction table that produces a linear output sweep.
'''

import numpy as np
import time
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    
    DELAY = params['delay']
    FILENAME_INPUT = params['ifile']
    ADDR_SP_AMPLITUDE = params['sp_amp_addr']
    ADDR_FF_TABLE_I = params['ff_table_I_addr']
    ADDR_FF_TABLE_Q = params['ff_table_Q_addr']
    ADDR_CORR_ENABLE = params['corr_enable_addr']
    ADDR_CORR_TABLE_I = params['corr_table_I_addr']
    ADDR_CORR_TABLE_Q = params['corr_table_Q_addr'] 

    FF_TABLE_I = csys.read(ADDR_FF_TABLE_I)
    FF_TABLE_LEN = len(FF_TABLE_I)
    FF_TABLE_AVG = np.mean(FF_TABLE_I)

    csys.write(ADDR_SP_AMPLITUDE, 0)
    csys.write(ADDR_CORR_ENABLE, 1)

    DICTFILE = dfile.read(FILENAME_INPUT)

    CORR_TABLE_IN = DICTFILE['CORR_TABLE_I']

    OUTPUT_AMPLITUDE = DICTFILE['AMPLIFIER_OUTPUT_AMPLITUDE']
    OUTPUT_PHASE = DICTFILE['AMPLIFIER_OUTPUT_PHASE']

    MAX_AMP = max(OUTPUT_AMPLITUDE)
    MAP_TRACE = np.linspace(0, MAX_AMP, FF_TABLE_LEN)
    CORR_AMP = np.interp(MAP_TRACE, OUTPUT_AMPLITUDE, CORR_TABLE_IN)

    CORR_PHASE = np.interp(MAP_TRACE, OUTPUT_PHASE, CORR_TABLE_IN)

    CORR_I = CORR_AMP*np.cos(-CORR_PHASE * np.pi/180) 
    CORR_Q = CORR_AMP*np.sin(-CORR_PHASE * np.pi/180)

    CORR_I = [int(el) for el in (CORR_I)]
    CORR_Q = [int(el) for el in (CORR_Q)]

    csys.write(ADDR_CORR_TABLE_I, CORR_I)
    csys.write(ADDR_CORR_TABLE_Q, CORR_Q)

    time.sleep(DELAY)

    csys.write(ADDR_CORR_ENABLE, 0)
    csys.write(ADDR_CORR_TABLE_I, [0]*FF_TABLE_LEN)
    csys.write(ADDR_CORR_TABLE_Q, [0]*FF_TABLE_LEN)

if __name__ == "__main__":
    main.run()
