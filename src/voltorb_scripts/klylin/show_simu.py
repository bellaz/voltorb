#!/usr/bin/python3
'''
    Show klylin simulation on real data and tables
'''

import numpy as np
from scipy.signal import detrend
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.klylin_table_generator import KlylinGenerator
from voltorb.parse_cmdline import parser_default
from scipy.signal import savgol_filter

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    filename_input = params['ifile']
    dictfile = dfile.read(filename_input)

    iq_bit_size = params['iq_bit_len']
    a2_bit_size = params['a2_bit_len']
    unity_bit_len = params['unity_bit_len']
    table_bit_len = params['table_bit_len']

    vm_values = dictfile['CORR_TABLE_I'][skip:-skip_back-1]
    amp = dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'][skip:-skip_back-1]
    pha = np.pi/180.0*dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'][skip:-skip_back-1]

    amp_orig = amp
    pha_orig = pha
    
    smooth_win = params.get('smooth_win', 0)
    smooth_order = params.get('smooth_order', 2)

    kgen_nonsmooth = KlylinGenerator(unity_bit=unity_bit_len, iq_bit_size=iq_bit_size,
                           a2_bit_size=a2_bit_size, table_bit_len=table_bit_len)

    klylin_nonsmooth = kgen_nonsmooth.generate_klylin_AP(amp, pha, vm_values)


    if smooth_win > 0:
        amp = savgol_filter(amp, smooth_win, smooth_order)
        pha = savgol_filter(pha, smooth_win, smooth_order)

    kgen = KlylinGenerator(unity_bit=unity_bit_len, iq_bit_size=iq_bit_size,
                           a2_bit_size=a2_bit_size, table_bit_len=table_bit_len)

    klylin = kgen.generate_klylin_AP(amp, pha, vm_values)

    plt.figure()
    plt.title('I and Q linearization base table')
    plt.plot(klylin.base_I_tables, label='I channel')
    plt.plot(klylin.base_Q_tables, label='Q channel')
    plt.xlabel('Table index')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('I and Q linearization derivative tables')
    plt.plot(klylin.deriv_I_tables, label='I channel')
    plt.plot(klylin.deriv_Q_tables, label='Q channel')
    plt.xlabel('Table index')
    plt.draw()

    derived_x = np.linspace(vm_values[0], vm_values[-1], vm_values[-1]-vm_values[0]+1)

    (new_i, new_q) = klylin.correct_IQ(derived_x)
    phase = np.arctan2(new_q, new_i)
    ampl_vm = np.sqrt(new_i**2 + new_q**2)
    out_amp = np.interp(ampl_vm, vm_values, amp)
    out_pha = np.interp(ampl_vm, vm_values, pha) + phase

    (new_i, new_q) = klylin.correct_IQ_base(derived_x)
    phase = np.arctan2(new_q, new_i)
    ampl_vm = np.sqrt(new_i**2 + new_q**2)
    out_amp_base = np.interp(ampl_vm, vm_values, amp)
    out_pha_base = np.interp(ampl_vm, vm_values, pha) + phase
    
    plt.figure()
    plt.title('Amplitude')
    plt.plot(vm_values, amp, label='Smoothed')
    plt.plot(vm_values, amp_orig, label='Non-smoothed')
    plt.ylabel('Amplitude [AU]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Phase')
    plt.plot(vm_values, 180/np.pi*pha, label='Smoothed')
    plt.plot(vm_values, 180/np.pi*pha_orig, label='Non-smoothed')
    plt.ylabel('Phase [degree]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()
    
    plt.figure()
    plt.title('Amplitude')
    plt.plot(derived_x, out_amp_base, label='Klylin BASE')
    plt.plot(derived_x, out_amp, label='Klylin ON')
    plt.plot(vm_values, amp, label='Klylin OFF')
    plt.ylabel('Amplitude [AU]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.title('Phase')
    plt.plot(derived_x, 180/np.pi*out_pha_base, label='Klylin BASE')
    plt.plot(derived_x, 180/np.pi*out_pha, label='Klylin ON')
    plt.plot(vm_values, 180/np.pi*pha, label='Klylin OFF')
    plt.ylabel('Phase [degree]')
    plt.xlabel('VM value')
    plt.legend()
    plt.draw()
 

    predistorter_amp_base_error = detrend(out_amp_base)/out_amp_base[-1]
    predistorter_amp_error = detrend(out_amp)/out_amp[-1]
    amp_error = detrend(amp)/amp[-1]

    plt.figure()
    plt.plot(vm_values/max(vm_values), 100*amp_error, '.', label='No predistortion')
    plt.plot(derived_x/max(derived_x), 100*predistorter_amp_base_error, '.', label='Predistortion')
    plt.plot(derived_x/max(derived_x), 100*predistorter_amp_error, '.', label='Predistortion w. interp')
    plt.ylabel('Amplitude error %')
    plt.xlabel('Requested amplitude [AU]')
    plt.legend()
    plt.draw()
    
    plt.figure()
    plt.plot(vm_values/max(vm_values), 180/np.pi*(pha-np.mean(pha)), '.', label='No predistortion')
    plt.plot(derived_x/max(derived_x), 180/np.pi*(out_pha_base-np.mean(out_pha_base)), '.', label='Predistortion')
    plt.plot(derived_x/max(derived_x), 180/np.pi*(out_pha-np.mean(out_pha)), '.', label='Predistortion w. interp')
    plt.ylabel('Phase error [deg]')
    plt.xlabel('Requested amplitude [AU]')
    plt.legend()
    plt.draw()
 
    plt.figure()
    plt.plot(klylin_nonsmooth.deriv_I_tables, 'b', label='I no smooth',)
    plt.plot(klylin.deriv_I_tables, 'r', label='I smooth')
    plt.plot(klylin_nonsmooth.deriv_Q_tables, 'y', label='Q no smooth')
    plt.plot(klylin.deriv_Q_tables, 'g', label='Q smooth')
    plt.xlabel('Table index')
    plt.ylabel('Table value')
    plt.legend()
    plt.draw()

    plt.figure()
    plt.semilogy(vm_values/max(vm_values), np.abs(100*amp_error), '.', ms=2, label='No predistortion')
    plt.semilogy(derived_x/max(derived_x), np.abs(100*predistorter_amp_base_error), '.', ms=2, label='Predistortion w/o interp')
    plt.semilogy(derived_x/max(derived_x), np.abs(100*predistorter_amp_error), '.', ms=2, label='Predistortion w. interp')
    plt.ylabel('Amplitude error %')
    plt.xlabel('Requested amplitude [AU]')
    plt.legend(markerscale=4)
    plt.draw()
    
    plt.figure()
    plt.semilogy(vm_values/max(vm_values), np.abs(180/np.pi*(pha-np.mean(pha))), '.', ms=2, label='No predistortion')
    plt.semilogy(derived_x/max(derived_x), np.abs(180/np.pi*(out_pha_base-np.mean(out_pha_base))), '.', ms=2, label='Predistortion w/o interp')
    plt.semilogy(derived_x/max(derived_x), np.abs(180/np.pi*(out_pha-np.mean(out_pha))), '.', ms=2, label='Predistortion w. interp')
    plt.ylabel('Phase error [deg]')
    plt.xlabel('Requested amplitude [AU]')
    plt.legend(markerscale=4)
    plt.draw()
   
    fig_err, (plot_amp_err, plot_pha_err) = plt.subplots(2,1)
    plot_amp_err.semilogy(vm_values/max(vm_values), np.abs(100*amp_error), '.', ms=2, label='No predistortion')
    plot_amp_err.semilogy(derived_x/max(derived_x), np.abs(100*predistorter_amp_base_error), '.', ms=2, label='Predistortion w/o interp')
    plot_amp_err.semilogy(derived_x/max(derived_x), np.abs(100*predistorter_amp_error), '.', ms=2, label='Predistortion w. interp')
    plot_amp_err.set_ylabel('Amplitude error %')
    plot_amp_err.set_xlabel('Requested amplitude [AU]')
    plot_amp_err.legend(markerscale=4)
    
    plot_pha_err.semilogy(vm_values/max(vm_values), np.abs(180/np.pi*(pha-np.mean(pha))), '.', ms=2, label='No predistortion')
    plot_pha_err.semilogy(derived_x/max(derived_x), np.abs(180/np.pi*(out_pha_base-np.mean(out_pha_base))), '.', ms=2, label='Predistortion w/o interp')
    plot_pha_err.semilogy(derived_x/max(derived_x), np.abs(180/np.pi*(out_pha-np.mean(out_pha))), '.', ms=2, label='Predistortion w. interp')
    plot_pha_err.set_ylabel('Phase error [deg]')
    plot_pha_err.set_xlabel('Requested amplitude [AU]')


    print("avg error amp", np.mean(np.abs(np.abs(100*amp_error))))
    print("avg error amp base", np.mean(np.abs(np.abs(100*predistorter_amp_base_error))))
    print("avg error amp pre", np.mean(np.abs(np.abs(100*predistorter_amp_error))))
    print("avg error pha", np.mean(np.abs(np.abs(180/np.pi*(pha-np.mean(pha))))))
    print("avg error pha base", np.mean(np.abs(np.abs(180/np.pi*(out_pha_base-np.mean(out_pha_base))))))
    print("avg error pha pre", np.mean(np.abs(np.abs(180/np.pi*(out_pha-np.mean(out_pha))))))

    print("max error amp", np.max(np.abs(np.abs(100*amp_error))))
    print("max error amp base", np.max(np.abs(np.abs(100*predistorter_amp_base_error))))
    print("max error amp pre", np.max(np.abs(np.abs(100*predistorter_amp_error))))
    print("max error pha", np.max(np.abs(np.abs(180/np.pi*(pha-np.mean(pha))))))
    print("max error pha base", np.max(np.abs(np.abs(180/np.pi*(out_pha_base-np.mean(out_pha_base))))))
    print("max error pha pre", np.max(np.abs(np.abs(180/np.pi*(out_pha-np.mean(out_pha))))))

    input('press enter')

if __name__ == '__main__':
    main.run()
