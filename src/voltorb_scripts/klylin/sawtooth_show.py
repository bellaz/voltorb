#!/usr/bin/env python
'''
    Show sawtooths of multiple files
'''

import numpy as np
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.matplotlib import pyplot as plt

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    fig_amp, plot_amp = plt.subplots(1, 1)
    fig_pha, plot_pha = plt.subplots(1, 1)
    titles = params['titles']

    idx = 0
    while True:
        idx += 1
        if 'ifile{}'.format(idx) in params:
            dictfile = dfile.read(params['ifile{}'.format(idx)])
            corr_table = dictfile['CORR_TABLE_I']
            amp_avg = dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG']
            pha_avg = dictfile['AMPLIFIER_OUTPUT_PHASE_AVG']
            plot_amp.plot(corr_table, amp_avg, '.', label=titles[idx-1])
            plot_pha.plot(corr_table, pha_avg, '.', label=titles[idx-1])
        else:
            break

    plot_amp.legend()
    plot_pha.legend()
    fig_amp.canvas.draw_idle()
    fig_pha.canvas.draw_idle()

    input('Enter something')

if __name__ == '__main__':
    main.run()
