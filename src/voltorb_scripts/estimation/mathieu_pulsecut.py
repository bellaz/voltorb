#!/usr/bin/env python3
'''
    Read Mathieu datafiles
'''

import pandas as pd
import numpy as np
from scipy.signal import savgol_filter
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default


@parser_default(__doc__)
def main(params):
    '''
        Scripts main entry point
    '''
    ifile = params['ifile']
    smooth_win = params.get('smooth_win', 101)
    smooth_win2 = params.get('smooth_win2', 2001)
    smooth_order = params.get('smooth_order', 1)
    data = pd.read_csv(ifile, sep=', ')
    frequency = params.get('frequency', 1.3E9)
    sample_rate = params.get('sample_rate', 9E6)
    Qext = params.get('Qext', 4.6E6)
    w0 = 2*np.pi*frequency
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)

    time_trace = np.linspace(0, len(data['PprobeDeg'])/sample_rate, len(data['PprobeDeg']))
    time_trace = time_trace[skip:-skip_back-1]
    rawprobe = (data['VprobeMV'] * np.exp(1j*data['PprobeDeg']*np.pi/180))[skip:-skip_back-1]
    rawvforw = (data['VforwardMV'] * np.exp(1j*data['VforwardDeg']*np.pi/180))[skip:-skip_back-1]
    rawvrefl = (data['VreflectedMV'] * np.exp(1j*data['PreflectedDeg']*np.pi/180))[skip:-skip_back-1]

    probe = 0j+savgol_filter(np.real(rawprobe), smooth_win, smooth_order)
    vforw = 0j+savgol_filter(np.real(rawvforw), smooth_win, smooth_order)
    vrefl = 0j+savgol_filter(np.real(rawvrefl), smooth_win, smooth_order)
    dprobe = 0j+savgol_filter(np.real(rawprobe), smooth_win, smooth_order, deriv=1, delta=1/sample_rate)

    probe += 1j*savgol_filter(np.imag(rawprobe), smooth_win, smooth_order)
    vforw += 1j*savgol_filter(np.imag(rawvforw), smooth_win, smooth_order)
    vrefl += 1j*savgol_filter(np.imag(rawvrefl), smooth_win, smooth_order)
    dprobe += 1j*savgol_filter(np.imag(rawprobe), smooth_win, smooth_order, deriv=1, delta=1/sample_rate)


    plt.figure()
    plt.title('derivative')
    plt.xlabel('Time [s]')
    plt.ylabel('Derivative magnitude')
    plt.plot(time_trace, np.real(dprobe), label='I')
    plt.plot(time_trace, np.imag(dprobe), label='Q')

    plt.figure()
    plt.title('Cavity signals')
    plt.xlabel('Time [s]')
    plt.ylabel('Signal magnitude [MV]')
    plt.plot(time_trace, np.real(probe), label='Probe I')
    plt.plot(time_trace, np.imag(probe), label='Probe Q')
    plt.plot(time_trace, np.real(vforw), label='VForw I')
    plt.plot(time_trace, np.imag(vforw), label='VForw Q')
    plt.plot(time_trace, np.real(vrefl), label='VRefl I')
    plt.plot(time_trace, np.imag(vrefl), label='VRefl Q')
    plt.legend()

    plt.figure()
    plt.title('Cavity signals')
    plt.xlabel('Time [s]')
    plt.ylabel('Signal magnitude [MV]')
    plt.plot(time_trace, np.real(probe), label='Probe I')
    plt.plot(time_trace, np.imag(probe), label='Probe Q')
    plt.plot(time_trace, np.real(vforw + vrefl), label='VProbe I')
    plt.plot(time_trace, np.imag(vforw + vrefl), label='VProbe Q')
    plt.legend()

    cavity_params = probe*np.conj(w0/(Qext)*vforw-dprobe)/(probe*np.conj(probe))
    w12 = np.real(cavity_params)
    detuning = np.imag(cavity_params)*0.5/np.pi

    QL = w0*0.5/(w12)

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Cavity bandwidth [Hz]')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(time_trace, w12/np.pi)
    #plt.plot(time_trace, savgol_filter(w12/np.pi, smooth_win2, smooth_order))

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Loaded Q')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.xlim(0.25e-3, 1.75e-3)
    plt.plot(time_trace, QL)
    #plt.plot(time_trace, savgol_filter(QL, smooth_win2, smooth_order))

    plt.figure()
    plt.ylabel('Detuning [Hz]')
    plt.xlabel('Time [s]')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.xlim(0.25e-3, 1.75e-3)
    plt.plot(time_trace, detuning)
    #plt.plot(time_trace, savgol_filter(detuning, smooth_win2, smooth_order))

    plt.figure()
    plt.title('Cavity amplitude')
    plt.ylabel('Cavity amplitude [Hz]')
    plt.xlabel('Time [s]')
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.plot(time_trace, np.abs(probe))

    input('Press Return..')

if __name__ == "__main__":
    main.run()
