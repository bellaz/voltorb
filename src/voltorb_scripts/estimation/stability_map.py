'''
    Estimate detuning threshold for the monotonic and oscillatory instabilities
'''
import numpy as np
import scipy
from voltorb.matplotlib import pyplot as plt
import matplotlib.cm as cm
from voltorb.parse_cmdline import parser_default


@parser_default(__doc__)
def main(params):
    Vg_range = params.get('Vg_range', [0,20])
    detuning_range = params.get('detuning_range', [-20, 10])
    QL = params.get('QL', 6e7)
    frequency = params.get('frequency', 1.3e9)
    voltage_steps = params.get('voltage_steps', 2.5)
    
    #resonances = params.get('resonances',[[150,200,-0.25],
    #                                      [152,200,-0.25],
    #                                      [202,200,-0.5],
    #                                      [202,200,-0.25],
    #                                      [270,200,-0.25]])

    resonances = params.get('resonances',[[180,200,-0.75],
                                          [260,200,-0.75]])

    #resonances = params.get('resonances',[[41.7,  200, -0.5],
    #                                      [93.8,  111, -0.5],
    #                                      [171.8, 139, -0.5],
    #                                      [193.6, 106, -0.5]]) #CONTROL ALGORITHM TESTS USING A VIRTUAL CW SRF CAVITY

    points = params.get('points', 500)

    f12 = frequency * 0.5 / QL
    w12 = 2.0 * np.pi * f12
    H = np.array([[0   , 0],
                  [-w12, 0]])

    Ams = []

    for (m_f, m_Q, m_K) in resonances:
        m_w = 2.0 * np.pi * m_f
        Ams.append(np.array([[0       ,        1],
                             [-m_w*m_w, -m_w/m_Q]]))

    def sys_matrix_Vg(Vg, detuning):
        '''
            define system matrix for generator voltage and detuning value
        '''
        matrix_size = len(resonances)*2 + 2
        y = detuning / f12
        A = np.zeros((matrix_size, matrix_size))
        A[0:2, 0:2] = w12 * np.array([[-1,  y],
                                     [-y, -1]])

        for i in range(len(resonances)):
            idx_m = i*2 + 2

            A[0:2, idx_m:idx_m+2] = H
            A[idx_m:idx_m+2, idx_m:idx_m+2] = Ams[i]

            [m_f, m_Q, m_K] = resonances[i]
            m_w = 2.0 * np.pi * m_f
            m_K_norm = 2.0 * np.pi * m_K / w12
            G_const = 2.0 * m_w * m_w * m_K_norm * Vg / (1 + (y*y))
            G = G_const *np.array([[0,  0],
                                   [1, -y]])
            A[idx_m:idx_m+2, 0:2] = G

        return A

    def sys_matrix_Vc(Vc, detuning):
        '''
            define system matrix for generator voltage and detuning value
        '''
        y = detuning / f12
        Vg = Vc*np.sqrt(1 + y**2)
        return(sys_matrix_Vg(Vg, detuning))


    Vgs = np.linspace(Vg_range[0], Vg_range[1], points)
    detunings = np.linspace(detuning_range[0], detuning_range[1], points)

    stability_matrix = np.zeros((points, points), dtype=int)

    for i in range(points): 
        for j in range(points): 
            A = sys_matrix_Vg(Vgs[j], detunings[i])
            eig_vals, _ = np.linalg.eig(A)
            positive = 0
            for eig_val in eig_vals:
                if np.real(eig_val) > 0.0:
                    positive += 1

            stability_matrix[j, i] = positive

    plt.figure()

    aspect = (detuning_range[0]-detuning_range[1])/(Vg_range[0]-Vg_range[1])
    im = plt.imshow(stability_matrix, 
                    origin='lower', 
                    extent=detuning_range+Vg_range,  
                    aspect=aspect,
                    cmap=cm.Greys)

    cbar = plt.colorbar(im)
    plt.grid(True)
    cbar.ax.get_yaxis().set_ticks(list(range(len(resonances)*2+1)))

    cbar.ax.get_yaxis().labelpad = 20
    cbar.ax.set_ylabel('number of positive eigenvalues', rotation=270)

    voltages_iso = np.linspace(Vg_range[0] + voltage_steps, 
                               Vg_range[1],
                               int((Vg_range[1]-Vg_range[0])/voltage_steps))

    for volt_iso in voltages_iso:
        volt_iso_curve = volt_iso*np.sqrt(1 + (detunings/f12)**2)
        plt.plot(detunings, volt_iso_curve, 'r--', linewidth=1)

    plt.xlim(detuning_range)
    plt.ylim(Vg_range)

    plt.xlabel('Detuning (Hz)')
    plt.ylabel('Generator amplitude (MV)')
    plt.title('Ponderomotive stability map')

    Vcs = np.linspace(Vg_range[0], Vg_range[1], points)
    detunings = np.linspace(detuning_range[0], detuning_range[1], points)

    stability_matrix = np.zeros((points, points), dtype=int)

    for i in range(points): 
        for j in range(points): 
            A = sys_matrix_Vc(Vcs[j], detunings[i])
            eig_vals, _ = np.linalg.eig(A)
            positive = 0
            for eig_val in eig_vals:
                if np.real(eig_val) > 0.0:
                    positive += 1

            stability_matrix[j, i] = positive

    plt.figure()

    aspect = (detuning_range[0]-detuning_range[1])/(Vg_range[0]-Vg_range[1])
    im = plt.imshow(stability_matrix, 
                    origin='lower', 
                    extent=detuning_range+Vg_range,  
                    aspect=aspect,
                    cmap=cm.Greys)

    cbar = plt.colorbar(im)
    plt.grid(True)
    cbar.ax.get_yaxis().set_ticks(list(range(len(resonances)*2+1)))

    cbar.ax.get_yaxis().labelpad = 20
    cbar.ax.set_ylabel('number of positive eigenvalues', rotation=270)

    plt.xlabel('Detuning (Hz)')
    plt.ylabel('Cavity voltage (MV)')
    plt.title('Ponderomotive stability map')

    input("press Return...")
