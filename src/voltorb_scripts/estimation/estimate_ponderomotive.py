'''
    Estimate detuning threshold for the monotonic and oscillatory instabilities
'''
import numpy as np
import scipy
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    Vg = params.get('Vg', 20)
    QL = params.get('QL', 6e7)
    frequency = params.get('frequency', 1.3e9)
    Klfd = params.get('Klfd', -0.5)
    Qmech = params.get('Qmech', 200)
    frequency_mech = params.get('frequency_mech', 260)
    opt_range_monotonic = tuple(params.get('opt_range_monotonic', [0, 10]))
    opt_range_oscillatory = tuple(params.get('opt_range_oscillatory', [-10, 0]))

    points = 1000
    tau = QL / (np.pi * frequency)
    tau_mech = Qmech / (np.pi * frequency_mech)
    Klfd_w = 2.0 * np.pi * Klfd
    w_mech = 2.0 * np.pi * frequency_mech
    cavity_bandwidth = frequency*0.5/QL

    print('Vg [MV]: ', Vg)
    print('QL: ', QL)
    print('frequency [MHz] :', frequency * 1e-6)
    print('Klfd [Hz/(MV²)]', Klfd)
    print('Qmech', Qmech)
    print('frequency_mech', frequency_mech)

    tau_r = tau / tau_mech
    print('tau_r: ', tau_r)

    def f_monotonic(y):
        '''
            Monotonic instability optimization function
        '''
        return (2*y*(Vg**2)*Klfd_w*tau)/((1+y**2)**2)+1

    def f_oscillatory(y):
        '''
            Oscillatory instability optimization function
        '''
        # 3.5.6 from Schulze's thesis
        return ((2*y/(1+y**2))*(Vg**2)*Klfd_w*tau_mech*((w_mech*tau*(1+tau_r))**2)/ 
                ((1+(y**2)-((w_mech*tau)**2))**2+4*(1+tau_r)*((w_mech*tau)**2+(1+y**2)*tau_r)))-1

    center_monotonic = (opt_range_monotonic[0]+opt_range_monotonic[1])*0.5
    center_oscillatory = (opt_range_oscillatory[0]+opt_range_oscillatory[1])*0.5

    monotonic_minimum = scipy.optimize.minimize(f_monotonic, 
                                                [center_monotonic])

    if f_monotonic(monotonic_minimum.x[0]) < 0:
        monotonic_threshold = scipy.optimize.brentq(f_monotonic, 
                                                    opt_range_monotonic[0],
                                                    monotonic_minimum.x[0]) 
        print("Monotonic threshold (y): ", monotonic_threshold)
        print("Monotonic threshold [Hz]: ", monotonic_threshold*cavity_bandwidth)

    oscillatory_maximum = scipy.optimize.minimize(lambda x : -f_oscillatory(x),
                                                  [center_oscillatory])

    if f_oscillatory(oscillatory_maximum.x[0]) > 0:
        oscillatory_threshold = scipy.optimize.brentq(f_oscillatory, 
                                                      oscillatory_maximum.x[0],
                                                      opt_range_oscillatory[1])
        print("Oscillatory threshold (y): ", oscillatory_threshold)
        print("Oscillatory threshold [Hz]: ", oscillatory_threshold*cavity_bandwidth)


    #root_monotonic = scipy.optimize.fsolve(f_monotonic,
    #                                       1,
    #                                       opt_range_monotonic[0], 
    #                                       opt_range_monotonic[1])

    #root_oscillatory = scipy.optimize.fsolve(f_oscillatory, 
    #                                         -1,
    #                                         opt_range_oscillatory[0], 
    #                                         opt_range_oscillatory[1])

    y_monotonic = np.linspace(opt_range_monotonic[0], opt_range_monotonic[1], points)
    mag_monotonic = f_monotonic(y_monotonic)
    plt.figure()
    plt.plot(y_monotonic, mag_monotonic)
    
    y_oscillatory = np.linspace(opt_range_oscillatory[0], opt_range_oscillatory[1], points)
    mag_oscillatory = f_oscillatory(y_oscillatory)
    plt.figure()
    plt.plot(y_oscillatory, mag_oscillatory)

    input("press Return...")


