#!/usr/bin/env python3
'''
    show ql temperature and power
'''
from datetime import datetime
import matplotlib.dates as mdates
import numpy as np
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    dictfile = dfile.read(params['ifile'])
    
    #dictfile['PULSE_FLATTOP'] = csys.read(params['pulse_flattop_percent_addr'])  
    #dictfile['PULSE_FILLING'] = csys.read(params['pulse_filling_percent_addr'])
    #dictfile['PULSE_DELAY'] = csys.read(params['pulse_delay_percent_addr'])

    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)

    temp_upper = params.get('temp_upper', 140)
    temp_lower = params.get('temp_lower', 70)

    ql_upper = params.get('ql_upper', 3.55E7)
    ql_lower = params.get('ql_lower', 2.40E7)

    probe_amps = dictfile['PROBE_AMPS'][skip:-skip_back-1]
    probe_phas = dictfile['PROBE_PHAS'][skip:-skip_back-1]
    vforw_amps = dictfile['VFORW_AMPS'][skip:-skip_back-1]
    vforw_phas = dictfile['VFORW_PHAS'][skip:-skip_back-1]
    vrefl_amps = dictfile['VREFL_AMPS'][skip:-skip_back-1]
    vrefl_phas = dictfile['VREFL_PHAS'][skip:-skip_back-1]
    cav_qls = dictfile['CAV_QLS'][skip:-skip_back-1]  
    times = dictfile['TIME'][skip:-skip_back-1]      
    cav_fpc_temp   = dictfile['FPC_TEMP'][skip:-skip_back-1]  
    cav_forw_power = dictfile['FORW_POWER'][skip:-skip_back-1]

    print(times[0], times[-1])
    print(np.mean(cav_forw_power))

    fig_qls, ax_qls = plt.subplots()
    ax_temp = ax_qls.twinx()
    ax_qls.set_xlabel('Time')
    ax_qls.set_ylabel('Loaded Q')
    ax_temp.set_ylabel('Coupler temp. [K]')

    time_trace = [datetime.fromtimestamp(timestamp) for timestamp in times]
    xformatter = mdates.DateFormatter('%H:%M')
    ax_qls.xaxis.set_major_formatter(xformatter)
    ax_temp.xaxis.set_major_formatter(xformatter)

    ql_pl = ax_qls.plot(time_trace, cav_qls, 'bo', label="Loaded Q")
    tm_pl = ax_temp.plot(time_trace, cav_fpc_temp, 'r*', label="Temperature")
    ax_qls.legend(ql_pl+tm_pl, [l.get_label() for l in (ql_pl+tm_pl)], loc='lower right',
                    bbox_to_anchor=(1, 1.02), ncol=2)
    
    
    ax_temp.set_ylim(temp_lower, temp_upper)
    ax_qls.set_ylim(ql_lower, ql_upper)

    fig_qls, ax_qls = plt.subplots()
    ax_temp = ax_qls.twinx()
    ax_qls.set_xlabel('Time [h]')
    ax_qls.set_ylabel('Loaded Q')
    ax_temp.set_ylabel('Coupler temp. [K]')

    time_trace = [(datetime.fromtimestamp(timestamp) - 
                    datetime.fromtimestamp(times[0])).total_seconds()/3600.0 for timestamp 
                                                      in times]
    ql_pl = ax_qls.plot(time_trace, cav_qls, 'bo', label="Loaded Q")
    tm_pl = ax_temp.plot(time_trace, cav_fpc_temp, 'r*', label="Temperature")

    ax_temp.set_ylim(temp_lower, temp_upper)
    ax_qls.set_ylim(ql_lower, ql_upper)

    ax_qls.legend(ql_pl+tm_pl, [l.get_label() for l in (ql_pl+tm_pl)], framealpha=1.0, 
            loc='lower right', bbox_to_anchor=(1, 1.02), ncol=2)
    
    input("Press return..")

if __name__ == "__main__":
    main.run()
