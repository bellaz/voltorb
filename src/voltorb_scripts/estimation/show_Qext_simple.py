 #!/usr/bin/env python3
'''
    show derived Qext
'''

import numpy as np
from scipy.optimize import minimize
from scipy.optimize import curve_fit
from voltorb.parse_cmdline import parser_default
import voltorb.dictfile as dfile
from voltorb.matplotlib import pyplot as plt

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


def mul_fun(x, *args):
    '''
        :params x: phase to optimize
        :params x: probe amplitude forward IQ reflected IQ
        :returns: :e 
    '''
    probe_amp = args[0]
    virtual_I = args[1] + np.cos(x)*args[3] - np.sin(x)*args[4]
    virtual_Q = args[2] + np.sin(x)*args[3] + np.cos(x)*args[4]
    virtual_probe = (virtual_I)**2 + (virtual_Q)**2
    return (probe_amp**2)/virtual_probe

def optimize_fun(x, *args):
    '''
        :params x: phase to optimize
        :params x: probe amplitude forward IQ reflected IQ
        :returns: variance
    '''
    return np.mean(mul_fun(x, *args))

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    ifile = params['ifile']

    dictfile = dfile.read(ifile)

    expected_ql = params.get('cav_ql_target', 3E7)
    cav_qls = dictfile['CAV_QLS']
    cav_qls_der = []
    cav_qes = []
    cav_qes_corr = []
    corr_phase = []

    frequency = params.get('frequency', 1.3E9)
    time_pulse = params.get('time_pulse', 1)

    pulse_flattop = dictfile['PULSE_FLATTOP']/100
    pulse_filling = dictfile['PULSE_FILLING']/100
    pulse_delay = dictfile['PULSE_DELAY']/100

    skip_decay = dictfile.get('skip_delay', 0.003)
    frac_decay = dictfile.get('frac_delay', 0.05)

    probe_amps = dictfile['PROBE_AMPS']
    vforw_amps = dictfile['VFORW_AMPS']
    vforw_phas = dictfile['VFORW_PHAS']
    vrefl_amps = dictfile['VREFL_AMPS']
    vrefl_phas = dictfile['VREFL_PHAS']

    trace_len = len(probe_amps[0, :])
    filling_start = int(trace_len*pulse_delay)
    filling_end = filling_start + int(trace_len*pulse_filling)
    flattop_start = filling_end
    flattop_end = flattop_start + int(trace_len*pulse_flattop)
    decay_start = flattop_end + int(trace_len*skip_decay)
    decay_end = decay_start + int(trace_len*frac_decay)

    analysis_fraction_start = flattop_end - int(trace_len*frac_decay)
    analysis_fraction_end = flattop_end + int(trace_len*frac_decay)


    for idx in range(len(cav_qls)):
        print(idx)
        probe_amp = probe_amps[idx, :]
        vforw_amp = vforw_amps[idx, :]
        vforw_pha = vforw_phas[idx, :]
        vrefl_amp = vrefl_amps[idx, :]
        vrefl_pha = vrefl_phas[idx, :]
        probe_decay_amp = probe_amp[decay_start:decay_end]

        time_decay = np.linspace(0, time_pulse*frac_decay, len(probe_decay_amp))
        coeffs = curve_fit(exp_fun, time_decay, probe_decay_amp,
                            [np.pi*frequency/expected_ql, 1, 0.])

        cav_qls_der.append(np.pi*frequency/coeffs[0][0])

        probe_flattop_amp = probe_amp[flattop_start:flattop_end] 
        vforw_flattop_amp = vforw_amp[flattop_start:flattop_end] 
        vforw_flattop_pha = vforw_pha[flattop_start:flattop_end] 
        vrefl_flattop_amp = vrefl_amp[flattop_start:flattop_end] 
        vrefl_flattop_pha = vrefl_pha[flattop_start:flattop_end]

        probe_analysis_amp = probe_amp[analysis_fraction_start:flattop_end] 
        vforw_analysis_amp = vforw_amp[analysis_fraction_start:flattop_end] 
        vforw_analysis_pha = vforw_pha[analysis_fraction_start:flattop_end] 
        vrefl_analysis_amp = vrefl_amp[analysis_fraction_start:flattop_end] 
        vrefl_analysis_pha = vrefl_pha[analysis_fraction_start:flattop_end]

        (vforw_flattop_I, vforw_flattop_Q) = APtoIQ(vforw_flattop_amp, vforw_flattop_pha)
        (vrefl_flattop_I, vrefl_flattop_Q) = APtoIQ(vrefl_flattop_amp, vrefl_flattop_pha)

        qes_trace = probe_flattop_amp**2/((vforw_flattop_I + vrefl_flattop_I)**2 +
                                  (vforw_flattop_Q + vrefl_flattop_Q)**2)

        cav_qes.append(np.mean(qes_trace)*expected_ql)

        (vforw_analysis_I, vforw_analysis_Q) = APtoIQ(vforw_analysis_amp, vforw_analysis_pha)
        (vrefl_analysis_I, vrefl_analysis_Q) = APtoIQ(vrefl_analysis_amp, vrefl_analysis_pha)

        res_arr = []
        angle_arr = np.linspace(-np.pi/3, np.pi/3, 100)
        #for angle in angle_arr:
        #    res_arr.append(optimize_fun(angle,
        #                                probe_analysis_amp, 
        #                                vforw_analysis_I, 
        #                                vforw_analysis_Q, 
        #                                vrefl_analysis_I, 
        #                                vrefl_analysis_Q))


        #angle_min = angle_arr[np.argmax(res_arr)]

        #cav_qes_corr.append(np.mean(expected_ql*mul_fun(angle_min,
        #                                                  probe_analysis_amp, 
        #                                                  vforw_analysis_I, 
        #                                                  vforw_analysis_Q, 
        #                                                  vrefl_analysis_I, 
        #                                                 vrefl_analysis_Q)))
        #corr_phase.append(angle_min)

    plt.figure()
    plt.xlabel('Diagnostic QL')
    plt.ylabel('Recomputed QL')
    plt.plot(cav_qls, cav_qls_der, 'o')
    plt.draw()

    plt.figure()
    plt.xlabel('QL (decay)')
    plt.ylabel('QExt')
    plt.plot(cav_qls_der, cav_qes, 'o')
    #plt.plot(cav_qls, cav_qes_corr, 'o')
    plt.draw()

    plt.figure()
    plt.ylabel('Recomputed QL')
    plt.plot(cav_qls_der, 'o')
    plt.draw()

    plt.figure()
    plt.ylabel('Diagnostic QL')
    plt.plot(cav_qls, 'o')
    plt.draw()


    #plt.figure()
    #plt.plot(corr_phase, 'o')
    #plt.draw()

    input('Press Enter')

if __name__ == '__main__':
    main.run()
