#!/usr/bin/env python3
'''
    compare calibrated reflected and forward signal to the probe and derive Qext
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default, merge_selected
import voltorb.dictfile as dfile

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''


    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    coupler_value_addr = params['coupler_value_addr']
    coupler_enable_addr = params['coupler_enable_addr']
    coupler_start = params['coupler_start']
    coupler_stop = params['coupler_stop']
    delay_move = params.get('delay_move', 5)
    steps = params['steps']
    
    cav_ql_addr = params['cav_ql_addr']
    cav_target_ql = params['cav_ql_target']
    delay = params.get('delay', 1)
    measurements = params.get('measurements', 10)

    ofile = params.get('ofile', 'out.h5')

    dictfile = {}
    dictfile['delay'] = delay
    dictfile['measurements'] = measurements
    
    if 'pulse_flattop_percent_addr' in params:
        dictfile['PULSE_FLATTOP'] = csys.read(params['pulse_flattop_percent_addr'])  
        dictfile['PULSE_FILLING'] = csys.read(params['pulse_filling_percent_addr'])
        dictfile['PULSE_DELAY'] = csys.read(params['pulse_delay_percent_addr'])

    probe_amps = []
    probe_phas = []
    vforw_amps = []
    vforw_phas = []
    vrefl_amps = []
    vrefl_phas = []
    cav_qls = []
    virt_Is = []
    virt_Qs = []
    multiplier_traces = []
    cav_qe_traces = []
    ratio_traces = []
    pos_save = []
    positions = np.linspace(coupler_start, coupler_stop, steps)

    for (meas, pos) in enumerate(positions):

        print('measuring {}/{}'.format(meas+1, steps), 'pos:', pos)
        csys.write(coupler_value_addr, int(pos))
        csys.write(coupler_enable_addr, 1)
        time.sleep(delay_move)
        csys.write(coupler_enable_addr, 0)
      
        
        for _ in range(measurements):
             
            pos_save.append(pos) 

            [probe_amp,
             probe_pha,
             vforw_amp,
             vforw_pha,
             vrefl_amp,
             vrefl_pha,
             cav_ql] = csys.read(probe_amp_addr,
                                 probe_pha_addr,
                                 vforw_amp_addr,
                                 vforw_pha_addr,
                                 vrefl_amp_addr,
                                 vrefl_pha_addr,
                                 cav_ql_addr)

            (probe_I, probe_Q) = APtoIQ(probe_amp, probe_pha)
            (vforw_I, vforw_Q) = APtoIQ(vforw_amp, vforw_pha)
            (vrefl_I, vrefl_Q) = APtoIQ(vrefl_amp, vrefl_pha)

            cav_ql_trace = [cav_ql]*len(probe_amp)

            virt_I = vforw_I + vrefl_I
            virt_Q = vforw_Q + vrefl_Q

            multiplier_trace = (probe_amp**2)/((virt_I)**2 + (virt_Q)**2)
            cav_qe_trace = cav_target_ql*multiplier_trace

            mmult= np.mean(multiplier_trace[20000:200000])
            mqe = np.mean(cav_qe_trace[20000:200000])
            mratio = cav_ql/np.mean(cav_qe_trace[20000:200000])
 
            print('multiplier: ', mmult,
                  'Qext: ', mqe,
                  'QL: ', cav_ql,
                  'ratio: ', mratio)

            probe_amps.append(probe_amp)
            probe_phas.append(probe_pha)
            vforw_amps.append(vforw_amp)
            vforw_phas.append(vforw_pha)
            vrefl_amps.append(vrefl_amp)
            vrefl_phas.append(vrefl_pha)
            cav_qls.append(cav_ql)
            virt_Is.append(virt_I)
            virt_Qs.append(virt_Q)
            multiplier_traces.append(multiplier_trace)
            cav_qe_traces .append(cav_qe_trace)
            ratio_traces.append(cav_ql/np.mean(cav_qe_trace))
            
            time.sleep(delay)

    dictfile['PROBE_AMPS'] = probe_amps
    dictfile['PROBE_PHAS'] = probe_phas
    dictfile['VFORW_AMPS'] = vforw_amps
    dictfile['VFORW_PHAS'] = vforw_phas
    dictfile['VREFL_AMPS'] = vrefl_amps
    dictfile['VREFL_PHAS'] = vrefl_phas
    dictfile['CAV_QLS'] = cav_qls
    dictfile['VIRT_IS'] = virt_Is
    dictfile['VIRT_QS'] = virt_Qs
    dictfile['MULTIPLIER_TRACES'] = multiplier_traces
    dictfile['RATIO_TRACES'] = ratio_traces
    dictfile['CAV_QE_TRACES'] = cav_qe_traces
    dictfile['COUPLER_POSITIONS'] = pos_save

    dfile.write(ofile, dictfile)

if __name__ == "__main__":
    main.run()
