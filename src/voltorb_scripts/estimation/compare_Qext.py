#!/usr/bin/env python3
'''
    compare calibrated reflected and forward signal to the probe and derive Qext
'''

import time
import numpy as np
from scipy.optimize import minimize
from voltorb import controlsys as csys
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

def mul_fun(x, *args):
    '''
        :params x: phase to optimize
        :params x: probe amplitude forward IQ reflected IQ
        :returns: variance
    '''
    probe_amp = args[0]
    virtual_I = args[1] + np.cos(x)*args[3] - np.sin(x)*args[4]
    virtual_Q = args[2] + np.sin(x)*args[3] + np.cos(x)*args[4]

    return (probe_amp**2)/((virtual_I)**2 + (virtual_Q)**2)

def optimize_fun(x, *args):
    '''
        :params x: phase to optimize
        :params x: probe amplitude forward IQ reflected IQ
        :returns: variance
    '''
    rval = mul_fun(x, *args)
    return np.var(rval[~np.isnan(rval)])

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)

@parser_default(__doc__)
def main(params):

    delay = 1

    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    cav_ql_addr = params['cav_ql_addr']
    cav_target_ql = params['cav_ql_target']

    [probe_amp,
     probe_pha,
     vforw_amp,
     vforw_pha,
     vrefl_amp,
     vrefl_pha] = csys.read(probe_amp_addr,
                         probe_pha_addr,
                         vforw_amp_addr,
                         vforw_pha_addr,
                         vrefl_amp_addr,
                         vrefl_pha_addr)

    cav_ql = csys.read(cav_ql_addr)

    probe_amp = probe_amp[skip:-skip_back-1] 
    probe_pha = probe_pha[skip:-skip_back-1]
    vforw_amp = vforw_amp[skip:-skip_back-1]
    vforw_pha = vforw_pha[skip:-skip_back-1]
    vrefl_amp = vrefl_amp[skip:-skip_back-1]
    vrefl_pha = vrefl_pha[skip:-skip_back-1]

    (probe_I, probe_Q) = APtoIQ(probe_amp, probe_pha)
    (vforw_I, vforw_Q) = APtoIQ(vforw_amp, vforw_pha)
    (vrefl_I, vrefl_Q) = APtoIQ(vrefl_amp, vrefl_pha)

    cav_ql_trace = [cav_ql]*len(probe_amp)
    cav_qe_trace = cav_target_ql*(probe_amp**2)/((vforw_I + vrefl_I)**2 + (vforw_Q + vrefl_Q)**2)

    virt_I = vforw_I + vrefl_I
    virt_Q = vforw_Q + vrefl_Q

    fig1 = plt.figure()
    ax1 = plt.subplot(1,1,1)
    plot_iprobe, = ax1.plot(probe_I, label='I probe')
    plot_ivirt, = ax1.plot(virt_I, label='I virtual')
    ax1.legend()

    fig2 = plt.figure()
    ax2 = plt.subplot(1,1,1)
    plot_qprobe, = ax2.plot(probe_Q, label='Q probe')
    plot_qvirt, = ax2.plot(virt_Q, label='Q virtual')
    ax2.legend()

    fig3 = plt.figure()
    ax3 = plt.subplot(1,1,1)
    plot_ql, = ax3.plot(cav_ql_trace, label='QL')
    plot_qext, = ax3.plot(cav_qe_trace, label='Qext')
    plot_qext_corr, = ax3.plot(cav_qe_trace, label='Qext corrected')
    ax3.set_ylim(0, cav_target_ql*2)
    ax3.legend()

    while True:
        [probe_amp,
         probe_pha,
         vforw_amp,
         vforw_pha,
         vrefl_amp,
         vrefl_pha] = csys.read(probe_amp_addr,
                             probe_pha_addr,
                             vforw_amp_addr,
                             vforw_pha_addr,
                             vrefl_amp_addr,
                             vrefl_pha_addr)

        cav_ql = csys.read(cav_ql_addr)

        probe_amp = probe_amp[skip:-skip_back-1] 
        probe_pha = probe_pha[skip:-skip_back-1]
        vforw_amp = vforw_amp[skip:-skip_back-1]
        vforw_pha = vforw_pha[skip:-skip_back-1]
        vrefl_amp = vrefl_amp[skip:-skip_back-1]
        vrefl_pha = vrefl_pha[skip:-skip_back-1]

        (probe_I, probe_Q) = APtoIQ(probe_amp, probe_pha)
        (vforw_I, vforw_Q) = APtoIQ(vforw_amp, vforw_pha)
        (vrefl_I, vrefl_Q) = APtoIQ(vrefl_amp, vrefl_pha)

        cav_ql_trace = [cav_ql]*len(probe_amp)
        cav_qe_trace = cav_target_ql*(probe_amp**2)/((vforw_I + vrefl_I)**2 + (vforw_Q + vrefl_Q)**2)

        virt_I = vforw_I + vrefl_I
        virt_Q = vforw_Q + vrefl_Q
        
        res_min = minimize(optimize_fun, 0, (probe_amp, vforw_I, vforw_Q, vrefl_I, vrefl_Q), bounds=[(-np.pi, np.pi)])

        print('multiplier: ', np.mean(((probe_amp**2)/((vforw_I + vrefl_I)**2 + (vforw_Q + vrefl_Q)**2))[2000:]),
                'Qext: ', np.mean(cav_qe_trace), 'QL: ', cav_ql,
                'ratio: ', np.mean(cav_qe_trace)/cav_ql,
                'angle min[deg]:', res_min.x[0]*180/np.pi)

        plot_iprobe.set_ydata(probe_I)
        plot_qprobe.set_ydata(probe_Q)
        plot_ivirt.set_ydata(virt_I)
        plot_qvirt.set_ydata(virt_Q)
        plot_ql.set_ydata(cav_ql_trace)
        plot_qext.set_ydata(cav_qe_trace)
        plot_qext_corr.set_ydata(cav_target_ql*mul_fun(res_min.x, probe_amp, vforw_I, vforw_Q, vrefl_I, vrefl_Q))

        ax1.relim()
        ax1.autoscale_view()
        fig1.canvas.flush_events()

        ax2.relim()
        ax2.autoscale_view()
        fig2.canvas.flush_events()

        ax3.relim()
        ax3.autoscale_view()
        fig3.canvas.flush_events()

        time.sleep(delay)

if __name__ == "__main__":
    main.run()
