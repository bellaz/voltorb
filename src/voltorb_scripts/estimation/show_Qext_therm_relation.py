#!/usr/bin/env python3
'''
    show ql temperature and power
'''
from datetime import datetime
import matplotlib.dates as mdates
import numpy as np
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default
from matplotlib import rc

rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
rc('text', usetex=True)

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    idx = 1
    
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)

    temp_upper = params.get('temp_upper', 140)
    temp_lower = params.get('temp_lower', 70)

    ql_upper = params.get('ql_upper', 3.55E7)
    ql_lower = params.get('ql_lower', 2.40E7)

    plt.figure()

    while ('ifile' + str(idx)) in params:
        dictfile = dfile.read(params['ifile' + str(idx)])

        cav_qls = dictfile['CAV_QLS'][skip:-skip_back-1]  
        cav_fpc_temp   = dictfile['FPC_TEMP'][skip:-skip_back-1]  

        plt.plot(cav_fpc_temp, cav_qls, '+', label="Cavity " + str(idx))
        idx += 1
    
    plt.xlabel("Temperature (K)")
    plt.ylabel("$Q_L$")
    plt.grid()
    plt.legend(prop={'size': 16})
    plt.draw()
    input("Press return..")

if __name__ == "__main__":
    main.run()
