#!/usr/bin/env python3
'''
    compare calibrated reflected and forward signal to the probe and derive Qext
'''

import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
import voltorb.dictfile as dfile

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    fraction_read = params.get('fraction_read', 0.005)
    calculation_start = params.get('additional_delay', 0.0015)
    cav_target_ql = params.get('cav_target_ql', 3E7)
    pulse_delay_percent_addr = params['pulse_delay_percent_addr']
    pulse_filling_percent_addr = params['pulse_filling_percent_addr']
    pulse_flattop_percent_addr = params['pulse_flattop_percent_addr']
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    delay = params.get('delay', 10)
    measurements = params.get('measurements', 1000)
    frequency = params.get('frequency', 1.3E9)


    trace_time = params.get('trace_time', 1)

    ofile = params['ofile']

    dictfile = {}

    probe_amps = []
    probe_phas = []
    vforw_amps = []
    vforw_phas = []
    vrefl_amps = []
    vrefl_phas = []
    cav_qls = []
    virt_Is = []
    virt_Qs = []
    multiplier_traces = []
    cav_qe_traces = []
    ratio_traces = []

    delay = csys.read(pulse_delay_percent_addr)
    filling = csys.read(pulse_filling_percent_addr)
    flattop = csys.read(pulse_flattop_percent_addr)
    decay_prop = (delay+filling+flattop)/100.0 + calculation_start


    for i in range(measurements):

        time.sleep(delay)
        print('Measuring {}/{}'.format(i+1, measurements))

        [probe_amp,
         probe_pha,
         vforw_amp,
         vforw_pha,
         vrefl_amp,
         vrefl_pha] = csys.read(probe_amp_addr,
                                probe_pha_addr,
                                vforw_amp_addr,
                                vforw_pha_addr,
                                vrefl_amp_addr,
                                vrefl_pha_addr)

        probe_decay = probe_amp[int(decay_prop*len(probe_amp)):
                                int((decay_prop+fraction_read)*len(probe_amp))]

        time_trace = np.linspace(0, fraction_read * trace_time, len(probe_decay))
        coeffs, = curve_fit(exp_fun, time_trace, probe_decay, [frequency/cav_target_ql, 10., 0.])
        cav_qls.append(np.pi*frequency/coeffs[0])
        cav_ql = cav_qls[-1]

        (probe_I, probe_Q) = APtoIQ(probe_amp, probe_pha)
        (vforw_I, vforw_Q) = APtoIQ(vforw_amp, vforw_pha)
        (vrefl_I, vrefl_Q) = APtoIQ(vrefl_amp, vrefl_pha)

        cav_ql_trace = [cav_ql]*len(probe_amp)

        virt_I = vforw_I + vrefl_I
        virt_Q = vforw_Q + vrefl_Q

        multiplier_trace = (probe_amp**2)/((virt_I)**2 + (virt_Q)**2)
        cav_qe_trace = cav_target_ql*multiplier_trace
        print('multiplier: ', np.mean(multiplier_trace[2000:]),
              'Qext: ', np.mean(cav_qe_trace[2000:]),
              'QL: ', cav_ql,
              'ratio: ', cav_ql/np.mean(cav_qe_trace[2000:]))

        probe_amps.append(probe_amp)
        probe_phas.append(probe_pha)
        vforw_amps.append(vforw_amp)
        vforw_phas.append(vforw_pha)
        vrefl_amps.append(vrefl_amp)
        vrefl_phas.append(vrefl_pha)
        cav_qls.append(cav_ql)
        virt_Is.append(virt_I)
        virt_Qs.append(virt_Q)
        multiplier_traces.append(multiplier_trace)
        cav_qe_traces .append(cav_qe_trace)
        ratio_traces.append(cav_ql/np.mean(cav_qe_trace))

        time.sleep(delay)

    dictfile['PROBE_AMPS'] = probe_amps
    dictfile['PROBE_PHAS'] = probe_phas
    dictfile['VFORW_AMPS'] = vforw_amps
    dictfile['VFORW_PHAS'] = vforw_phas
    dictfile['VREFL_AMPS'] = vrefl_amps
    dictfile['VREFL_PHAS'] = vrefl_phas
    dictfile['CAV_QLS'] = cav_qls
    dictfile['VIRT_IS'] = virt_Is
    dictfile['VIRT_QS'] = virt_Qs
    dictfile['MULTIPLIER_TRACES'] = multiplier_traces
    dictfile['RATIO_TRACES'] = ratio_traces
    dictfile['CAV_QE_TRACES'] = cav_qe_traces

    dfile.write(ofile, dictfile)

if __name__ == "__main__":
    main.run()
