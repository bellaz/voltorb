'''
    This script read amplitude and phase data of probe, forward and reflected
    to a file. Apply a varying sinusoidal signal to piezos
'''
import time
import numpy as np
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
import voltorb.dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    forward_amp_addr = params['forward_amp_addr']
    forward_pha_addr = params['forward_pha_addr']
    reflected_amp_addr = params['reflected_amp_addr']
    reflected_pha_addr = params['reflected_pha_addr']
    cavity_cal_amp_addr = params['cavity_cal_amp_addr']
    piezo_dc_addr = params['piezo_dc_addr']
    piezo_sensor_addr = params['piezo_sensor_addr']
    vforw_power_addr = params['vforw_power_addr']

    piezo_voltage = params['piezo_voltage']
    piezo_freq_start = params['piezo_freq_start']
    piezo_freq_stop = params['piezo_freq_stop']
    piezo_ff_table_addr = params['piezo_ff_table_addr']

    measurements = params.get('measurements', 100)
    delay = params.get('delay', 1)
    ofile = params['ofile']

    trace_time = params.get('trace_time', 1)

    probe_amp = []
    probe_pha = []
    forward_amp = []
    forward_pha = []
    reflected_amp = []
    reflected_pha = []
    cavity_cal_amp = []
    piezo_dc = []
    vforw_power = []
    piezo_sensor = []

    piezo_table_len = len(csys.read(piezo_ff_table_addr))

    piezo_freqs = np.linspace(piezo_freq_start, piezo_freq_stop, measurements)

    for piezo_freq in piezo_freqs:

        print('Frequency [Hz]:', piezo_freq)

        ff_table = piezo_voltage * np.sin(np.linspace(0,
                                                      2*np.pi*piezo_freq/trace_time,
                                                      piezo_table_len,
                                                      endpoint=False))

        csys.write(piezo_ff_table_addr, ff_table)
        time.sleep(delay)

        probe_amp.append(csys.read(probe_amp_addr))
        probe_pha.append(csys.read(probe_pha_addr))
        forward_amp.append(csys.read(forward_amp_addr))
        forward_pha.append(csys.read(forward_pha_addr))
        reflected_amp.append(csys.read(reflected_amp_addr))
        reflected_pha.append(csys.read(reflected_pha_addr))
        cavity_cal_amp.append(csys.read(cavity_cal_amp_addr))
        piezo_dc.append(csys.read(piezo_dc_addr))
        vforw_power.append(csys.read(vforw_power_addr))
        piezo_sensor.append(csys.read(piezo_sensor_addr))



    csys.write(piezo_ff_table_addr, [0.]*piezo_table_len)
    dictfile = {}
    dictfile['PROBE_AMP'] = probe_amp
    dictfile['PROBE_PHA'] = probe_pha
    dictfile['FORWARD_AMP'] = forward_amp
    dictfile['FORWARD_PHA'] = forward_pha
    dictfile['REFLECTED_AMP'] = reflected_amp
    dictfile['REFLECTED_PHA'] = reflected_pha
    dictfile['CAVITY_CAL_AMP'] = cavity_cal_amp
    dictfile['PIEZO_DC'] = piezo_dc
    dictfile['VFORW_POWER'] = vforw_power
    dictfile['PIEZO_SENSOR'] = piezo_sensor
    dictfile['PIEZO_FREQS'] = piezo_freqs

    dictfile['PIEZO_VOLTAGE'] = piezo_voltage
    dictfile['PIEZO_FREQ_START'] = piezo_freq_start
    dictfile['PIEZO_FREQ_STOP'] = piezo_freq_stop

    dfile.write(ofile, dictfile)

if __name__ == '__main__':
    main.run()
