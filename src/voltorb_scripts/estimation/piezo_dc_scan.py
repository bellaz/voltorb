'''
    This script read amplitude and phase data of probe, forward and reflected
    to a file. useful to study ponderomotive effects. Also piezo dc value and
    loop gain are logged.
    on CMTB
    First argument: cavity number
    Second argument: file name
'''
import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
import voltorb.dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    forward_amp_addr = params['forward_amp_addr']
    forward_pha_addr = params['forward_pha_addr']
    reflected_amp_addr = params['reflected_amp_addr']
    reflected_pha_addr = params['reflected_pha_addr']
    cavity_cal_amp_addr = params['cavity_cal_amp_addr']
    piezo_dc_addr = params['piezo_dc_addr']
    piezo_sensor_addr = params['piezo_sensor_addr']
    vforw_power_addr = params['vforw_power_addr']

    piezo_voltage_start = params.get('piezo_voltage_start' -20)
    piezo_voltage_stop = params.get('piezo_voltage_stop', 20)
    measurements = params.get('measurements', 1000)
    delay = params.get('delay', 1)
    ofile = params['ofile']

    probe_amp_fwd = []
    probe_pha_fwd = []
    forward_amp_fwd = []
    forward_pha_fwd = []
    reflected_amp_fwd = []
    reflected_pha_fwd = []
    cavity_cal_amp_fwd = []
    piezo_dc_fwd = []
    vforw_power_fwd = []
    piezo_sensor_fwd = []

    probe_amp_rev = []
    probe_pha_rev = []
    forward_amp_rev = []
    forward_pha_rev = []
    reflected_amp_rev = []
    reflected_pha_rev = []
    cavity_cal_amp_rev = []
    piezo_dc_rev = []
    vforw_power_rev = []
    piezo_sensor_rev = []

    initial_dc = csys.read(piezo_dc_addr)

    for dc_value in np.linspace(piezo_voltage_start, piezo_voltage_stop, measurements):
        csys.write(piezo_dc_addr, dc_value)
        print('Voltage:', dc_value)
        time.sleep(delay)

        probe_amp_fwd.append(csys.read(probe_amp_addr))
        probe_pha_fwd.append(csys.read(probe_pha_addr))
        forward_amp_fwd.append(csys.read(forward_amp_addr))
        forward_pha_fwd.append(csys.read(forward_pha_addr))
        reflected_amp_fwd.append(csys.read(reflected_amp_addr))
        reflected_pha_fwd.append(csys.read(reflected_pha_addr))
        cavity_cal_amp_fwd.append(csys.read(cavity_cal_amp_addr))
        piezo_dc_fwd.append(csys.read(piezo_dc_addr))
        vforw_power_fwd.append(csys.read(vforw_power_addr))
        piezo_sensor_fwd.append(csys.read(piezo_sensor_addr))

    for dc_value in np.linspace(piezo_voltage_stop, piezo_voltage_start, measurements):
        csys.write(piezo_dc_addr, dc_value)
        print('Voltage:', dc_value)
        time.sleep(delay)

        probe_amp_rev.append(csys.read(probe_amp_addr))
        probe_pha_rev.append(csys.read(probe_pha_addr))
        forward_amp_rev.append(csys.read(forward_amp_addr))
        forward_pha_rev.append(csys.read(forward_pha_addr))
        reflected_amp_rev.append(csys.read(reflected_amp_addr))
        reflected_pha_rev.append(csys.read(reflected_pha_addr))
        cavity_cal_amp_rev.append(csys.read(cavity_cal_amp_addr))
        piezo_dc_rev.append(csys.read(piezo_dc_addr))
        vforw_power_rev.append(csys.read(vforw_power_addr))
        piezo_sensor_rev.append(csys.read(piezo_sensor_addr))

    csys.write(piezo_dc_addr, initial_dc)

    dictfile = {}
    dictfile['PROBE_AMP_FWD'] = probe_amp_fwd
    dictfile['PROBE_PHA_FWD'] = probe_pha_fwd
    dictfile['FORWARD_AMP_FWD'] = forward_amp_fwd
    dictfile['FORWARD_PHA_FWD'] = forward_pha_fwd
    dictfile['REFLECTED_AMP_FWD'] = reflected_amp_fwd
    dictfile['REFLECTED_PHA_FWD'] = reflected_pha_fwd
    dictfile['CAVITY_CAL_AMP_FWD'] = cavity_cal_amp_fwd
    dictfile['PIEZO_DC_FWD'] = piezo_dc_fwd
    dictfile['VFORW_POWER_FWD'] = vforw_power_fwd
    dictfile['PIEZO_SENSOR_FWD'] = piezo_sensor_fwd

    dictfile['PROBE_AMP_REV'] = probe_amp_rev
    dictfile['PROBE_PHA_REV'] = probe_pha_rev
    dictfile['FORWARD_AMP_REV'] = forward_amp_rev
    dictfile['FORWARD_PHA_REV'] = forward_pha_rev
    dictfile['REFLECTED_AMP_REV'] = reflected_amp_rev
    dictfile['REFLECTED_PHA_REV'] = reflected_pha_rev
    dictfile['CAVITY_CAL_AMP_REV'] = cavity_cal_amp_rev
    dictfile['PIEZO_DC_REV'] = piezo_dc_rev
    dictfile['VFORW_POWER_REV'] = vforw_power_rev
    dictfile['PIEZO_SENSOR_REV'] = piezo_sensor_rev

    dictfile['PIEZO_VOLTAGE_START'] = piezo_voltage_start
    dictfile['PIEZO_VOLTAGE_STOP'] = piezo_voltage_stop

    dfile.write(ofile, dictfile)

if __name__ == '__main__':
    main.run()
