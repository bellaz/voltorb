#!/usr/bin/env python3
'''
    read ql temperature and power
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default, merge_selected
import voltorb.dictfile as dfile

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    dictfile = {}
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    cav_ql_addr = params['cav_ql_addr']
    dictfile['cav_target_ql'] = params.get('cav_ql_target', 3e7)
    cav_fpc_temp_addr = params['fpc_temp_addr']
    cav_forw_power_addr = params['vforw_power_addr']
    delay = params.get('delay', 30)
    measurements = params.get('measurements', 320)

    ofile = params.get('ofile', 'out.h5')

    dictfile['delay'] = delay
    dictfile['measurements'] = measurements
    
    if 'pulse_flattop_percent_addr' in params:
        dictfile['PULSE_FLATTOP'] = csys.read(params['pulse_flattop_percent_addr'])  
        dictfile['PULSE_FILLING'] = csys.read(params['pulse_filling_percent_addr'])
        dictfile['PULSE_DELAY'] = csys.read(params['pulse_delay_percent_addr'])

    times = []
    probe_amps = []
    probe_phas = []
    vforw_amps = []
    vforw_phas = []
    vrefl_amps = []
    vrefl_phas = []
    cav_qls = []
    cav_fpc_temp = []
    cav_forw_power = []
    
    for i in range(measurements):
        try:
            [probe_amp,
             probe_pha,
             vforw_amp,
             vforw_pha,
             vrefl_amp,
             vrefl_pha,
             cav_ql] = csys.read(probe_amp_addr,
                                 probe_pha_addr,
                                 vforw_amp_addr,
                                 vforw_pha_addr,
                                 vrefl_amp_addr,
                                 vrefl_pha_addr,
                                 cav_ql_addr)

            probe_amps.append(probe_amp)
            probe_phas.append(probe_pha)
            vforw_amps.append(vforw_amp)
            vforw_phas.append(vforw_pha)
            vrefl_amps.append(vrefl_amp)
            vrefl_phas.append(vrefl_pha)
            cav_qls.append(cav_ql)
            cav_fpc_temp.append(csys.read(cav_fpc_temp_addr))
            cav_forw_power.append(csys.read(cav_forw_power_addr))
            times.append(time.time())
        except Exception:
            pass
        time.sleep(delay)

    dictfile['PROBE_AMPS'] = probe_amps
    dictfile['PROBE_PHAS'] = probe_phas
    dictfile['VFORW_AMPS'] = vforw_amps
    dictfile['VFORW_PHAS'] = vforw_phas
    dictfile['VREFL_AMPS'] = vrefl_amps
    dictfile['VREFL_PHAS'] = vrefl_phas
    dictfile['CAV_QLS'] = cav_qls
    dictfile['TIME'] = times
    dictfile['FPC_TEMP'] = cav_fpc_temp
    dictfile['FORW_POWER'] = cav_forw_power 
    dfile.write(ofile, dictfile)

if __name__ == "__main__":
    main.run()
