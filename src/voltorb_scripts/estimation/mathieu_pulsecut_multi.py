#!/usr/bin/env python3
'''
    Read Mathieu datafiles multi version
'''

import pandas as pd
import numpy as np
from scipy.signal import savgol_filter
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default


@parser_default(__doc__)
def main(params):
    '''
        Scripts main entry point
    '''
    ifilebase = params['ifilebase']
    numbers = params['numbers']
    smooth_win = params.get('smooth_win', 101)
    smooth_win2 = params.get('smooth_win2', 601)
    smooth_order = params.get('smooth_order', 1)
    frequency = params.get('frequency', 1.3E9)
    sample_rate = params.get('sample_rate', 9E6)
    Qext = params.get('Qext', 4.6E6)
    w0 = 2*np.pi*frequency
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)

    plt.figure(1)
    plt.title('Cavity bandwidth')
    plt.xlabel('Time [s]')
    plt.ylabel('Cavity bandwidth [Hz]')

    plt.figure(2)
    plt.title('Cavity QL')
    plt.xlabel('Time [s]')
    plt.ylabel('QL')

    plt.figure(3)
    plt.title('Cavity detuning')
    plt.ylabel('Cavity detuning [Hz]')
    plt.xlabel('Time [s]')

    plt.figure(4)
    plt.title('Cavity amplitude')
    plt.ylabel('Cavity amplitude [Hz]')
    plt.xlabel('Time [s]')

    for number in  numbers:
        filename = ifilebase + str(number) + '.csv'
        data = pd.read_csv(filename, sep=', ')
        time_trace = np.linspace(0, len(data['PprobeDeg'])/sample_rate, len(data['PprobeDeg']))
        time_trace = time_trace[skip:-skip_back-1]
        rawprobe = (data['VprobeMV']*np.exp(1j*data['PprobeDeg']*np.pi/180))[skip:-skip_back-1]
        rawvforw = (data['VforwardMV']*np.exp(1j*data['VforwardDeg']*np.pi/180))[skip:-skip_back-1]
        rawvrefl = (data['VreflectedMV']*np.exp(1j*data['PreflectedDeg']*np.pi/180))[skip:-skip_back-1]

        probe = 0j+savgol_filter(np.real(rawprobe), smooth_win, smooth_order)
        vforw = 0j+savgol_filter(np.real(rawvforw), smooth_win, smooth_order)
        vrefl = 0j+savgol_filter(np.real(rawvrefl), smooth_win, smooth_order)
        dprobe = 0j+savgol_filter(np.real(rawprobe), smooth_win, smooth_order, deriv=1, delta=1/sample_rate)

        probe += 1j*savgol_filter(np.imag(rawprobe), smooth_win, smooth_order)
        vforw += 1j*savgol_filter(np.imag(rawvforw), smooth_win, smooth_order)
        vrefl += 1j*savgol_filter(np.imag(rawvrefl), smooth_win, smooth_order)
        dprobe += 1j*savgol_filter(np.imag(rawprobe), smooth_win, smooth_order, deriv=1, delta=1/sample_rate)

        cavity_params = probe*np.conj(w0/(Qext)*vforw-dprobe)/(probe*np.conj(probe))
        w12 = np.real(cavity_params)
        detuning = np.imag(cavity_params)*0.5/np.pi

        QL = w0*0.5/(w12)

        plt.figure(1)
        plt.plot(time_trace, savgol_filter(w12/np.pi, smooth_win2, smooth_order), label=filename)

        plt.figure(2)
        plt.plot(time_trace, savgol_filter(QL, smooth_win2, smooth_order), label=filename)

        plt.figure(3)
        plt.plot(time_trace, savgol_filter(detuning, smooth_win2, smooth_order), label=filename)

        plt.figure(4)
        plt.plot(time_trace, savgol_filter(np.abs(probe), smooth_win2, smooth_order), label=filename)

    plt.figure(1)
    plt.legend()

    plt.figure(2)
    plt.legend()

    plt.figure(3)
    plt.legend()

    plt.figure(4)
    plt.legend()

    input('Press Return..')

if __name__ == "__main__":
    main.run()
