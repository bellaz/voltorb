#!/usr/bin/env python3
'''
    show derived Qext
'''

import numpy as np
from scipy.optimize import minimize
from voltorb.parse_cmdline import parser_default
import voltorb.dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from matplotlib import rc

rc('font', **{'family':'sans-serif', 'sans-serif':['Helvetica']})
rc('text', usetex=True)

def mul_fun(x, *args):
    '''
        :params x: phase to optimize
        :params x: probe amplitude forward IQ reflected IQ
        :returns: variance
    '''
    probe_amp = args[0]
    virtual_I = args[1] + np.cos(x)*args[3] - np.sin(x)*args[4]
    virtual_Q = args[2] + np.sin(x)*args[3] + np.cos(x)*args[4]

    return (probe_amp**2)/((virtual_I)**2 + (virtual_Q)**2)

def optimize_fun(x, *args):
    '''
        :params x: phase to optimize
        :params x: probe amplitude forward IQ reflected IQ
        :returns: variance
    '''
    return np.var(mul_fun(x, *args))

def APtoIQ(trace_amp, trace_pha):
    '''
        transforms an amplitude and phase (degrees) trace in an IQ one
    '''
    trace_I = trace_amp * np.cos(np.pi/180*trace_pha)
    trace_Q = trace_amp * np.sin(np.pi/180*trace_pha)
    return (trace_I, trace_Q)


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)

    ifile = params['ifile']
    dictfile = dfile.read(ifile)

    cav_target_ql = params.get('cav_ql_target', 4.876E6)
    cav_qls = dictfile['CAV_QLS'][skip:-skip_back-1]

    cav_qe_traces = dictfile['CAV_QE_TRACES'][skip:-skip_back-1, :]
    cav_qes_corr = []
    corr_phase = []

    probe_amps = dictfile['PROBE_AMPS'][skip:-skip_back-1,:]
    vforw_amps = dictfile['VFORW_AMPS'][skip:-skip_back-1,:]
    vforw_phas = dictfile['VFORW_PHAS'][skip:-skip_back-1,:]
    vrefl_amps = dictfile['VREFL_AMPS'][skip:-skip_back-1,:]
    vrefl_phas = dictfile['VREFL_PHAS'][skip:-skip_back-1,:]

    for idx in range(len(cav_qls)):
        probe_amp = probe_amps[idx, :] 
        vforw_amp = vforw_amps[idx, :]
        vforw_pha = vforw_phas[idx, :]
        vrefl_amp = vrefl_amps[idx, :]
        vrefl_pha = vrefl_phas[idx, :]

        (vforw_I, vforw_Q) = APtoIQ(vforw_amp, vforw_pha)
        (vrefl_I, vrefl_Q) = APtoIQ(vrefl_amp, vrefl_pha)

        res_min = minimize(optimize_fun, 0, (probe_amp, 
                                             vforw_I, 
                                             vforw_Q, 
                                             vrefl_I, 
                                             vrefl_Q), bounds=[(-np.pi, np.pi)])

        cav_qes_corr.append(np.mean(cav_target_ql*mul_fun(res_min.x,
                                                          probe_amp, 
                                                          vforw_I, 
                                                          vforw_Q, 
                                                          vrefl_I, 
                                                          vrefl_Q)))
        corr_phase.append(res_min.x[0])


    cav_qes = np.mean(cav_qe_traces, axis=1)

    plt.figure()
    plt.xlabel('Decay QL')
    plt.ylabel('Virtual probe QExt')
    plt.plot(cav_qls, cav_qes, '.', label='No phase correction')
    plt.plot(cav_qls, cav_qes_corr, '.', label='Phase correction')
    plt.legend()
    plt.ticklabel_format(style='sci', axis='x', scilimits=(-2,2))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(-2,2))
    plt.draw()

    plt.figure()
    plt.xlabel('Decay QL')
    plt.ylabel('QExt/QL ratio')
    plt.plot(cav_qls, cav_qes/cav_qls, '.', label='No phase correction')
    plt.plot(cav_qls, cav_qes_corr/cav_qls, '.', label='Phase correction')
    plt.legend()
    plt.draw()
    
    fit_orig_1ord = np.polyfit(cav_qls, cav_qes, 1) 
    fit_orig_2ord = np.polyfit(cav_qls, cav_qes, 2)
    fit_corr_1ord = np.polyfit(cav_qls, cav_qes_corr, 1)
    fit_corr_2ord = np.polyfit(cav_qls, cav_qes_corr, 2)

    #fit_orig_1ord_trace = (fit_orig_1ord[0]*cav_qls + 
    #                       fit_orig_1ord[1])
    #fit_orig_2ord_trace = (fit_orig_2ord[0]*(cav_qls)**2 + 
    #                       fit_orig_2ord[1]*cav_qls + 
    #                       fit_orig_2ord[2])
    fit_corr_1ord_trace = (fit_corr_1ord[0]*cav_qls + 
                           fit_corr_1ord[1])
    #fit_corr_2ord_trace = (fit_corr_2ord[0]*(cav_qls)**2 +
    #                       fit_corr_2ord[1]*cav_qls +
    #                       fit_corr_2ord[2])

    plt.figure()
    plt.xlabel('Decay $Q_L$')
    plt.ylabel('Extrapolated $Q_{ext}$')
    plt.plot(cav_qls, cav_qes, '+', label='No phase correction')
    plt.plot(cav_qls, cav_qes_corr, '+', label='Phase correction')
    
    #plt.plot(cav_qls, fit_orig_1ord_trace, 'r', linewidth=3)
    plt.plot(cav_qls, fit_corr_1ord_trace, 'g', linewidth=3)
    #plt.plot(cav_qls, fit_orig_2ord_trace)
    #plt.plot(cav_qls, fit_corr_2ord_trace)
    plt.ticklabel_format(style='sci', axis='x', scilimits=(-2,2))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(-2,2))
    plt.legend(prop={'size': 16})
    plt.grid()
    plt.draw()

    plt.figure()
    plt.xlabel('Decay QL')
    plt.ylabel('Reflected phase correction [deg]')
    plt.plot(cav_qls, np.array(corr_phase)*180/np.pi, '+')
    plt.draw()

    print('Avg ratio original: ', np.mean(cav_qes/cav_qls), 
          'Error(STD) %', np.std(cav_qes/cav_qls)/np.mean(cav_qes/cav_qls)*100)
    print('Avg ratio corrected:', np.mean(cav_qes_corr/cav_qls), 
          'Error(STD) %', np.std(cav_qes_corr/cav_qls)/np.mean(cav_qes_corr/cav_qls)*100)

    input('Press Enter')

if __name__ == '__main__':
    main.run()
