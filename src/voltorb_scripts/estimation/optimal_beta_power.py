'''
    Estimate optimal beta/power
'''
import numpy as np
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    micro_limits = params.get('micro_limits', [0, 20]) # microphonics limits in Hz
    q0 = params.get('q0', 2.8E10)
    frequency = params.get('frequency', 1.3e9)
    roq = params.get('roq', 1036)
    length = params.get('length', 1.038)
    beam_current = params.get('beam_current', 0.025e-3)
    gradients = params.get('gradients', [7.9e6, 11e6, 13.5e6, 16e6])
    points = params.get('points', 1000)
    intrinsic_bw = frequency/q0
    detuning = np.linspace(micro_limits[0], micro_limits[1], points)

    fig, ax_ql = plt.subplots()
    ax_pow = ax_ql.twinx()

    fig_ql_pow, ax_ql_pow = plt.subplots()

    for gradient in gradients:
        b = beam_current*roq*q0/(gradient*length)
        beta_opt = np.sqrt((b+1)**2+(2*detuning/intrinsic_bw)**2)
        ql = q0/(1+beta_opt)
        power_cavity = (gradient*length)**2/(2*roq*q0)
        power_generator = power_cavity*0.5*(b+1+beta_opt)*1e-3
        ax_ql.plot(detuning, ql, label="%.2f" % (gradient*1e-6) + " [MV/m]")
        ax_pow.plot(detuning, power_generator, '--') 
        ax_ql_pow.plot(ql, power_generator, label="%.2f" % (gradient*1e-6) + " [MV/m]")

    ax_ql.legend(loc='upper center')
    ax_ql.set_xlabel('Microphonics amplitude [Hz]')
    ax_ql.set_ylabel('Loaded Q')
    ax_ql.set_ylim([0, 4e8])
    ax_ql.set_xlim(micro_limits)
    ax_pow.set_ylabel('Forward power [kW] (dashed)')

    # ax_ql_pow.legend(loc='upper center')
    ax_ql_pow.set_ylabel('Forward power [kW]')
    ax_ql_pow.set_xlabel('Loaded Q')
    ax_ql_pow.legend()
    # ax_ql.set_ylim([0, 4e8])
    # ax_ql.set_xlim(micro_limits)

    input('Press return..')


