'''
    Estimate detuning threshold for the monotonic and oscillatory instabilities
'''
import numpy as np
from numpy.fft import fft
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default
from voltorb import dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    files = []
    voltage_limit = (0, 22)
    detuning_limit = (0, 15)

    while(('ifile' + str(len(files) + 1)) in params):
        files.append(dfile.read(params['ifile' + str(len(files) + 1)]))

    titles = params.get("titles", [None for i in range(len(files))])

    plt.figure()
    plt.title("Detuning standard deviation")
    plt.xlabel("Time (s)")
    plt.ylabel("Detuning STD (Hz)")

    for (data, title) in zip(files, titles):
        sample_rate = data['SAMPLE_RATE']
        det_std_trace = data['DETUNING_STD_TRACE_LONG']
        time_trace = np.linspace(0.0, len(det_std_trace)/sample_rate, len(det_std_trace))
        plt.plot(time_trace, det_std_trace, label=title)
        plt.ylim(detuning_limit)
        plt.legend()

    plt.grid(True, which="both")

    plt.figure()
    plt.title("Detuning distribution")
    plt.ylabel("Detuning STD (Hz)")
    plt.xlabel("Frequency (Hz)")

    for (data, title) in zip(files, titles):
        det_trace = data['DETUNING_TRACE']
        sample_rate = data['SAMPLE_RATE']
        frequency_trace = np.linspace(0.0, sample_rate/2, len(det_trace)/2)
        det_fft = fft(det_trace - np.mean(det_trace))[:int(len(det_trace)/2)]*np.sqrt(2)/len(det_trace)
        det_amp2_fft = np.abs(det_fft)**2
        det_amp2_fft_acc = np.zeros(len(det_amp2_fft))
        for i in range(len(det_amp2_fft)):
            det_amp2_fft_acc[i] = np.sqrt(np.sum(det_amp2_fft[:(i+1)]))

        det_amp2_fft_acc[0] = det_amp2_fft_acc[1]
        plt.semilogy(frequency_trace, det_amp2_fft_acc, label=title)
        plt.legend()

    plt.grid(True, which="both")

    if ('AMPLITUDE_TRACE_LONG' in data):
        plt.figure()
        plt.title("Cavity voltage")
        plt.ylabel("Amplitude (MV)")
        plt.xlabel("Time (s)")

        for (data, title) in zip(files, titles):
            sample_rate = data['SAMPLE_RATE']
            amplitude_trace = data['AMPLITUDE_TRACE_LONG']
            time_trace = np.linspace(0.0, len(amplitude_trace)/sample_rate, len(amplitude_trace))
            plt.plot(time_trace, amplitude_trace, label=title)
            plt.ylim(voltage_limit)
            plt.legend()
    
    plt.grid(True, which="both")

    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    ax1.set_title("Secondary path transfer function (S)")
    ax1.set_ylabel("Amplitude (a.u.)")
    ax2.set_ylabel("Phase (deg)")
    ax2.set_xlabel("Frequency (Hz)")
    ax2.set_ylim(-180, 180)
    ax2.set_yticks([-180, -135, -90, -45, 0, 45, 90, 135, 180])

    for (data, title) in zip(files, titles):
        if 'S_FILTER' in data:
            s_fft = None
            s_fft = (fft(data['S_FILTER']))[:int(len(data['S_FILTER'])/2)]

            frequency_trace = np.linspace(0.0, 
                                          sample_rate/2, 
                                          len(s_fft))
            
            amplitudes = np.abs(s_fft)
            angles = np.angle(s_fft, True)
            ax1.plot(frequency_trace, amplitudes, label=title)
            ax2.plot(frequency_trace, angles)
    
    ax1.legend()

    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    ax1.set_title("Feedback transfer function (W)")
    ax1.set_ylabel("Amplitude (a.u.)")
    ax2.set_ylabel("Phase (deg)")
    ax2.set_xlabel("Frequency (Hz)")
    ax2.set_ylim(-180, 180)
    ax2.set_yticks([-180, -135, -90, -45, 0, 45, 90, 135, 180])

    for (data, title) in zip(files, titles):
        if 'W_FILTER' in data:
            w_ftt = None
            w_fft = (fft(data['W_FILTER']))[:int(len(data['W_FILTER'])/2)]

            frequency_trace = np.linspace(0.0, 
                                          sample_rate/2, 
                                          len(w_fft))
            
            amplitudes = np.abs(w_fft)
            angles = np.angle(w_fft, True)
            ax1.plot(frequency_trace, amplitudes, label=title)
            ax2.plot(frequency_trace, angles)

    ax1.legend()

    plt.figure()
    plt.title('Second path filter (S) weight amplitude')
    plt.xlabel('Weight')
    plt.ylabel('Amplitude (a.u.)')

    for (data, title) in zip(files, titles):
        if 'S_FILTER' in data:
            plt.plot(data['S_FILTER'], label=title)

    plt.legend()

    plt.figure()
    plt.title('Feedback filter (W) weight amplitude')
    plt.xlabel('Weight')
    plt.ylabel('Amplitude (a.u.)')

    for (data, title) in zip(files, titles):
        if 'W_FILTER' in data:
            plt.plot(data['W_FILTER'], label=title)

    plt.legend()

    input("press Return...")
