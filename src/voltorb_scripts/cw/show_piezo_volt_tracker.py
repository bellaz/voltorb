#!/usr/bin/env python3
'''
    Show the piezo voltage
'''

import time
import numpy as np
from datetime import datetime
from scipy.signal import savgol_filter
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default
import matplotlib.dates as md

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    
    dictfile = dfile.read(params['ifile'])
    times = [datetime.fromtimestamp(tm) for tm in dictfile['TIME'][skip:-skip_back-1]]
    #times = times - times[0]
    volts = dictfile['VOLT'][skip:-skip_back-1]
    volt_stds = dictfile['VOLT_STD'][skip:-skip_back-1]
    piezo_sens = params.get('piezo_sens', 7.0)
    
    xfmt = md.DateFormatter('%H:%M:%S')

    plt.figure()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(times, volts)
    plt.xlabel('Time')
    plt.ylabel('Piezo volts [V]')
     
    plt.figure()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(times, volts*piezo_sens)
    plt.xlabel('Time')
    plt.ylabel('Static detuning [Hz]')
    ax.xaxis.set_major_formatter(xfmt)
    
    plt.figure()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(times, (volts-np.mean(volts))*piezo_sens)
    plt.xlabel('Time')
    plt.ylabel('Static detuning variation [Hz]')


    times = [datetime.fromtimestamp(tm - dictfile['TIME'][skip:-skip_back-1][0]) for tm in dictfile['TIME'][skip:-skip_back-1]]
    xfmt = md.DateFormatter('%M:%S')
    plt.figure()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(times, volts)
    plt.xlabel('Time')
    plt.ylabel('Piezo volts [V]')
     
    plt.figure()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(times, volts*piezo_sens)
    plt.xlabel('Time')
    plt.ylabel('Static detuning [Hz]')
    ax.xaxis.set_major_formatter(xfmt)
    
    plt.figure()
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    plt.plot(times, (volts-np.mean(volts))*piezo_sens)
    plt.xlabel('Time')
    plt.ylabel('Static detuning variation [Hz]')

    input('Press Return...')
