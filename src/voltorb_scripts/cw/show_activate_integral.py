#!/usr/bin/env python3
'''
    Check the output of the piezo fb
'''

import numpy as np
from time import time, sleep
from datetime import datetime
from scipy.signal import savgol_filter
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

plt.rcParams['text.latex.preamble'] = [
       r'\usepackage{siunitx}',   # i need upright \micro symbols, but you need...
       r'\sisetup{detect-all}',   # ...this to force siunitx to actually use your fonts
       r'\usepackage{helvet}',    # set the normal font here
       r'\usepackage{sansmath}',  # load up the sansmath so that math -> helvet
       r'\sansmath'               # <- tricky! -- gotta actually tell tex to use!
]  

symbols = ['v', 'o', 's', 'd', '+', 'o', 's', 'd']

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    timelen = params.get('time', 2)
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True)
    time_limits = params.get('time_limits', None)
    hbw = params.get('hbw', 21.7)
    
    if time_limits is not None:
        ax1.set_xlim(time_limits)

    global_shift = params.get('global_shift', 0)
    phase_shift = params.get('phase_shift', 0)
    phase_fraction = params.get('phase_fraction', 0.05)
    
    idx = 1
    
    while 'ifile{}'.format(idx) in params:
        filename = params['ifile{}'.format(idx)]
        
        dictfile = dfile.read(filename)
        amp = dictfile['AMP']
        time_trace = np.linspace(0, timelen, len(amp)) + global_shift
        
        if 'shift{}'.format(idx) in params:
            time_trace += params['shift{}'.format(idx)]
            print(filename, "shift", params['shift{}'.format(idx)])
        else:
            print(filename)

        pha = dictfile['PHA'] + phase_shift
        pha_init = np.pi/180*np.mean(pha[0:int(len(pha)*phase_fraction)])
        detuning_init = hbw*np.tan(-pha_init)
        ax1.plot(time_trace, amp, label=r'$\Delta f(t=0) = {:.0f}$ [Hz]'.format(detuning_init),
                marker=symbols[idx-1], markevery=20000)
        ax2.plot(time_trace, pha,
                marker=symbols[idx-1], markevery=20000)
        idx = idx + 1
   
    ax1.legend()
    ax1.set_ylabel('Amplitude [MV/m]')
    ax2.set_ylabel('Phase [deg]')
    ax2.set_xlabel('Time [s]')
    ax2.set_yticks([-80, -60, -40, -20, 0, 20, 40, 60])

    input('Press Return ...')
