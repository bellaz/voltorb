#!/usr/bin/env python3
'''
    Check the output of the piezo
'''

import numpy as np
from time import time, sleep
from datetime import datetime
from scipy.signal import savgol_filter
from voltorb import dictfile as dfile
from voltorb import controlsys as csys
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    delay = params.get('delay', 1)
    ofile = params['ofile']
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    show = params.get('show', None)
   
    skip = params.get('skip', 0)

    csys.read(params['piezo_fb_enable_addr'])
    sleep(0.5)
    csys.write(params['piezo_fb_enable_addr'], 1)
    sleep(delay)
    amp = csys.read(probe_amp_addr)
    pha = csys.read(probe_pha_addr)
    sleep(1)
    amp2 = csys.read(probe_amp_addr)
    pha2 = csys.read(probe_pha_addr) 
    sleep(1)
    amp3 = csys.read(probe_amp_addr)
    pha3 = csys.read(probe_pha_addr) 
    sleep(1)
    amp3 = list(amp)[skip:] + list(amp2)[skip:] + list(amp3)[skip:] 
    pha3 = list(pha)[skip:] + list(pha2)[skip:] + list(pha3)[skip:]    

    csys.write(params['piezo_fb_enable_addr'], 0)
    dictfile = {}
    dictfile['AMP'] = amp3
    dictfile['PHA'] = pha3
    dfile.write(ofile, dictfile)

    if show is not None:
        time_trace = np.linspace(0, 3, len(amp3))
        fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True)
        
        ax1.plot(time_trace, amp3)
        ax2.plot(time_trace, pha3)
        ax1.set_ylabel('Amplitude [MV/m]')
        ax2.set_ylabel('Phase [deg]')
        ax2.set_xlabel('Time [s]')

        input('Press Return ...')
