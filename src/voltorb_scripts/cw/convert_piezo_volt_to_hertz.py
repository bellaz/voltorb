#!/usr/bin/env python3
'''
    Convert the piezo voltage to hertz
'''

import numpy as np
from datetime import datetime
from scipy.signal import savgol_filter
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    
    dictfile = dfile.read(params['ifile'])
    time = dictfile['TIME'][skip:-skip_back-1]
    dictfile['TIME'] = time
    dictfile['TIME_STR'] = [str(datetime.fromtimestamp(tm)) for tm in time]
    volts = dictfile['VOLT'][skip:-skip_back-1]
    volt_stds = dictfile['VOLT_STD'][skip:-skip_back-1]
    dictfile['VOLT'] = volts
    dictfile['VOLT_STD'] = volt_stds
    
    piezo_sens = params.get('piezo_sens', 7.0)
    
    dictfile['PIEZO_SENS_HZ_V'] = piezo_sens
    dictfile['DETUNING_RAW'] = piezo_sens * volts
    dictfile['DETUNING_DETRENDED'] = piezo_sens * (volts - np.mean(volts))

    dfile.write(params['ofile'], dictfile)

