#!/usr/bin/env python3
'''
    quadratically ramp a station
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    sp_amp_addr = params['sp_amp_addr']
    start_amp = csys.read(sp_amp_addr)
    start_amp = params.get('start_amp', start_amp)
    stop_amp = params.get('stop_amp', 0)
    ramp_time = params.get('time', 3600)
    delay = params.get('delay', 1)
    steps = ramp_time/delay

    for amp in np.sqrt(np.linspace(start_amp**2, stop_amp**2, steps)):
        print('Amplitude:', amp)
        csys.write(sp_amp_addr, amp)
        time.sleep(delay)

if __name__ == "__main__":
    main.run()
