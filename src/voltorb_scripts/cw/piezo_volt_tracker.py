#!/usr/bin/env python3
'''
    Check the output of the piezo
'''

import numpy as np
from time import time, sleep
from datetime import datetime
from scipy.signal import savgol_filter
from voltorb import dictfile as dfile
from voltorb import controlsys as csys
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    skip = params.get('skip', 0)
    skip_back = params.get('skip_back', 0)
    time_s = params['time']
    delay = params.get('delay', 1)
    ofile = params['ofile']
    piezo_voltage_addr = params['piezo_voltage_addr']
    times = []
    volts = []
    volt_stds = []

    while True:
        sleep(delay)
        piezo_volt = csys.read(piezo_voltage_addr)[skip:-skip_back-1]
        volts.append(np.mean(piezo_volt))
        volt_stds.append(np.std(piezo_volt))
        times.append(time())

        if (times[-1] - times[0]) > time_s:
            break
            
        dictfile = {}
        dictfile['TIME'] = times
        dictfile['VOLT'] = volts
        dictfile['VOLT_STD'] = volt_stds
        dfile.write(ofile, dictfile)
