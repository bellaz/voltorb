#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!/usr/bin/
'''
    Simulate CW trace with noise and modulation on QL detuning and forward power
'''

import numpy as np
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default
from matplotlib.ticker import MaxNLocator
import voltorb.dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    prefix = params['prefix']
    ofile = params['ofile']
    extension = params.get('extension', '.mat')
    lower_idx = int(params['lower_idx'])
    upper_idx = int(params['upper_idx'])

    has_quenched = []
    cavity_pid = []
    quench_bin = []
    

    for idx in range(lower_idx, upper_idx+1):

        ifile = prefix + str(idx) + extension 
        dictfile = dfile.read(ifile)
        
        quench_trace_simulated = dictfile['QUENCH_TRACE_SIMULATED']
        pid = dictfile['CAVITY_PID']
        cavity_pid.append(pid)
        quidx = np.nonzero(quench_trace_simulated)[0]

        if len(quidx) != 0:
            has_quenched.append(True)
            quench_bin.append(np.min(quidx))
        else:
            has_quenched.append(False)
            quench_bin.append(len(quench_trace_simulated))
        
        print(idx, quench_bin[-1])

    dictfile = {}
    dictfile['PID'] = np.array(cavity_pid)
    dictfile['QUENCHED'] = np.array(has_quenched)
    dictfile['QUENCH_BIN'] = np.array(quench_bin)
    dfile.write(ofile, dictfile)


    plt.figure()
    plt.plot(cavity_pid, has_quenched)

    plt.figure()
    plt.plot(cavity_pid, quench_bin)

    input("Press enter.")
