#!/usr/bin/env python3
'''
    Simulate CIC and compensator
'''

from scipy.signal import firwin2, freqz
import numpy as np
from voltorb.parse_cmdline import parser_default
from voltorb.matplotlib import pyplot as plt
import voltorb.dictfile as dfile
from matplotlib.pyplot import figure

np.seterr(divide='ignore', invalid='ignore')

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    R = params.get('R', 25)
    M = params.get('M', 1)
    N = params.get('N', 2)
    cutOff = params.get('cutOff', 0.2)
    calcRes = params.get('calcRes', 1024)
    numTaps = params.get('numTaps', 32)
    wideband = params.get('wideband', False)
    quantization = params.get('quantization', None)
    show_graph = params.get('show_graph', True)
    ofile = params.get('ofile', None)

    taps = getFIRCompensationFilter(R,M,N,cutOff,numTaps, calcRes)
    taps2 = getFIRCompensationFilterDeriv(R,M,N,cutOff,numTaps, calcRes)
    print('R', R, 'M', M, 'N', N, 'cutOff', cutOff, 'calcRes', calcRes, 'numTaps', numTaps, 'wideband', wideband)
    
    dictfile = {}
    quantized_taps = None
    quantized_taps2 = None

    if quantization is not None:
        quantized_taps = np.round(taps*(2**quantization))/(2**quantization)
        quantized_taps2 = np.round(taps2*(2**quantization))/(2**quantization)
        dictfile['quantization'] = quantization
        dictfile['quantized_taps'] = quantized_taps
    
    dictfile['R'] = R
    dictfile['M'] = M
    dictfile['N'] = N
    dictfile['cutOff'] = cutOff
    dictfile['calcRes'] = calcRes
    dictfile['numTaps'] = numTaps
    dictfile['wideband'] = wideband
    dictfile['taps'] = taps
 



    print('abs max tap:', np.max(np.abs(taps)),
          'abs min tap:', np.min(np.abs(taps)),
          'ratio abs max-min', np.max(np.abs(taps))/np.min(np.abs(taps)))

    if wideband: # Interpolate FIR filter to higher sample rate
        interp = np.zeros(len(taps)*R)
        interp[::R] = taps
        freqs,compResponse = freqz(interp)
        w = np.arange(len(freqs)) * np.pi/len(freqs) * R
    else:
        freqs,compResponse = freqz(taps)
        w = np.arange(len(freqs)) * np.pi/len(freqs)        

    Hcic = lambda w : (1/((M*R)**N))*np.abs( (np.sin((w*M)/2.)) / (np.sin(w/(2.*R))) )**N
    dictfile['compResponse'] = compResponse
    dictfile['freqs'] = freqs/(2*np.pi)
    cicMagResponse = np.array(list(map(Hcic, w)))
    dictfile['cicMagResponse'] = cicMagResponse
    combinedResponse = cicMagResponse * compResponse 
    dictfile['combinedResponse'] = combinedResponse

    if ofile is not None:
        dfile.write(ofile, dictfile)

    if show_graph:
        plotFIRCompFilter(R, M, N, taps, wideband, quantized_taps)
        plotFIRCompFilter(R, M, N, taps2, wideband, quantized_taps2)
        plotFIRCompFilterLogLog(R, M, N, taps2, wideband, quantized_taps2)
        plotFIRCompFilterLinear(R, M, N, taps2, wideband, quantized_taps2)
        input('Press return...')
 
# cutOff is the cut off freq as a fraction of the lower sample rate
# i.e 0.5 = Nyquist frequency
def getFIRCompensationFilter(R,M,N,cutOff,numTaps,calcRes=1024):    
    w = np.arange(calcRes) * np.pi/(calcRes - 1)    
    print('w', w)
    Hcomp = lambda w : ((M*R)**N)*(np.abs((np.sin(w/(2.*R))) / 
                        (np.sin((w*M)/2.)) ) **N)
    cicCompResponse = np.array(list(map(Hcomp, w)))
    # Set DC response to 1 as it is calculated as 'nan' by Hcomp
    cicCompResponse[0] = 1
    # Set stopband response to 0
    cicCompResponse[int(calcRes*cutOff*2):] = 0
     
    normFreq = np.arange(calcRes) / (calcRes - 1)
    taps = firwin2(numTaps, normFreq, cicCompResponse)
    return taps

def getFIRCompensationFilterDeriv(R,M,N,cutOff,numTaps,calcRes=1024):    
    w = np.arange(calcRes) * np.pi/(calcRes - 1)    
    print('w', w)
    Hcomp = lambda w : ((M*R)**N)*(np.abs((np.sin(w/(2.*R))) / 
                        (np.sin((w*M)/2.)) ) **N)
    cicCompResponse = np.array(list(map(Hcomp, w)))
    # Set DC response to 1 as it is calculated as 'nan' by Hcomp
    cicCompResponse[0] = 1
    # Set stopband response to 0
    cicCompResponse[int(calcRes*cutOff*2):] = 0
     
    normFreq = np.arange(calcRes) / (calcRes - 1)
    cicCompResponse *= normFreq
    taps = firwin2(numTaps, normFreq, cicCompResponse, antisymmetric=True)
    return taps


def plotFIRCompFilterLinear(R,M,N,taps,wideband, quantized_taps):
    figure()
    if wideband: # Interpolate FIR filter to higher sample rate
        interp = np.zeros(len(taps)*R)
        interp[::R] = taps
        freqs,compResponse = freqz(interp)
        w = np.arange(len(freqs)) * np.pi/len(freqs) * R
    else:
        freqs,compResponse = freqz(taps)
        w = np.arange(len(freqs)) * np.pi/len(freqs)        
    Hcic = lambda w : (1/((M*R)**N))*np.abs( (np.sin((w*M)/2.)) / (np.sin(w/(2.*R))) )**N
    cicMagResponse = np.array(list(map(Hcic, w)))
    combinedResponse = cicMagResponse * compResponse    
    plt.xlabel('Normalized frequency')
    plt.ylabel('Gain linear (a.u.)')
    plt.plot(freqs/(2*np.pi),abs(cicMagResponse), label="CIC Filter")
    plt.plot(freqs/(2*np.pi),abs(compResponse), label="Compensation Filter")
    plt.plot(freqs/(2*np.pi),abs(combinedResponse), label="Combined Response")
    if quantized_taps is not None:
        if wideband: # Interpolate FIR filter to higher sample rate
            interp = np.zeros(len(quantized_taps)*R)
            interp[::R] = quantized_taps
            freqs,compResponse = freqz(interp)
            w = np.arange(len(freqs)) * np.pi/len(freqs) * R
        else:
            freqs,compResponse = freqz(quantized_taps)
            w = np.arange(len(freqs)) * np.pi/len(freqs)   
        
        combinedResponse = cicMagResponse * compResponse    
        plt.plot(freqs/(2*np.pi), abs(compResponse), label="Compensation Filter Quantized")
        plt.plot(freqs/(2*np.pi), abs(combinedResponse), label="Combined Response Quantized")

    plt.grid()
    plt.legend()
    axes = plt.gca()

def plotFIRCompFilter(R,M,N,taps,wideband, quantized_taps):

    figure()
    if wideband: # Interpolate FIR filter to higher sample rate
        interp = np.zeros(len(taps)*R)
        interp[::R] = taps
        freqs,compResponse = freqz(interp)
        w = np.arange(len(freqs)) * np.pi/len(freqs) * R
    else:
        freqs,compResponse = freqz(taps)
        w = np.arange(len(freqs)) * np.pi/len(freqs)        
    Hcic = lambda w : (1/((M*R)**N))*np.abs( (np.sin((w*M)/2.)) / (np.sin(w/(2.*R))) )**N
    cicMagResponse = np.array(list(map(Hcic, w)))
    combinedResponse = cicMagResponse * compResponse    
    plt.xlabel('Normalized frequency')
    plt.ylabel('Gain (dB)')
    plt.plot(freqs/(2*np.pi),20*np.log10(abs(cicMagResponse)), label="CIC Filter")
    plt.plot(freqs/(2*np.pi),20*np.log10(abs(compResponse)), label="Compensation Filter")
    plt.plot(freqs/(2*np.pi),20*np.log10(abs(combinedResponse)), label="Combined Response")
    if quantized_taps is not None:
        if wideband: # Interpolate FIR filter to higher sample rate
            interp = np.zeros(len(quantized_taps)*R)
            interp[::R] = quantized_taps
            freqs,compResponse = freqz(interp)
            w = np.arange(len(freqs)) * np.pi/len(freqs) * R
        else:
            freqs,compResponse = freqz(quantized_taps)
            w = np.arange(len(freqs)) * np.pi/len(freqs)   
        
        combinedResponse = cicMagResponse * compResponse    
        plt.plot(freqs/(2*np.pi),20*np.log10(abs(compResponse)), label="Compensation Filter Quantized")
        plt.plot(freqs/(2*np.pi),20*np.log10(abs(combinedResponse)), label="Combined Response Quantized")

    plt.grid()
    plt.legend()
    axes = plt.gca()
    axes.set_ylim([-200,25])

def plotFIRCompFilterLogLog(R,M,N,taps,wideband, quantized_taps):

    figure()
    if wideband: # Interpolate FIR filter to higher sample rate
        interp = np.zeros(len(taps)*R)
        interp[::R] = taps
        freqs,compResponse = freqz(interp)
        w = np.arange(len(freqs)) * np.pi/len(freqs) * R
    else:
        freqs,compResponse = freqz(taps)
        w = np.arange(len(freqs)) * np.pi/len(freqs)        
    Hcic = lambda w : (1/((M*R)**N))*np.abs( (np.sin((w*M)/2.)) / (np.sin(w/(2.*R))) )**N
    cicMagResponse = np.array(list(map(Hcic, w)))
    combinedResponse = cicMagResponse * compResponse    
    plt.xlabel('Normalized frequency')
    plt.ylabel('Gain (dB)')
    plt.semilogx(freqs/(2*np.pi),20*np.log10(abs(cicMagResponse)), label="CIC Filter")
    plt.semilogx(freqs/(2*np.pi),20*np.log10(abs(compResponse)), label="Compensation Filter")
    plt.semilogx(freqs/(2*np.pi),20*np.log10(abs(combinedResponse)), label="Combined Response")
    if quantized_taps is not None:
        if wideband: # Interpolate FIR filter to higher sample rate
            interp = np.zeros(len(quantized_taps)*R)
            interp[::R] = quantized_taps
            freqs,compResponse = freqz(interp)
            w = np.arange(len(freqs)) * np.pi/len(freqs) * R
        else:
            freqs,compResponse = freqz(quantized_taps)
            w = np.arange(len(freqs)) * np.pi/len(freqs)   
        
        combinedResponse = cicMagResponse * compResponse    
        plt.semilogx(freqs/(2*np.pi),20*np.log10(abs(compResponse)), label="Compensation Filter Quantized")
        plt.semilogx(freqs/(2*np.pi),20*np.log10(abs(combinedResponse)), label="Combined Response Quantized")

    plt.grid()
    plt.legend()
    axes = plt.gca()
    axes.set_ylim([-200,25])
