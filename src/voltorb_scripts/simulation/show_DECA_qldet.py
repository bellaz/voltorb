#!/usr/bin/
'''
    Simulate CW trace with noise and modulation on QL detuning and forward power
'''

import numpy as np
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default
from matplotlib.ticker import MaxNLocator
from scipy import signal
import voltorb.dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    plt.ioff()
    ifile = params['ifile']
    skip = params.get('skip', 0.0)

    dictfile = dfile.read(ifile)

    time_trace = dictfile['TIME_TRACE']
    plot_title = dictfile.get('CAVITY_NAME', ifile)
    decimated_time_trace = dictfile['DECIMATED_TIME_TRACE']
    detuning_fetch = params.get('detuning_fetch', 1.0)
    detuning_trace = dictfile['DETUNING_TRACE']
    detuning_trace_simulated = dictfile['DETUNING_TRACE_SIMULATED']*detuning_fetch
    bw_calibration = dictfile['BW_CALIBRATION']
    bw_trace = dictfile['BW_TRACE']
    bw_trace_simulated = dictfile['BW_TRACE_SIMULATED']
    quench_trace_simulated = dictfile['QUENCH_TRACE_SIMULATED']
    quench_threshold_bw = dictfile['QUENCH_THRESHOLD'] * bw_calibration
    probe_amp = dictfile['PROBE_AMP']
    forward_amp = dictfile['FORWARD_AMP']
    hertz_xaxis = params.get('hertz_xaxis', bw_calibration*2.5)
    reduce_point = params.get('reduce_point', False)
    use_errorbar = params.get('use_errorbar', False)


    yerr_simulated_detuning  = None
    yerr_simulated_bandwidth = None

    if reduce_point:
        cut = -(len(decimated_time_trace) % reduce_point)
        print(cut)
        print(len(decimated_time_trace))
        newshape = (len(decimated_time_trace) // reduce_point, reduce_point)
        decimated_time_trace = np.reshape(decimated_time_trace[cut:], 
                                newshape)

        detuning_trace_simulated = np.reshape(detuning_trace_simulated[cut:], 
                                              newshape)
        bw_trace_simulated = np.reshape(bw_trace_simulated[cut:], 
                                        newshape)               
        
        if use_errorbar:
            yerr_simulated_detuning  = np.ptp(detuning_trace_simulated, axis=1)/2
            yerr_simulated_bandwidth = np.ptp(bw_trace_simulated, axis=1)/2

        decimated_time_trace = np.mean(decimated_time_trace, axis=1)
        detuning_trace_simulated = np.mean(detuning_trace_simulated, axis=1)
        bw_trace_simulated = np.mean(bw_trace_simulated, axis=1)
        
        quench_trace_simulated = np.reshape(quench_trace_simulated[cut:], newshape)
        quench_trace_simulated = np.mean(quench_trace_simulated, axis=1)
        print(len(decimated_time_trace))


    fig, ((detuning_ax, bandwidth_ax), 
          (amplitude_ax, quench_ax)) = plt.subplots(2, 2, sharex=True, figsize=(19.2, 12), dpi=100)


    #fig.suptitle(plot_title)
    bandwidth_quench_ax = quench_ax.twinx()
    bandwidth_quench_ax.set_ylim(-hertz_xaxis, hertz_xaxis)
    detuning_ax.set_ylim(-hertz_xaxis, hertz_xaxis)
    bandwidth_ax.set_ylim(-hertz_xaxis, hertz_xaxis)

    detuning_ax.set_xlabel('Time (s)')
    bandwidth_ax.set_xlabel('Time (s)')
    amplitude_ax.set_xlabel('Time (s)')
    quench_ax.set_xlabel('Time (s)')

    detuning_ax.set_ylabel("Detuning (hertz)")
    bandwidth_ax.set_ylabel("Bandwidth (hertz)")
    amplitude_ax.set_ylabel("Amplitude (a.u.)")
    quench_ax.set_ylabel("Quench detected")
    bandwidth_quench_ax.set_ylabel("Bandwidth (hertz)")
    
    if np.count_nonzero(detuning_trace):
        detuning_ax.plot(time_trace, detuning_trace,
                         color="b", label="Reference")
 
    detuning_ax.errorbar(decimated_time_trace, detuning_trace_simulated, 
                     yerr=yerr_simulated_detuning,
                     color="r", marker=".", ls="None", label="Computed")
    
    amplitude_ax.plot(time_trace, probe_amp, label="Probe")
    amplitude_ax.plot(time_trace, forward_amp, label="Forward")

    if np.count_nonzero(bw_trace):
        bandwidth_ax.plot(time_trace, bw_trace,
                          color="b", label="Reference")

    bandwidth_ax.plot(time_trace, np.ones(len(time_trace))*bw_calibration,
                      color="g", ls="--", label="Target")

    bandwidth_ax.errorbar(decimated_time_trace, bw_trace_simulated,
                      yerr=yerr_simulated_bandwidth,
                      color="r", marker=".", ls="None", label="Computed")

    lns1 = bandwidth_quench_ax.plot(time_trace, 
                                    quench_threshold_bw*np.ones(len(time_trace)),
                                    color="b", ls="--", label="Quench threshold")

    lns2 = bandwidth_quench_ax.errorbar(decimated_time_trace, bw_trace_simulated,
                                    yerr=yerr_simulated_bandwidth,
                                    color="r", marker=".", ls="None", label="Computed")

    quench_trace_simulated = [["false", "true"][bool(q)] 
                              for q 
                              in quench_trace_simulated]

    lns3 = quench_ax.plot(decimated_time_trace, quench_trace_simulated,
                          color="g", label="Quench detected")

    lns = lns1+[lns2[0]]+lns3
    labs = [l.get_label() for l in lns]
    quench_ax.set_yticks(["false", "true"])


    locator9 = MaxNLocator(nbins=10)
    locator92 = MaxNLocator(nbins=10)
    detuning_ax.yaxis.set_major_locator(locator9)
    bandwidth_ax.yaxis.set_major_locator(locator9)
    amplitude_ax.yaxis.set_major_locator(locator92)
    bandwidth_quench_ax.yaxis.set_major_locator(locator9)
    
    detuning_ax.minorticks_on()
    bandwidth_ax.minorticks_on()
    amplitude_ax.minorticks_on()
    bandwidth_quench_ax.minorticks_on()

    bandwidth_quench_ax.tick_params(axis='both', which='both', direction='in')
    detuning_ax.tick_params(axis='both', which='both', direction='in')
    bandwidth_ax.tick_params(axis='both', which='both', direction='in')
    amplitude_ax.tick_params(axis='both', which='both', direction='in')
    bandwidth_quench_ax.tick_params(axis='both', which='both', direction='in')

    detuning_ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
    bandwidth_ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
    amplitude_ax.ticklabel_format(style='sci', scilimits=(0, 0))
    quench_ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))

    detuning_ax.legend()
    bandwidth_ax.legend()
    amplitude_ax.legend()
    quench_ax.legend(lns, labs)

    fig.savefig(str(plot_title) + ".pdf", bbox_inches='tight')

    plt.figure()
    plt.plot(np.fft.fft(detuning_trace - np.mean(detuning_trace))/len(detuning_trace))
    plt.show()


    detuning_trace = np.interp(decimated_time_trace, time_trace, detuning_trace)
    bw_trace = np.interp(decimated_time_trace, time_trace, bw_trace)
    detuning_trace_simulated = detuning_trace_simulated[decimated_time_trace > skip]
    bw_trace_simulated = bw_trace_simulated[decimated_time_trace > skip]
    detuning_trace = detuning_trace[decimated_time_trace > skip] 
    bw_trace = bw_trace[decimated_time_trace > skip] 
    decimated_time_trace = decimated_time_trace[decimated_time_trace > skip]


    plt.figure()
    plt.ylim(-500,500)
    plt.plot(decimated_time_trace, detuning_trace)
    plt.plot(decimated_time_trace, detuning_trace_simulated)
    
    print("RMSE detuning:", np.sqrt(np.mean((detuning_trace-detuning_trace_simulated)**2)))
    print("RMSE bandwidth:", 2*np.sqrt(np.mean((bw_trace-bw_trace_simulated)**2)))



    input("Press enter.")
