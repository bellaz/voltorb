#!/usr/bin/
'''
    Simulate CW trace with noise and modulation on QL detuning and forward power
'''

import numpy as np
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default
import voltorb.dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ifile = params['ifile']
    dictfile = dfile.read(ifile)
   
    show_points = params.get('show_points', None)

    time_trace = dictfile['TIME_TRACE']
    qext_trace = dictfile['QEXT_TRACE']
    q0_trace = dictfile['Q0_TRACE']
    ql_trace = dictfile['QL_TRACE']
    bw_trace = dictfile['BW_TRACE']
    drive_I_trace = dictfile['DRIVE_I_TRACE'] 
    drive_Q_trace = dictfile['DRIVE_Q_TRACE'] 
    detuning_trace = dictfile['DETUNING_TRACE']
    clean_forward_i_trace = dictfile['CLEAN_FORWARD_I_TRACE']
    clean_forward_q_trace = dictfile['CLEAN_FORWARD_Q_TRACE']
    clean_probe_i_trace = dictfile['CLEAN_PROBE_I_TRACE']
    clean_probe_q_trace = dictfile['CLEAN_PROBE_Q_TRACE']
    clean_reflected_i_trace = dictfile['CLEAN_REFLECTED_I_TRACE']
    clean_reflected_q_trace = dictfile['CLEAN_REFLECTED_Q_TRACE']
    forward_i_trace = dictfile['FORWARD_I_TRACE']
    forward_q_trace = dictfile['FORWARD_Q_TRACE']
    probe_i_trace = dictfile['PROBE_I_TRACE']
    probe_q_trace = dictfile['PROBE_Q_TRACE']
    reflected_i_trace = dictfile['REFLECTED_I_TRACE']
    reflected_q_trace = dictfile['REFLECTED_Q_TRACE']
    disc_forward_i_trace = dictfile['DISC_FORWARD_I_TRACE']
    disc_forward_q_trace = dictfile['DISC_FORWARD_Q_TRACE']
    disc_probe_i_trace = dictfile['DISC_PROBE_I_TRACE']
    disc_probe_q_trace = dictfile['DISC_PROBE_Q_TRACE']
    disc_reflected_i_trace = dictfile['DISC_REFLECTED_I_TRACE']
    disc_reflected_q_trace = dictfile['DISC_REFLECTED_Q_TRACE']


    if show_points is not None:
        incr = int(len(time_trace)/show_points)
        indexes = np.arange(0, len(time_trace)-1, incr)

        time_trace = time_trace[indexes]
        qext_trace = qext_trace[indexes]
        q0_trace = q0_trace[indexes]
        ql_trace = ql_trace[indexes]
        bw_trace = bw_trace[indexes]
        drive_I_trace = drive_I_trace[indexes]
        drive_Q_trace = drive_Q_trace[indexes]
        detuning_trace = detuning_trace[indexes]
        clean_forward_i_trace = clean_forward_i_trace[indexes]
        clean_forward_q_trace = clean_forward_q_trace[indexes]
        clean_probe_i_trace = clean_probe_i_trace[indexes]
        clean_probe_q_trace = clean_probe_q_trace[indexes]
        clean_reflected_i_trace = clean_reflected_i_trace[indexes]
        clean_reflected_q_trace = clean_reflected_q_trace[indexes]
        forward_i_trace = forward_i_trace[indexes]
        forward_q_trace = forward_q_trace[indexes]
        probe_i_trace = probe_i_trace[indexes]
        probe_q_trace = probe_q_trace[indexes]
        reflected_i_trace = reflected_i_trace[indexes]
        reflected_q_trace = reflected_q_trace[indexes]
        disc_forward_i_trace = disc_forward_i_trace[indexes]
        disc_forward_q_trace = disc_forward_q_trace[indexes]
        disc_probe_i_trace = disc_probe_i_trace[indexes]
        disc_probe_q_trace = disc_probe_q_trace[indexes]
        disc_reflected_i_trace = disc_reflected_i_trace[indexes]
        disc_reflected_q_trace = disc_reflected_q_trace[indexes]

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Qext')
    plt.plot(time_trace, qext_trace)
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Q0')
    plt.plot(time_trace, q0_trace)
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('QL')
    plt.plot(time_trace, ql_trace)
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Bandwidth [Hz]')
    plt.plot(time_trace, 1.3E9/(2*ql_trace))
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('QL')
    plt.semilogy(time_trace, ql_trace)
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Drive amplitude')
    plt.plot(time_trace, drive_I_trace, label="I")
    plt.plot(time_trace, drive_Q_trace, label="Q")
    plt.legend()
    plt.draw()
    
    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Detuning [Hz]')
    plt.plot(time_trace, detuning_trace)
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Signal amplitude (no noise and q)')
    plt.plot(time_trace, clean_probe_i_trace, label="Probe I")
    plt.plot(time_trace, clean_probe_q_trace, label="Probe Q")
    plt.plot(time_trace, clean_forward_i_trace, label="Forward I")
    plt.plot(time_trace, clean_forward_q_trace, label="Forward Q")
    plt.plot(time_trace, clean_reflected_i_trace, label="Reflected I")
    plt.plot(time_trace, clean_reflected_q_trace, label="Reflected Q")
    plt.legend()
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Signal amplitude (noise)')
    plt.plot(time_trace, probe_i_trace, label="Probe I")
    plt.plot(time_trace, probe_q_trace, label="Probe Q")
    plt.plot(time_trace, forward_i_trace, label="Forward I")
    plt.plot(time_trace, forward_q_trace, label="Forward Q")
    plt.plot(time_trace, reflected_i_trace, label="Reflected I")
    plt.plot(time_trace, reflected_q_trace, label="Reflected Q")
    plt.legend()
    plt.draw()

    plt.figure()
    plt.xlabel('Time [s]')
    plt.ylabel('Signal amplitude (noise and q)')
    plt.plot(time_trace, disc_probe_i_trace, label="Probe I")
    plt.plot(time_trace, disc_probe_q_trace, label="Probe Q")
    plt.plot(time_trace, disc_forward_i_trace, label="Forward I")
    plt.plot(time_trace, disc_forward_q_trace, label="Forward Q")
    plt.plot(time_trace, disc_reflected_i_trace, label="Reflected I")
    plt.plot(time_trace, disc_reflected_q_trace, label="Reflected Q")
    plt.legend()
    plt.draw()

    input("Press return..")
