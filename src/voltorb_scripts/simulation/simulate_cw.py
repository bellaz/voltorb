#!/usself._evaluater/bin/env python3
'''
    Simulate CW trace with noise and modulation on QL detuning and forward power
'''

import numpy as np
from scipy.integrate import solve_ivp
from scipy.interpolate import interp1d
from numpy.random import RandomState
from voltorb.parse_cmdline import parser_default
import voltorb.dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ofile = params['ofile']
    
    dictfile = {}
    if 'name' in params:
        dictfile['CAVITY_NAME'] = params['name']

    dictfile['SAMPLE_RATE'] = sample_rate = float(params.get('sample_rate', 9E6)) #MHz
    dictfile['SIMULATION_TIME'] = simulation_time = float(params.get('simulation_time', 1)) #seconds
    dictfile['QEXT'] = qext = float(params.get('qext', 3E7))
    dictfile['Q0'] = q0 = float(params.get('q0', 1E10))
    dictfile['Q0_FINAL'] = q0_final = float(params.get('q0_final', 1E4))
    dictfile['DETUNING'] = detuning = float(params.get('detuning', 0.0)) #Hertz
    dictfile['FREQUENCY'] = frequency = float(params.get('frequency', 1.3E9)) #Hertz cavity frequency
    dictfile['DRIVE_AMPLITUDE'] = drive_amplitude = np.array(params.get('drive_amplitude', [float(2**16)/np.sqrt(2), 0.0])) #Drive amplitude UA
    ql = 1/(1/qext + 1/q0)
    dictfile['CAVITY_STATE'] = cavity_state = params.get('cavity_state', 2*ql/qext*drive_amplitude) #Cavity state UA
    dictfile['DRIVE_STOP'] = drive_stop = params.get('drive_stop', 2*simulation_time)
    dictfile['RANDOM_SEED'] = random_seed = params.get('random_seed', 717182)
    dictfile['NORMAL_STD'] = normal_std = params.get('normal_std', 0) # normal distribution std
    dictfile['UNIFORM_HEIGHT'] = uniform_height = params.get('uniform_height', 0)
    dictfile['SAMPLES'] = samples = int(sample_rate*simulation_time + 1)

    qext_modulation = params.get('qext_modulation', []) # list of one-two-three tuples -> #1 freq #2 amp #3 pha
    if not isinstance(qext_modulation, list):
        print('qext_modulation should be a list')
    q0_modulation = params.get('q0_modulation', []) # list of one-two-three tuples -> #1 freq #2 amp #3 pha
    if not isinstance(q0_modulation, list):
        print('q0_modulation should be a list')
    q0_quench = params.get('q0_quench', [simulation_time*2, 1]) # one-two element tuple #1 quench time #2 decay 
    if not isinstance(q0_quench, list):
        print('q0_quench should be a list')
    detuning_modulation = params.get('detuning_modulation', []) # list of one-two-three tuples -> #1 freq #2 amp #3 pha
    if not isinstance(detuning_modulation, list):
        print('detuning_modulation should be a list')
    drive_I_modulation = params.get('drive_I_modulation', []) # list of one-two-three tuples -> #1 freq #2 amp #3 pha
    if not isinstance(drive_I_modulation, list):
        print('drive_I_modulation should be a list')
    drive_Q_modulation = params.get('drive_Q_modulation', []) # list of one-two-three tuples -> #1 freq #2 amp #3 pha
    if not isinstance(drive_Q_modulation, list):
        print('drive_Q_modulation should be a list')

    q0_quench = [float(v) for v in q0_quench]
    
    qext_modulation = [el for el in qext_modulation if (len(el) > 0) and (len(el) < 4)]
    q0_modulation = [el for el in q0_modulation if (len(el) > 0) and (len(el) < 4)]
    detuning_modulation = [el for el in detuning_modulation if (len(el) > 0) and (len(el) < 4)]
    drive_I_modulation = [el for el in drive_I_modulation if (len(el) > 0) and (len(el) < 4)]
    drive_Q_modulation = [el for el in drive_Q_modulation if (len(el) > 0) and (len(el) < 4)]

    for i in range(len(qext_modulation)):
        if len(qext_modulation[i]) == 1:
            qext_modulation[i].append(0.05*qext)
        if len(qext_modulation[i]) == 2:
            qext_modulation[i].append(0)
        
    for i in range(len(q0_modulation)):
        if len(q0_modulation[i]) == 1:
            q0_modulation[i].append(0.05*q0)
        if len(qext_modulation[i]) == 2:
            q0_modulation[i].append(0)
   
    for i in range(len(detuning_modulation)):
        if len(detuning_modulation[i]) == 1:
            detuning_modulation[i].append(0.025*frequency/qext)
        if len(detuning_modulation[i]) == 2:
            detuning_modulation[i].append(0)

    for i in range(len(drive_I_modulation)):
        if len(drive_I_modulation[i]) == 1:
            drive_I_modulation[i].append(0.10*drive_amplitude[0])
        if len(drive_I_modulation[i]) == 2:
            drive_I_modulation[i].append(0)

    for i in range(len(drive_Q_modulation)):
        if len(drive_Q_modulation[i]) == 1:
            drive_Q_modulation[i].append(0.10*drive_amplitude[1])
        if len(drive_Q_modulation[i]) == 2:
            drive_Q_modulation[i].append(0)

    if len(q0_quench) == 1:
        q0_quench.append(1)

    dictfile['QEXT_MODULATION'] = qext_modulation
    dictfile['Q0_MODULATION'] = q0_modulation
    dictfile['Q0_QUENCH'] = q0_quench
    dictfile['DETUNING_MODULATION'] = detuning_modulation
    dictfile['DRIVE_I_MODULATION'] = drive_I_modulation
    dictfile['DRIVE_Q_MODULATION'] = drive_Q_modulation

    # generate time trace
    time_trace = np.linspace(0, simulation_time, samples, endpoint=True)
    dictfile['TIME_TRACE'] = time_trace 

    # generate Qext

    def generate_qs(time_c):
        qext_c = np.zeros(np.shape(time_c))
        qext_c += qext
        for freq, amp, pha in qext_modulation:
            qext_c += amp*np.sin(2*np.pi*freq*time_c + pha/180*np.pi) + qext_c

        # generate Q0
        q0_c = np.zeros(np.shape(q0))
        q0_c += q0
        for freq, amp, pha in q0_modulation:
            q0_c += amp*np.sin(2*np.pi*freq*time_c + pha/180*np.pi)

        q0_c = q0_final+(q0_c-q0_final)*(np.heaviside(q0_quench[0]-time_c, 0)+
                                                 np.heaviside(time_c-q0_quench[0], 1)*
                                                 np.exp((q0_quench[0]-time_c)/q0_quench[1]))
    
        ql_c = 1/(1/qext_c + 1/q0_c)
        bw_c = frequency*0.5/ql_c

        return qext_c, q0_c, ql_c, bw_c
    
    qext_trace, q0_trace, ql_trace, bw_trace = generate_qs(time_trace)

    dictfile['Q0_TRACE'] = q0_trace
    dictfile['QEXT_TRACE'] = qext_trace
    # generate QL and bandwidth
    dictfile['QL_TRACE'] = ql_trace
    dictfile['BW_TRACE'] = bw_trace
    dictfile['BW_CALIBRATION'] = bw_trace[0]

    drive_amplitude[0] = float(drive_amplitude[0])
    drive_amplitude[1] = float(drive_amplitude[1])

    # generate drive

    def generate_drive(time_c):
        drive_I_c = np.zeros(np.shape(time_c))
        drive_Q_c = np.zeros(np.shape(time_c))
        drive_I_c += 1 
        drive_Q_c += 1

        for freq, amp, pha in drive_I_modulation:
            drive_I_c += amp*np.sin(2*np.pi*freq*time_c + pha/180*np.pi)

        for freq, amp, pha in drive_Q_modulation:
            drive_Q_c += amp*np.sin(2*np.pi*freq*time_c + pha/180*np.pi) 

        drive_I_c *= np.heaviside(drive_stop - time_c, 1) * drive_amplitude[0]
        drive_Q_c *= np.heaviside(drive_stop - time_c, 1) * drive_amplitude[1]
        return drive_I_c, drive_Q_c

    drive_I_trace, drive_Q_trace = generate_drive(time_trace)

    dictfile['DRIVE_I_TRACE'] = drive_I_trace
    dictfile['DRIVE_Q_TRACE'] = drive_Q_trace

    # generate detuning

    def generate_detuning(time_c):
        detuning_c = np.zeros(np.shape(time_c))
        detuning_c += detuning
        for freq, amp, pha in detuning_modulation:
            detuning_c += amp*np.sin(2*np.pi*freq*time_c + pha/180*np.pi)
        return detuning_c

    detuning_trace = generate_detuning(time_trace)
    dictfile['DETUNING_TRACE'] = detuning_trace
   
    def cavity_equation(t, y):
        '''
            cavity equation
        '''
        qext_g, _, ql_g, bw_g = generate_qs(t)
        bw_g *= 2*np.pi
        K_g = 2*bw_g*ql_g/np.sqrt(qext_g*qext)
        detuning_g = generate_detuning(t)
        drive_I_g, drive_Q_g = generate_drive(t)
        detuning_g *= -2*np.pi
        drive_I_g *= K_g
        drive_Q_g *= K_g

        dy = np.empty(2)
        dy[0] = -bw_g*y[0] - detuning_g*y[1] + drive_I_g
        dy[1] = detuning_g*y[0] - bw_g*y[1] + drive_Q_g
        return dy

    def cavity_jacobian(t, y):
        '''
            cavity jacobian
        '''
        _, _, _, bw_g = generate_qs(t)
        bw_g *= 2*np.pi
        detuning_g = generate_detuning(t)
        detuning_g *= -2*np.pi

        jac = np.empty((2, 2))
        jac[0, 0] = -bw_g 
        jac[0, 1] = -detuning_g
        jac[1, 0] = detuning_g
        jac[1, 1] = -bw_g
        return jac

    sol = solve_ivp(cavity_equation, 
                    [time_trace[0], time_trace[-1]],
                    cavity_state,
                    method='LSODA',
                    t_eval=time_trace,
                    jac=cavity_jacobian)

    couple_fact = np.sqrt(qext_trace/qext)

    dictfile['CLEAN_FORWARD_I_TRACE'] = drive_I_trace
    dictfile['CLEAN_FORWARD_Q_TRACE'] = drive_Q_trace
    dictfile['CLEAN_PROBE_I_TRACE'] = sol.y[0, :]
    dictfile['CLEAN_PROBE_Q_TRACE'] = sol.y[1, :]
    dictfile['CLEAN_REFLECTED_I_TRACE'] = (-drive_I_trace+ 
                                           sol.y[0, :]/couple_fact)
    dictfile['CLEAN_REFLECTED_Q_TRACE'] = (-drive_Q_trace+ 
                                           sol.y[1, :]/couple_fact)
    
    dictfile['FORWARD_I_TRACE'] = dictfile['CLEAN_FORWARD_I_TRACE']
    dictfile['FORWARD_Q_TRACE'] = dictfile['CLEAN_FORWARD_Q_TRACE'] 
    dictfile['PROBE_I_TRACE'] = dictfile['CLEAN_PROBE_I_TRACE']
    dictfile['PROBE_Q_TRACE'] = dictfile['CLEAN_PROBE_Q_TRACE']
    dictfile['REFLECTED_I_TRACE'] = dictfile['CLEAN_REFLECTED_I_TRACE']
    dictfile['REFLECTED_Q_TRACE'] = dictfile['CLEAN_REFLECTED_Q_TRACE']

    rstate = RandomState(seed=random_seed)

    dictfile['FORWARD_I_TRACE'] += rstate.normal(scale=normal_std, size=samples) + rstate.uniform(-0.5, 0.5, samples)*uniform_height
    dictfile['FORWARD_Q_TRACE'] += rstate.normal(scale=normal_std, size=samples) + rstate.uniform(-0.5, 0.5, samples)*uniform_height
    dictfile['PROBE_I_TRACE'] += rstate.normal(scale=normal_std, size=samples) + rstate.uniform(-0.5, 0.5, samples)*uniform_height
    dictfile['PROBE_Q_TRACE'] += rstate.normal(scale=normal_std, size=samples) + rstate.uniform(-0.5, 0.5, samples)*uniform_height
    dictfile['REFLECTED_I_TRACE'] += rstate.normal(scale=normal_std, size=samples) + rstate.uniform(-0.5, 0.5, samples)*uniform_height
    dictfile['REFLECTED_Q_TRACE'] += rstate.normal(scale=normal_std, size=samples) + rstate.uniform(-0.5, 0.5, samples)*uniform_height

    dictfile['DISC_FORWARD_I_TRACE'] = dictfile['FORWARD_I_TRACE'].astype(int)
    dictfile['DISC_FORWARD_Q_TRACE'] = dictfile['FORWARD_Q_TRACE'].astype(int)
    dictfile['DISC_PROBE_I_TRACE'] = dictfile['PROBE_I_TRACE'].astype(int) 
    dictfile['DISC_PROBE_Q_TRACE'] = dictfile['PROBE_Q_TRACE'].astype(int) 
    dictfile['DISC_REFLECTED_I_TRACE'] = dictfile['REFLECTED_I_TRACE'].astype(int)
    dictfile['DISC_REFLECTED_Q_TRACE'] = dictfile['REFLECTED_Q_TRACE'].astype(int)


    dfile.write(ofile, dictfile)
