'''
    Script to show DECA qldet results in CW
'''
import time
import numpy as np
from matplotlib.ticker import AutoMinorLocator
from scipy.optimize import curve_fit
from numpy.fft import fft
import matplotlib.ticker as ticker
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import ap_to_cplx, cplx_to_ap, butter_lowpass_filter
from voltorb.math import calculate_ql 
from voltorb.calibrate import calibrate_abcd_scan
from voltorb.matplotlib import pyplot as plt
from voltorb.bwdet import bwdet
from voltorb import dictfile

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    cutoff = params.get('cutoff', 7.5E2)
    cutoff2 =  params.get('cutoff2', 7.5E2) 
    time_shift = params.get('time_shift', 0.0001685)

    decimation = params.get('decimation', 100)
    frequency = params.get('frequency', 1.3E9)
    
    titles = params.get('titles', None)
    fig_bw, ax_bw = plt.subplots(1, 1)
    fig_det, ax_det = plt.subplots(1, 1)
    fig_det_fft, ax_det_fft = plt.subplots(1, 1)

    ax_det.xaxis.set_minor_locator(AutoMinorLocator())
    ax_det.yaxis.set_minor_locator(AutoMinorLocator())
    
    fig_det_clean, ax_det_clean = plt.subplots(1, 1)

    ax_bw.set_xlabel("Time (s)")
    ax_det.set_xlabel("Time (s)")
    ax_det_fft.set_xlabel("Frequency (Hz)")
    ax_det_clean.set_xlabel("Time (s)")
    
    ax_bw.set_ylabel("Bandwidth (Hz)")
    ax_det.set_ylabel("Detuning (Hz)")
    ax_det_clean.set_ylabel("Detuning (Hz)")
    ax_det_fft.set_ylabel("Microphonics amplitude (u.a.)")

    ax_bw.tick_params(which="both",
                      bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_det.tick_params(which="both",
                      bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_det_clean.tick_params(which="both",
                             bottom=True, 
                             top=True, 
                             left=True, 
                             right=True, 
                             axis="both", 
                             direction="in")

    ax_det_fft.tick_params(which="both",
                           bottom=True, 
                           top=True, 
                           left=True, 
                           right=True, 
                           axis="both", 
                           direction="in")

    i = 1

    fig_bw_error, ax_bw_error = plt.subplots(1, 1)
    fig_det_error, ax_det_error = plt.subplots(1, 1)

    bw_error_mean = []
    bw_error_mse = []

    det_error_mean = []
    det_error_mse = []

    ax_bw_error.set_ylabel("Error (%)")
    ax_det_error.set_ylabel("Error (Hz)")

    ax_bw_error.tick_params(bottom=True, 
                            top=True, 
                            left=True, 
                            right=True, 
                            axis="both", 
                            direction="in")
                                  
    ax_det_error.tick_params(bottom=True, 
                             top=True, 
                             left=True, 
                             right=True,         
                             axis="both", 
                             direction="in")


    while "ifile" + str(i) in params:
        dfile = dictfile.read(params["ifile" + str(i)])
        i += 1

        #cut_idx = len(dfile["bandwidth"]) % decimation
        #bw = np.array(np.split(dfile["bandwidth"][:-cut_idx], decimation))
        #bw_mean = np.mean(bw, 0)
        #bw_std = np.std(bw, 0)
        #det = np.array(np.split(dfile["detuning"][:-cut_idx], decimation))
        #det_mean = np.mean(bw, 0)
        #det_std = np.std(bw, 0)

        fs = dfile["fs"]
        print(fs)

        time_trace = np.linspace(0,
                                 1.0/dfile["fs"]*len(dfile["detuning"]),
                                 len(dfile["detuning"]))
    
        ax_det.plot(time_trace, dfile["detuning"]/2, "+", label="FPGA")
       
        rem_bins = len(dfile["detuning"]) % decimation

        decimated_time_trace = np.mean(np.split(time_trace[:-rem_bins], decimation), 1)
        decimated_bandwidth_mean = np.mean(np.split(dfile["bandwidth"][:-rem_bins], decimation), 1)
        decimated_bandwidth_std = np.std(np.split(dfile["bandwidth"][:-rem_bins], decimation), 1)
        
        ql = dfile["QL"]

        #ax_bw.plot(time_trace, dfile["bandwidth"], "+")
        ax_bw.errorbar(decimated_time_trace, 
                       decimated_bandwidth_mean, 
                       decimated_bandwidth_std,
                       fmt='o',
                       markersize=4,
                       capsize=4,
                       label="$Q_L={:3.2e}$".format(ql))

        #ax_det.plot(time_trace, butter_lowpass_filter(dfile["detuning"]/2, 
        #            cutoff2,
        #            fs=fs), "+", label="FPGA, low pass filtered by {:d} Hz".format(cutoff2))

        ax_det.plot(time_trace, butter_lowpass_filter(dfile["detuning"]/2, 
                    cutoff2,
                    fs=fs), ".", label="FPGA, low pass filtered")


        #ax_bw.plot(time_trace, butter_lowpass_filter(dfile["bandwidth"],
        #           cutoff2, 
        #           fs=fs), "+")


        vforw_pha = dfile["vforw_pha_raw"]
        vforw_amp = dfile["vforw_amp_raw"]
        probe_pha = dfile["probe_pha_raw"]
        probe_amp = dfile["probe_amp_raw"]
        vrefl_pha = dfile["vrefl_pha_raw"]
        vrefl_amp = dfile["vrefl_amp_raw"]

        a = dfile["a"]
        b = dfile["b"]
        c = dfile["c"]
        d = dfile["d"]

        vforw_cmplx_tmp = vforw_amp*np.exp(1.0j*vforw_pha*np.pi/180)
        probe_cmplx  = probe_amp*np.exp(1.0j*probe_pha*np.pi/180)
        vrefl_cmplx_tmp = vrefl_amp*np.exp(1.0j*vrefl_pha*np.pi/180)

        
        #vforw_cmplx = a*vforw_cmplx_tmp + b*vrefl_cmplx_tmp
        #vrefl_cmplx = c*vforw_cmplx_tmp + d*vrefl_cmplx_tmp
        
        vforw_cmplx = vforw_cmplx_tmp + vrefl_cmplx_tmp*b/d
        vrefl_cmplx = vforw_cmplx_tmp*c/a + vrefl_cmplx_tmp

        
        probe_cmplx = butter_lowpass_filter(probe_cmplx, 
                                            cutoff, 
                                            fs=fs)

        vforw_cmplx = butter_lowpass_filter(vforw_cmplx, 
                                            cutoff, 
                                            fs=fs)

        vrefl_cmplx = butter_lowpass_filter(vrefl_cmplx, 
                                            cutoff, 
                                            fs=fs)

        vforw_pha = np.arctan2(vforw_cmplx.imag, vforw_cmplx.real)
        det_angle = np.tan(-vforw_pha+(probe_pha*np.pi/180))*0.5*frequency/ql

        #ax_det.plot(time_trace, det_angle, label="Detuning angle")
        #ax_det_clean.plot(time_trace, det_angle, label="Detuning angle")

        (bw, det) = bwdet(probe_cmplx, vforw_cmplx, 
                          fs, ql)

        std = np.std(bw)
        mean = np.mean(bw)

        bw = frequency*1.0/ql

        print(mean+std)
        print("calculated", bw, "mean", mean)
        print(mean-std)

        ax_bw.plot(time_trace[[0,-1]], [bw, bw])

        ax_det.plot(time_trace, -det/2, "r", label="Offline measured detuning")
        ax_det_clean.plot(time_trace, -det/2, label="Cavity detuning")


        det_lp = butter_lowpass_filter(dfile["detuning"], 
                                       cutoff2,
                                       fs=fs)

        print(time_trace[-1])
        freq_trace = np.linspace(0, 
                                 len(-det/2)/2, 
                                 int(len(-det/2)/2))/(time_trace[-1] - time_trace[0])
        ax_det_fft.plot(freq_trace, np.abs(fft(-det, int(len(-det/2)/2), norm="ortho")))
        ax_det_fft.plot(freq_trace, np.abs(fft(dfile["detuning"], int(len(det_lp)/2), norm="ortho")))

        print("mean diff bw  ", np.mean(dfile["bandwidth"] - bw))
        print("std  diff bw  ", np.sqrt(np.mean((dfile["bandwidth"] - bw)**2)))
        print("ptp  diff bw  ", np.ptp(dfile["bandwidth"]))
        print("Q0 quench     ", 2*frequency / np.ptp(dfile["bandwidth"]))
        print("mean diff det ", np.mean(0.5*(det + det_lp)))
        print("std  diff det ", np.sqrt(np.mean((0.5*(det + dfile["detuning"])**2))))
        print("std  diff filtered det ", np.sqrt(np.mean((0.5*(det + det_lp)**2))))

        bw_error_mean.append(np.mean(dfile["bandwidth"]/bw - 1.0) * 100)
        bw_error_mse.append(100*np.sqrt(np.mean((dfile["bandwidth"]/bw - 1.0)**2)))
              
        det_error_mean.append(np.mean(0.5*(det + det_lp)))
        det_error_mse.append(np.sqrt(np.mean((0.5*(det + det_lp))**2)))

    ax_bw_error.plot(bw_error_mean, label="Average error", marker="o", linestyle="none", markersize=12) 
    ax_bw_error.plot(bw_error_mse, label="MSE error", marker="*", linestyle="none", markersize=12) 
    ax_det_error.plot(det_error_mean, label="Average error", marker="o", linestyle="none", markersize=12)
    ax_det_error.plot(det_error_mse, label="MSE error", marker="*", linestyle="none", markersize=12)

    xes = list(range(len(titles)))
    
    ax_bw_error.xaxis.set_major_locator(ticker.FixedLocator(xes))
    ax_det_error.xaxis.set_major_locator(ticker.FixedLocator(xes))
    
    ax_bw_error.xaxis.set_major_formatter(ticker.FixedFormatter(xes))
    ax_det_error.xaxis.set_major_formatter(ticker.FixedFormatter(xes))
    ax_bw_error.set_xticklabels(titles, rotation=30)
    ax_det_error.set_xticklabels(titles, rotation=30)

    ax_bw_error.legend()
    ax_det_error.legend()

    ax_det.legend()
    ax_det_clean.legend()
    ax_bw.legend()

    plt.draw()
    input("Press return..")
 
if __name__ == '__main__':
    main.run()


