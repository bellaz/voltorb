#!/usr/bin
'''
    Set batman on the correction tables using amplitude and phase
'''

import numpy as np
from voltorb import controlsys as csys
from voltorb.batman import batman_up, batman_down
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    scale_factor = params['scale_factor']
    addr_corr_table_i = params['addr_corr_table_I']
    addr_corr_table_q = params['addr_corr_table_Q']

    table_len = len(csys.read(addr_corr_table_i))

    table_i = []
    table_q = []

    for i in np.linspace(-7, 7, table_len):
        b_u = batman_up(i)/3*scale_factor
        b_d = batman_down(i)*50*np.pi/180
        table_i.append(b_u*np.cos(b_d))
        table_q.append(b_u*np.sin(b_d))

    csys.write(addr_corr_table_i, table_i)
    csys.write(addr_corr_table_q, table_q)

if __name__ == '__main__':
    main.run()
