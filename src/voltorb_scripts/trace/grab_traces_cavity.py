#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ofile = params.get('ofile', 'out.h5')
    delay = params.get('delay', 2)
    measurements = params.get('measurements', 256)

    cavity_name = params['cavity_name'] 
    cavity_cal_amp_addr = params['cavity_cal_amp_addr']
    vforw_power_addr = params['vforw_power_addr']
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    probe_amp_scale_addr = params['probe_amp_scale_addr']
    probe_pha_rotation_addr = params['probe_pha_rotation_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vforw_amp_scale_addr = params['vforw_amp_scale_addr']
    vforw_pha_rotation_addr = params['vforw_pha_rotation_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    vrefl_amp_scale_addr = params['vrefl_amp_scale_addr']
    vrefl_pha_rotation_addr = params['vrefl_pha_rotation_addr']
    cav_ql_addr = params['cav_ql_addr']
    piezo_voltage_addr = params['piezo_voltage_addr']
    fpc_temp_addr = params['fpc_temp_addr']
    vm_output_amp_addr = params['vm_output_amp_addr']
    vm_output_pha_addr = params['vm_output_pha_addr'] 

    dictfile = {}
    dictfile['TIME'] = []
    dictfile['TIMESTAMP'] = []
    dictfile['CAVITY_NAME'] = cavity_name
    dictfile['CAVITY_CAL_AMP'] = []
    dictfile['VFORW_POWER'] = []
    dictfile['PROBE_AMP'] = []
    dictfile['PROBE_PHA'] = []
    dictfile['PROBE_AMP_SCALE'] = []
    dictfile['PROBE_PHA_ROTATION'] = []
    dictfile['VFORW_AMP'] = []
    dictfile['VFORW_PHA'] = []
    dictfile['VFORW_AMP_SCALE'] = []
    dictfile['VFORW_PHA_ROTATION'] = []
    dictfile['VREFL_AMP'] = []
    dictfile['VREFL_PHA'] = []
    dictfile['VREFL_AMP_SCALE'] = []
    dictfile['VREFL_PHA_ROTATION'] = []
    dictfile['CAV_QL_ADDR'] = []
    dictfile['PIEZO_VOLTAGE'] = []
    dictfile['FPC_TEMP'] = []
    dictfile['VM_OUTPUT_AMP'] = []
    dictfile['VM_OUTPUT_PHA'] = []

    time0 = time.time()

    for i in range(measurements):

        print('Measurement', i+1, '/', measurements)

        time.sleep(delay)

        try:
            [probe_amp, probe_pha, vforw_amp, 
                    vforw_pha, vrefl_amp, vrefl_pha] = csys.read(probe_amp_addr, 
                                                                 probe_pha_addr,
                                                                 vforw_amp_addr, 
                                                                 vforw_pha_addr,
                                                                 vrefl_amp_addr, 
                                                                 vrefl_pha_addr)

            cavity_cal_amp = csys.read(cavity_cal_amp_addr)
            vforw_power = csys.read(vforw_power_addr)
            probe_amp_scale = 0 # csys.read(probe_amp_scale_addr)
            probe_pha_rotation = 0#csys.read(probe_pha_rotation_addr)
            vforw_amp_scale = 0#csys.read(vforw_amp_scale_addr)
            vforw_pha_rotation = 0#csys.read(vforw_pha_rotation_addr)
            vrefl_amp_scale = 0#csys.read(vrefl_amp_scale_addr)
            vrefl_pha_rotation = 0 #csys.read(vrefl_pha_rotation_addr)
            cav_ql = csys.read(cav_ql_addr)
            piezo_voltage = csys.read(piezo_voltage_addr)
            fpc_temp = csys.read(fpc_temp_addr)
            vm_output_amp = csys.read(vm_output_amp_addr)
            vm_output_pha = csys.read(vm_output_pha_addr)

            dictfile['TIME'].append(time.time()-time0)
            dictfile['TIMESTAMP'].append(time.time())
            dictfile['PROBE_AMP'].append(probe_amp)
            dictfile['PROBE_PHA'].append(probe_pha)
            dictfile['VFORW_AMP'].append(vforw_amp)
            dictfile['VFORW_PHA'].append(vforw_pha)
            dictfile['VREFL_AMP'].append(vrefl_amp)
            dictfile['VREFL_PHA'].append(vrefl_pha)

            dictfile['CAVITY_CAL_AMP'].append(cavity_cal_amp) 
            dictfile['VFORW_POWER'].append(vforw_power) 
            dictfile['PROBE_AMP_SCALE'].append(probe_amp_scale) 
            dictfile['PROBE_PHA_ROTATION'].append(probe_pha_rotation)
            dictfile['VFORW_AMP_SCALE'].append(vforw_amp_scale)
            dictfile['VFORW_PHA_ROTATION'].append(vforw_pha_rotation)
            dictfile['VREFL_AMP_SCALE'].append(vrefl_amp_scale)
            dictfile['VREFL_PHA_ROTATION'].append(vrefl_pha_rotation)
            dictfile['CAV_QL_ADDR'].append(cav_ql) 
            dictfile['PIEZO_VOLTAGE'].append(piezo_voltage) 
            dictfile['FPC_TEMP'].append(fpc_temp) 
            dictfile['VM_OUTPUT_AMP'].append(vm_output_amp) 
            dictfile['VM_OUTPUT_PHA'].append(vm_output_pha) 

            dfile.write(ofile, dictfile)
        except Exception as e:
            print(e)

if __name__ == "__main__":
    main.run()
