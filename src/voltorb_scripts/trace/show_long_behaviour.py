#!/usr/bin/env python
'''
    Plot amplitude and phase from a cavity
'''

import numpy as np
from scipy.signal import spectrogram
from datetime import datetime
from voltorb import dictfile
from voltorb.parse_cmdline import parser_default
from voltorb.matplotlib import pyplot as plt
import matplotlib.dates as md
import datetime as dt
from scipy import signal

window = 900

def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = signal.butter(order, normal_cutoff, btype='high', analog=False)
    return b, a

def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = signal.filtfilt(b, a, data)
    return y


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    QL = 1e7
    freq = 1.3e9
    hbw = freq*0.5/QL

    ifile = params.get('ifile', 'out.h5')
    dfile = dictfile.read(ifile)

    plt.figure()
    plt.plot(dfile["TIME"], 
             dfile["PROBE_AMP"])
    plt.xlabel("Time [s]")
    plt.ylabel("Probe Amp [MV/m]")
    plt.draw()

    plt.figure()
    plt.plot(dfile["TIME"], 
             dfile["PROBE_PHA"])
    plt.xlabel("Time [s]")
    plt.ylabel("Probe Phase [deg]")
    plt.draw()

    plt.figure()
    plt.plot(dfile["TIME"], 
             dfile["VFORW_AMP"])
    plt.xlabel("Time [s]")
    plt.ylabel("Forward Amp [MV/m]")
    plt.draw()
    
    plt.figure()
    plt.plot(dfile["TIME"], 
             dfile["VFORW_PHA"])
    plt.xlabel("Time [s]")
    plt.ylabel("Forward Phase [deg]")
    plt.draw()

    plt.figure()
    plt.plot(dfile["TIME"], 
             dfile["VREFL_AMP"])
    plt.xlabel("Time [s]")
    plt.ylabel("Reflected Amp [MV/m]")
    plt.draw()

    plt.figure()
    plt.plot(dfile["TIME"], 
             dfile["VREFL_PHA"])
    plt.xlabel("Time [s]")
    plt.ylabel("Reflected Phase [deg]")
    plt.draw()

    plt.figure()
    plt.plot(dfile["TIME"], 
             dfile["PIEZO_VOLTAGE"])
    plt.xlabel("Time [s]")
    plt.ylabel("Piezo voltage [V]")
    plt.draw()

    detrended = butter_highpass_filter(dfile["PIEZO_VOLTAGE"], 0.01, 1.0)

    plt.figure()
    plt.plot(dfile["TIME"], 
             detrended)
    plt.xlabel("Time [s]")
    plt.ylabel("Piezo voltage (detrended) [V]")
    plt.draw()


    plt.figure()
    plt.plot(dfile["TIME"], 
             hbw*np.arctan(np.deg2rad(dfile["PROBE_PHA"]-dfile["VFORW_PHA"])))
    plt.xlabel("Time [s]")
    plt.ylabel("Detuning [Hz]")
    plt.draw()

    plt.figure()
    plt.title("Disturbance spectrogram (log10)")
    plt.grid()
    t = np.linspace(0, np.floor(dfile["TIME"][-1]), int(np.floor(dfile["TIME"][-1])))
    pv = np.interp(t, dfile["TIME"], dfile["PIEZO_VOLTAGE"])

    f, t, Sxx = spectrogram(pv, 1.0)

    timestamps = np.interp(t, dfile["TIME"], dfile["TIMESTAMP"])
    dates = [dt.datetime.fromtimestamp(ts) for ts in timestamps]
    datenums = md.date2num(dates)

    plt.xticks(rotation=75)
    ax=plt.gca()
    xfmt = md.DateFormatter('%H:%M')
    ax=plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    ax.locator_params(nbins=36, axis='x')
    ax.locator_params(nbins=15, axis='y')

    plt.pcolormesh(datenums, f, np.log10(Sxx), shading='gouraud')
    plt.ylabel('Frequency PZT ecitation [Hz]')
    plt.xlabel('Time')
    plt.show()

    detrended = butter_highpass_filter(pv, 0.01, 1.0)
    detrended = detrended[:-(detrended.shape[0] % window)]
    detrended = detrended.reshape([int(detrended.shape[0]/window), window])
    
    t = np.linspace(0, detrended.shape[0]*detrended.shape[1], detrended.shape[0])
    timestamps = np.interp(t, dfile["TIME"], dfile["TIMESTAMP"])
    dates = [dt.datetime.fromtimestamp(ts) for ts in timestamps]
    datenums = md.date2num(dates)

    power = np.std(detrended, axis=1)
    fft = np.abs(np.fft.fft(detrended, axis=1))
    fft = fft[:, :int(window/2)]
    freq = np.argmax(fft, axis=1)/window

    print(power.shape)
    print(freq.shape)
    print(t.shape)

    plt.figure()
    plt.title("Detrended disturbance power")
    plt.grid()
    plt.plot(datenums, power)
    ax = plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    ax.locator_params(nbins=36, axis='x')
    ax.locator_params(nbins=15, axis='y')
    plt.xticks(rotation=75)
    plt.xlabel("Time [s]")
    plt.ylabel("Piezo voltage STD [V]")
    plt.draw()

    plt.figure()
    plt.title("Frequency of the highest amplitude frequency component")
    plt.plot(datenums, freq)
    plt.grid()
    ax = plt.gca()
    ax.xaxis.set_major_formatter(xfmt)
    ax.locator_params(nbins=36, axis='x')
    ax.locator_params(nbins=15, axis='y')
    plt.xticks(rotation=75)
    plt.xlabel("Time [s]")
    plt.ylabel("Detuning principal disturbance [Hz]")
    plt.draw()



    input("Press return..")


if __name__ == "__main__":
    main()

    
