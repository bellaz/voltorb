'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
from matplotlib.ticker import AutoMinorLocator
from datetime import datetime
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import ap_to_cplx, cplx_to_ap, butter_lowpass_filter
from voltorb.math import calculate_ql 
from voltorb.calibrate import calibrate_abcd_scan
from voltorb.matplotlib import pyplot as plt
from voltorb.bwdet import bwdet
from voltorb import dictfile

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    fit_degree = params.get("fit_degree", 3)
    decay_start = params.get("decay_start", 0.550997986)
    fit_decay_range = params.get('fit_decay_range', (0.57, 0.70))
    fit_pulse_range = params.get('fit_pulse_range', (0.40, 0.53))
    decay_range = params.get('decay_range', (0.6, 1.0))
    pulse_range = params.get('pulse_range', (0.05, 0.5))
    cutoff = params.get('cutoff', 1E5)
    time_shift = params.get('time_shift', 0.0001685)
    trace_time = params.get('time_shift', 0.001813)

    decimation = params.get('decimation', 200)
    frequency = params.get('frequency', 1.3E9)
    
    k_add_steps = params.get('k_add_steps', 100)
    k_add_min = params.get('k_add_min', 0.001)
    k_add_max = params.get('k_add_max', 0.005)

    titles = params.get('titles', None)
    idxs = params.get('idxs', None)
    fig_bw, ax_bw = plt.subplots(1, 1)
    fig_q, (ax_bw_q, ax_amp_q) = plt.subplots(2, 1, sharex=True)

    fig_det, ax_det = plt.subplots(1, 1)
    fig_ql, ax_ql = plt.subplots(1, 1)
    
    ax_bw.set_xlabel("Time (s)")
    ax_det.set_xlabel("Time (s)")
    ax_ql.set_xlabel("Time (s)")
    ax_bw_q.set_xlabel("Time (s)")
    ax_amp_q.set_xlabel("Time (s)")

    ax_bw_q.set_ylabel("Bandwidth (Hz)")
    ax_amp_q.set_ylabel("Amplitude (MV/m)")     

    ax_bw.set_ylabel("Bandwidth (Hz)")
    ax_det.set_ylabel("Detuning (Hz)")
    ax_ql.set_ylabel("QL")

    ax_bw_q.xaxis.set_minor_locator(AutoMinorLocator())
    ax_amp_q.xaxis.set_minor_locator(AutoMinorLocator())
    ax_bw_q.yaxis.set_minor_locator(AutoMinorLocator())
    ax_amp_q.yaxis.set_minor_locator(AutoMinorLocator())

    ax_bw.tick_params(bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_det.tick_params(bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_ql.tick_params(bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_bw_q.tick_params(which="both",
                        bottom=True, 
                        top=True, 
                        left=True, 
                        right=True, 
                        axis="both", 
                        direction="in")

    ax_amp_q.tick_params(which="both",
                         bottom=True, 
                         top=True, 
                         left=True, 
                         right=True, 
                         axis="both", 
                         direction="in")


    i = 1
    
    dfile = dictfile.read(params["ifile1"])
    bw = dfile["BANDWIDTH"][-1, :] 
    time_trace = np.linspace(0.0, trace_time, len(bw))

    while "ifile" + str(i) in params:
        dfile = dictfile.read(params["ifile" + str(i)])
        i += 1
        t = -1
        if idxs:
            t = idxs[i-2]
        
        label = None
        if titles:
            label = titles[i-2]

        bw = dfile["BANDWIDTH"][t, :]
        det = dfile["DETUNING"][t, :]
        ql = dfile["QL"][t, :]
        probe_amp = dfile["VFORW_AMP"][t, :]*0.000794085

        dec_idx = range(0, len(bw), decimation)
        
        ax_bw.plot(time_trace[dec_idx]-time_shift, 
                       bw[dec_idx], "+", label=label)

        ax_det.plot(time_trace[dec_idx]-time_shift, 
                        det[dec_idx]*0.5, label=label)

        ax_ql.plot(time_trace[dec_idx]-time_shift, 
                        ql[dec_idx]*0.5, label=label)

        ax_bw_q.plot(time_trace, bw, label=label)
        ax_amp_q.plot(np.linspace(0, 
                                  time_trace[-1], 
                                  len(probe_amp)), 
                                  probe_amp, 
                                  label=label)

    if titles:
        ax_bw_q.legend()
        ax_det.legend()
        ax_bw.legend()
        ax_ql.legend()

    ax_bw.ticklabel_format(axis="x", style="sci", scilimits=(0,0))
    ax_amp_q.ticklabel_format(axis="x", style="sci", scilimits=(0,0))

    plt.draw()
    input("Press return..")
 
if __name__ == '__main__':
    main.run()


