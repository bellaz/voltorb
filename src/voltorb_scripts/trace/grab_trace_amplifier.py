#!/usr/bin/env python
'''
    Grab amplitude and phase from the amplifier filename_output
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    delay = params['delay']
    filename_output = params['ofile']
    measurements = params['measurements']

    ff_table_i_addr = params['ff_table_I_addr']
    ff_table_q_addr = params['ff_table_Q_addr']
    corr_table_i_addr = params['corr_table_I_addr']
    corr_table_q_addr = params['corr_table_Q_addr']
    vm_output_i_addr = params['vm_output_I_addr']
    vm_output_q_addr = params['vm_output_Q_addr']
    amplifier_out_amp_addr = params['amplifier_out_amp_addr']
    amplifier_out_pha_addr = params['amplifier_out_pha_addr']

    print('Measuring nonlinearity')

    measurement_amplitudes = []
    measurement_phases = []

    for j in range(measurements):
        print('Measure', str(j+1) + '/' + str(measurements))
        time.sleep(delay)
        measurement_amplitudes.append(csys.read(amplifier_out_amp_addr))
        measurement_phases.append(csys.read(amplifier_out_pha_addr))

    dictfile = {}
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'] = np.array(measurement_amplitudes)
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'] = np.mean(dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'],
                                                         axis=0)
    dictfile['AMPLIFIER_OUTPUT_PHASE'] = np.array(measurement_phases)
    dictfile['AMPLIFIER_OUTPUT_PHASE_AVG'] = np.mean(dictfile['AMPLIFIER_OUTPUT_PHASE'],
                                                     axis=0)
    dictfile['FF_TABLE_I'] = csys.read(ff_table_i_addr)
    dictfile['FF_TABLE_Q'] = csys.read(ff_table_q_addr)
    dictfile['CORR_TABLE_I'] = csys.read(corr_table_i_addr)
    dictfile['CORR_TABLE_Q'] = csys.read(corr_table_q_addr)
    dictfile['VM_OUTPUT_I'] = csys.read(vm_output_i_addr)
    dictfile['VM_OUTPUT_Q'] = csys.read(vm_output_q_addr)

    dfile.write(filename_output, dictfile)

if __name__ == "__main__":
    main.run()
