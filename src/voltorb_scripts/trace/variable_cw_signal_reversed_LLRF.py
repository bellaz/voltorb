#!/usr/bin/env python
'''
    Measures the VM -> Amplifier output characteristics varying the CW amplitude (low to high)
'''

import numpy as np
import time
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    delay = params['delay']
    filename_output = params['ofile']
    measurements = params['measurements']
    addr_sp_amplitude = params['addr_sp_amp']
    addr_ff_table_i = params['addr_ff_table_I']
    addr_corr_enable = params['addr_corr_enable']
    addr_corr_table_i = params['addr_corr_table_I']
    addr_corr_table_q = params['addr_corr_table_Q']
    addr_vm_output_i = params['addr_vm_output_I']
    addr_vm_output_q = params['addr_vm_output_Q']
    addr_vm_output_amp = params['addr_vm_output_amp']
    addr_vm_output_pha = params['addr_vm_output_pha']
    addr_amplifier_input_amplitude = params['addr_amplifier_in_amp']
    addr_amplifier_input_phase = params['addr_amplifier_in_pha']
    addr_amplifier_output_amplitude = params['addr_amplifier_out_amp']
    addr_amplifier_output_phase = params['addr_amplifier_out_pha']

    measurement_fractions = np.linspace(1/measurements, 1, measurements)

    print('Waiting ', 10*delay, 'seconds to warm up the IOT')

    ff_table_i = csys.read(addr_ff_table_i)
    ff_table_len = len(ff_table_i)
    ff_table_avg = np.mean(ff_table_i)


    initial_amp = csys.read(addr_sp_amplitude)
    csys.write(addr_sp_amplitude, 0)
    csys.write(addr_corr_enable, 1)

    dictfile = {}
    dictfile['CORR_TABLE_I'] = []
    dictfile['CORR_TABLE_Q'] = []
    dictfile['VM_OUTPUT_AMPLITUDE'] = []
    dictfile['VM_OUTPUT_PHASE'] = []
    dictfile['VM_OUTPUT_I'] = []
    dictfile['VM_OUTPUT_Q'] = []
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'] = []
    dictfile['AMPLIFIER_OUTPUT_PHASE'] = []
    dictfile['AMPLIFIER_INPUT_AMPLITUDE'] = []
    dictfile['AMPLIFIER_INPUT_PHASE'] = []

    time.sleep(delay*10)
    
    for frac in measurement_fractions:

        table_new = [int(ff_table_avg * frac)] * ff_table_len
        csys.write(addr_corr_table_i, table_new)
        print('done {} %'.format(int((1-frac)*100)))
        time.sleep(delay)
        dictfile['CORR_TABLE_I'].insert(0, np.mean(csys.read(addr_corr_table_i)))
        dictfile['CORR_TABLE_Q'].insert(0, np.mean(csys.read(addr_corr_table_q)))
        dictfile['VM_OUTPUT_I'].insert(0, np.mean(csys.read(addr_vm_output_i)))
        dictfile['VM_OUTPUT_Q'].insert(0, np.mean(csys.read(addr_vm_output_q)))
        dictfile['VM_OUTPUT_AMPLITUDE'].insert(0, np.mean(csys.read(addr_vm_output_amp)))
        dictfile['VM_OUTPUT_PHASE'].insert(0, np.mean(csys.read(addr_vm_output_pha)))
        dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'].insert(0, np.mean(csys.read(addr_amplifier_output_amplitude)))
        dictfile['AMPLIFIER_OUTPUT_PHASE'].insert(0, np.mean(csys.read(addr_amplifier_output_phase)))
        dictfile['AMPLIFIER_INPUT_AMPLITUDE'].insert(0, np.mean(csys.read(addr_amplifier_input_amplitude)))
        dictfile['AMPLIFIER_INPUT_PHASE'].insert(0, np.mean(csys.read(addr_amplifier_input_phase)))

    dfile.write(filename_output, dictfile)
    csys.write(addr_corr_enable, 0)
    csys.write(addr_corr_table_i, [0]*ff_table_len)
    csys.write(addr_sp_amplitude, initial_amp)

if __name__ == "__main__":
    main.run()
