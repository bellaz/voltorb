#!/usr/bin/env python
'''
    Plot microphonics stats of the passed files
'''

import time
import numpy as np
from scipy.signal import detrend
from numpy.fft import rfft, fft
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.matplotlib import pyplot as plt
from voltorb.math import phase_wrap
CAVITY_FREQUENCY = 1.3E9

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    skip_back = params.get('skip_back', 0)
    fig_micro_fft, plot_micro_fft = plt.subplots(1, 1)
    plot_micro_fft.set_title('Microphonics FFT')
    fig_micro_histo, plot_micro_histo = plt.subplots(1, 1)
    plot_micro_histo.set_title('Microphonics distribution')
    fig_micro_cumsum, plot_micro_cumsum = plt.subplots(1, 1)
    plot_micro_cumsum.set_title('Microphonics cumulative error')
    fig_forw_amp_fft, plot_forw_amp_fft = plt.subplots(1, 1)
    plot_forw_amp_fft.set_title('Forward amplitude FFT')
    fig_forw_pha_fft, plot_forw_pha_fft = plt.subplots(1, 1)
    plot_forw_pha_fft.set_title('Forward phase FFT')
    fig_refl_amp_fft, plot_refl_amp_fft = plt.subplots(1, 1)
    plot_refl_amp_fft.set_title('Reflected amplitude FFT')
    fig_refl_pha_fft, plot_refl_pha_fft = plt.subplots(1, 1)
    plot_refl_pha_fft.set_title('Reflected phase FFT')

    plot_micro_fft.set_xlabel('Frequency [Hz]')
    plot_micro_histo.set_xlabel('Detuning [Hz]')
    plot_micro_cumsum.set_xlabel('Frequency [Hz]')
    plot_forw_amp_fft.set_xlabel('Frequency [Hz]')
    plot_forw_pha_fft.set_xlabel('Frequency [Hz]')
    plot_refl_amp_fft.set_xlabel('Frequency [Hz]')
    plot_refl_pha_fft.set_xlabel('Frequency [Hz]')

    plot_micro_fft.set_ylabel('Detuning [Hz]')
    plot_micro_histo.set_ylabel('Counts (Normalized)')
    plot_micro_cumsum.set_ylabel('Detuning [Hz]')
    plot_forw_amp_fft.set_ylabel('Amplitude [UA]')
    plot_forw_pha_fft.set_ylabel('Phase [deg]')
    plot_refl_amp_fft.set_ylabel('Amplitude [UA]')
    plot_refl_pha_fft.set_ylabel('Phase [deg]')

    max_fft_freq = params.get('max_fft_freq', 400)
    bins = params.get('bins', 1000)

    plot_micro_fft.set_xlim(-10, max_fft_freq)
    plot_micro_cumsum.set_xlim(-10, max_fft_freq)
    plot_forw_amp_fft.set_xlim(-10, max_fft_freq)
    plot_forw_pha_fft.set_xlim(-10, max_fft_freq)
    plot_refl_amp_fft.set_xlim(-10, max_fft_freq)
    plot_refl_pha_fft.set_xlim(-10, max_fft_freq)


    trace_time = params.get('trace_time', 1)

    idx = 0
    while True:
        idx += 1
        if 'ifile{}'.format(idx) not in params:
            break
 
        dictfile = dfile.read(params['ifile{}'.format(idx)])
        
        label = dictfile['CAVITY_NAME']
        print('Cavity', label)
        #dictfile['CAVITY_CAL_AMP']
        #dictfile['VFORW_POWER']
        #dictfile['PROBE_AMP']
        probe_pha = dictfile['PROBE_PHA'][:, :-1-skip_back]
        #dictfile['PROBE_AMP_SCALE']
        #dictfile['PROBE_PHA_ROTATION']
        forw_amp = dictfile['VFORW_AMP'][:, :-1-skip_back]
        forw_pha = dictfile['VFORW_PHA'][:, :-1-skip_back]
        #dictfile['VFORW_AMP_SCALE']
        #dictfile['VFORW_PHA_ROTATION']
        refl_amp = dictfile['VREFL_AMP'][:, :-1-skip_back]
        refl_pha = dictfile['VREFL_PHA'][:, :-1-skip_back]
        #dictfile['VREFL_AMP_SCALE']
        #dictfile['VREFL_PHA_ROTATION']
        ql = np.mean(dictfile['CAV_QL_ADDR'])
        print('QL', ql)
        
        detuning = CAVITY_FREQUENCY*0.5/ql*np.tan(np.deg2rad(detrend(phase_wrap(forw_pha - probe_pha), type='constant', axis=1)))
        detuning_std = np.std(detuning)
        print('detuning std', detuning_std, 'Hz')
        detuning_spectrum = np.mean(np.abs(rfft(detuning, axis=1)), axis=0)
        detuning_spectrum /= len(detuning_spectrum)
        freqs = np.linspace(0, len(detuning_spectrum), len(detuning_spectrum))/trace_time
        plot_micro_fft.plot(freqs, detuning_spectrum, label=label)
        plot_micro_histo.hist(detuning.flatten(), bins=bins, histtype='step', label=label)
        plot_micro_cumsum.plot(freqs, np.sqrt(np.cumsum(detuning_spectrum**2)/2), label=label)
        plot_forw_amp_fft.plot(freqs, np.mean(np.abs(rfft(detrend(forw_amp, type='constant', axis=1), axis=1)/len(detuning_spectrum)), axis=0), label=label)
        plot_forw_pha_fft.plot(freqs, np.mean(np.abs(rfft(detrend(forw_pha, type='constant', axis=1), axis=1)/len(detuning_spectrum)), axis=0), label=label)
        plot_refl_amp_fft.plot(freqs, np.mean(np.abs(rfft(detrend(refl_amp, type='constant', axis=1), axis=1)/len(detuning_spectrum)), axis=0), label=label)
        plot_refl_pha_fft.plot(freqs, np.mean(np.abs(rfft(detrend(refl_pha, type='constant', axis=1), axis=1)/len(detuning_spectrum)), axis=0), label=label)

    plot_micro_fft.legend()
    plot_micro_histo.legend()
    plot_micro_cumsum.legend()
    plot_forw_amp_fft.legend()
    plot_forw_pha_fft.legend()
    plot_refl_amp_fft.legend()
    plot_refl_pha_fft.legend()

    fig_micro_fft.canvas.draw_idle()
    fig_micro_histo.canvas.draw_idle()
    fig_micro_cumsum.canvas.draw_idle()
    fig_forw_amp_fft.canvas.draw_idle()
    fig_forw_pha_fft.canvas.draw_idle()
    fig_refl_amp_fft.canvas.draw_idle()
    fig_refl_pha_fft.canvas.draw_idle()

    input('Press return..')

if __name__ == "__main__":
    main.run()
