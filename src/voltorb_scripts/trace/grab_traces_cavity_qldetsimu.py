#!/usr/bin/env python
''' 
    Grab amplitude and phase from a cavity
'''

import numpy as np
from scipy.signal import resample
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.math import ap_to_cplx
from voltorb.calibrate import calibrate_minimize, calibrate_decay
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ofile = params.get('ofile', None)

    detuning_time_fetch = params.get('detuning_time_fetch', 150E-6)
    calibrate = params.get('calibrate', False) 
    show_calib = params.get('show_calibration', False)
    show_detuning = params.get('show_detuning', False)
    calibrate_method = params.get('calibrate_method', 'calibrate_minimize')
    cavity_frequency = params.get('cavity_frequency', 1.3E9)
    max_disc = params.get('max_disc', (2**15*np.sqrt(2)))

    cavity_name = params['cavity_name'] 
    cavity_sig_time_addr = params['cavity_sig_time_addr']
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    detuning_addr  = params['detuning_addr']
    detuning_time_addr = params['detuning_time_addr']
    cav_ql_addr = params['cav_ql_addr']
    resample_frequency = params.get('resample_frequency', 9E6)

    [probe_amp, probe_pha, vforw_amp, 
            vforw_pha, vrefl_amp, vrefl_pha] = csys.read(probe_amp_addr, 
                    probe_pha_addr,
                    vforw_amp_addr, 
                    vforw_pha_addr,
                    vrefl_amp_addr, 
                    vrefl_pha_addr)


    cavity_sig_time = csys.read(cavity_sig_time_addr)/1E6
    detuning = csys.read(detuning_addr)
    detuning_time = csys.read(detuning_time_addr)/1E6 + detuning_time_fetch
    cav_ql = csys.read(cav_ql_addr)

    probe_cplx = ap_to_cplx(probe_amp, probe_pha)
    vforw_cplx = ap_to_cplx(vforw_amp, vforw_pha) 
    vrefl_cplx = ap_to_cplx(vrefl_amp, vrefl_pha)

    probe_I = probe_cplx.real 
    vforw_I = vforw_cplx.real
    vrefl_I = vrefl_cplx.real
    probe_Q = probe_cplx.imag 
    vforw_Q = vforw_cplx.imag
    vrefl_Q = vrefl_cplx.imag

    if calibrate:
        signals = None
        calib = None
        loss = None
        print(calibrate_method)

        if calibrate_method == "calibrate_minimize":
            calib, signals, loss = calibrate_minimize(probe_I, probe_Q,
                    vforw_I, vforw_Q,
                    vrefl_I, vrefl_Q)
        else:
            calib, signals, loss = calibrate_decay(probe_I, probe_Q,
                    vforw_I, vforw_Q,
                    vrefl_I, vrefl_Q)
        
        (vforw_I, vforw_Q,
        vrefl_I, vrefl_Q) = signals
            
        (a1, b1, a2, b2) = calib
        print('a1: ', a1, ' b1: ', b1)
        print('a2: ', a2, ' b2: ', b2)
        print('loss', loss)




    print(len(vforw_I))
    print(len(detuning))
    timespan = (cavity_sig_time[-1] - cavity_sig_time[0])
    print("timespan", timespan)
    samples = int(timespan*resample_frequency)
    print("new samples", samples)

    cavity_sig_time_orig = cavity_sig_time
    cavity_sig_time = np.linspace(cavity_sig_time[0], cavity_sig_time[-1], 
            samples, endpoint=True)

    probe_I  = np.interp(cavity_sig_time, cavity_sig_time_orig, probe_I)
    vforw_I  = np.interp(cavity_sig_time, cavity_sig_time_orig, vforw_I)
    vrefl_I  = np.interp(cavity_sig_time, cavity_sig_time_orig, vrefl_I)
    probe_Q  = np.interp(cavity_sig_time, cavity_sig_time_orig, probe_Q)
    vforw_Q  = np.interp(cavity_sig_time, cavity_sig_time_orig, vforw_Q)
    vrefl_Q  = np.interp(cavity_sig_time, cavity_sig_time_orig, vrefl_Q)
    detuning = np.interp(cavity_sig_time, detuning_time, detuning)

    max_val = max(max(probe_I),
           max(vforw_I), 
           max(vrefl_I), 
           max(probe_Q), 
           max(vforw_Q), 
           max(vrefl_Q)) 

    rescale_val = max_disc/max_val
    # discretize
    disc_probe_I = (probe_I*rescale_val).astype(int)
    disc_probe_Q = (probe_Q*rescale_val).astype(int)
    disc_vforw_I = (vforw_I*rescale_val).astype(int)
    disc_vforw_Q = (vforw_Q*rescale_val).astype(int)
    disc_vrefl_I = (vrefl_I*rescale_val).astype(int)
    disc_vrefl_Q = (vrefl_Q*rescale_val).astype(int)

    if show_detuning:
        plt.figure()
        plt.plot(detuning)
        plt.draw()
        input("press Enter")

    if ofile:
        dictfile = {}
        dictfile["CAVITY_NAME"] = cavity_name
        dictfile["TIME_TRACE"] = cavity_sig_time
        dictfile["DISC_PROBE_I_TRACE"] =  disc_probe_I
        dictfile["DISC_PROBE_Q_TRACE"] =  disc_probe_Q
        dictfile["DISC_FORWARD_I_TRACE"] = disc_vforw_I
        dictfile["DISC_FORWARD_Q_TRACE"] = disc_vforw_Q
        dictfile["DETUNING_TRACE"] = detuning
        dictfile["BW_TRACE"] = np.zeros(samples)
        dictfile["QL_TRACE"] = np.ones(samples)*cav_ql
        dictfile["BW_CALIBRATION"] = cavity_frequency*0.5/cav_ql
        print("BW calib: ", dictfile["BW_CALIBRATION"])
        dfile.write(ofile, dictfile)

    if show_calib:
        plt.figure()
        plt.plot(probe_I, label="probe I")
        plt.plot(probe_Q, label="probe I")
        plt.plot(vforw_I+vrefl_I, label="virtual probe I")
        plt.plot(vforw_Q+vrefl_Q, label="virtual probe I")
        plt.legend()
        plt.draw()
        plt.figure()
        plt.ylim(-1000, 1000)
        plt.plot(cavity_sig_time, detuning)
        plt.draw()
        input("press enter")



if __name__ == "__main__":
    main.run()
