#!/usr/bin/env python
'''
    Show the modulation curve of a file
'''

from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    ifile = params['ifile']
    dictfile = dfile.read(ifile)

    just_cavity = params.get('just_cavity', None) 
    exclude_cavity = params.get('exclude_cavity', None) 

    fig_vforw, (ax_vforw_amp, ax_vforw_pha) = plt.subplots(2, 1, True)
    fig_vrefl, (ax_vrefl_amp, ax_vrefl_pha) = plt.subplots(2, 1, True)
    fig_probe, (ax_probe_amp, ax_probe_pha) = plt.subplots(2, 1, True)
    
    time_trace = dictfile['TIME_TRACE']

    fig_vforw.suptitle("Forward")
    fig_vrefl.suptitle("Reflected")
    fig_probe.suptitle("Probe")

    ax_vforw_amp.set_ylabel('Amplitude (a.u.)')
    ax_vrefl_amp.set_ylabel('Amplitude (a.u.)')
    ax_probe_amp.set_ylabel('Amplitude (a.u.)')
    ax_vforw_pha.set_ylabel('Phase (deg)')
    ax_vrefl_pha.set_ylabel('Phase (deg)')
    ax_probe_pha.set_ylabel('Phase (deg)')

    ax_vforw_pha.set_xlabel('Time (sec)')
    ax_vrefl_pha.set_xlabel('Time (sec)')
    ax_probe_pha.set_xlabel('Time (sec)')

    
    for i in range(8):
        if ((just_cavity == None) or (just_cavity == i+1)) and (exclude_cavity != i+1):
            ax_vforw_amp.plot(time_trace, dictfile['VFORW_AMP' + str(i+1)], label="Cavity " + str(i+1))
            ax_vrefl_amp.plot(time_trace, dictfile['VREFL_AMP' + str(i+1)], label="Cavity " + str(i+1))
            ax_probe_amp.plot(time_trace, dictfile['PROBE_AMP' + str(i+1)], label="Cavity " + str(i+1))
            ax_vforw_pha.plot(time_trace, dictfile['VFORW_PHA' + str(i+1)])
            ax_vrefl_pha.plot(time_trace, dictfile['VREFL_PHA' + str(i+1)])
            ax_probe_pha.plot(time_trace, dictfile['PROBE_PHA' + str(i+1)])

    ax_vforw_amp.legend()
    ax_vrefl_amp.legend()
    ax_probe_amp.legend()

    input('Insert something')

if __name__ == '__main__':
    main.run()
