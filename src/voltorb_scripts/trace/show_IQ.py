#!/usr/bin/env python3
'''
    shows an AP channel as IQ
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    amplifier_output_amplitude_addr = params['amplifier_out_amp_addr']
    amplifier_output_phase_addr = params['amplifier_out_pha_addr']

    delay = 0.1
    plt.ion()
    fig = plt.figure()
    ax = plt.subplot(1,1,1)
    amp = csys.read(amplifier_output_amplitude_addr)
    phase = csys.read(amplifier_output_phase_addr)
    idata = amp*np.cos(np.pi/180*phase)
    qdata = amp*np.sin(np.pi/180*phase)
    plot, = ax.plot(idata, label='I channel')
    qplot, = ax.plot(qdata, label='Q channel')
    ax.legend()

    while True:
        amp = csys.read(amplifier_output_amplitude_addr)
        phase = csys.read(amplifier_output_phase_addr)
        idata = amp*np.cos(np.pi/180*phase)
        qdata = amp*np.sin(np.pi/180*phase)
        iplot.set_ydata(idata)
        qplot.set_ydata(qdata)
        ax.relim()
        ax.autoscale_view()
        fig.canvas.flush_events()
        time.sleep(delay)

if __name__ == "__main__":
    main.run()

