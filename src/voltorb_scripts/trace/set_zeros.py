#!/usr/bin/env python
'''
    Set zeros on the correction tables
'''
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    corr_table_i_addr = params['corr_table_I_addr']
    corr_table_q_addr = params['corr_table_Q_addr']
    table_len = len(csys.read(corr_table_i_addr))

    csys.write(corr_table_i_addr, [0]*table_len)
    csys.write(corr_table_q_addr, [0]*table_len)

if __name__ == "__main__":
    main.run()
