#!/usr/bin/env python
'''
    Show the modulation curve of a file
'''

from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.matplotlib import pyplot as plt
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    ifile = params['ifile']
    dictfile = dfile.read(ifile)
    skip = 0
    skip_back = 0

    if 'skip' in params:
        skip = params['skip']

    if 'skip_back' in params:
        skip_back = params['skip_back']

    corr_table_in = dictfile['CORR_TABLE_I'][skip:-skip_back-1]
    output_amplitude = dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'][skip:-skip_back-1]
    output_phase = dictfile['AMPLIFIER_OUTPUT_PHASE'][skip:-skip_back-1]

    input_amplitude = dictfile['AMPLIFIER_INPUT_AMPLITUDE'][skip:-skip_back-1]
    input_phase = dictfile['AMPLIFIER_INPUT_PHASE'][skip:-skip_back-1]

    vm_output_i = dictfile['VM_OUTPUT_I'][skip:-skip_back-1]
    vm_output_q = dictfile['VM_OUTPUT_Q'][skip:-skip_back-1]

    _, plot_amplitude = plt.subplots()
    plot_amplitude.set_title('IOT output amplitude vs VM value')
    plot_amplitude.set_xlabel('VM value [AU]')
    plot_amplitude.set_ylabel('IOT output amplitude [AU]')
    plot_amplitude.plot(corr_table_in, output_amplitude)

    _, plot_phase = plt.subplots()
    plot_phase.set_title('IOT output phase vs VM value')
    plot_phase.set_xlabel('VM value [AU]')
    plot_phase.set_ylabel('IOT output phase [AU]')
    plot_phase.plot(corr_table_in, output_phase)

    _, plot_amplitude = plt.subplots()
    plot_amplitude.set_title('IOT input amplitude vs VM value')
    plot_amplitude.set_xlabel('VM value [AU]')
    plot_amplitude.set_ylabel('IOT input amplitude [AU]')
    plot_amplitude.plot(corr_table_in, input_amplitude)

    _, plot_phase = plt.subplots()
    plot_phase.set_title('IOT input phase vs VM value')
    plot_phase.set_xlabel('VM value [AU]')
    plot_phase.set_ylabel('IOT input phase [AU]')
    plot_phase.plot(corr_table_in, input_phase)

    _, plot_iq = plt.subplots()
    plot_iq.set_title('LLRF VM loopback channels')
    plot_iq.set_xlabel('VM value [AU]')
    plot_iq.plot(corr_table_in, vm_output_i, label='Channel I')
    plot_iq.plot(corr_table_in, vm_output_q, label='Channel Q')

    input('Insert something')

if __name__ == '__main__':
    main.run()
