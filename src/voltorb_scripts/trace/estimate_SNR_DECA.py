'''
    Script to evaluate the SNR of DECA (for CMTB)
'''
import time
from datetime import datetime
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import ap_to_cplx, cplx_to_ap, butter_lowpass_filter
from voltorb.math import calculate_ql 
from voltorb.calibrate import calibrate_abcd_scan
from voltorb.matplotlib import pyplot as plt
from voltorb.bwdet import bwdet
from voltorb import dictfile

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    fit_degree = params.get("fit_degree", 3)
    decay_start = params.get("decay_start", 0.550997986)
    fit_decay_range = params.get('fit_decay_range', (0.57, 0.70))
    fit_pulse_range = params.get('fit_pulse_range', (0.40, 0.53))
    decay_range = params.get('decay_range', (0.60, 1.0))
    pulse_range = params.get('pulse_range', (0.2, 0.5))
    cutoff_range = params.get('cutoff_range', (1E3, 4.5E6))
    cutoff_orig = params.get('cutoff_orig', 5E5)
    cutoff_points = params.get('cutoff_points', 100)
    time_shift = params.get('time_shift', 0.0001685)

    frequency = params.get('frequency', 1.3E9)
    

    
    k_add_steps = params.get('k_add_steps', 100)
    k_add_min = params.get('k_add_min', 0.001)
    k_add_max = params.get('k_add_max', 0.005)

    ifile = params['ifile']

    SNR = []
    cutoffs = []

    dfile = dictfile.read(ifile)

    vforw_pha = np.mean(dfile["vforw_pha_raw"], 0)
    vforw_amp = np.mean(dfile["vforw_amp_raw"], 0)
    probe_pha = np.mean(dfile["probe_pha_raw"], 0)
    probe_amp = np.mean(dfile["probe_amp_raw"], 0)
    vrefl_pha = np.mean(dfile["vrefl_pha_raw"], 0)
    vrefl_amp = np.mean(dfile["vrefl_amp_raw"], 0)

    vforw_cmplxo = vforw_amp*np.exp(1.0j*vforw_pha*np.pi/180)
    probe_cmplxo = probe_amp*np.exp(1.0j*probe_pha*np.pi/180)
    vrefl_cmplxo = vrefl_amp*np.exp(1.0j*vrefl_pha*np.pi/180)
    
    trace_time = dfile["time_trace"][-1] - dfile["time_trace"][0]
    fs = len(probe_amp)/trace_time
    print(fs)

    for cutoff in np.exp(np.linspace(np.log(cutoff_range[0]), np.log(cutoff_range[1]), cutoff_points)):
        cutoffs.append(cutoff)
        print(cutoff)
        probe_cmplx = butter_lowpass_filter(probe_cmplxo, 
                                            cutoff_orig, 
                                            fs=fs, order=1)

        vforw_cmplx = butter_lowpass_filter(vforw_cmplxo, 
                                            cutoff_orig, 
                                            fs=fs, order=1)

        vrefl_cmplx = butter_lowpass_filter(vrefl_cmplxo, 
                                            cutoff_orig, 
                                            fs=fs, order=1)


        amp_decay = probe_amp[[int(decay_range[0]*len(probe_amp)),
                               int(decay_range[1]*len(probe_amp))-1]]

        decay_time = (decay_range[1] -
                      decay_range[0]) * trace_time

        ql = calculate_ql(decay_time, amp_decay, frequency)

        calib, bwdetres, t3d = calibrate_abcd_scan(probe_cmplx,
                                                   vforw_cmplx,
                                                   vrefl_cmplx,
                                                   ql,
                                                   trace_time,
                                                   decay_start,
                                                   fit_decay_range,
                                                   fit_pulse_range,
                                                   decay_range,
                                                   pulse_range,
                                                   k_add_min=k_add_min,
                                                   k_add_max=k_add_max,
                                                   k_add_steps=k_add_steps,
                                                   fit_degree=fit_degree,
                                                   frequency=frequency)

        probe_cmplx_a = butter_lowpass_filter(probe_cmplxo, 
                                              cutoff, 
                                              fs=fs, order=1)

        vforw_cmplx_a = butter_lowpass_filter(vforw_cmplxo, 
                                              cutoff, 
                                              fs=fs, order=1)

        vrefl_cmplx_a = butter_lowpass_filter(vrefl_cmplxo, 
                                              cutoff, 
                                              fs=fs, order=1)


        (a, b, c, d) = calib
        
        vforw_cmplx_a =  vforw_cmplx_a*a + vrefl_cmplx_a*b

        bwres = bwdet(probe_cmplx_a, vforw_cmplx_a, fs, ql)

        #plt.figure()
        #plt.plot(bwres[0])
        #plt.draw()

        bw = bwres[0][int(pulse_range[0]*len(vforw_cmplx)):int(decay_range[1]*len(vforw_cmplx))]
        SNR.append(np.mean(bw)/np.std(bw))
        print(SNR[-1])


    fig_SNR, ax_SNR = plt.subplots(1, 1)

    ax_SNR.set_ylabel("Magnitude (dB)")
    
    ax_SNR.set_xlabel("Cutoff frequency (kHz)")

    ax_SNR.tick_params(which="major",
                      bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_SNR.tick_params(which="minor",
                      bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")


    plt.semilogx(np.array(cutoffs)/1e3, 20*np.log10(SNR), 'ro-')
    plt.grid(which="major", axis="both")
    plt.grid(which="minor", axis="both", alpha=0.2, linestyle="--")
    plt.draw()

    input("Press return..")
 
if __name__ == '__main__':
    main.run()


