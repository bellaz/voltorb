#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity
'''

import time
import numpy as np
import schedule
import atexit
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ofile = params.get('ofile', 'out.h5')
    delay = params.get('delay', 1)
    measurements = params.get('measurements', 3600)

    cavity_name = params['cavity_name'] 
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    piezo_voltage_addr = params['piezo_voltage_addr']

    dictfile = {}
    dictfile['TIME'] = []
    dictfile['TIMESTAMP'] = []
    dictfile['CAVITY_NAME'] = cavity_name
    dictfile['PROBE_AMP'] = []
    dictfile['PROBE_PHA'] = []
    dictfile['VFORW_AMP'] = []
    dictfile['VFORW_PHA'] = []
    dictfile['VREFL_AMP'] = []
    dictfile['VREFL_PHA'] = []
    dictfile['PIEZO_VOLTAGE'] = []

    time0 = time.time()

    def measure():
        measure.counter += 1

        if measure.counter > measurements:
            save_data()
            exit()

        print("Tick", measure.counter, "/", measurements)

        try:
            [probe_amp, probe_pha, vforw_amp, 
                    vforw_pha, vrefl_amp, vrefl_pha] = csys.read(probe_amp_addr, 
                                                                 probe_pha_addr,
                                                                 vforw_amp_addr, 
                                                                 vforw_pha_addr,
                                                                 vrefl_amp_addr, 
                                                                 vrefl_pha_addr)

            piezo_voltage = csys.read(piezo_voltage_addr)

            dictfile['TIME'].append(time.time()-time0)
            dictfile['TIMESTAMP'].append(time.time())
            dictfile['PROBE_AMP'].append(np.mean(probe_amp))
            dictfile['PROBE_PHA'].append(np.mean(probe_pha))
            dictfile['VFORW_AMP'].append(np.mean(vforw_amp))
            dictfile['VFORW_PHA'].append(np.mean(vforw_pha))
            dictfile['VREFL_AMP'].append(np.mean(vrefl_amp))
            dictfile['VREFL_PHA'].append(np.mean(vrefl_pha))
            dictfile['PIEZO_VOLTAGE'].append(np.mean(piezo_voltage)) 

        except Exception as e:
            exit()

    measure.counter = 0

    def save_data():
        dfile.write(ofile, dictfile)

    atexit.register(save_data)

    schedule.every(delay).seconds.do(measure)

    while True:
        schedule.run_pending()
        time.sleep(delay*0.1)

if __name__ == "__main__":
    main.run()
