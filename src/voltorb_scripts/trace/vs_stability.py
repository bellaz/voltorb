#!/usr/bin/env python
'''
    Grab VS
'''
import time
import numpy as np
from voltorb import controlsys as csys
import voltorb.dictfile as dfile
from voltorb.parse_cmdline import parser_default
from voltorb.matplotlib import pyplot as plt

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    input_file = params['ifile']
    
    sample_freq_mhz = params.get('sample_freq_mhz', 9)
    sample_start = int(params.get('start_us', 0) * sample_freq_mhz)
    sample_stop =  int(params.get('stop_us', -1) * sample_freq_mhz)

    measurement_amplitudes = []
    measurement_phases = []

    dictfile = dfile.read(input_file)
    amps = dictfile['VS_OUTPUT_AMPLITUDE'][:, sample_start:sample_stop]
    phas = dictfile['VS_OUTPUT_PHASE'][:, sample_start:sample_stop]

    pulses_amp_avg = np.mean(np.mean(amps, axis=1))
    total_amp_avg = np.mean(pulses_amp_avg)

    print('intra pulse amp error STD [%]   ', 
          np.mean(100*np.std(amps, axis=1)/pulses_amp_avg)) 
    print('intra pulse pha error STD [deg] ',
          np.mean(np.std(phas, axis=1)))
    print('intra pulse amp error ptp [%]   ',
          np.mean(100*np.ptp(amps, axis=1)/pulses_amp_avg))
    print('intra pulse pha error ptp [deg] ', 
          np.mean(np.ptp(phas, axis=1)))
    print('pulse to pulse amp err STD [%]  ',
          np.std(100*np.mean(amps, axis=1))/total_amp_avg)
    print('pulse to pulse pha err STD [deg]',
          np.std(np.mean(phas, axis=1)))
    print('pulse to pulse amp err ptp [%]  ',
          np.ptp(100*np.mean(amps, axis=1))/total_amp_avg)
    print('pulse to pulse pha err ptp [deg]',
          np.ptp(np.mean(phas, axis=1)))

    avg_amp_pulses = np.mean(amps, axis=0)
    avg_pha_pulses = np.mean(phas, axis=0)

    plt.figure()
    plt.plot(avg_amp_pulses)
    plt.figure()
    plt.plot(avg_pha_pulses)
    
    input('Press Enter..')

if __name__ == "__main__":
    main.run()
