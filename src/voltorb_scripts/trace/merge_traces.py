#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity
'''

from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    filename_output = params['ofile']

    idx = 1

    read_file = dfile.read(params['ifile1'])
    write_file = {}

    write_file['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'] = read_file['AMPLIFIER_OUTPUT_AMPLITUDE_AVG']
    write_file['AMPLIFIER_OUTPUT_PHASE_AVG'] = read_file['AMPLIFIER_OUTPUT_PHASE_AVG']

    while True:
        idx += 1
        if 'ifile{}'.format(idx) not in params:
            break

        read_file = dfile.read(params['ifile{}'.format(idx)])
        write_file['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'] += read_file['AMPLIFIER_OUTPUT_AMPLITUDE_AVG']
        write_file['AMPLIFIER_OUTPUT_PHASE_AVG'] += read_file['AMPLIFIER_OUTPUT_PHASE_AVG']


    write_file['AMPLIFIER_OUTPUT_AMPLITUDE_AVG'] /= (idx-1)
    write_file['AMPLIFIER_OUTPUT_PHASE_AVG'] /= (idx-1)
    dfile.write(filename_output, write_file)

if __name__ == "__main__":
    main.run()
