#!/usr/bin
'''
    Set batman on correction tables in IQ
'''
import numpy as np
from voltorb import controlsys as csys
from voltorb.batman import batman_up, batman_down
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    scale_factor = params['scale_factor']
    addr_corr_table_i = params['addr_corr_table_I']
    addr_corr_table_q = params['addr_corr_table_Q']

    table_len = len(csys.read(addr_corr_table_i))

    table_i = []
    table_q = []

    for i in np.linspace(-7, 7, table_len):
        table_i.append(batman_up(i)/3*scale_factor)
        table_q.append(batman_down(i)/3*scale_factor)

    csys.write(addr_corr_table_i, table_i)
    csys.write(addr_corr_table_q, table_q)

if __name__ == "__main__":
    main.run()
