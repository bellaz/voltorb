#!/usr/bin/env python
'''
    Grab VS
'''
import time
import numpy as np
from voltorb import controlsys as csys
import voltorb.dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    output_file = params['ofile']
    vs_amp_addr = params['vs_amp_addr']
    vs_pha_addr = params['vs_pha_addr']
    measurements = params.get('measurements', 10)
    delay = params.get('delay', 1)

    measurement_amplitudes = []
    measurement_phases = []

    for j in range(measurements):
        print('Measure', str(j+1) + '/' + str(measurements))
        time.sleep(delay)
        measurement_amplitudes.append(csys.read(vs_amp_addr))
        measurement_phases.append(csys.read(vs_pha_addr))


    dictfile = {}
    dictfile['VS_OUTPUT_AMPLITUDE'] = np.array(measurement_amplitudes)
    dictfile['VS_OUTPUT_AMPLITUDE_AVG'] = np.mean(dictfile['VS_OUTPUT_AMPLITUDE'],
                                                           axis=0)
    dictfile['VS_OUTPUT_PHASE'] = np.array(measurement_phases)
    dictfile['VS_OUTPUT_PHASE_AVG'] = np.mean(dictfile['VS_OUTPUT_PHASE'], axis=0)

    dfile.write(output_file, dictfile)

if __name__ == "__main__":
    main.run()
