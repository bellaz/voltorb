#!/usr/bin/env python
'''
    Measures the VM -> Amplifier output characteristics varying the CW amplitude (high to low)
'''

import numpy as np
import time
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    delay = params['delay']
    filename_output = params['ofile']
    measurements = params['measurements']
    sp_amplitude_addr = params['sp_amp_addr']
    ff_table_i_addr = params['ff_table_I_addr']
    corr_enable_addr = params['corr_enable_addr']
    corr_table_i_addr = params['corr_table_I_addr']
    corr_table_q_addr = params['corr_table_Q_addr']
    vm_output_i_addr = params['vm_output_I_addr']
    vm_output_q_addr = params['vm_output_Q_addr']
    vm_output_amp_addr = params['vm_output_amp_addr']
    vm_output_pha_addr = params['vm_output_pha_addr']
    amplifier_input_amplitude_addr = params['amplifier_in_amp_addr']
    amplifier_input_phase_addr = params['amplifier_in_pha_addr']
    amplifier_output_amplitude_addr = params['amplifier_out_amp_addr']
    amplifier_output_phase_addr = params['amplifier_out_pha_addr']
    iot_input_power_addr = params['iot_input_power_addr']
    iot_output_power_addr = params['iot_output_power_addr']

    measurement_fractions = np.linspace(1, 1/measurements, measurements)

    print('Waiting ', delay, 'seconds to warm up the IOT')

    ff_table_i = csys.read(ff_table_i_addr)
    ff_table_len = len(ff_table_i)
    ff_table_avg = np.mean(ff_table_i)


    initial_amp = csys.read(sp_amplitude_addr)
    csys.write(sp_amplitude_addr, 0)
    csys.write(corr_enable_addr, 1)

    dictfile = {}
    dictfile['CW_IOT_IN_W'] = []
    dictfile['CW_IOT_OUT_W'] = []
    dictfile['CORR_TABLE_I'] = []
    dictfile['CORR_TABLE_Q'] = []
    dictfile['VM_OUTPUT_I'] = []
    dictfile['VM_OUTPUT_Q'] = []
    dictfile['VM_OUTPUT_AMPLITUDE'] = []
    dictfile['VM_OUTPUT_PHASE'] = []
    dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'] = []
    dictfile['AMPLIFIER_OUTPUT_PHASE'] = []
    dictfile['AMPLIFIER_INPUT_AMPLITUDE'] = []
    dictfile['AMPLIFIER_INPUT_PHASE'] = []

    for frac in measurement_fractions:

        table_new = [int(ff_table_avg * frac)] * ff_table_len
        csys.write(corr_table_i_addr, table_new)
        print('done {} %'.format(int((1-frac)*100)))
        time.sleep(delay)
        dictfile['CW_IOT_IN_W'].insert(0, csys.read(iot_input_power_addr))
        dictfile['CW_IOT_OUT_W'].insert(0, csys.read(iot_output_power_addr))
        dictfile['CORR_TABLE_I'].insert(0, np.mean(csys.read(corr_table_i_addr)))
        dictfile['CORR_TABLE_Q'].insert(0, np.mean(csys.read(corr_table_q_addr)))
        dictfile['VM_OUTPUT_I'].insert(0, np.mean(csys.read(vm_output_i_addr)))
        dictfile['VM_OUTPUT_Q'].insert(0, np.mean(csys.read(vm_output_q_addr)))
        dictfile['VM_OUTPUT_AMPLITUDE'].insert(0, np.mean(csys.read(vm_output_amp_addr)))
        dictfile['VM_OUTPUT_PHASE'].insert(0, np.mean(csys.read(vm_output_pha_addr)))

        dictfile['AMPLIFIER_OUTPUT_AMPLITUDE'].insert(0, np.mean(csys.read(amplifier_output_amplitude_addr)))
        dictfile['AMPLIFIER_OUTPUT_PHASE'].insert(0, np.mean(csys.read(amplifier_output_phase_addr)))
        dictfile['AMPLIFIER_INPUT_AMPLITUDE'].insert(0, np.mean(csys.read(amplifier_input_amplitude_addr)))
        dictfile['AMPLIFIER_INPUT_PHASE'].insert(0, np.mean(csys.read(amplifier_input_phase_addr)))


    dfile.write(filename_output, dictfile)
    csys.write(corr_enable_addr, 0)
    csys.write(corr_table_i_addr, [0]*ff_table_len)
    csys.write(sp_amplitude_addr, initial_amp)

if __name__ == "__main__":
    main.run()
