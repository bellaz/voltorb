#!/usr/bin/env python
'''
    Grab amplitude and phase from a cavity
'''

import time
import numpy as np
from voltorb import controlsys as csys
from voltorb import dictfile as dfile
from voltorb.parse_cmdline import parser_default

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    ofile = params.get('ofile', 'out.h5')
    
    probe_amp_addr = "doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/PROBE.AMPL&spectrum=\"data\""
    probe_pha_addr = "doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/PROBE.PHASE&spectrum=\"data\""
    vforw_amp_addr = "doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VFORW.AMPL&spectrum=\"data\""
    vforw_pha_addr = "doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VFORW.PHASE&spectrum=\"data\""
    vrefl_amp_addr = "doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VREFL.AMPL&spectrum=\"data\""
    vrefl_pha_addr = "doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VREFL.PHASE&spectrum=\"data\""

    addresses = []

    for i in range(8):
        addresses.extend([
            probe_amp_addr.format(i+1),
            probe_pha_addr.format(i+1),
            vforw_amp_addr.format(i+1),
            vforw_pha_addr.format(i+1),
            vrefl_amp_addr.format(i+1),
            vrefl_pha_addr.format(i+1)])

    traces = csys.read(*addresses)
    
    dictfile = {}
    dictfile["TIME_TRACE"] = np.linspace(0.0, 1.0, len(traces[0]))

    for i in range(8):
        dictfile['PROBE_AMP' + str(i+1)] = traces[i*6 + 0] 
        dictfile['PROBE_PHA' + str(i+1)] = traces[i*6 + 1]
        dictfile['VFORW_AMP' + str(i+1)] = traces[i*6 + 2]
        dictfile['VFORW_PHA' + str(i+1)] = traces[i*6 + 3]
        dictfile['VREFL_AMP' + str(i+1)] = traces[i*6 + 4]
        dictfile['VREFL_PHA' + str(i+1)] = traces[i*6 + 5]
 
    dfile.write(ofile, dictfile)

if __name__ == "__main__":
    main.run()
