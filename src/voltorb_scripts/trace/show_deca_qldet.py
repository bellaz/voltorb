'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
from matplotlib.ticker import AutoMinorLocator
from datetime import datetime
import numpy as np
import matplotlib.ticker as ticker
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import ap_to_cplx, cplx_to_ap, butter_lowpass_filter
from voltorb.math import calculate_ql 
from voltorb.calibrate import calibrate_abcd_scan
from voltorb.matplotlib import pyplot as plt
from voltorb.bwdet import bwdet
from voltorb import dictfile
from scipy.stats import pearsonr

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    skip = params.get("skip", 0)
    fit_degree = params.get("fit_degree", 3)
    decay_start = params.get("decay_start", 0.550997986)
    fit_decay_range = params.get('fit_decay_range', (0.57, 0.70))
    fit_pulse_range = params.get('fit_pulse_range', (0.40, 0.53))
    decay_range = params.get('decay_range', (0.6, 1.0))
    pulse_range = params.get('pulse_range', (0.05, 0.5))
    cutoff = params.get('cutoff', 1E5)
    time_shift = params.get('time_shift', 0.0001685)

    starting_trace = params.get("starting_trace", 0.25)
    decimation = params.get('decimation', 200)
    frequency = params.get('frequency', 1.3E9)
    
    k_add_steps = params.get('k_add_steps', 100)
    k_add_min = params.get('k_add_min', 0.001)
    k_add_max = params.get('k_add_max', 0.005)

    titles = params.get('titles', None)
    fig_bw, ax_bw = plt.subplots(1, 1)
    fig_det, ax_det = plt.subplots(1, 1)
    
    fig_bw_error, ax_bw_error = plt.subplots(1, 1)
    fig_det_error, ax_det_error = plt.subplots(1, 1)
    fig_res, ax_res = plt.subplots(1, 1)

    test_det = []
    test_bw = []

    bw_error_mean = []
    bw_error_mse = []

    det_error_mean = []
    det_error_mse = []

    ax_bw.set_xlabel("Time (s)")
    ax_det.set_xlabel("Time (s)")

    ax_bw.ticklabel_format(axis='x', style="sci", scilimits=(0,0))
    ax_det.ticklabel_format(axis='x', style="sci", scilimits=(0,0))

    ax_bw.set_ylabel("Bandwidth (Hz)")
    ax_det.set_ylabel("Detuning (Hz)")

    ax_bw_error.set_ylabel("Error (%)")
    ax_det_error.set_ylabel("Error (Hz)")

    ax_bw.tick_params(which="both",
                      bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_det.tick_params(which="both",
                      bottom=True, 
                      top=True, 
                      left=True, 
                      right=True, 
                      axis="both", 
                      direction="in")

    ax_bw_error.tick_params(which="both",
                            bottom=True, 
                            top=True, 
                            left=True, 
                            right=True, 
                            axis="both", 
                            direction="in")
                                  
    ax_det_error.tick_params(which="both",
                             bottom=True, 
                             top=True, 
                             left=True, 
                             right=True,         
                             axis="both", 
                             direction="in")

    i = 1

    ax_bw.xaxis.set_minor_locator(AutoMinorLocator())
    ax_det.xaxis.set_minor_locator(AutoMinorLocator())
    ax_bw.yaxis.set_minor_locator(AutoMinorLocator())
    ax_det.yaxis.set_minor_locator(AutoMinorLocator())


    while "ifile" + str(i) in params:
        dfile = dictfile.read(params["ifile" + str(i)])
        i += 1

        bw = dfile["bandwidth"] 
        bw_mean = np.mean(bw, 0)
        bw_std = np.std(bw, 0)
        det_mean = np.mean(dfile["detuning"], 0)
        det_std = np.std(dfile["detuning"], 0)
        dec_idx = range(0, len(bw_mean), decimation)

        if(titles):
            label=titles[i-2]
        
        ax_bw.errorbar(dfile["time_trace"][dec_idx]-time_shift, 
                       bw_mean[dec_idx],
                       bw_std[dec_idx],
                       fmt='o',
                       markersize=4,
                       capsize=4,
                       label=label)

        ax_det.errorbar(dfile["time_trace"][dec_idx]-time_shift, 
                        -det_mean[dec_idx]*0.5,
                        det_std[dec_idx]*0.5,
                        fmt='o',
                        markersize=4,
                        capsize=4,
                        label=label)


        vforw_pha = np.mean(dfile["vforw_pha_raw"], 0)
        vforw_amp = np.mean(dfile["vforw_amp_raw"], 0)
        probe_pha = np.mean(dfile["probe_pha_raw"], 0)
        probe_amp = np.mean(dfile["probe_amp_raw"], 0)
        vrefl_pha = np.mean(dfile["vrefl_pha_raw"], 0)
        vrefl_amp = np.mean(dfile["vrefl_amp_raw"], 0)

        vforw_cmplx = vforw_amp*np.exp(1.0j*vforw_pha*np.pi/180)
        probe_cmplx = probe_amp*np.exp(1.0j*probe_pha*np.pi/180)
        vrefl_cmplx = vrefl_amp*np.exp(1.0j*vrefl_pha*np.pi/180)
        
        trace_time = dfile["time_trace"][-1] - dfile["time_trace"][0]
        print("time..", trace_time-time_shift)
        
        red_trace = dfile["time_trace"][dfile["time_trace"] < time_shift]
        analysis_len = len(red_trace)

        fs = len(probe_amp)/trace_time

        probe_cmplx = butter_lowpass_filter(probe_cmplx, 
                                            cutoff, 
                                            fs=fs)

        vforw_cmplx = butter_lowpass_filter(vforw_cmplx, 
                                            cutoff, 
                                            fs=fs)

        vrefl_cmplx = butter_lowpass_filter(vrefl_cmplx, 
                                            cutoff, 
                                            fs=fs)


        amp_decay = probe_amp[[int(decay_range[0]*len(probe_amp)),
                               int(decay_range[1]*len(probe_amp))-1]]

        decay_time = (decay_range[1] -
                      decay_range[0]) * trace_time

        ql = calculate_ql(decay_time, amp_decay, frequency)

        #ax_bw.plot([dfile["time_trace"][0], dfile["time_trace"][-1]],
        #           np.ones(2)*frequency/ql)

        calib, bwdetres, t3d = calibrate_abcd_scan(probe_cmplx,
                                                   vforw_cmplx,
                                                   vrefl_cmplx,
                                                   ql,
                                                   trace_time,
                                                   decay_start,
                                                   fit_decay_range,
                                                   fit_pulse_range,
                                                   decay_range,
                                                   pulse_range,
                                                   k_add_min=k_add_min,
                                                   k_add_max=k_add_max,
                                                   k_add_steps=k_add_steps,
                                                   fit_degree=fit_degree,
                                                   frequency=frequency)

        label=None
        analysis_time = np.linspace(skip, trace_time-time_shift, analysis_len)
        det_analysis_meas = np.interp(analysis_time, dfile["time_trace"]-time_shift, det_mean)
        det_analysis_simu = -np.interp(analysis_time, dfile["time_trace"], bwdetres[1])
        det_residuals = det_analysis_meas - det_analysis_simu
        
        #ax_res.plot(analysis_time, det_analysis_meas)
        #ax_res.plot(analysis_time, det_analysis_simu)
        #hist, edges = np.histogram(det_residuals, 20)
        ax_res.hist(det_residuals, 20)
        print("> det mse <", np.sqrt(np.mean(det_residuals**2)))
        c, p = pearsonr(det_analysis_meas, det_analysis_simu)
        print("correlation", c, "p-value", p)
        #ax_res.plot(dfile["time_trace"]-time_shift, det_mean)
        #ax_res.plot(dfile["time_trace"], bwdetres[1]*0.5)

        ax_det.plot(dfile["time_trace"], bwdetres[1]*0.5)
        ax_bw.plot(dfile["time_trace"], bwdetres[0])

        (a, b, c, d) = calib
        vprobe_cmplx = (a+c)*vforw_cmplx+(b+d)*vrefl_cmplx
        #plt.figure()
        #plt.plot(vprobe_cmplx.real)
        #plt.plot(vprobe_cmplx.imag)
        #plt.plot(probe_cmplx.real)
        #plt.plot(probe_cmplx.imag)

        red_time_trace = (dfile["time_trace"]-time_shift)
        bool_trace = (red_time_trace >= starting_trace*red_time_trace[-1])
        red_time_trace = red_time_trace[bool_trace]
        bw = dfile["bandwidth"][0, bool_trace]
        det = 0.5*-dfile["detuning"][0, bool_trace]

        bw_interp = interp1d(dfile["time_trace"], bwdetres[0])(red_time_trace)
        det_interp = interp1d(dfile["time_trace"], bwdetres[1]*0.5)(red_time_trace)

        test_det.append(det_interp)
        test_det.append(det)
        test_bw.append(bw_interp)
        test_bw.append(bw)

        bw_diff = bw - bw_interp
        bw_ptp = np.ptp(bw)
        bw_error_mean.append(np.mean(bw_diff))
        bw_error_mse.append(np.sqrt(np.mean(bw_diff*bw_diff)))

        det_diff = det - det_interp
        det_error_mean.append(np.mean(det_diff))
        det_error_mse.append(np.sqrt(np.mean(det_diff*det_diff)))

        print(ql, frequency/ql)
        print("mean diff bw ", bw_error_mean[-1])
        print("std  diff bw ", bw_error_mse[-1])
        print("ptp  diff bw ", bw_ptp)
        print("Q0 quenc     ", 2*frequency/bw_ptp)
        print("mean diff det", det_error_mean[-1])
        print("std  diff det", det_error_mse[-1])

    if titles:
        ax_det.legend()
        ax_bw.legend()

    
    ax_bw_error.plot(bw_error_mean, label="Average error", marker="o", linestyle="none", markersize=12) 
    ax_bw_error.plot(bw_error_mse, label="MSE error", marker="*", linestyle="none", markersize=12) 
    ax_det_error.plot(det_error_mean, label="Average error", marker="o", linestyle="none", markersize=12)
    ax_det_error.plot(det_error_mse, label="MSE error", marker="*", linestyle="none", markersize=12)
    xes = list(range(len(titles)))
    
    ax_bw_error.xaxis.set_major_locator(ticker.FixedLocator(xes))
    ax_det_error.xaxis.set_major_locator(ticker.FixedLocator(xes))
    
    ax_bw_error.xaxis.set_major_formatter(ticker.FixedFormatter(xes))
    ax_det_error.xaxis.set_major_formatter(ticker.FixedFormatter(xes))
    ax_bw_error.set_xticklabels(titles, rotation=30)
    ax_det_error.set_xticklabels(titles, rotation=30)

    ax_bw_error.legend()
    ax_det_error.legend()
    
    plt.figure()

    for el in test_bw:
        plt.plot(el)

    plt.figure()

    for el in test_det:
        plt.plot(el)

    plt.draw()
    input("Press return..")
 
if __name__ == '__main__':
    main.run()


