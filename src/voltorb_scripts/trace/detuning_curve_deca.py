import numpy as np
from scipy.optimize import curve_fit
from voltorb.parse_cmdline import parser_default
from voltorb.math import ap_to_cplx, cplx_to_ap, butter_lowpass_filter
from voltorb.math import calculate_ql 
from voltorb.calibrate import calibrate_abcd_scan
from voltorb.matplotlib import pyplot as plt
from voltorb.bwdet import bwdet
from voltorb import dictfile

def resonance_curve_amp(detuning, bwidth, amp):
    return amp/np.sqrt(1.0 + np.square(2.0*detuning/bwidth))

def resonance_curve_phase(detuning, bwidth):
    return np.arctan(2.0*detuning/bwidth)*180.0/np.pi

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''

    fig_det_amp, ax_det_amp = plt.subplots(1, 1)
    fig_det_pha, ax_det_pha = plt.subplots(1, 1)

    frequency = params.get("frequency", 1.3E9)
    detuning_points = params.get("detuning_points", 100)

    amp = []
    amp_std = []
    phase = []
    phase_std = []
    det = []
    det_std = []

    ax_det_pha.tick_params(bottom=True, 
                           top=True, 
                           left=True, 
                           right=True, 
                           axis="both", 
                           direction="in")


    ax_det_amp.tick_params(bottom=True, 
                           top=True, 
                           left=True, 
                           right=True, 
                           axis="both", 
                           direction="in")

    ax_det_pha.set_xlabel("Detuning (Hz)")
    ax_det_amp.set_xlabel("Detuning (Hz)")
    ax_det_pha.set_ylabel("Phase (deg)")
    ax_det_amp.set_ylabel("Amplitude (a.u.)")

    i = 1
    ql = 0

    while "ifile" + str(i) in params:
        dfile = dictfile.read(params["ifile" + str(i)])
        i += 1
        
        det.append(np.mean(dfile["detuning"])*0.5)
        det_std.append(np.std(dfile["detuning"])*0.5)

        vforw_pha = dfile["vforw_pha_raw"]
        vforw_amp = dfile["vforw_amp_raw"]
        probe_pha = dfile["probe_pha_raw"]
        probe_amp = dfile["probe_amp_raw"]
        vrefl_pha = dfile["vrefl_pha_raw"]
        vrefl_amp = dfile["vrefl_amp_raw"]

        ql = dfile["QL"]

        b = dfile["b"]
        d = dfile["d"]

        vforw_cmplx_tmp = vforw_amp*np.exp(1.0j*vforw_pha*np.pi/180)
        vrefl_cmplx_tmp = vrefl_amp*np.exp(1.0j*vrefl_pha*np.pi/180)

        vforw_cmplx = vforw_cmplx_tmp + vrefl_cmplx_tmp*b/d
        (vforw_amp, vforw_pha) = cplx_to_ap(vforw_cmplx)

        amp.append(np.mean(probe_amp))
        amp_std.append(np.std(probe_amp))
        phase.append(np.mean(probe_pha - vforw_pha))
        phase_std.append(np.std(probe_pha - vforw_pha))

    amp_std = np.array(amp_std)/max(amp)
    amp = np.array(amp)/max(amp)
    phase = np.array(phase)
    phase_std = np.array(phase_std)
    det = np.array(det)
    det_std = np.array(det_std)  

    ax_det_amp.errorbar(det, amp, amp_std, det_std, 
                        fmt='o',
                        markersize=4,
                        capsize=4,
                        label="Measured points")
 
    ax_det_pha.errorbar(det, phase, phase_std, det_std,
                        fmt='o',
                        markersize=4,
                        capsize=4,
                        label="Measured points")

    popt, pcov = curve_fit(resonance_curve_amp, det, amp, p0=[100.0, 1.0])

    print(pcov)
    print(frequency/(popt[0]+pcov[0,0]))
    print(frequency/popt[0])
    print(frequency/(popt[0]-pcov[0,0]))
    print("pm = ", frequency * (0.5/(popt[0]-pcov[0,0]) - 0.5/(popt[0]+pcov[0,0])))
    print(ql)

    detuning_trace = np.linspace(min(det)*1.1, max(det)*1.1, detuning_points)

    ax_det_amp.plot(detuning_trace, resonance_curve_amp(detuning_trace, popt[0], popt[1]), label="Curve fit")
    #ax_det_amp.plot(detuning_trace, resonance_curve_amp(detuning_trace, popt[0]+pcov[0, 0], popt[1]))
    #ax_det_amp.plot(detuning_trace, resonance_curve_amp(detuning_trace, popt[0]-pcov[0, 0], popt[1]))
    ax_det_pha.plot(detuning_trace, resonance_curve_phase(detuning_trace, popt[0]), label="Curve fit")

    ax_det_amp.legend()
    ax_det_pha.legend()

    input("Press return..")
