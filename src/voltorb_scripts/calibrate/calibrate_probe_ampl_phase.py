'''
    Script to calibrate forward rotation signals in CW (for CMTB)
'''
import time
import numpy as np
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_amp_cal_addr = params['cavity_cal_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    probe_amp_scale_addr = params['probe_amp_scale_addr']
    probe_pha_rotation_addr = params['probe_pha_rotation_addr']

    probe_amp = np.mean(csys.read(probe_amp_addr))
    probe_amp_cal = csys.read(probe_amp_cal_addr)
    probe_pha = np.mean(csys.read(probe_pha_addr))
    orig_scale = csys.read(probe_amp_scale_addr)
    orig_rotation = csys.read(probe_pha_rotation_addr)
    csys.write(probe_pha_rotation_addr, phase_wrap(orig_rotation - probe_pha))
    print("probe cal", probe_amp_cal)
    print("probe uncal", probe_amp)
    csys.write(probe_amp_scale_addr, orig_scale*probe_amp_cal/(probe_amp*1e6))

if __name__ == '__main__':
    main.run()
