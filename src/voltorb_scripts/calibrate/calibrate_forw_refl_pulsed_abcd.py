'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
from datetime import datetime
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import ap_to_cplx, cplx_to_ap, butter_lowpass_filter
from voltorb.bwdet import bwdet
from voltorb.calibrate import calibrate_abcd
from voltorb.matplotlib import pyplot as plt

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    forward_amp_addr = params['vforw_amp_addr']
    forward_pha_addr = params['vforw_pha_addr']
    reflected_amp_addr = params['vrefl_amp_addr']
    reflected_pha_addr = params['vrefl_pha_addr']
    
    forward_amp_scale_addr = params['vforw_amp_scale_addr']
    forward_pha_rotation_addr = params['vforw_pha_rotation_addr']
    reflected_amp_scale_addr = params['vrefl_amp_scale_addr']
    reflected_pha_rotation_addr = params['vrefl_pha_rotation_addr']

    fit_deg = params.get("fit_deg", 3)
    decay_limit = params.get("decay_limit", 0.550997986)
    decay_fit_fraction = params.get('decay_fit_fraction', (0.57, 0.70))
    pulse_fit_fraction = params.get('pulse_fit_fraction', (0.40, 0.53))
    decay_fraction = params.get('decay_fraction', (0.6, 1.0))
    pulse_fraction = params.get('pulse_fraction', (0.05, 0.4))
    k_add_angles = params.get('k_add_angles', 100)
    cutoff = params.get('cutoff', 1E5)
    ql = params.get('ql', None)

    frequency = params.get('frequency', 1.3E9)
    measurements = params.get('measurements', 100)
    delay = params.get('delay', 1.0)
    trace_time = params.get('time_trace', 0.001813)
    
    amps = params.get('amps', 10)
    amp_min = params.get('amp_min', 0.1)
    amp_max = params.get('amp_max', 2)
    
    save_file = params.get('save_file', "out.h5")

    qls = []

    probe_amps = []
    probe_phas = []
    reflected_amps = []
    reflected_phas = []
    forward_amps = []
    forward_phas = []

    decay_start = 0
    decay_stop = 0
    pulse_start = 0
    pulse_stop = 0 

    for i in range(measurements):
        print('Performing measurement: ', i+1, '/', measurements)
        time.sleep(delay)

        probe_amp = csys.read(probe_amp_addr)
        probe_pha = csys.read(probe_pha_addr)
        forward_amp = csys.read(forward_amp_addr)
        forward_pha = csys.read(forward_pha_addr)
        reflected_amp = csys.read(reflected_amp_addr)
        reflected_pha = csys.read(reflected_pha_addr)

        decay_start = int(len(probe_amp) * decay_fraction[0])
        decay_stop = int(len(probe_amp) * decay_fraction[1])
        pulse_start = int(len(probe_amp) * pulse_fraction[0])
        pulse_stop = int(len(probe_amp) * pulse_fraction[1])

        probe_amps.append(probe_amp)
        probe_phas.append(probe_pha)
        forward_amps.append(forward_amp)
        forward_phas.append(forward_pha)
        reflected_amps.append(reflected_amp)
        reflected_phas.append(reflected_pha)

        decay_time = (decay_fraction[1] - decay_fraction[0]) * trace_time
        decay_probe_amp = probe_amp[decay_start:decay_stop]

        qlm = np.pi*frequency*decay_time/np.log(decay_probe_amp[0] /
                                               decay_probe_amp[-1])
        qls.append(qlm)

    if ql is None:
        ql = np.mean(qls)

    print(ql)

    probe_amp = np.mean(probe_amps, axis=0) 
    probe_pha = np.mean(probe_phas, axis=0)  
    vforw_amp = np.mean(forward_amps, axis=0)
    vforw_pha = np.mean(forward_phas, axis=0)
    vrefl_amp = np.mean(reflected_amps, axis=0)
    vrefl_pha = np.mean(reflected_phas, axis=0)

    probe_cmplx = ap_to_cplx(probe_amp, probe_pha)
    vforw_cmplx = ap_to_cplx(vforw_amp, vforw_pha)
    vrefl_cmplx = ap_to_cplx(vrefl_amp, vrefl_pha)

    print(len(probe_amp)/trace_time)

    probe_cmplx = butter_lowpass_filter(probe_cmplx, cutoff, 
                                        fs=len(probe_amp)/trace_time)
    vforw_cmplx = butter_lowpass_filter(vforw_cmplx, cutoff, 
                                        fs=len(probe_amp)/trace_time)
    vrefl_cmplx = butter_lowpass_filter(vrefl_cmplx, cutoff, 
                                        fs=len(probe_amp)/trace_time)
    a = 0
    b = 0
    c = 0
    d = 0

    #flat_diff = []
    #bw_diff = []
    #det_diff = []
    #k_adds = []

    amplitudes = np.linspace(amp_min, amp_max, amps)
    angles = np.linspace(0, 2*np.pi, k_add_angles)

    decay_limit_idx = int(len(probe_amp) * decay_limit)
    decay_fit_start = int(len(probe_amp) * decay_fit_fraction[0])
    decay_fit_stop = int(len(probe_amp) * decay_fit_fraction[1])
    pulse_fit_start = int(len(probe_amp) * pulse_fit_fraction[0])
    pulse_fit_stop = int(len(probe_amp) * pulse_fit_fraction[1])

    time_trace = np.linspace(0, trace_time, len(probe_amp))
    time_fit_pulse = time_trace[pulse_fit_start:pulse_fit_stop]
    time_fit_decay = time_trace[decay_fit_start:decay_fit_stop]

    R, P = np.meshgrid(amplitudes, angles)
    flat_diff = np.zeros((len(angles), len(amplitudes)))
    bw_diff = np.zeros((len(angles), len(amplitudes)))
    det_diff = np.zeros((len(angles), len(amplitudes)))
    thre_diff = np.zeros((len(angles), len(amplitudes)))
    k_adds = np.zeros((len(angles), len(amplitudes)), dtype=complex)
    flat_diff_list = []
    flat_kadd_list = []

    for i in range(len(amplitudes)):
        for j in range(len(angles)):
            #print("angle", angle)
            amp = amplitudes[i]
            angle = angles[j]

            (a, b, c, d) = calibrate_abcd(probe_cmplx,
                                          vforw_cmplx,
                                          vrefl_cmplx,
                                          decay_fraction,
                                          pulse_fraction,
                                          k_add=amp*np.exp(1.0j*angle))

                       
            k_adds[j,i] = (amp*np.exp(1.0j*angle))
            real_vforw_cmplx = vforw_cmplx*a + vrefl_cmplx*b
            (bw, det) = bwdet(probe_cmplx, real_vforw_cmplx, 
                        fs=len(probe_amp)/trace_time, ql=ql)

            pulse_fit_det_f = np.poly1d(np.polyfit(time_fit_pulse, 
                                               det[pulse_fit_start:pulse_fit_stop],
                                               fit_deg))

            decay_fit_det_f = np.poly1d(np.polyfit(time_fit_decay, 
                                               det[decay_fit_start:decay_fit_stop],
                                               fit_deg)) 

            thre_error = (np.mean(np.concatenate([bw[pulse_fit_start:pulse_fit_stop], 
                                                 bw[decay_fit_start:decay_fit_stop]])) 
                          -frequency*0.5/(ql))

            bw_error = (np.mean(bw[pulse_fit_start:pulse_fit_stop]) - 
                        np.mean(bw[decay_fit_start:decay_fit_stop]))

            det_error = (pulse_fit_det_f(decay_limit*trace_time) -
                         decay_fit_det_f(decay_limit*trace_time))

            flat_diff[j, i] = (np.sqrt(bw_error*bw_error + det_error*det_error))
            bw_diff[j, i] = (np.abs(bw_error))
            det_diff[j, i] = (np.abs(det_error))
            thre_diff[j, i] = (np.abs(thre_error))

            flat_diff_list.append(np.sqrt(bw_error*bw_error + det_error*det_error))
            flat_kadd_list.append(amp*np.exp(1.0j*angle))


    X, Y = R*np.cos(P), R*np.sin(P)
    fig = plt.figure()
    
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, flat_diff, cmap=plt.cm.YlGnBu_r) 
    ax.set_title("Physical error")
    ax.set_xlabel("k_add real")
    ax.set_ylabel("k_add imag")
    ax.set_zlabel("error (Hz)")
   
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, bw_diff, cmap=plt.cm.YlGnBu_r) 
    ax.set_title("Bandwidth error")
    ax.set_xlabel("k_add real")
    ax.set_ylabel("k_add imag")
    ax.set_zlabel("error (Hz)")
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, det_diff, cmap=plt.cm.YlGnBu_r) 
    ax.set_title("Detuning error")
    ax.set_xlabel("k_add real")
    ax.set_ylabel("k_add imag")
    ax.set_zlabel("error (Hz)")

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(X, Y, thre_diff, cmap=plt.cm.YlGnBu_r) 
    ax.set_title("Threshold error")
    ax.set_xlabel("k_add real")
    ax.set_ylabel("k_add imag")
    ax.set_zlabel("error (Hz)")


    #plt.figure()
    #plt.plot(angles*180/np.pi, flat_diff)
    #plt.xlabel("Angle")
    #plt.ylabel("Physical error [Hz]")

    #plt.figure()
    #plt.plot(angles*180/np.pi, bw_diff)
    #plt.xlabel("Angle")
    #plt.ylabel("Bandwidth error [Hz]")

    #plt.figure()
    #plt.plot(angles*180/np.pi, det_diff)
    #plt.xlabel("Angle")
    #plt.ylabel("Detuning error [Hz]")

    #plt.figure()
    #plt.plot(angles*180/np.pi, flat_diff, label="Phy")
    #plt.plot(angles*180/np.pi, bw_diff, label="Bandwidth")
    #plt.plot(angles*180/np.pi, det_diff, label="Detuning")
    #plt.legend()
    #plt.xlabel("Angle")
    #plt.ylabel("Error [Hz]")

    #plt.figure()
    #plt.polar(angles, flat_diff, label="Phy")
    #plt.polar(angles, bw_diff, label="Bandwidth")
    #plt.polar(angles, det_diff, label="Detuning")
    #plt.legend()
    #plt.xlabel("Angle")
    #plt.ylabel("Error [Hz]")
 
    k_add = flat_kadd_list[np.argmin(flat_diff_list)]

    print("min diff:", min(flat_diff_list))
    print("k_add", np.abs(k_add), np.angle(k_add, deg=True))
    (a, b, c, d) = calibrate_abcd(probe_cmplx,
                                  vforw_cmplx,
                                  vrefl_cmplx,
                                  decay_fraction,
                                  pulse_fraction,
                                  k_add=k_add)

    real_vforw_cmplx = vforw_cmplx*a + vrefl_cmplx*b
    (bw, det) = bwdet(probe_cmplx, real_vforw_cmplx, 
                      fs=len(probe_amp)/trace_time, ql=ql)

    pulse_fit_det_f = np.poly1d(np.polyfit(time_fit_pulse, 
                                           det[pulse_fit_start:pulse_fit_stop],
                                           fit_deg))

    decay_fit_det_f = np.poly1d(np.polyfit(time_fit_decay, 
                                           det[decay_fit_start:decay_fit_stop],
                                           fit_deg)) 

    plt.figure()
    plt.plot(bw)
    plt.ylim(-500, 500)

    plt.figure()
    plt.plot(det)
    plt.plot(np.array(range(pulse_fit_start, decay_limit_idx)), 
                            pulse_fit_det_f(time_trace[pulse_fit_start:decay_limit_idx]), ls="--")

    plt.plot(np.array(range(pulse_fit_start, decay_fit_stop)),
                            decay_fit_det_f(time_trace[pulse_fit_start:decay_fit_stop]), ls="--")
    plt.ylim(-1000, 1000)
    
    forward_amp_scale = csys.read(forward_amp_scale_addr)
    forward_pha_rotation = csys.read(forward_pha_rotation_addr)
    reflected_amp_scale = csys.read(reflected_amp_scale_addr)
    reflected_pha_rotation = csys.read(reflected_pha_rotation_addr)

    forward_scale_cmplx = ap_to_cplx(forward_amp_scale, forward_pha_rotation)
    reflected_scale_cmplx = ap_to_cplx(reflected_amp_scale, reflected_pha_rotation)

    (forward_amp_scale, forward_pha_rotation) = cplx_to_ap(forward_scale_cmplx*a)
    (reflected_amp_scale, reflected_pha_rotation) = cplx_to_ap(reflected_scale_cmplx*d)

    vprobe_cmplx = vforw_cmplx*(a+c) + vrefl_cmplx*(b+d)

    print(angle*180/np.pi)
    print("a: ", a, "b: ", b, "c", c, "d", d)
    print(b/d)
    print(b/d*(2**18))

    csys.write(forward_amp_scale_addr, float(forward_amp_scale))
    csys.write(forward_pha_rotation_addr, float(forward_pha_rotation))
    csys.write(reflected_amp_scale_addr, float(reflected_amp_scale))
    csys.write(reflected_pha_rotation_addr, float(reflected_pha_rotation))

    plt.figure()
    plt.plot(probe_cmplx.real)
    plt.plot(vprobe_cmplx.real)
    plt.plot(probe_cmplx.imag)
    plt.plot(vprobe_cmplx.imag)

    now = datetime.now()
    filename = now.strftime("calibration_ql:" + ("%2.2e" % ql) + "_%m.%d.%Y-%H:%M:%S.npz")

    np.savez(filename, 
             QL=ql,
             forward_amp_scale = forward_amp_scale,
             forward_pha_rotation = forward_pha_rotation,    
             reflected_amp_scale = reflected_amp_scale,     
             reflected_pha_rotation = reflected_pha_rotation,
             a = a, b = b, c = c, d = d, R = R, P = P,
             flat_diff = flat_diff,
             bw_diff = bw_diff,
             det_diff = det_diff,
             thre_diff = thre_diff,
             decay_time = decay_time,
             trace_time = trace_time,
             time_trace = time_trace,
             k_add = k_add,
             k_adds = k_adds,
             probe_amps = probe_amps, 
             probe_phas = probe_phas,     
             reflected_amps = reflected_amps, 
             reflected_phas = reflected_phas, 
             forward_amps = forward_amps,   
             forward_phas = forward_phas,   
             decay_start = decay_start,    
             decay_stop = decay_stop,     
             pulse_start = pulse_start,    
             pulse_stop = pulse_stop,      
             decay_fit_fraction = decay_fit_fraction,
             pulse_fit_fraction = pulse_fit_fraction,
             decay_fraction = decay_fraction,
             pulse_fraction = pulse_fraction,
             cutoff = cutoff)


    input(".")

if __name__ == '__main__':
    main.run()

