'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap, scale_match, rotate_match, ap_to_cplx, cplx_to_ap
import voltorb.dictfile as dfile

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']

    pulse_decay_fraction = params.get('pulse_decay_fraction', 0.4)
    pulse_decay_end = params.get('pulse_decay_end', 1.0)

    frequency = params.get('frequency', 1.3E9)
    expected_ql = params.get('expected_ql', 3.0E7)
    trace_time = params.get('time_trace', 0.001813)
    delay = params.get('delay', 1)

    while True:
        time.sleep(delay)

        probe_amp = csys.read(probe_amp_addr)
        probe_pha = csys.read(probe_pha_addr)

        signal_len = len(probe_amp)
        decay_start = int((pulse_decay_end - pulse_decay_fraction) * signal_len)
        decay_stop = int(pulse_decay_end*signal_len) - 1

        decay_probe_amp = probe_amp[decay_start:decay_stop]
        decay_probe_pha = probe_pha[decay_start:decay_stop]

        decay_time = trace_time * pulse_decay_fraction 

        time_trace = np.linspace(0, decay_time, len(decay_probe_amp))
  #      coeffs, pcov = curve_fit(exp_fun, time_trace, decay_probe_amp,
  #                          [np.pi*frequency/expected_ql, 1, 0.])

        ql = np.pi*frequency*decay_time/np.log(decay_probe_amp[0]/decay_probe_amp[-1])
        #ql = np.pi * frequency / coeffs[0]

        print("QL: " + str(ql))

if __name__ == '__main__':
    main.run()
