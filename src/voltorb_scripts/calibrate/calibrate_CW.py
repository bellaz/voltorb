'''
    Script to calibrate probe signals in CW (for CMTB)
'''

import numpy as np
import time
from bayes_opt import BayesianOptimization
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default, merge_selected
from voltorb.math import phase_wrap
import voltorb.dictfile as dfile

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    print('This script will bring the cavities to resonance')
    print('The piezo integrator must be on. Only CW feedforward.')

    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    probe_amp_scale_addr = params['probe_amp_scale_addr']
    probe_pha_rotation_addr = params['probe_pha_rotation_addr']
    cavity_cal_amp_addr = params['cavity_cal_amp_addr']

    vforw_amp_addr = params['vforw_amp_addr']
    vforw_pha_addr = params['vforw_pha_addr']
    vforw_amp_scale_addr = params['vforw_amp_scale_addr']
    vforw_pha_rotation_addr = params['vforw_pha_rotation_addr']

    vrefl_amp_addr = params['vrefl_amp_addr']
    vrefl_pha_addr = params['vrefl_pha_addr']
    vrefl_amp_scale_addr = params['vrefl_amp_scale_addr']
    vrefl_pha_rotation_addr = params['vrefl_pha_rotation_addr']

    phase_start = params.get('phase_start', -160)
    phase_end = params.get('phase_stop', 160)
    measurements = params.get('measurements', 20)
    delay = params.get('delay', 5)
    ofile = params.get('ofile', None)
    method = params.get('method', 'bayes')
    init_points = params.get('init_points', 10)
    tune = params.get('tune', True)
    initial_rotation = csys.read(probe_pha_rotation_addr)
    phase_range = np.linspace(phase_start, phase_end, measurements)
    amplitudes = []

    time.sleep(delay*2)

    vforw_pha  = csys.read(vforw_pha_addr)
    initial_vforw_rotation = csys.read(vforw_pha_rotation_addr)

    csys.write(vforw_pha_rotation_addr, 
               phase_wrap(initial_vforw_rotation - np.mean(vforw_pha)))

    time.sleep(delay*2)
        
    if tune:
        if method == 'linear':
            for phase_shift in phase_range:

                print('Phase shift:', phase_shift)

                csys.write(probe_pha_rotation_addr.format(), 
                           phase_wrap(phase_shift + initial_rotation))

                time.sleep(delay)

                amplitudes.append(np.mean(csys.read(probe_amp_addr)))

            final_rotation = []
            final_scales = []

            ret = np.polyfit(phase_range, amplitudes, 2)
            final_rotation = phase_wrap(-ret[1]*0.5/ret[0] + initial_rotation)
            csys.write(probe_pha_rotation_addr, phase_wrap(final_rotation))


        if method == 'bayes':
            def bayes_opt(phase_shift):
                print('Phase shift:', phase_shift)

                csys.write(probe_pha_rotation_addr.format(), phase_wrap(phase_shift + initial_rotation))

                time.sleep(delay)

                return np.mean(csys.read(probe_amp_addr))

            pbounds = {'phase_shift': (phase_start, phase_end)}

            optimizer = BayesianOptimization(f=bayes_opt,
                                             pbounds=pbounds,
                                             random_state=1)

            optimizer.maximize(init_points=init_points,
                               n_iter=measurements,)

            final_rotation = phase_wrap(optimizer.max["params"]['phase_shift'] + initial_rotation)
            csys.write(probe_pha_rotation_addr, phase_wrap(final_rotation))

    print('Computing amplitude scaling..')
    time.sleep(delay*4)

    [probe_amp, probe_pha, vforw_amp, 
        vforw_pha, vrefl_amp, vrefl_pha] = csys.read(probe_amp_addr, 
                                                     probe_pha_addr,
                                                     vforw_amp_addr, 
                                                     vforw_pha_addr,
                                                     vrefl_amp_addr, 
                                                     vrefl_pha_addr)


    probe_amp_cal = csys.read(cavity_cal_amp_addr)

    initial_probe_scale = csys.read(probe_amp_scale_addr)
    initial_probe_rotation = csys.read(probe_pha_rotation_addr)

    initial_vforw_scale = csys.read(vforw_amp_scale_addr)
    initial_vforw_rotation = csys.read(vforw_pha_rotation_addr)

    initial_vrefl_scale = csys.read(vrefl_amp_scale_addr)
    initial_vrefl_rotation = csys.read(vrefl_pha_rotation_addr)

    csys.write(probe_pha_rotation_addr, 
               phase_wrap(initial_probe_rotation - np.mean(probe_pha)))

    csys.write(vforw_pha_rotation_addr, 
               phase_wrap(initial_vforw_rotation - np.mean(vforw_pha)))

    csys.write(vrefl_pha_rotation_addr, 
               phase_wrap(initial_vrefl_rotation - np.mean(vrefl_pha)))

    new_probe_scale = initial_probe_scale * probe_amp_cal * 1e-6   / np.mean(probe_amp)
    new_vforw_scale = initial_vforw_scale * probe_amp_cal * 5e-7 / np.mean(vforw_amp)
    new_vrefl_scale = initial_vrefl_scale * probe_amp_cal * 5e-7 / np.mean(vrefl_amp)

    if 0.5 < new_probe_scale < 2.0:
        csys.write(probe_amp_scale_addr, new_probe_scale)
    else:
        print("--> Probe scale not in range (0.5, 2.0) :", new_probe_scale)
        print("--> Apply ", -int(20.0 * np.log10(new_probe_scale)), "dB first")

    if 0.5 < new_vforw_scale < 2.0:
        csys.write(vforw_amp_scale_addr, new_vforw_scale)
    else:
        print("--> Forward scale not in range (0.5, 2.0) :", new_vforw_scale)
        print("--> Apply ", -int(20.0 * np.log10(new_vforw_scale)), "dB first")

    if 0.5 < new_vrefl_scale < 2.0:
        csys.write(vrefl_amp_scale_addr, new_vrefl_scale)
    else:
        print("--> Reflected scale not in range (0.5, 2.0) :", new_vrefl_scale)
        print("--> Apply ", -int(20.0 * np.log10(new_vrefl_scale)), "dB first")

    if ofile is not None:
        dictfile = {}
        dictfile['INITIAL_PHASE_ROTATION'] = initial_rotation
        dictfile['FINAL_PHASE_ROTATION'] = final_rotation
        dictfile['FINAL_AMPLITUDE_SCALE'] = final_scales
        dictfile['AMPLITUDE'] = probe_amp
        dictfile['AMPLITUDES_CAL'] = probe_amp_cal
        dfile.write(ofile, dictfile)

if __name__ == '__main__':
    main.run()


