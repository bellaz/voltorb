'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap, scale_match, rotate_match, ap_to_cplx, cplx_to_ap
import voltorb.dictfile as dfile

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    forward_amp_addr = params['vforw_amp_addr']
    forward_pha_addr = params['vforw_pha_addr']
    reflected_amp_addr = params['vrefl_amp_addr']
    reflected_pha_addr = params['vrefl_pha_addr']
    forward_amp_scale_addr = params['vforw_amp_scale_addr']
    forward_pha_rotation_addr = params['vforw_pha_rotation_addr']
    reflected_amp_scale_addr = params['vrefl_amp_scale_addr']
    reflected_pha_rotation_addr = params['vrefl_pha_rotation_addr']

    pulse_delay_fraction = csys.read(params['pulse_delay_percent_addr'])/100
    pulse_filling_fraction = csys.read(params['pulse_filling_percent_addr'])/100
    pulse_flattop_fraction = csys.read(params['pulse_flattop_percent_addr'])/100

    pulse_decay_delay_fraction = params.get('pulse_decay_delay_fraction', 0.003)
    pulse_decay_fraction = params.get('pulse_decay_fraction', 0.01)

    cav_ql_addr = params['cav_ql_addr']
    frequency = params.get('frequency', 1.3E9)
    expected_ql = params.get('expected_ql', 3.0E7)
    measurements = params.get('measurements', 100)
    delay = params.get('delay', 2)
    ofile = params.get('ofile', None)
    trace_time = params.get('time_trace', 1)

    qls = []
    reflected_scales = []
    reflected_rotations  = []
    forward_scales = []
    forward_rotations = []

    probe_amps = []
    probe_phas = []
    reflected_amps = []
    reflected_phas = []
    forward_amps = []
    forward_phas = []

    pulse_start = 0
    pulse_stop = 0
    decay_start = 0
    decay_stop = 0

    for i in range(measurements):
        print('Performing measurement: ', i+1, '/', measurements)
        time.sleep(delay)

        probe_amp = csys.read(probe_amp_addr)
        probe_pha = csys.read(probe_pha_addr)
        forward_amp = csys.read(forward_amp_addr)
        forward_pha = csys.read(forward_pha_addr)
        reflected_amp = csys.read(reflected_amp_addr)
        reflected_pha = csys.read(reflected_pha_addr)

        probe_amps.append(probe_amps)
        probe_phas.append(probe_phas)
        forward_amps.append(forward_amp)
        forward_phas.append(forward_pha)
        reflected_amps.append(reflected_amp)
        reflected_phas.append(reflected_pha)

        signal_len = len(probe_amp)

        decay_start = int((pulse_delay_fraction +
                           pulse_filling_fraction +
                           pulse_flattop_fraction +
                           pulse_decay_delay_fraction) * signal_len)

        decay_stop = decay_start + int(pulse_decay_fraction*signal_len)

        decay_probe_amp = probe_amp[decay_start:decay_stop]
        decay_probe_pha = probe_pha[decay_start:decay_stop]
        decay_reflected_amp = reflected_amp[decay_start:decay_stop]
        decay_reflected_pha = reflected_pha[decay_start:decay_stop]

        scale_reflected = scale_match(decay_probe_amp, decay_reflected_amp)
        rotate_reflected = rotate_match(decay_probe_pha, decay_reflected_pha)

        pulse_start = int(pulse_filling_fraction*signal_len)
        pulse_stop = pulse_start + int(pulse_flattop_fraction*signal_len)

        pulse_probe_amp = probe_amp[pulse_start:pulse_stop]
        pulse_probe_pha = probe_pha[pulse_start:pulse_stop]
        pulse_reflected_amp = reflected_amp[pulse_start:pulse_stop]
        pulse_reflected_pha = reflected_pha[pulse_start:pulse_stop]
        pulse_forward_amp = forward_amp[pulse_start:pulse_stop]
        pulse_forward_pha = forward_pha[pulse_start:pulse_stop]

        pulse_probe_cplx = ap_to_cplx(pulse_probe_amp, pulse_probe_pha)
        pulse_reflected_cplx = ap_to_cplx(scale_reflected * pulse_reflected_amp,
                                          rotate_reflected + pulse_reflected_pha)

        (virtual_pulse_forward_amp,
         virtual_pulse_forward_pha) = cplx_to_ap(pulse_probe_cplx - pulse_reflected_cplx)

        #plt.ioff()
        #plt.figure()
        #plt.plot(virtual_pulse_forward_amp)
        #plt.plot(pulse_forward_amp)
        #plt.figure()
        #plt.plot(virtual_pulse_forward_pha)
        #plt.plot(pulse_forward_pha)

        plt.show()

 
        scale_forward = scale_match(virtual_pulse_forward_amp, pulse_forward_amp)
        rotate_forward = rotate_match(virtual_pulse_forward_pha, pulse_forward_pha)

        time_trace = np.linspace(0, pulse_decay_fraction * trace_time, len(decay_probe_amp))
        coeffs = curve_fit(exp_fun, time_trace, decay_probe_amp,
                            [np.pi*frequency/expected_ql, 1, 0.])

        qls.append(np.pi*frequency/coeffs[0][0])
        #qls.append(csys.read(cav_ql_addr))
        print(np.pi*frequency/coeffs[0][0])
        reflected_scales.append(scale_reflected)
        reflected_rotations.append(rotate_reflected)
        forward_scales.append(scale_forward)
        forward_rotations.append(rotate_forward)

    reflected_scale = np.mean(reflected_scales)
    reflected_rotation = np.mean(reflected_rotations)
    forward_scale = np.mean(forward_scales)
    forward_rotation = np.mean(forward_rotations)

    initial_reflected_scale = csys.read(reflected_amp_scale_addr)
    initial_reflected_rotation = csys.read(reflected_pha_rotation_addr)
    final_reflected_scale = initial_reflected_scale * reflected_scale
    print('final sca refl', final_reflected_scale)
    final_reflected_rotation = phase_wrap(initial_reflected_rotation + reflected_rotation)
    print('final rot refl', final_reflected_rotation)
    csys.write(reflected_amp_scale_addr, final_reflected_scale)
    csys.write(reflected_pha_rotation_addr, final_reflected_rotation)

    initial_forward_scale = csys.read(forward_amp_scale_addr)
    initial_forward_rotation = csys.read(forward_pha_rotation_addr)
    final_forward_scale = initial_forward_scale * forward_scale
    print('final sca forw', final_forward_scale)
    final_forward_rotation = phase_wrap(initial_forward_rotation + forward_rotation)
    print('final rot forw', final_forward_rotation)
    csys.write(forward_amp_scale_addr, final_forward_scale)
    csys.write(forward_pha_rotation_addr, final_forward_rotation)

    
    if ofile is not None:
        dictfile = {}
        dictfile['QLS'] = np.array(qls)
        dictfile['QL_AVG'] = np.mean(qls)
        dictfile['INITIAL_REFLECTED_SCALE'] = initial_reflected_scale
        dictfile['INITIAL_REFLECTED_ROTATION'] = initial_reflected_rotation
        dictfile['FINAL_REFLECTED_SCALE'] = final_reflected_scale
        dictfile['FINAL_REFLECTED_ROTATION'] = final_reflected_rotation
        dictfile['INITIAL_FORWARD_SCALE'] = initial_forward_scale
        dictfile['INITIAL_FORWARD_ROTATION'] = initial_forward_rotation
        dictfile['FINAL_FORWARD_SCALE'] = final_forward_scale
        dictfile['FINAL_FORWARD_ROTATION'] = final_forward_rotation
        dictfile['PROBE_AMPS'] = probe_amps
        dictfile['PROBE_PHAS'] = probe_phas
        dictfile['FORWARD_AMPS'] = forward_amps
        dictfile['FORWARD_PHAS'] = forward_phas
        dictfile['REFLECTED_AMPS'] = reflected_amps
        dictfile['REFLECTED_PHAS'] = reflected_phas
        dictfile['PULSE_START'] = pulse_start
        dictfile['PULSE_STOP'] = pulse_stop 
        dictfile['DECAY_START'] = decay_start
        dictfile['DECAY_STOP'] = decay_stop 
        dfile.write(ofile, dictfile)

if __name__ == '__main__':
    main.run()
