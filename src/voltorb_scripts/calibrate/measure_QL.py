
#!/usr/bin/env python3
'''
    Script to calculate the QL (for CMTB)
'''
import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap
import voltorb.dictfile as dfile

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c

@parser_default(__doc__)
def main(params):
    '''
        Script entry point
    '''
    fraction_read = params.get('fraction_read', 0.005)
    calculation_start = params.get('additional_delay', 0.0015)
    pulse_delay_percent_addr = params['pulse_delay_percent_addr']
    pulse_filling_percent_addr = params['pulse_filling_percent_addr']
    pulse_flattop_percent_addr = params['pulse_flattop_percent_addr']
    template_probe_amp_addr = params['template_probe_amp_addr']
    expected_ql = params.get('expected_ql', 3E7)
    frequency = params.get('frequency', 1.3E9)
    trace_time = params.get('trace_time', 1) #seconds
    ofile = params.get('ofile', None)
    measurements = params.get('measurements', 10)
    delay = params.get('delay', 1)

    qls = []
    delay = csys.read(pulse_delay_percent_addr)
    filling = csys.read(pulse_filling_percent_addr)
    flattop = csys.read(pulse_flattop_percent_addr)
    decay_prop = (delay+filling+flattop)/100.0 + calculation_start

    for i in range(measurements):
        time.sleep(delay)
        trace = csys.read(template_probe_amp_addr)
        trace = trace[int(decay_prop*len(trace)):
                      int((decay_prop+fraction_read)*len(trace))]
        time_trace = np.linspace(0, fraction_read * trace_time, len(trace))
        coeffs, = curve_fit(exp_fun, time_trace, trace, [frequency/expected_ql, 10., 0.])
        qls.append(np.pi*frequency/coeffs[0])
        print('Measurement', i+1, '/', measurements, 'QL:', format(qls[-1], '.3e'))

    if ofile is not None:
        dictfile = {}
        dictfile['QLS'] = np.array(qls)
        dictfile['QL_AVG'] = np.mean(qls)
        dfile.write(ofile, dictfile)

if __name__ == '__main__':
    main.run()

