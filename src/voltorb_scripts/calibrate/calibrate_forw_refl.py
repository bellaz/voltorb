'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap, ap_to_cplx, cplx_to_ap
import voltorb.dictfile as dfile
from voltorb.calibrate import calibrate_minimize, calibrate_decay

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    forward_amp_addr = params['vforw_amp_addr']
    forward_pha_addr = params['vforw_pha_addr']
    reflected_amp_addr = params['vrefl_amp_addr']
    reflected_pha_addr = params['vrefl_pha_addr']
    forward_amp_scale_addr = params['vforw_amp_scale_addr']
    forward_pha_rotation_addr = params['vforw_pha_rotation_addr']
    reflected_amp_scale_addr = params['vrefl_amp_scale_addr']
    reflected_pha_rotation_addr = params['vrefl_pha_rotation_addr']

    cav_ql_addr = params['cav_ql_addr']
    frequency = params.get('frequency', 1.3E9)
    expected_ql = params.get('expected_ql', 3.0E7)
    measurements = params.get('measurements', 100)
    delay = params.get('delay', 0.2)
    ofile = params.get('ofile', None)
    trace_time = params.get('time_trace', 1)

    qls = []
    reflected_scales = []
    reflected_rotations  = []
    forward_scales = []
    forward_rotations = []

    probe_amps = []
    probe_phas = []
    reflected_amps = []
    reflected_phas = []
    forward_amps = []
    forward_phas = []

    for i in range(measurements):
        print('Performing measurement: ', i+1, '/', measurements)
        time.sleep(delay)

        probe_amp = csys.read(probe_amp_addr)
        probe_pha = csys.read(probe_pha_addr)
        forward_amp = csys.read(forward_amp_addr)
        forward_pha = csys.read(forward_pha_addr)
        reflected_amp = csys.read(reflected_amp_addr)
        reflected_pha = csys.read(reflected_pha_addr)

        vforw_amp = forward_amp 
        vforw_pha = forward_pha
        vrefl_amp = reflected_amp
        vrefl_pha = reflected_pha

        probe_amps.append(probe_amps)
        probe_phas.append(probe_phas)
        forward_amps.append(forward_amp)
        forward_phas.append(forward_pha)
        reflected_amps.append(reflected_amp)
        reflected_phas.append(reflected_pha)

        probe_cplx = ap_to_cplx(probe_amp, probe_pha)
        vforw_cplx = ap_to_cplx(vforw_amp, vforw_pha) 
        vrefl_cplx = ap_to_cplx(vrefl_amp, vrefl_pha)

        probe_I = probe_cplx.real 
        vforw_I = vforw_cplx.real
        vrefl_I = vrefl_cplx.real
        probe_Q = probe_cplx.imag 
        vforw_Q = vforw_cplx.imag
        vrefl_Q = vrefl_cplx.imag

        calib, signals, loss = calibrate_decay(probe_I, probe_Q,
                    vforw_I, vforw_Q,
                    vrefl_I, vrefl_Q)


        (a1, b1, a2, b2) = calib

        (f_amp, f_rot) = cplx_to_ap(a1+i*b1)
        (r_amp, r_rot) = cplx_to_ap(a2+i*b2)
        print(f_amp, f_rot, r_amp, r_rot)

        reflected_scales.append(r_amp)
        reflected_rotations.append(r_rot)
        forward_scales.append(f_amp)
        forward_rotations.append(f_rot)


    reflected_scale = np.mean(reflected_scales)
    reflected_rotation = np.mean(reflected_rotations)
    forward_scale = np.mean(forward_scales)
    forward_rotation = np.mean(forward_rotations)

    initial_reflected_scale = csys.read(reflected_amp_scale_addr)
    initial_reflected_rotation = csys.read(reflected_pha_rotation_addr)
    final_reflected_scale = initial_reflected_scale * reflected_scale
    print('final sca refl', final_reflected_scale)
    final_reflected_rotation = phase_wrap(initial_reflected_rotation + reflected_rotation)
    print('final rot refl', final_reflected_rotation)
    csys.write(reflected_amp_scale_addr, final_reflected_scale)
    csys.write(reflected_pha_rotation_addr, final_reflected_rotation)

    initial_forward_scale = csys.read(forward_amp_scale_addr)
    initial_forward_rotation = csys.read(forward_pha_rotation_addr)
    final_forward_scale = initial_forward_scale * forward_scale
    print('final sca forw', final_forward_scale, "fw scale", forward_scale)
    final_forward_rotation = phase_wrap(initial_forward_rotation + forward_rotation)
    print('final rot forw', final_forward_rotation, "fw rot", forward_rotation)
    csys.write(forward_amp_scale_addr, final_forward_scale)
    csys.write(forward_pha_rotation_addr, final_forward_rotation)

if __name__ == '__main__':
    main.run()
