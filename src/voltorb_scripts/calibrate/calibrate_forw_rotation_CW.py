'''
    Script to calibrate forward rotation signals in CW (for CMTB)
'''
import time
import numpy as np
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap

@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    forward_pha_addr = params['vforw_pha_addr']
    forward_pha_rotation_addr = params['vforw_pha_rotation_addr']

    forward_pha = np.mean(csys.read(forward_pha_addr))
    orig_rotation = csys.read(forward_pha_rotation_addr)
    csys.write(forward_pha_rotation_addr, phase_wrap(orig_rotation - forward_pha))

if __name__ == '__main__':
    main.run()
