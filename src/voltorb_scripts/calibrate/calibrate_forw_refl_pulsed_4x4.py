'''
    Script to calibrate probe signals in CW (for CMTB)
'''
import time
import numpy as np
from scipy.optimize import curve_fit
from voltorb import controlsys as csys
from voltorb.parse_cmdline import parser_default
from voltorb.math import phase_wrap, scale_match, rotate_match, ap_to_cplx, cplx_to_ap
import voltorb.dictfile as dfile
from noisyopt import minimizeSPSA



def optimize_parameters(probe_I_pulse, 
                        probe_Q_pulse,
                        forward_I_pulse,
                        forward_Q_pulse,
                        reflected_I_pulse,
                        reflected_Q_pulse,
                        probe_I_decay, 
                        probe_Q_decay,
                        forward_I_decay,
                        forward_Q_decay,
                        reflected_I_decay,
                        reflected_Q_decay):

    Af = [1.0, 0.0]
    Ar = [1.0, 0.0]
    Cf = [0.0, 0.0]
    Cr = [0.0, 0.0]

    iter_outer = 100

    def optimize_r(x):
        pseudo_I = x[0]*reflected_I_decay - x[1]*reflected_I_decay + Cr[0]*forward_I_decay - Cr[1]*forward_Q_decay
        pseudo_Q = x[1]*reflected_Q_decay + x[0]*reflected_Q_decay + Cr[1]*forward_I_decay + Cr[0]*forward_Q_decay
        e_I = probe_I_decay - pseudo_I
        e_Q = probe_Q_decay - pseudo_Q
        return np.mean(e_I**2 + e_Q**2)
    
    def optimize_cf(x):
        e_I = x[0]*reflected_I_decay - x[1]*reflected_I_decay + Af[0]*forward_I_decay - Af[1]*forward_Q_decay
        e_Q = x[1]*reflected_Q_decay + x[0]*reflected_Q_decay + Af[1]*forward_I_decay + Af[0]*forward_Q_decay
        return np.mean(e_I**2 + e_Q**2)

    def optimize_pulse(x):
        pseudo_reflected_I = Ar[0]*reflected_I_pulse - Ar[1]*reflected_I_pulse + x[0]*forward_I_pulse - x[1]*forward_Q_pulse
        pseudo_reflected_Q = Ar[1]*reflected_Q_pulse + Ar[0]*reflected_Q_pulse + x[1]*forward_I_pulse + x[0]*forward_Q_pulse
        pseudo_forward_I   = Cf[0]*reflected_I_pulse - Cf[1]*reflected_I_pulse + x[2]*forward_I_pulse - x[3]*forward_Q_pulse
        pseudo_forward_Q   = Cf[1]*reflected_Q_pulse + Cf[0]*reflected_Q_pulse + x[3]*forward_I_pulse + x[2]*forward_Q_pulse
        
        e_I = pseudo_reflected_I + pseudo_forward_I - probe_I_pulse
        e_Q = pseudo_reflected_Q + pseudo_forward_Q - probe_Q_pulse
        
        return np.mean(e_I**2 + e_Q**2)
    
    opt_r_bounds = [[-2.0, 2.0], [-2.0, 2.0]]
    opt_cf_bounds = [[-0.2, 0.2], [-0.2, 0.2]]
    opt_pulse_bounds = [[-1.0, 1.0], [-1.0, 1.0], [-2.0, 2.0], [-2.0, 2.0]]

    for i in range(iter_outer):
        res = minimizeSPSA(optimize_r, bounds=opt_r_bounds, x0=[Ar[0],  Ar[1]], niter=1000, paired=False)
        Ar[0] = res.x[0]
        Ar[1] = res.x[1]
        res = minimizeSPSA(optimize_cf, bounds=opt_cf_bounds, x0=[Cf[0], Cf[1]], niter=1000, paired=False)
        Cf[0] = res.x[0]
        Cf[1] = res.x[1]
        res = minimizeSPSA(optimize_pulse, bounds=opt_pulse_bounds, x0=[Cr[0], Cr[1], Af[0], Af[1]], niter=1000, paired=False)
        Cr[0] = res.x[0]
        Cr[1] = res.x[1]
        Af[0] = res.x[2]
        Af[1] = res.x[3]

        print("Ar", Ar, "Cf", Cf, "Cr", Cr, "Af", Af)

    return Ar, Cf, Cr, Af

def exp_fun(x, a, b, c):
    '''
        exponential function for curve fitting
    '''
    return np.exp(-x*a)*b+c


@parser_default(__doc__)
def main(params):
    '''
        Script main entry point
    '''
    probe_amp_addr = params['probe_amp_addr']
    probe_pha_addr = params['probe_pha_addr']
    forward_amp_addr = params['vforw_amp_addr']
    forward_pha_addr = params['vforw_pha_addr']
    reflected_amp_addr = params['vrefl_amp_addr']
    reflected_pha_addr = params['vrefl_pha_addr']
    forward_amp_scale_addr = params['vforw_amp_scale_addr']
    forward_pha_rotation_addr = params['vforw_pha_rotation_addr']
    reflected_amp_scale_addr = params['vrefl_amp_scale_addr']
    reflected_pha_rotation_addr = params['vrefl_pha_rotation_addr']

    pulse_decay_fraction = params.get('pulse_decay_fraction', 0.3)
    pulse_decay_start_fraction = params['pulse_decay_start_fraction']

    cav_ql_addr = params['cav_ql_addr']
    frequency = params.get('frequency', 1.3E9)
    expected_ql = params.get('expected_ql', 3.0E7)
    measurements = params.get('measurements', 100)
    delay = params.get('delay', 0.2)
    ofile = params.get('ofile', None)
    trace_time = params.get('time_trace', 1)

    qls = []
    reflected_scales = []
    reflected_rotations  = []
    forward_scales = []
    forward_rotations = []

    probe_amps = []
    probe_phas = []
    reflected_amps = []
    reflected_phas = []
    forward_amps = []
    forward_phas = []

    pulse_start = 0
    pulse_stop = 0
    decay_start = 0
    decay_stop = 0

    for i in range(measurements):
        print('Performing measurement: ', i+1, '/', measurements)
        time.sleep(delay)

        probe_amp = csys.read(probe_amp_addr)
        probe_pha = csys.read(probe_pha_addr)
        forward_amp = csys.read(forward_amp_addr)
        forward_pha = csys.read(forward_pha_addr)
        reflected_amp = csys.read(reflected_amp_addr)
        reflected_pha = csys.read(reflected_pha_addr)

        probe_amps.append(probe_amps)
        probe_phas.append(probe_phas)
        forward_amps.append(forward_amp)
        forward_phas.append(forward_pha)
        reflected_amps.append(reflected_amp)
        reflected_phas.append(reflected_pha)

        signal_len = len(probe_amp)

        decay_start = int(pulse_decay_start_fraction * signal_len)

        decay_stop = decay_start + int(pulse_decay_fraction * signal_len)

        decay_probe_amp = probe_amp[decay_start:decay_stop]
        decay_probe_pha = probe_pha[decay_start:decay_stop]
        decay_reflected_amp = reflected_amp[decay_start:decay_stop]
        decay_reflected_pha = reflected_pha[decay_start:decay_stop]
        decay_forward_amp = forward_amp[decay_start:decay_stop]
        decay_forward_pha = forward_pha[decay_start:decay_stop]

        decay_probe_cplx = ap_to_cplx(decay_probe_amp, decay_probe_pha)
        decay_reflected_cplx = ap_to_cplx(decay_reflected_amp, decay_reflected_pha)
        decay_forward_cplx = ap_to_cplx(decay_forward_amp, decay_forward_pha)

        pulse_start = int(0.05*signal_len)
        pulse_stop = int(0.45*signal_len)

        pulse_probe_amp = probe_amp[pulse_start:pulse_stop]
        pulse_probe_pha = probe_pha[pulse_start:pulse_stop]
        pulse_reflected_amp = reflected_amp[pulse_start:pulse_stop]
        pulse_reflected_pha = reflected_pha[pulse_start:pulse_stop]
        pulse_forward_amp = forward_amp[pulse_start:pulse_stop]
        pulse_forward_pha = forward_pha[pulse_start:pulse_stop]

        pulse_probe_cplx = ap_to_cplx(pulse_probe_amp, pulse_probe_pha)
        pulse_reflected_cplx = ap_to_cplx(pulse_reflected_amp, pulse_reflected_pha)
        pulse_forward_cplx = ap_to_cplx(pulse_forward_amp, pulse_forward_pha)

        optimize_parameters(pulse_probe_cplx.real, 
                            pulse_probe_cplx.imag,
                            pulse_reflected_cplx.real,
                            pulse_reflected_cplx.imag,
                            pulse_forward_cplx.real,
                            pulse_forward_cplx.imag,
                            decay_probe_cplx.real,
                            decay_probe_cplx.imag,
                            decay_reflected_cplx.real,
                            decay_reflected_cplx.imag,
                            decay_forward_cplx.real,
                            decay_forward_cplx.imag)

        decay_time = pulse_decay_fraction * trace_time

        time_trace = np.linspace(0, decay_time, len(decay_probe_amp))
        coeffs = curve_fit(exp_fun, time_trace, decay_probe_amp,
                            [np.pi*frequency/expected_ql, 1, 0.])

        ql = np.pi*frequency*decay_time/np.log(decay_probe_amp[0]/decay_probe_amp[-1])
        qls.append(ql)
        #reflected_scales.append(scale_reflected)
        #reflected_rotations.append(rotate_reflected)
        #forward_scales.append(scale_forward)
        #forward_rotations.append(rotate_forward)

    reflected_scale = np.mean(reflected_scales)
    reflected_rotation = np.mean(reflected_rotations)
    forward_scale = np.mean(forward_scales)
    forward_rotation = np.mean(forward_rotations)

    initial_reflected_scale = csys.read(reflected_amp_scale_addr)
    initial_reflected_rotation = csys.read(reflected_pha_rotation_addr)
    final_reflected_scale = initial_reflected_scale * reflected_scale
    print('final sca refl', final_reflected_scale)
    final_reflected_rotation = phase_wrap(initial_reflected_rotation + reflected_rotation)
    print('final rot refl', final_reflected_rotation)
    csys.write(reflected_amp_scale_addr, final_reflected_scale)
    csys.write(reflected_pha_rotation_addr, final_reflected_rotation)

    initial_forward_scale = csys.read(forward_amp_scale_addr)
    initial_forward_rotation = csys.read(forward_pha_rotation_addr)
    final_forward_scale = initial_forward_scale * forward_scale
    print('final sca forw', final_forward_scale, "fw scale", forward_scale)
    final_forward_rotation = phase_wrap(initial_forward_rotation + forward_rotation)
    print('final rot forw', final_forward_rotation, "fw rot", forward_rotation)

    print('final ql', np.mean(qls))
    csys.write(forward_amp_scale_addr, final_forward_scale)
    csys.write(forward_pha_rotation_addr, final_forward_rotation)

    
    if ofile is not None:
        dictfile = {}
        dictfile['QLS'] = np.array(qls)
        dictfile['QL_AVG'] = np.mean(qls)
        dictfile['INITIAL_REFLECTED_SCALE'] = initial_reflected_scale
        dictfile['INITIAL_REFLECTED_ROTATION'] = initial_reflected_rotation
        dictfile['FINAL_REFLECTED_SCALE'] = final_reflected_scale
        dictfile['FINAL_REFLECTED_ROTATION'] = final_reflected_rotation
        dictfile['INITIAL_FORWARD_SCALE'] = initial_forward_scale
        dictfile['INITIAL_FORWARD_ROTATION'] = initial_forward_rotation
        dictfile['FINAL_FORWARD_SCALE'] = final_forward_scale
        dictfile['FINAL_FORWARD_ROTATION'] = final_forward_rotation
        dictfile['PROBE_AMPS'] = probe_amps
        dictfile['PROBE_PHAS'] = probe_phas
        dictfile['FORWARD_AMPS'] = forward_amps
        dictfile['FORWARD_PHAS'] = forward_phas
        dictfile['REFLECTED_AMPS'] = reflected_amps
        dictfile['REFLECTED_PHAS'] = reflected_phas
        dictfile['PULSE_START'] = pulse_start
        dictfile['PULSE_STOP'] = pulse_stop 
        dictfile['DECAY_START'] = decay_start
        dictfile['DECAY_STOP'] = decay_stop 
        dfile.write(ofile, dictfile)

if __name__ == '__main__':
    main.run()
