import numpy as np
from scipy.optimize import minimize
from scipy.optimize import Bounds
from scipy.linalg import inv
from .bwdet import bwdet
#from .matplotlib import pyplot as plt

def rotate_signals(calib, forward_I, forward_Q,
                                     reflected_I, reflected_Q):
    '''
        rotate signals with calibration values 
    '''

    (a1, b1, a2, b2) = calib

    forward_I_calib = a1*forward_I - b1*forward_Q
    forward_Q_calib = b1*forward_I + a1*forward_Q
    
    reflected_I_calib = a2*reflected_I - b2*reflected_Q
    reflected_Q_calib = b2*reflected_I + b1*reflected_Q

    return (forward_I_calib, forward_Q_calib, 
            reflected_I_calib, reflected_Q_calib)

def calibrate_minimize(probe_I, probe_Q,
                       forward_I, forward_Q,
                       reflected_I, reflected_Q,
                       max_rescale=2.0):
    '''
        calibrate the signals using multivariate minimization
    '''
    probe_I     = np.array(probe_I)    
    probe_Q     = np.array(probe_Q)  
    forward_I   = np.array(forward_I)  
    forward_Q   = np.array(forward_Q)
    reflected_I = np.array(reflected_I)
    reflected_Q = np.array(reflected_Q) 

    def loss_function(x, *args):
        '''
            compare the loss of the difference between probe and 
            forward+reflected
        '''
        a1 = x[0]
        b1 = x[1]
        a2 = x[2]
        b2 = x[3]

        virt_I = a1*forward_I - b1*forward_Q + a2*reflected_I - b2*reflected_Q 
        virt_Q = b1*forward_I + a1*forward_Q + b2*reflected_I + a1*reflected_Q

        loss_I = probe_I - virt_I
        loss_Q = probe_Q - virt_Q

        return np.mean(np.sqrt(loss_I**2 + loss_Q**2))


    bound = (-max_rescale,max_rescale)
    res = minimize(loss_function, 
                  [1.0, 0.0, 1.0, 0.0], 
                  bounds=[bound, bound, bound, bound])

    a1 = res.x[0]
    b1 = res.x[1]
    a2 = res.x[2]
    b2 = res.x[3]

    forward_I_corr = a1*forward_I - b1*forward_Q
    forward_Q_corr = b1*forward_I + a1*forward_Q
    reflected_I_corr = a2*reflected_I - b2*reflected_Q
    reflected_Q_corr = b2*reflected_I + a2*reflected_Q

    loss = np.sqrt(loss_function(res.x, []))

    return (a1, b1, a2, b2), (forward_I_corr, forward_Q_corr, 
                              reflected_I_corr, reflected_Q_corr), loss 

def calibrate_decay(probe_I, probe_Q, 
                    forward_I, forward_Q, 
                    reflected_I, reflected_Q, max_rescale=2.0, threshold=0.03):
    
    probe_I     = np.array(probe_I)    
    probe_Q     = np.array(probe_Q)  
    forward_I   = np.array(forward_I)  
    forward_Q   = np.array(forward_Q)
    reflected_I = np.array(reflected_I)
    reflected_Q = np.array(reflected_Q) 

    forward_A = np.sqrt(forward_I**2 + forward_Q**2)
    amplitude_threshold = max(forward_A)*threshold
    
    idx_decay_reverse = 0

    for idx, is_decay in enumerate(reversed(forward_A<amplitude_threshold)):
        if not is_decay:
            idx_decay_reverse = idx-1
            break

    probe_I_rev = np.flip(probe_I)
    probe_Q_rev = np.flip(probe_Q)
    reflected_I_rev = np.flip(reflected_I)
    reflected_Q_rev = np.flip(reflected_Q)


    decay_probe_I = probe_I_rev[:idx]
    decay_probe_Q = probe_Q_rev[:idx]
    decay_reflected_I = reflected_I_rev[:idx]
    decay_reflected_Q = reflected_Q_rev[:idx]

    filltop_probe_I = probe_I_rev[idx:]
    filltop_probe_Q = probe_Q_rev[idx:]
    filltop_reflected_I = reflected_I_rev[idx:]
    filltop_reflected_Q = reflected_Q_rev[idx:]
 
    def loss_function_decay(x, *args):
        '''
            loss function for decay
        '''
        a2 = x[0]
        b2 = x[1]

        loss_I = decay_probe_I-a2*decay_reflected_I+b2*decay_reflected_Q
        loss_Q = decay_probe_Q-b2*decay_reflected_I-a2*decay_reflected_Q

        return np.mean(np.sqrt(loss_I**2 + loss_Q**2))

    bound = (-max_rescale, max_rescale)
    res = minimize(loss_function_decay,
                   [1.0, 0.0],
                   bounds=[bound, bound])

    a2 = res.x[0]
    b2 = res.x[1]

    virt_forward_I = (filltop_probe_I-a2*filltop_reflected_I +
                      b2*filltop_reflected_Q)
    virt_forward_Q = (filltop_probe_Q-b2*filltop_reflected_I -
                      b2*filltop_reflected_Q)

    def loss_function_filltop(x, *args):
        '''
            loss function for the filling and flattop
        '''
        a1 = x[0]
        b1 = x[1]

        loss_I = virt_forward_I-a1*filltop_reflected_I+b1*filltop_reflected_Q
        loss_Q = virt_forward_Q-b1*filltop_reflected_I-a1*filltop_reflected_Q

        return np.mean(np.sqrt(loss_I**2 + loss_Q**2))

    res = minimize(loss_function_filltop,
                   [1.0, 0.0],
                   bounds=[bound, bound])

    a1 = res.x[0]
    b1 = res.x[1]

    forward_I_corr = a1*forward_I - b1*forward_Q
    forward_Q_corr = b1*forward_I + a1*forward_Q
    reflected_I_corr = a2*reflected_I - b2*reflected_Q
    reflected_Q_corr = b2*reflected_I + a2*reflected_Q

    def loss_function(x, *args):
        '''
            compare the loss of the difference between probe and
            forward+reflected
        '''
        a1 = x[0]
        b1 = x[1]
        a2 = x[2]
        b2 = x[3]

        virt_I = a1*forward_I - b1*forward_Q + a2*reflected_I - b2*reflected_Q
        virt_Q = b1*forward_I + a1*forward_Q + b2*reflected_I + a1*reflected_Q

        loss_I = probe_I - virt_I
        loss_Q = probe_Q - virt_Q

        return np.mean(np.sqrt(loss_I**2 + loss_Q**2))

    loss = np.sqrt(loss_function((a1, b1, a2, b2), []))

    return (a1, b1, a2, b2), (forward_I_corr, forward_Q_corr,
                              reflected_I_corr, reflected_Q_corr), loss


def calibrate_abcd(probe_cmplx,
                   vforw_cmplx,
                   vrefl_cmplx,
                   decay_range,
                   pulse_range=(0, 1),
                   k_add=1):

    decay_start_idx = int(decay_range[0]*len(probe_cmplx))
    decay_stop_idx = int(decay_range[1]*len(probe_cmplx))
    pulse_start_idx = int(pulse_range[0]*len(probe_cmplx))
    pulse_stop_idx = int(pulse_range[1]*len(probe_cmplx))

    # Select signals for decay phase
    decay_probe_cmplx = np.array([probe_cmplx[decay_start_idx:
                                              decay_stop_idx]]).transpose()
    decay_vforw_cmplx = np.array([vforw_cmplx[decay_start_idx:
                                              decay_stop_idx]]).transpose()
    decay_vrefl_cmplx = np.array([vrefl_cmplx[decay_start_idx:
                                              decay_stop_idx]]).transpose()
    # Select signals for pulse
    pulse_probe_cmplx = np.array([probe_cmplx[pulse_start_idx:
                                              pulse_stop_idx]]).transpose()
    pulse_vforw_cmplx = np.array([vforw_cmplx[pulse_start_idx:
                                              pulse_stop_idx]]).transpose()
    pulse_vrefl_cmplx = np.array([vrefl_cmplx[pulse_start_idx:
                                              pulse_stop_idx]]).transpose()

    # define A and B to have always same computation formula, see below
    A = -decay_vrefl_cmplx
    B = decay_vforw_cmplx

    S = inv(A.transpose().dot(A)).dot(A.transpose().dot(B))

    # compute x (=a+c) and y (=b+d) for entire pulse
    A = np.block([pulse_vforw_cmplx, pulse_vrefl_cmplx])
    B = pulse_probe_cmplx

    coeff1 = inv(A.transpose().dot(A)).dot(A.transpose().dot(B))

    x = coeff1[0]
    y = coeff1[1]

    # select signals for coupling computation
    a1 = pulse_vforw_cmplx
    a2 = pulse_vrefl_cmplx
    a3 = decay_vforw_cmplx
    a4 = decay_vrefl_cmplx

    b1 = pulse_probe_cmplx
    b2 = decay_probe_cmplx

    # define zeros for "big" matrix
    za = np.zeros((len(a3), 1))

    # put all vectors together to calibration in-/output matrix
    Wb = np.abs(S)
    Wc = k_add*Wb

    # here A and B are given by Eqn. 10 in Paper
    # with B = A * x with x =[a b c d]'
    B = np.vstack([b1, b2, za, np.abs(x), np.abs(y)])

    A = np.block([[a1, a2, a1, a2],
                  [za, za, a3, a4],
                  [a3, a4, za, za],
                  [np.abs(x)-Wc, 0, inv(Wc), 0],
                  [0, inv(Wb), 0, np.abs(y)-Wb]])

    # compute coefficients
    coeff = inv(A.transpose().dot(A)).dot(A.transpose().dot(B))
    # get a, b, c, d

    a = coeff[0]
    b = coeff[1]
    c = coeff[2]
    d = coeff[3]

    return (a, b, c, d)


def calibrate_abcd_scan(probe_cmplx,
                        vforw_cmplx,
                        vrefl_cmplx,
                        ql,
                        trace_time,
                        decay_start,
                        fit_decay_range,
                        fit_pulse_range,
                        decay_range,
                        pulse_range=(0, 1),
                        k_add_min=0.0,
                        k_add_max=2.0,
                        k_add_steps=100,
                        fit_degree=3,
                        frequency=1.3E9):
    '''
        Scan k_add to enforce the bandwidth/ql continuity
    '''
    amplitudes = np.linspace(k_add_min, k_add_max, k_add_steps)
    angles = np.linspace(0, 2*np.pi, k_add_steps)

    pulse_fit_start_idx = int(len(probe_cmplx) * fit_pulse_range[0])
    pulse_fit_stop_idx = int(len(probe_cmplx) * fit_pulse_range[1])
    decay_fit_start_idx = int(len(probe_cmplx) * fit_decay_range[0])
    decay_fit_stop_idx = int(len(probe_cmplx) * fit_decay_range[1])

    time_trace = np.linspace(0, trace_time, len(probe_cmplx))
    time_fit_pulse = time_trace[pulse_fit_start_idx:pulse_fit_stop_idx]
    time_fit_decay = time_trace[decay_fit_start_idx:decay_fit_stop_idx]

    R, P = np.meshgrid(amplitudes, angles)
    X, Y = R*np.cos(P), R*np.sin(P)

    flat_diff = np.zeros((k_add_steps, k_add_steps))
    bw_diff = np.zeros((k_add_steps, k_add_steps))
    det_diff = np.zeros((k_add_steps, k_add_steps))
    k_adds = np.zeros((k_add_steps, k_add_steps), dtype=complex)

    for i in range(k_add_steps):
        for j in range(k_add_steps):
            amp = amplitudes[i]
            angle = angles[j]

            (a, b, c, d) = calibrate_abcd(probe_cmplx,
                                          vforw_cmplx,
                                          vrefl_cmplx,
                                          decay_range,
                                          pulse_range,
                                          k_add=amp*np.exp(1.0j*angle))

            k_adds[j, i] = (amp*np.exp(1.0j*angle))
            real_vforw_cmplx = vforw_cmplx*a + vrefl_cmplx*b
            (bw, det) = bwdet(probe_cmplx, real_vforw_cmplx,
                              fs=len(probe_cmplx)/trace_time, ql=ql)

            pulse_fit_det_f = np.poly1d(np.polyfit(time_fit_pulse,
                                                   det[pulse_fit_start_idx:
                                                       pulse_fit_stop_idx],
                                                   fit_degree))

            decay_fit_det_f = np.poly1d(np.polyfit(time_fit_decay,
                                                   det[decay_fit_start_idx:
                                                       decay_fit_stop_idx],
                                                   fit_degree))

            bw_error = (np.mean(bw[pulse_fit_start_idx:pulse_fit_stop_idx]) -
                        np.mean(bw[decay_fit_start_idx:decay_fit_stop_idx]))

            det_error = (pulse_fit_det_f(decay_start*trace_time) -
                         decay_fit_det_f(decay_start*trace_time))

            flat_diff[j, i] = np.sqrt(bw_error*bw_error+det_error*det_error)
            bw_diff[j, i] = np.abs(bw_error)
            det_diff[j, i] = np.abs(det_error)

    min_k_add_idx = np.unravel_index(flat_diff.argmin(), flat_diff.shape)
    k_add = k_adds[min_k_add_idx]

    (a, b, c, d) = calibrate_abcd(probe_cmplx,
                                  vforw_cmplx,
                                  vrefl_cmplx,
                                  decay_range,
                                  pulse_range,
                                  k_add=k_add)

    real_vforw_cmplx = vforw_cmplx*a + vrefl_cmplx*b
    (bw, det) = bwdet(probe_cmplx, real_vforw_cmplx,
                      fs=len(probe_cmplx)/trace_time, ql=ql)

    #fig = plt.figure()
    #
    #ax = fig.add_subplot(111, projection='3d')
    #ax.plot_surface(X, Y, flat_diff, cmap=plt.cm.YlGnBu_r) 
    #ax.set_title("Physical error")
    #ax.set_xlabel("k_add real")
    #ax.set_ylabel("k_add imag")
    #ax.set_zlabel("error (Hz)")
   
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    #ax.plot_surface(X, Y, bw_diff, cmap=plt.cm.YlGnBu_r) 
    #ax.set_title("Bandwidth error")
    #ax.set_xlabel("k_add real")
    #ax.set_ylabel("k_add imag")
    #ax.set_zlabel("error (Hz)")
    #
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    #ax.plot_surface(X, Y, det_diff, cmap=plt.cm.YlGnBu_r) 
    #ax.set_title("Detuning error")
    #ax.set_xlabel("k_add real")
    #ax.set_ylabel("k_add imag")
    #ax.set_zlabel("error (Hz)")

    decay_fit_start = int(len(probe_cmplx) * fit_decay_range[0])
    decay_fit_stop = int(len(probe_cmplx) * fit_decay_range[1])
    pulse_fit_start = int(len(probe_cmplx) * fit_pulse_range[0])
    pulse_fit_stop = int(len(probe_cmplx) * fit_pulse_range[1])

    real_vforw_cmplx = vforw_cmplx*a + vrefl_cmplx*b
    (bw, det) = bwdet(probe_cmplx, real_vforw_cmplx, 
                      fs=len(probe_cmplx)/trace_time, ql=ql)

    pulse_fit_det_f = np.poly1d(np.polyfit(time_fit_pulse, 
                                           det[pulse_fit_start:pulse_fit_stop],
                                           fit_degree))

    decay_fit_det_f = np.poly1d(np.polyfit(time_fit_decay, 
                                           det[decay_fit_start:decay_fit_stop],
                                           fit_degree)) 

    decay_start_idx = int(len(probe_cmplx)*decay_start)

    #plt.figure()
    #plt.plot(bw)
    #plt.ylim(-500,500)

    #plt.figure()
    #plt.plot(det)
    #plt.plot(np.array(range(pulse_fit_start, decay_start_idx)), 
    #                        pulse_fit_det_f(time_trace[pulse_fit_start:decay_start_idx]), ls="--")

    #plt.plot(np.array(range(decay_start_idx, decay_fit_stop)),
    #                        decay_fit_det_f(time_trace[decay_start_idx:decay_fit_stop]), ls="--")
    #plt.ylim(-1000,1000)
    
    return (a, b, c, d), (bw, det), (X, Y, flat_diff)
