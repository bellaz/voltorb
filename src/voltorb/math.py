'''
    math helper module
'''

import numpy as np
from scipy.optimize import minimize
from scipy.signal import butter, filtfilt

def phase_wrap(phases):
    '''
        wrap phase between -180 and 180 degrees
    '''
    return (phases+180) % 360 - 180


def ap_to_cplx(amp, phase):
    '''
        amplitude and phase(degree) to complex
    '''
    return amp * np.exp(1j*np.deg2rad(phase))

def cplx_to_ap(cplx):
    '''
        complex to amplitude and phase(degree)
    '''
    return (np.abs(cplx), np.angle(cplx, deg=True))

def scale_match(amp1, amp2):
    '''
        find the scaling to apply to amp2 to minimize the difference compared to amp1
    '''
    return np.mean(amp1/amp2)

def rotate_match(pha1, pha2):
    '''
        find the rotation to apply to pha1 to minimize the difference compared to pha2
    '''
    return np.mean(phase_wrap(pha1 - pha2))

def butter_lowpass_filter(data, cutoff, fs, order=2):
    '''
        filter with a butterworth filter
    '''
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

def calculate_ql(decay_time, decay_probe_amp, frequency=1.3E9):
    '''
        calculate ql
    '''
    return  np.pi*frequency*decay_time/np.log(decay_probe_amp[0]/decay_probe_amp[-1])

