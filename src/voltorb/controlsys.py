'''
    Interface to control systems modules
'''
import collections
import copy
import re
import json

from .controlsys_backend.manager_doocs import ManagerDoocs
from .controlsys_backend.manager_tine import ManagerTine
from .controlsys_backend.manager_epics import ManagerEpics

ADDRESS_REGEX = '^(.+)://([^&]+)&*(.*)$' # index 1: scheme, index 2: address, : index3

ControlSysType = collections.namedtuple('ControlSysType', ['name', 'scheme', 'manager'])

# Available control systems
CONTROLSYS_DOOCS = ControlSysType('DOOCS', 'doocs', ManagerDoocs)
CONTROLSYS_TINE = ControlSysType('TINE', 'tine', ManagerTine)
CONTROLSYS_EPICS = ControlSysType('EPICS', 'epics', ManagerEpics)

CONTROLSYS_TYPES = [
    CONTROLSYS_DOOCS,
    CONTROLSYS_TINE,
    CONTROLSYS_EPICS]

DEFAULT_MTCA_ADDRESS = '/export/doocs/server/llrfCtrl/devMapFile.dmap'

def read(address, *addresses):
    '''
        read values from list of addresses.
        If a single address is provided, the read data from the channel is returned
        otherwise a list of the read data from each address is returned
        Useful to make coordinate reads

        :param address: the address to read
        :type address: str
        :param addresses: additional addresses
        :type addresses: list(str)
        :returns: the readed values
    '''
    addresses = list(copy.copy(addresses))
    addresses.insert(0, address)
    parsed_addresses = parse_addresses(addresses)
    subdivided_addresses = subdivide_backends(parsed_addresses)

    results = []

    for manager in subdivided_addresses:
        backend_addresses = subdivided_addresses[manager]
        backend_indices = [idx for (idx, _, _) in backend_addresses]
        backend_results = manager.manager.read([(address, query) for
                                                (_, address, query) in
                                                backend_addresses])
        results.extend(list(zip(backend_indices, backend_results)))

    results = reorder_divided(results)

    if len(results) == 1:
        return results[0]

    return results

def write(address, value):
    '''
        write a value from an address

        :param values: the dictionary to write in the file
        :type values: dict
    '''
    #pylint: disable=unbalanced-tuple-unpacking
    [(manager, address, query)] = parse_addresses([address])
    manager.manager.write((address, query), value)

def parse_addresses(addresses):
    '''
        Parse addresses in tuples with (Controlsys Type/Address/Additional params)

        :param addresses: addresses
        :type addresses: list(str)
        :returns: list((AbstractManager, str, dict))
    '''

    parsed = []

    for address in addresses:
        match = re.search(ADDRESS_REGEX, address)
        manager = search_type_by_scheme(match.group(1))
        address_parsed = match.group(2)
        query_dict = parse_query(match.group(3))
        parsed.append((manager, address_parsed, query_dict))

    return parsed


def parse_query(query_str):
    '''
        produce an array using query_str
    '''
    splitted = [el.split('=', 1) for el in query_str.split(';')]
    if splitted == [['']]:
        splitted = []

    return {el[0] : json.loads(el[1]) for el in splitted}

def search_type_by_scheme(scheme):
    '''
        search a control system by name

        :param scheme: the scheme used to guess the control system
        :type scheme: str
        :returns: the found control system
        :raises: RuntimeError
    '''
    for control_system in CONTROLSYS_TYPES:
        if scheme == control_system.scheme:
            return control_system

    raise RuntimeError('{} is not a valid FileType schemeension'.format(scheme))

def subdivide_backends(parsed_addresses):
    '''
        subdivide parsed_address per_backend

        :param parsed_addresses: list of parsed address objects
        :type parsed_addresses: list((AbstractManager, str, dict))
        :returns: dict(list(idx, str, dict))
    '''

    return_divided = {}

    for (idx, parsed_addr) in enumerate(parsed_addresses):

        (manager, addr_parsed, query) = parsed_addr
        if manager not in return_divided:
            return_divided[manager] = []

        return_divided[manager].append((idx, addr_parsed, query))

    return return_divided

def reorder_divided(divided_results):
    '''
        reorder and return elements based on their idx

        :param divided_results: listo of indices and results
        :type divided_results: list((idx, result))
        :returns: list(result)
    '''

    results = copy.copy(divided_results)
    results.sort(key=lambda x: x[0])
    return list(list(zip(*results))[1])
