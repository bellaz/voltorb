'''
    Interface to dictfiles
'''
import collections
import os.path

from .dictfile_backend.manager_abstract import ManagerAbstract as AbstractDictFile
from .dictfile_backend.manager_hdf5 import ManagerHDF5
from .dictfile_backend.manager_json import ManagerJSON
from .dictfile_backend.manager_matfile import ManagerMatFile
from .dictfile_backend.manager_pickle import ManagerPickle
from .dictfile_backend.manager_yaml import ManagerYAML
from .dictfile_backend.manager_excel2010 import ManagerExcel2010
from .dictfile_backend.manager_npz import ManagerNPZ

DictFileType = collections.namedtuple('DictFileType', ['name', 'ext', 'manager'])

# Available file types
DICTFILE_HDF5 = DictFileType('HDF5', 'h5', ManagerHDF5)
DICTFILE_JSON = DictFileType('JSON', 'json', ManagerJSON)
DICTFILE_YAML = DictFileType('YAML', 'yml', ManagerYAML)
DICTFILE_MATFILE = DictFileType('MATFILE', 'mat', ManagerMatFile)
DICTFILE_PICKLE = DictFileType('PICKLE', 'pickle', ManagerPickle)
DICTFILE_EXCEL2010 = DictFileType('EXCEL2010', 'xlsx', ManagerExcel2010)
DICTFILE_NPZ = DictFileType('NUMPY', 'npz', ManagerNPZ)

DICTFILE_TYPES = [
        DICTFILE_HDF5,
        DICTFILE_JSON,
        DICTFILE_YAML,
        DICTFILE_MATFILE,
        DICTFILE_PICKLE,
        DICTFILE_EXCEL2010,
        DICTFILE_NPZ]

def search_type_by_name(name):
    '''
        search a filetype by name

        :param name: the name of the type used to guess the filetype
        :type name: str
        :returns: the found filetype
        :raises: RuntimeError
    '''
    for filetype in DICTFILE_TYPES:
        if name == filetype.name:
            return filetype

    raise RuntimeError('{} is not a valid FileType name'.format(name))

def search_type_by_ext(ext):
    '''
        search a filetype by name

        :param ext: the extension used to guess the filetype
        :type ext: str
        :returns: the found filetype
        :raises: RuntimeError
    '''
    for filetype in DICTFILE_TYPES:
        if ext == filetype.ext:
            return filetype

    raise RuntimeError('{} is not a valid FileType extension'.format(ext))

def guess_type_by_filename(filename):
    '''
        search a filetype by filename

        :param filename: the filename used to guess the filetype
        :type filename: str
        :returns: the found filetype
        :raises: RuntimeError
    '''
    ext = os.path.splitext(filename)[1]

    if bool(ext) and ext[0] == '.':
        ext = ext[1:]

    return search_type_by_ext(ext)

class DictFile(AbstractDictFile):
    '''
        Dictionary file
    '''
    def __init__(self, filename, name=None, ext=None, manager=None):
        '''
            I/O dictionary file facility

            If no hints are passed to the constructor (name,ext,manager) the
            dictionary type is guessed from the filename

            :param filename: the filename to use
            :type filename: str
            :param name: the type of dictionary file to use
            :type name: str
            :param ext: the extension of dictionary file to use
            :type ext: str
            :param manager: the manager of dictionary file to use
            :retuns: Instance of DictFile
            :raises: RuntimeError
        '''
        super(DictFile, self).__init__(filename)

        self.manager = None
        self.filename = filename

        if name is not None:
            self.manager = search_type_by_name(name).manager
        elif ext is not  None:
            self.manager = search_type_by_ext(ext).manager
        elif manager is not None:
            self.manager = manager
        else:
            self.manager = guess_type_by_filename(filename).manager

        self.ioexecutor = self.manager(self.filename)

    def read(self):
        '''
            read a dictionary from a file

            :returns: a dictionary of the readed values
        '''
        return self.ioexecutor.read()

    def write(self, values):
        '''
            write a dictionary from a file

            :param values: the dictionary to write in the file
            :type values: dict
        '''
        self.ioexecutor.write(values)

    def update(self, added_values):
        '''
            add values to a currently existing file

            :param added_values: the dictionary to add in the file
            :type added_values: dict
        '''
        self.ioexecutor.update(added_values)

def read(filename, name=None, ext=None, manager=None):
    '''
        read from a dictionary file
    '''
    return DictFile(filename, name, ext, manager).read()

def write(filename, dictionary, name=None, ext=None, manager=None):
    '''
        write to a dictionary file

    '''
    DictFile(filename, name, ext, manager).write(dictionary)

def update(filename, dictionary, name=None, ext=None, manager=None):
    '''
        update to a dictionary file

    '''
    DictFile(filename, name, ext, manager).update(dictionary)
