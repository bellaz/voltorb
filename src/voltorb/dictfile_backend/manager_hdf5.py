'''
    HDF5 file manager
'''
import h5py
import numpy as np
from .manager_abstract import ManagerAbstract
from .sanitize_data import sanitize_numpy

class ManagerHDF5(ManagerAbstract):
    '''
        Manager for HDF5 file format
    '''
    def read(self):
        '''
            read dictionary from file

            :returns: a dictionary of the read data
        '''
        return_dict = dict(h5py.File(self.filename, 'r'))
        sanitized = sanitize_numpy(return_dict)
        for key, value in sanitized.items():
            if value.dtype.type == np.dtype(bytes).type:
                sanitized[key] = np.array(value, dtype='U')

        return sanitized

    def write(self, values):
        '''
            write dictionary from file

            :param values: dictionary to write to the file
            :type values: dict
        '''
        with h5py.File(self.filename, 'w') as datafile:
            sanitized = sanitize_numpy(values)
            for key, value in sanitized.items():
                if value.dtype.type == np.dtype(str).type:
                    datafile.create_dataset(key, data=np.array(value, dtype='S'))
                else:
                    datafile.create_dataset(key, data=value)
