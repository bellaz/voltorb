'''
    Excel/Excel2010/ODS file manager
'''
import re
import openpyxl as xl
from .encode_spreadsheet import encode, decode
from .manager_abstract import ManagerAbstract

COMPLEX_REGEX = r'^=COMPLEX\((.+),(.+)\)'

class ManagerExcel2010(ManagerAbstract):
    '''
        Manager for Excel2010 file format
    '''
    def read(self):
        '''
            read dictionary from file
        '''
        book = xl.load_workbook(self.filename, read_only=True)
        sheet = book['Data']
        parsed_rows = [[self.fun_to_complex(cell.value) for cell in row] for row in sheet.rows]
        return decode(parsed_rows)

    def write(self, values):
        '''
            write dictionary from file
        '''
        encoded_rows = encode(values)
        encoded_rows = [[self.complex_to_fun(elem) for elem in row] for row in encoded_rows]

        book = xl.Workbook(write_only=True)
        sheet = book.create_sheet('Data')
        for row in encoded_rows:
            sheet.append(row)

        book.save(self.filename)

    @staticmethod
    def complex_to_fun(elem):
        '''
            convert value if is instance of complex to excel value
        '''
        if isinstance(elem, complex):
            return '=COMPLEX({},{})'.format(elem.real, elem.imag)
        return elem

    @staticmethod
    def fun_to_complex(elem):
        '''
            convert value from excel COMPLEX function to python complex
        '''
        if isinstance(elem, str):
            matched = re.search(COMPLEX_REGEX, elem)
            if matched:
                return complex(float(matched.group(1)), float(matched.group(2)))
        return elem
