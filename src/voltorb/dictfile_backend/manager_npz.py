'''
    HDF5 file manager
'''
import numpy as np
from .manager_abstract import ManagerAbstract
from .sanitize_data import sanitize_numpy

class ManagerNPZ(ManagerAbstract):
    '''
        Manager for HDF5 file format
    '''
    def read(self):
        '''
            read dictionary from file

            :returns: a dictionary of the read data
        '''
        return_dict = np.load(self.filename)
        sanitized = sanitize_numpy(return_dict)
        return sanitized

    def write(self, values):
        '''
            write dictionary from file

            :param values: dictionary to write to the file
            :type values: dict
        '''
        np.savez(self.filename, **values)
