'''
    Excel/Excel2010/ODS file manager
'''
from .sanitize_data import sanitize_numpy, sanitize_native


def encode(values):
    '''
        encode dict of values in a spreadsheet-like list of elements

        :param values: dictionary of values to be encoded
        :type values: dict
        :return: spreadsheet-like list of elements
    '''
    sanitized = sanitize_native(values)
    scalar_values = [[], []]
    vector_values = []
    matrix_values = []
    #
    # The scalar elements are put in the first two columns
    # The vector (1D) elements are put after the third column
    # The matrix (2D) elements are put after the vector elements plus 1 space
    for key, value in sanitized.items():
        if isinstance(value, list):
            if bool(value) and isinstance(value[0], list):
                for elem in value:
                    matrix_values.append([key] + elem)
            else:
                vector_values.append([key] + value)
        else:
            scalar_values[0].append(key)
            scalar_values[1].append(value)

    rows = len(scalar_values[0])
    rows = max([len(v) for v in vector_values] + [rows])
    rows = max([len(v) for v in matrix_values] + [rows])
    table = ([scalar_values[0], scalar_values[1], []]
             + vector_values + [[]]
             + matrix_values)
    table = [elem+[None]*(rows-len(elem)) for elem in table]
    table = [list(elem) for elem in zip(*table)]
    return table

def __reduce_col(col):
    '''
        returns only the part of the columns that doesn't contain
        'None' elements

        :param col: a spreadsheet column
        :type col: list
        :return: the reduced column
    '''
    if None not in col:
        return col
    return col[0:col.index(None)]

def decode(table):
    '''
        decode a spreadsheet like list of values in a dictionary

        :param table: a spreadsheet-like list of element
        :type table: list
        :return: dictionary of the decoded elements
    '''
    result = {}
    if not bool(table):
        return {}

    num_col = len(table[0])
    table = [row + [None] * (num_col - len(row)) for row in table]
    table = [__reduce_col(col) for col in zip(*table)]

    if len(table) > 1:
        for i in range(len(table[0])):
            if table[0][i] is None:
                break
            result[table[0][i]] = table[1][i]

    i = 3

    for i in range(3, len(table)):
        if not bool(table[i]):
            break
        result[table[i][0]] = table[i][1:]

    i += 1

    for i in range(i, len(table)):
        if not bool(table[i]):
            break
        if table[i][0] not in result:
            result[table[i][0]] = []
        result[table[i][0]].append(table[i][1:])

    return sanitize_numpy(result)
