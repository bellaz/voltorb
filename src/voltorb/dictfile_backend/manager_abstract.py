'''
    Abstract file manager class
'''
from abc import ABC, abstractmethod

class ManagerAbstract(ABC):
    '''
        Abstract manager class
    '''
    def __init__(self, filename):
        '''
            Initialize the manager
        '''
        self.filename = filename
        super(ManagerAbstract, self).__init__()

    @abstractmethod
    def read(self):
        '''
            read dictionary from file
        '''

    @abstractmethod
    def write(self, values):
        '''
            write dictionary to file
        '''

    def update(self, added_values):
        '''
            update a file adding/substituting value_dictionary
        '''
        values = self.read()
        values.update(added_values)
        self.write(values)
