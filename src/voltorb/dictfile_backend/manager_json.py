'''
    JSON file manager
'''
import json
import numpy as np
from .sanitize_data import sanitize_numpy, sanitize_native
from .manager_abstract import ManagerAbstract

class ManagerJSON(ManagerAbstract):
    '''
        Manager for JSON file format
    '''
    def read(self):
        '''
            read dictionary from file
        '''
        sanitized_complex = {key:self.decode_complex(value) for key, value in
                             json.load(self.filename).items()}
        return sanitize_numpy(sanitized_complex)

    def write(self, values):
        '''
            write dictionary from file
        '''
        sanitized_complex = {key:self.encode_complex(value) for
                             key, value in
                             sanitize_native(values).items()}

        json.dump(sanitized_complex, self.filename, indent=2)

    @staticmethod
    def encode_complex(value):
        '''
            encode complex in JSON
        '''
        if isinstance(value, complex):
            return ['cplx', value.real, value.imag]

        if isinstance(value, list) and bool(value):
            if isinstance(value[0], complex):
                arr = [[cplx.real, cplx.imag] for cplx in value]
                arr.insert(0, 'cplx')
                return arr
            if isinstance(value[0], list) and bool(value[0]):
                if isinstance(value[0][0], complex):
                    arr = [[[cplx.real, cplx.imag] for cplx in cplxlist] for
                           cplxlist in
                           value]
                    arr.insert(0, 'cplx')
                    return arr

        return value

    @staticmethod
    def decode_complex(value):
        '''
            decode complex in JSON
        '''
        if isinstance(value, list) and bool(value) and value[0] == 'cplx':
            arr = value[1:]
            if arr == [[]]:
                return np.array([], dtype=np.complex)
            if arr == [[[]]]:
                return np.array([[]], dtype=np.complex)

            if isinstance(arr[0], list):
                if isinstance(arr[0][0], list):
                    return [[complex(x[0], x[1]) for x in y] for y in arr]
                return [complex(x[0], x[1]) for x in arr]

            return complex(arr[0], arr[1])

        return value
