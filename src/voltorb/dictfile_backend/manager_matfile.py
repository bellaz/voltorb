'''
    MatFile file manager
'''
import scipy.io as sio
from .sanitize_data import sanitize_numpy, sanitize_native
from .manager_abstract import ManagerAbstract

class ManagerMatFile(ManagerAbstract):
    '''
        Manager for MatFile file format
    '''
    def read(self):
        '''
            read dictionary from file
        '''
        result = sanitize_native(sio.loadmat(self.filename, squeeze_me=True))
        del result['__header__']
        del result['__version__']
        del result['__globals__']
        return sanitize_numpy(result)

    def write(self, values):
        '''
            write dictionary from file
        '''
        sio.savemat(self.filename, sanitize_native(values))
