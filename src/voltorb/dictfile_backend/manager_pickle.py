'''
    Pickle file manager
'''
import pickle
from .sanitize_data import sanitize_numpy, sanitize_native
from .manager_abstract import ManagerAbstract

class ManagerPickle(ManagerAbstract):
    '''
        Manager for Pickle file format
    '''
    def read(self):
        '''
            read dictionary from file
        '''
        return sanitize_numpy(pickle.load(self.filename))

    def write(self, values):
        '''
            write dictionary from file
        '''
        return pickle.dump(sanitize_native(values), self.filename)
