'''
    This module does the appropriate sanitization of the input/output data. to be
    written and read.
'''

# For this version of sanitize data, apart scalar values, only numeric 1D and 2D
# arrays are supported

# all complex numbers are casted to scalar!!!

import numpy as np

def sanitize_value(value):
    '''
        sanitize single value
    '''
    inferred = np.array(value)

    if inferred.dtype.type == np.dtype(object).type:
        raise TypeError('unknown passed value')

    if len(inferred.shape) > 2:
        raise TypeError('at maximum 2 dimensional arrays are allowed')

    return inferred

def sanitize_numpy(dictionary):
    '''
        sanitize a dictionary of values
    '''
    return {key : sanitize_value(value) for key, value in dictionary.items()}

def numpy_to_native(value):
    '''
        transform numpy value to python value.
        unsafe to use on non sanitized data
    '''

    if isinstance(value, np.ndarray):
        if value.shape == ():
            return value.item()
        return value.tolist()

    return TypeError('unknown passed value')

def sanitize_native(dictionary):
    '''
        sanitize a dictionary of values.
        numpy elements becomes native
    '''
    return {key : numpy_to_native(value) for key, value in sanitize_numpy(dictionary).items()}
