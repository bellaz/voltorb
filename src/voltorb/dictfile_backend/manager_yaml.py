'''
    YAML file manager
'''
import yaml
from .sanitize_data import sanitize_numpy, sanitize_native
from .manager_abstract import ManagerAbstract

class ManagerYAML(ManagerAbstract):
    '''
        Manager for YAML file format
    '''
    def read(self):
        '''
            read dictionary from file
        '''
        return sanitize_numpy(yaml.load(self.filename))

    def write(self, values):
        '''
            write dictionary from file
        '''
        yaml.dump(sanitize_native(values), self.filename)
