'''
    Utility function to parse command line arguments
'''

import os
import glob
import pkg_resources
import collections
import argparse
import toml

EntryPointManager = collections.namedtuple('EntryPointManager', ['desc', 'manager', 'main', 'run'])

_conf_dir = pkg_resources.resource_filename(__name__, '../../conf')
_confs = [os.path.relpath(conf_path, _conf_dir) for
          conf_path in
          glob.glob(_conf_dir + '/**/*.toml', recursive=True)]

def substitute_template(dict_, cavity, module, prefix='template_'):
    '''
        Substitute cavity and module in dict_ that has 'template'
        Returns a new dictionary
    '''
    return {k : v.format(cavity, module) for k, v in dict_.values() if prefix in k}


def merge_selected(orig, dest, *items):
    '''
        put items from orig to dest
    '''
    for item in items:
        dest[item] = orig[item]

def parser_default(desc=''):
    '''
        Default parser manager. It provides the interface to .toml config files
        and command line arguments
    '''
    def decorator(entry_point):
        '''
            Decorator
        '''
        def manager(parser):
            '''
                Add keys to a parser
            '''
            parser.add_argument('-c', '--config',
                                dest='config',
                                action='append',
                                metavar='CONF',
                                nargs=1,
                                type=str,
                                help='TOML configuration file path',
                                default=None)

            parser.add_argument('-l', '--local',
                                dest='local',
                                choices=_confs,
                                action='append',
                                metavar='LOCAL_CONF',
                                nargs=1,
                                help=argparse.SUPPRESS,
                                default=None)

            parser.add_argument('-d', '--dump',
                                dest='dump',
                                action='store_true',
                                help='''dump the configuration passed to the program and exit''',
                                default=False)

            parser.add_argument('-L', '--local-configurations',
                                dest='local_confs',
                                action='store_true',
                                help='''list the available local configurations and exit''',
                                default=False)

            parser.add_argument('set',
                                type=str,
                                metavar='KEY=VALUE',
                                action='store',
                                nargs='*',
                                help='''change the configuration values using key-value pairs.
                                        if a value is a space-containting string, it should be
                                        wrapped within quotes''',
                                default={})

            parser.set_defaults(func=main)

        def main(parsed):
            '''
                Handle parsed arguments
            '''
            if parsed.local_confs:
                for conf in _confs:
                    print(conf)
                exit(0)

            args = {}

            if parsed.local is not None and parsed.local != [[]]:
                print(parsed.local)
                for config in parsed.local[0]:
                    config_args = toml.load(os.path.join(_conf_dir, config))
                    args = merge_dict(args, config_args)

            if parsed.config is not None and parsed.config != [[]]:
                for config in parsed.config[0]:
                    config_args = toml.load(config)
                    args = merge_dict(args, config_args)

            cmd_args = parse_vars(parsed.set)
            args = merge_dict(args, cmd_args)

            if parsed.dump:
                print(toml.dumps(args))
                exit(0)

            return entry_point(args)

        def run():
            '''
                Run the entry point
            '''
            parser = argparse.ArgumentParser(description=desc)
            manager(parser)
            main(parser.parse_args())

        return EntryPointManager(desc=desc, manager=manager, main=main, run=run)
    return decorator

def parser_plain(desc=''):
    '''
        "plain" parser manager. It passes a plain list of arguments to the entry point
    '''
    def decorator(entry_point):
        '''
            Decorator
        '''
        def manager(parser):
            '''
                Add keys to a parser
            '''
            parser.add_argument('set',
                                type=str,
                                metavar='ITEM',
                                action='store',
                                nargs='*',
                                help='''a list of passed arguments''',
                                default={})

            parser.set_defaults(func=main)

        def main(parsed):
            '''
                Handle parsed arguments
            '''
            return entry_point(parsed.set)

        def run():
            '''
                Run the entry point
            '''
            parser = argparse.ArgumentParser(description=desc)
            manager(parser)
            main(parser.parse_args())

        return EntryPointManager(desc=desc, manager=manager, main=main, run=run)
    return decorator

def parser_custom(custom_manager, desc=''):
    '''
        Custom parser manager. It uses a custom menager as parser
    '''
    def decorator(entry_point):
        '''
            Decorator
        '''
        def manager(parser):
            '''
                Add keys to a parser
            '''
            custom_manager(parser)
            parser.set_defaults(func=main)

        def main(parsed):
            '''
                Handle parsed arguments
            '''
            return entry_point(parsed.set)

        def run():
            '''
                Run the entry point
            '''
            parser = argparse.ArgumentParser(description=desc)
            manager(parser)
            main(parser.parse_args())

        return EntryPointManager(desc=desc, manager=manager, main=main, run=run)
    return decorator


def parser_none(desc=''):
    '''
        The entry point is called without arguments
    '''
    def decorator(entry_point):
        '''
            Decorator
        '''
        def manager(parser):
            '''
                Add keys to a parser
            '''
            parser.set_defaults(func=main)

        def main(parsed):
            '''
                Handle parsed arguments
            '''
            #pylint: disable=unused-argument
            return entry_point()

        def run():
            '''
                Run the entry point
            '''
            parser = argparse.ArgumentParser(description=desc)
            manager(parser)
            main(parser.parse_args())

        return EntryPointManager(desc=desc, manager=manager, main=main, run=run)
    return decorator

def parse_var(key_value):
    '''
        Parse a key, value pair, separated by '='
        That's the reverse of ShellArgs.

        On the command line (argparse) a declaration will typically look like:
            foo=hello
        or
            foo="hello world"
    '''
    items = key_value.split('=', 1)
    key = items[0].strip() # we remove blanks around keys, as is logical
    try:
        value = items[1]
    except KeyError:
        print('element "{}" not = separated'.format(key_value))
        exit(1)

    return (key, value)


def parse_vars(items):
    '''
        Parse a series of key-value pairs and return a dictionary
    '''
    data = {}

    if items:
        for item in items:
            key, value = parse_var(item)
            try:
                data[key] = eval(value)
            except:
                data[key] = value

    return data

def merge_dict(base, added, path=None):
    '''
        Merges added into base
    '''
    if path is None:
        path = []

    for key in added:
        if key in base:
            if isinstance(base[key], dict) and isinstance(added[key], dict):
                merge_dict(base[key], added[key], path + [str(key)])
            elif base[key] == added[key]:
                pass # same leaf value
            else:
                base[key] = added[key]
        else:
            base[key] = added[key]
    return base
