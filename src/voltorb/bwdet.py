import numpy as np
from scipy.fftpack import diff

def bwdet(probe_cmplx, vforw_cmplx, 
          fs, ql, frequency=1.3E9, 
          beamloading_cmplx=0, B=0, 
          periodic=False):
    '''
        return a cavity bandwidth and detuning in Hertz
    '''
    K = frequency/ql
    deriv = np.gradient(probe_cmplx)*fs/np.pi
    if periodic:
        deriv = diff(probe_cmplx)*fs/np.pi
    A2 = probe_cmplx.conjugate()*probe_cmplx
    result = probe_cmplx.conjugate()*(K*vforw_cmplx - 
                                      deriv + 
                                      B*beamloading_cmplx)/A2
    return (result.real, result.imag)
