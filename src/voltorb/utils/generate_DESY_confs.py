#pylint: disable=invalid-name
'''
    This module generate the DESY toml configurations
'''

import os
import toml
from .generate_XFEL_confs import generate_XFEL_confs
from .generate_FLASH_confs import generate_FLASH_confs
from .generate_CMTB_confs import generate_CMTB_confs

DEFAULT_CONF = {
    'ofile' : "out.xlsx",
    'delay' : 1,
    'iq_bit_len' : 17,
    'a2_bit_len' : 25,
    'unity_bit_len' : 15,
    'table_bit_len' : 10,
    'dmapfile' : "/export/doocs/server/llrfCtrl_server/devMapFile.dmap",
    'max_val' : 131072,
    'scale_factor' : 8192}

def generate_DESY_confs(path):
    '''
        Generate the DESY toml configurations
    '''
    os.mkdir(os.path.join(path, 'DESY'))
    desypath = os.path.join(path, 'DESY')
    generate_XFEL_confs(desypath)
    generate_FLASH_confs(desypath)
    generate_CMTB_confs(desypath)

    print('Creating  default.toml')
    with open(os.path.join(desypath, 'default.toml'), 'w') as f_default:
        toml.dump(DEFAULT_CONF, f_default)
