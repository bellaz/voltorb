#pylint: disable=invalid-name
'''
    This module generate the FLASH toml configurations for cavity and stations
'''

import os
import toml

CMTB_SYSTEM = {
    'cavity_name' : 'C1',
    'probe_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Probe.amplitude&spectrum="data"',
    'probe_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Probe.phase&spectrum="data"',
    'vforw_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Forward.amplitude&spectrum="data"',
    'vforw_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Forward.phase&spectrum="data"',
    'vrefl_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Reflected.AMPL&spectrum="data"',
    'vrefl_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Reflected.PHASE&spectrum="data"',
    'cav_ql_addr' : 'doocs://TTF.RF/LLRF.DIAGNOSTICS/C1.M1.CMTB/CAV_QL',
    'coupler_value_addr' : 'doocs://TTF.RF/CPL.MOVER/MOTOR1/POS.SET',
    'coupler_enable_addr' : 'doocs://TTF.RF/CPL.MOVER/MOTOR1/CMD',
    'piezo_enable_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C1.M1.CMTB/PIEZO.ENA',
    'dac_enable_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C1.M1.CMTB/DAC.ENA',
    'piezo_sensor_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C1.M1.CMTB/PIEZO_SENSOR&spectrum="data"',
    'piezo_ff_table_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C1.M1.CMTB/CTABLE&spectrum="data"',
    'piezo_ff_table_enable_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C1.M1.CMTB/CTABLE.ENA&spectrum="data"',
    'sp_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.SetPoint.amplitude',
    'ff_table_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.FeedForward.Table.I',
    'ff_table_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.FeedForward.Table.Q',
    'corr_enable_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.FeedForward.Correction.enable',
    'corr_table_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.FeedForward.Correction.I',
    'corr_table_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.FeedForward.Correction.Q',
    'amplifier_out_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/PowerAmpOutput.amplitude',
    'amplifier_out_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/PowerAmpOutput.phase',
    'amplifier_in_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/PowerAmpInput.amplitude',
    'amplifier_in_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/PowerAmpInput.phase',
    'vm_output_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.Output.Total.DAQ.I',
    'vm_output_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.Output.Total.DAQ.Q',
    'vm_output_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/VMMonitor.amplitude',
    'vm_output_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/VMMonitor.phase',
    'vm_dac_offset_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VM.SINCAV.CMTB/Controller.VectorModulator.DacOffset.I',
    'vm_dac_offset_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VM.SINCAV.CMTB/Controller.VectorModulator.DacOffset.Q',
    'pha_out_rotation_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.SINCAV.CMTB/Controller.Output.Calibration.scale',
    'amp_out_scale_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.SINCAV.CMTB/Controller.Output.Calibration.angle',
    'vs_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.VectorSum.DAQ.amplitude&spectrum="data"',
    'vs_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/SINCAV.CMTB/Controller.VectorSum.DAQ.phase&spectrum="data"'}

#pylint: disable=invalid-name
def generate_CMTB_single_confs(path):
    '''
        Generate CMTB toml configurations for cavity and stations
    '''
    with open(os.path.join(path, 'single.toml'), 'w') as f:
         print('Creating ', os.path.join(path, 'single.toml'))
         toml.dump(CMTB_SYSTEM, f)
