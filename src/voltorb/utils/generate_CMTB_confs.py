#pylint: disable=invalid-name
'''
    This module generate the CMTB toml configurations for cavity and stations
'''
import os
from .generate_CMTB_single_confs import generate_CMTB_single_confs
from .generate_CMTB_multi_confs import generate_CMTB_multi_confs

#pylint: disable=invalid-name
def generate_CMTB_confs(path):
    '''
        Generate the CMTB toml configurations for cavity and stations
    '''
    os.mkdir(os.path.join(path, 'CMTB'))
    generate_CMTB_single_confs(os.path.join(path, 'CMTB'))
    generate_CMTB_multi_confs(os.path.join(path, 'CMTB'))
