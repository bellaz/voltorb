#pylint: disabl:vne=invalid-name
'''
    This module generate the FLASH toml configurations for cavity and stations
'''

import os
import copy
import toml

CMTB_CAVITY = {
    'cavity_name' : 'C{}.M1.CMTB',
    'fpc_temp_addr' : 'doocs://TTF.RF/MOBILE_INTERLOCK/C{}.MTS/CH06',
    'vforw_power_addr' : 'doocs://TTF.RF/GPIB/C{}.MTS.PMAIN/CH0.VALUE',
    'probe_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/PROBE.AMPL&spectrum="data"',
    'probe_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/PROBE.PHASE&spectrum="data"',
    'probe_amp_scale_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/PROBE.CAL_SCA&spectrum="data"',
    'probe_pha_rotation_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/PROBE.CAL_ROT&spectrum="data"',
    'vforw_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VFORW.AMPL&spectrum="data"',
    'vforw_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VFORW.PHASE&spectrum="data"',
    'vforw_amp_scale_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VFORW.CAL_SCA&spectrum="data"',
    'vforw_pha_rotation_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VFORW.CAL_ROT&spectrum="data"',
    'vrefl_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VREFL.AMPL&spectrum="data"',
    'vrefl_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VREFL.PHASE&spectrum="data"',
    'vrefl_amp_scale_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VREFL.CAL_SCA&spectrum="data"',
    'vrefl_pha_rotation_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/C{}.M1.CMTB/VREFL.CAL_ROT&spectrum="data"',
    'cav_ql_addr' : 'doocs://TTF.RF/LLRF.DIAGNOSTICS/C{}.M1.CMTB/CAV_QL',
    'coupler_value_addr' : 'doocs://TTF.RF/CPL.MOVER/MOTOR{}/POS.SET',
    'coupler_enable_addr' : 'doocs://TTF.RF/CPL.MOVER/MOTOR{}/CMD',
    'piezo_enable_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/PIEZO.ENA',
    'dac_enable_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/DAC.ENA',
    'piezo_dc_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/DAC_OFFSET_SP',
    'piezo_sensor_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/PIEZO_SENSOR&spectrum="data"',
    'piezo_ff_table_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/CTABLE&spectrum="data"',
    'piezo_voltage_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/PIEZO_VOLTAGE&spectrum="data"',
    'piezo_ff_table_enable_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/CTABLE.ENA&spectrum="data"',
    'piezo_fb_enable_addr' : 'doocs://TTF.RF/LLRF.PIEZO/C{}.M1.CMTB/FB.ENA&spectrum="data"'}

CMTB_STATION = {
    'sp_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/SP.AMPL',
    'ff_table_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/FF_I.TD&spectrum="data"',
    'ff_table_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/FF_Q.TD&spectrum="data"',
    'corr_enable_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/FF_USER.ON.ENA',
    'corr_table_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/FF_USER_I.TD&spectrum="data"',
    'corr_table_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/FF_USER_Q.TD&spectrum="data"',
    'vm_output_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/COUT_I.TD&spectrum="data"',
    'vm_output_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/COUT_Q.TD&spectrum="data"',
    'vm_output_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VM.CMTB/VM.AMPL&spectrum="data"',
    'vm_output_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VM.CMTB/VM.PHASE&spectrum="data"',
    'vm_dac_offset_I_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VM.CMTB/DAC_OFFSET_I',
    'vm_dac_offset_Q_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VM.CMTB/DAC_OFFSET_Q',
    'pha_out_rotation_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/ANGLE.OUT_ROTATION',
    'ovc_pha_rotation_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/OVC.CMTB/ROTATION',
    'amp_out_scale_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/CTRL.CMTB/AMPL.OUT_ROTATION',
    'ovc_amp_scale_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/OVC.CMTB/SCALE',
    'vs_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VS.CMTB/AMPL.TD&spectrum="data"',
    'vs_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/VS.CMTB/PHASE.TD&spectrum="data"',
    'iot_input_power_addr' : 'tine://MHF/IOT_TEST/IOT/P_Treiber_VL',
    'iot_output_power_addr' : 'tine://MHF/IOT_TEST/IOT/Ausgangsleistung_VL',
    'pulse_delay_percent_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/MAIN.CMTB/PULSE_DELAY_PERCENT',
    'pulse_filling_percent_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/MAIN.CMTB/PULSE_FILLING_PERCENT',
    'pulse_flattop_percent_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/MAIN.CMTB/PULSE_FLATTOP_PERCENT',
     'amplifier_out_amp_addr': 'doocs://TTF.RF/LLRF.CONTROLLER/IOTOUT.M1.CMTB/AMPL&spectrum="data"',
     'amplifier_out_pha_addr': 'doocs://TTF.RF/LLRF.CONTROLLER/IOTOUT.M1.CMTB/PHASE&spectrum="data"',
     'amplifier_in_amp_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/DRIVEROUT.M1.CMTB/AMPL&spectrum="data"',
     'amplifier_in_pha_addr' : 'doocs://TTF.RF/LLRF.CONTROLLER/DRIVEROUT.M1.CMTB/PHASE&spectrum="data"',

    }

def generate_cavity_conf(path):
    '''
        Generate cavity configurations
    '''
    for cavity in range(1, 9):
        cavity_name = 'C{}.M1.CMTB'.format(cavity)
        cavity_elements = {k : v.format(cavity) for
                           k, v in
                           CMTB_CAVITY.items()}

        sl_cal_amp_addr = (cavity-1)%2
        ch_cal_amp_addr = ['12', '34', '56', '78'][int((cavity-1)/2)]

        cavity_elements['cavity_cal_amp_addr'] = 'doocs://TTF.RF/GPIB/C{}.MTS.PROBE/CH{}.VALUE.VPM'.format(ch_cal_amp_addr, 
                                                                                                       sl_cal_amp_addr)

        cavity_elements.update(CMTB_STATION)
        conf_path = os.path.join(path, 'multi.C' + str(cavity) + '.toml')
        print('Creating ', conf_path)
        with open(conf_path, 'w') as f_cav:
            toml.dump(cavity_elements, f_cav)

    cavity_elements = {'template_' + k : v for k, v in CMTB_CAVITY.items()}
    cavity_elements.update(CMTB_STATION)
    conf_path = os.path.join(path, 'multi.template.toml')
    print('Creating ', conf_path)
    with open(conf_path, 'w') as f_cav:
       toml.dump(cavity_elements, f_cav)


def generate_station_conf(path):
    '''
        Generate station configurations
    '''
    print('Creating ', os.path.join(path, 'multi.toml'))
    with open(os.path.join(path, 'multi.toml'), 'w') as f:
        toml.dump(CMTB_STATION, f)

#pylint: disable=invalid-name
def generate_CMTB_multi_confs(path):
    '''
        Generate the FLASH toml configurations for cavity and stations
    '''
    generate_cavity_conf(path)
    generate_station_conf(path)
