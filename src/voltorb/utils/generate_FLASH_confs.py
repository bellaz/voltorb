#pylint: disable=invalid-name
'''
    This module generate the FLASH toml configurations for cavity and stations
'''

import os
import toml

FLASH_CAVITY = {
    'cavity_name' : '{}',
    'cavity_cal_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/PROBE.AMPL.SAMPLE&spectrum="data"',
    'cavity_sig_time_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/PROBE.AMPL&spectrum="time"',
    'probe_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/PROBE.AMPL&spectrum="data"',
    'probe_pha_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/PROBE.PHASE&spectrum="data"',
    'vforw_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VFORW.AMPL&spectrum="data"',
    'vforw_pha_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VFORW.PHASE&spectrum="data"',
    'vrefl_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VREFL.AMPL&spectrum="data"',
    'vrefl_pha_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VREFL.PHASE&spectrum="data"',
    'probe_amp_scale_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/PROBE.CAL_SCA&spectrum="data"',
    'probe_pha_rotation_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/PROBE.CAL_ROT&spectrum="data"',

    'vforw_amp_scale_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VFORW.CAL_SCA&spectrum="data"',
    'vforw_pha_rotation_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VFORW.CAL_ROT&spectrum="data"',
    'vrefl_amp_scale_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VREFL.CAL_SCA&spectrum="data"',
    'vrefl_pha_rotation_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VREFL.CAL_ROT&spectrum="data"',
    'vforw_power_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/{}/VFORW.AMPL.SAMPLE&spectrum="data"',
    'cav_ql_addr' : 'doocs://FLASH.RF/LLRF.DIAGNOSTICS/{}/CAV_QL',
    'coupler_value_addr' : 'doocs://FLASH.RF/LLRF.QLSET/{}/QL_SETPOINT',
    'coupler_enable_addr' : 'doocs://FLASH.RF/LLRF.QLSET/{}/QL_ADJ_ENA',
    'piezo_enable_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/PIEZO.ENA',
    'dac_enable_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/DAC.ENA',
    'piezo_ac_voltage_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/PIEZO_AC_VOLTAGE',
    'piezo_pulses_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/PIEZO_PULSES',
    'piezo_freq_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/PIEZO_FREQ',
    'piezo_delay_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/PIEZO_DLY',
    'piezo_sensor_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/PIEZO_SENSE&spectrum="data"',
    'piezo_voltage_addr' : 'doocs://FLASH.RF/LLRF.PIEZO/{}/DAC_OFFSET_SP&spectrum="data"',
    'detuning_addr' : 'doocs://FLASH.RF/LLRF.DIAGNOSTICS/{}/DET_PULSE&spectrum="data"',
    'detuning_time_addr' : 'doocs://FLASH.RF/LLRF.DIAGNOSTICS/{}/DET_PULSE&spectrum="time"',
    }

FLASH_STATION = {
    'sp_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/SP.AMPL',
    'ff_table_I_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/FF_I.TD&spectrum="data"',
    'ff_table_Q_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/FF_Q.TD&spectrum="data"',
    'corr_enable_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/FFC_USER.ENA',
    'corr_table_I_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/FFC_USER_I.TD&spectrum="data"',
    'corr_table_Q_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/FFC_USER_Q.TD&spectrum="data"',
    'amplifier_out_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/C1.M1.{}/VFORW.AMPL&spectrum="data"',
    'amplifier_out_pha_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/C1.M1.{}/VFORW.PHASE&spectrum="data"',
    #'amplifier_in_amp_addr' : 'doocs://FLASH.RF/LLRF.KLM/MAIN.{}/FD.FI.AMP&spectrum="data"',
    #'amplifier_in_pha_addr' : 'doocs://FLASH.RF/LLRF.KLM/MAIN.{}/FD.FI.PHA&spectrum="data"',
    'vm_output_I_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/COUT_I.TD&spectrum="data"',
    'vm_output_Q_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/COUT_Q.TD&spectrum="data"',
    'vm_output_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/VM.{}/VM.MON.AMPL&spectrum="data"',
    'vm_output_pha_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/VM.{}/VM.MON.PHASE&spectrum="data"',
    'vm_dac_offset_I_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/VM.{}/DAC_OFFSET_I',
    'vm_dac_offset_Q_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/VM.{}/DAC_OFFSET_Q',
    'pha_out_rotation_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/ANGLE.OUT_ROTATION',
    'ovc_pha_rotation_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/OVC.{}/ROTATION',
    'amp_out_scale_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/CTRL.{}/AMPL.OUT_ROTATION',
    'ovc_amp_scale_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/OVC.{}/SCALE',
    'vs_amp_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/VS.{}/AMPL.TD&spectrum="data"',
    'vs_pha_addr' : 'doocs://FLASH.RF/LLRF.CONTROLLER/VS.{}/PHASE.TD&spectrum="data"',}

def generate_stations():
    '''
        Generate FLASH station names
    '''
    stations = ['ACC1', 'ACC39', 'ACC23', 'ACC45', 'ACC67']
    return stations

def generate_cavity_conf(path):
    '''
        Generate cavity configurations
    '''
    stations = generate_stations()
    for station in stations:
        modules = 2
        if (station == 'ACC1') or (station == 'ACC39'):
            modules = 1

        cavities = 8
        if station == 'ACC39':
            cavities = 4

        station_elements = {k : v.format(station) for k, v in FLASH_STATION.items()}
        for module in range(1, modules+1):
            for cavity in range(1, cavities+1):
                cavity_name = 'C{}.M{}.{}'.format(cavity, module, station)
                cavity_elements = {k : v.format(cavity_name) for
                                   k, v in
                                   FLASH_CAVITY.items() if
                                   ('piezo' not in k) or (station != 'AH1.I1')}

                cavity_elements.update(station_elements)
                cavity_elements['cav_ql_target'] = 4.6E6
                conf_path = os.path.join(path, 'FLASH/cavity', cavity_name + '.toml')
                print('Creating ', conf_path)
                with open(conf_path, 'w') as f_cav:
                    toml.dump(cavity_elements, f_cav)

        cavity_name = 'C{}.M{}.{}'.format('{}', '{}', station)
        cavity_elements = {'template_' + k : v.format(cavity_name) for
                           k, v in
                           FLASH_CAVITY.items() if
                           ('piezo' not in k) or (station != 'AH1.I1')}

        cavity_elements.update(station_elements)
        cavity_elements['cav_ql_target'] = 4.6E6
        conf_path = os.path.join(path, 'FLASH/cavity/template', station + '.toml')
        print('Creating ', conf_path)
        with open(conf_path, 'w') as f_cav:
            toml.dump(cavity_elements, f_cav)


def generate_station_conf(path):
    '''
        Generate station configurations
    '''
    stations = generate_stations()
    for station in stations:
        station_elements = {k : v.format(station) for k, v in FLASH_STATION.items()}
        station_elements['cav_ql_target'] = 4.6E6
        conf_path = os.path.join(path, 'FLASH/station', station + '.toml')
        print('Creating ', conf_path)
        with open(conf_path, 'w') as f_cav:
            toml.dump(station_elements, f_cav)

#pylint: disable=invalid-name
def generate_FLASH_confs(path):
    '''
        Generate the FLASH toml configurations for cavity and stations
    '''
    os.mkdir(os.path.join(path, 'FLASH'))
    os.mkdir(os.path.join(path, 'FLASH/cavity'))
    os.mkdir(os.path.join(path, 'FLASH/cavity/template'))
    os.mkdir(os.path.join(path, 'FLASH/station'))
    os.mkdir(os.path.join(path, 'FLASH/station/template'))
    generate_cavity_conf(path)
    generate_station_conf(path)
