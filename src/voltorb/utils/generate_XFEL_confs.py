#pylint: disable=invalid-name
'''
    This module generate the XFEL toml configurations for cavity and stations
'''

import os
import toml

XFEL_CAVITY = {
    'cavity_name' : '{}',
    'cavity_cal_amp_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/PROBE.AMPL.SAMPLE&spectrum="data"',
    'cavity_sig_time_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/PROBE.AMPL&spectrum="time"',
    'probe_amp_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/PROBE.AMPL&spectrum="data"',
    'probe_pha_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/PROBE.PHASE&spectrum="data"',
    'vforw_amp_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VFORW.AMPL&spectrum="data"',
    'vforw_pha_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VFORW.PHASE&spectrum="data"',
    'vrefl_amp_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VREFL.AMPL&spectrum="data"',
    'vrefl_pha_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VREFL.PHASE&spectrum="data"',
    'cav_ql_addr' : 'doocs://XFEL.RF/LLRF.DIAGNOSTICS/{}/CAV_QL',
    'coupler_value_addr' : 'doocs://XFEL.RF/LLRF.QLSET/{}/QL_SETPOINT',
    'coupler_enable_addr' : 'doocs://XFEL.RF/LLRF.QLSET/{}/QL_ADJ_ENA',
    'piezo_enable_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/PIEZO.ENA',
    'dac_enable_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/DAC.ENA',
    'piezo_ac_voltage_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/PIEZO_AC_VOLTAGE',
    'piezo_pulses_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/PIEZO_PULSES',
    'piezo_freq_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/PIEZO_FREQ',
    'piezo_delay_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/PIEZO_DLY',
    'piezo_sensor_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/PIEZO_SENSE&spectrum="data"',
    'probe_amp_scale_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/PROBE.CAL_SCA&spectrum="data"',
    'probe_pha_rotation_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/PROBE.CAL_ROT&spectrum="data"',

    'vforw_amp_scale_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VFORW.CAL_SCA&spectrum="data"',
    'vforw_pha_rotation_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VFORW.CAL_ROT&spectrum="data"',
    'vrefl_amp_scale_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VREFL.CAL_SCA&spectrum="data"',
    'vrefl_pha_rotation_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VREFL.CAL_ROT&spectrum="data"',
    'vforw_power_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/{}/VFORW.AMPL.SAMPLE&spectrum="data"',
    'piezo_voltage_addr' : 'doocs://XFEL.RF/LLRF.PIEZO/{}/DAC_OFFSET_SP&spectrum="data"',
    'detuning_addr' : 'doocs://XFEL.RF/LLRF.DIAGNOSTICS/{}/DET_PULSE&spectrum="data"',
    'detuning_time_addr' : 'doocs://XFEL.RF/LLRF.DIAGNOSTICS/{}/DET_PULSE&spectrum="time"',
    'fpc_temp_addr' : 'doocs://XFEL.RF/TIL.CPL/{}/PTADC.70K&spectrum="data"'}

XFEL_STATION = {
    'sp_amp_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/SP.AMPL',
    'ff_table_I_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FF_I.TD&spectrum="data"',
    'ff_table_Q_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FF_Q.TD&spectrum="data"',
    'corr_enable_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FFC_USER.ENA',
    'corr_table_I_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FFC_USER_I.TD&spectrum="data"',
    'corr_table_Q_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FFC_USER_Q.TD&spectrum="data"',
    'amplifier_out_amp_addr' : 'doocs://XFEL.RF/LLRF.KLM/MAIN.{}/FD.F1.AMP&spectrum="data"',
    'amplifier_out_pha_addr' : 'doocs://XFEL.RF/LLRF.KLM/MAIN.{}/FD.F1.PHA&spectrum="data"',
    'amplifier_out_amp2_addr' : 'doocs://XFEL.RF/LLRF.KLM/MAIN.{}/FD.F2.AMP&spectrum="data"',
    'amplifier_out_pha2_addr' : 'doocs://XFEL.RF/LLRF.KLM/MAIN.{}/FD.F2.PHA&spectrum="data"',
    'amplifier_in_amp_addr' : 'doocs://XFEL.RF/LLRF.KLM/MAIN.{}/FD.FI.AMP&spectrum="data"',
    'amplifier_in_pha_addr' : 'doocs://XFEL.RF/LLRF.KLM/MAIN.{}/FD.FI.PHA&spectrum="data"',
    'vm_output_I_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/COUT_I.TD&spectrum="data"',
    'vm_output_Q_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/COUT_Q.TD&spectrum="data"',
    'vm_output_amp_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/VM.{}/VM.MON.AMPL&spectrum="data"',
    'vm_output_pha_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/VM.{}/VM.MON.PHASE&spectrum="data"',
    'vm_dac_offset_I_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/VM.{}/DAC_OFFSET_I',
    'vm_dac_offset_Q_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/VM.{}/DAC_OFFSET_Q',
    'pha_out_rotation_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/ANGLE.OUT_ROTATION',
    'ovc_pha_rotation_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/OVC.{}/ROTATION',
    'amp_out_scale_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/CTRL.{}/AMPL.OUT_ROTATION',
    'ovc_amp_scale_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/OVC.{}/SCALE',
    'vs_amp_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/VS.{}/AMPL.TD&spectrum="data"',
    'vs_pha_addr' : 'doocs://XFEL.RF/LLRF.CONTROLLER/VS.{}/PHASE.TD&spectrum="data"'}


def generate_stations():
    '''
        Generate xfel station names
    '''
    stations = ['A1.I1', 'AH1.I1', 'A2.L1', 'A3.L2', 'A4.L2', 'A5.L2']
    stations += ['A{}.L3'.format(i) for i in range(6, 26)]
    return stations

def generate_cavity_conf(path):
    '''
        Generate cavity configurations
    '''
    stations = generate_stations()
    for station in stations:
        modules = 4
        if (station == 'A1.I1') or (station == 'AH1.I1'):
            modules = 1

        station_elements = {k : v.format(station) for k, v in XFEL_STATION.items()}

        for module in range(1, modules+1):
            for cavity in range(1, 9):
                cavity_name = 'C{}.M{}.{}'.format(cavity, module, station)
                cavity_elements = {k : v.format(cavity_name) for
                                   k, v in
                                   XFEL_CAVITY.items() if
                                   ('piezo' not in k) or (station != 'AH1.I1')}

                cavity_elements.update(station_elements)
                cavity_elements['cav_ql_target'] = 4.6E6
                conf_path = os.path.join(path, 'XFEL/cavity', cavity_name + '.toml')
                print('Creating ', conf_path)
                with open(conf_path, 'w') as f_cav:
                    toml.dump(cavity_elements, f_cav)

        cavity_name = 'C{}.M{}.{}'.format('{}', '{}', station)
        cavity_elements = {'template_' + k : v.format(cavity_name) for
                           k, v in
                           XFEL_CAVITY.items() if
                           ('piezo' not in k) or (station != 'AH1.I1')}

        cavity_elements.update(station_elements)
        cavity_elements['cav_ql_target'] = 4.6E6
        conf_path = os.path.join(path, 'XFEL/cavity/template', station + '.toml')
        print('Creating ', conf_path)
        with open(conf_path, 'w') as f_cav:
            toml.dump(cavity_elements, f_cav)


def generate_station_conf(path):
    '''
        Generate station configurations
    '''
    stations = generate_stations()
    for station in stations:
        station_elements = {k : v.format(station) for k, v in XFEL_STATION.items()}
        station_elements['cav_ql_target'] = 4.6E6
        conf_path = os.path.join(path, 'XFEL/station', station + '.toml')
        print('Creating ', conf_path)
        with open(conf_path, 'w') as f_cav:
            toml.dump(station_elements, f_cav)

#pylint: disable=invalid-name
def generate_XFEL_confs(path):
    '''
        Generate the XFEL toml configurations for cavity and stations
    '''
    os.mkdir(os.path.join(path, 'XFEL'))
    os.mkdir(os.path.join(path, 'XFEL/cavity'))
    os.mkdir(os.path.join(path, 'XFEL/cavity/template'))
    os.mkdir(os.path.join(path, 'XFEL/station'))
    os.mkdir(os.path.join(path, 'XFEL/station/template'))
    generate_cavity_conf(path)
    generate_station_conf(path)
