'''
    Return a multisine signal with amplitude [-0.5, 0.5]
'''

import numpy as np

def multisine(size, fs, maxf=None):
    '''
        Return a multisine signal with amplitude between -0.5 and 0.5
    '''
    if maxf is None:
        maxf = fs * 0.5
    if maxf > fs * 0.5:
        print("maxf should be less than the Nyquist frequency")

    result = np.zeros(size)
    frequencies = np.arange(1.0/fs, maxf/size, 1.0/fs) * (2.0 * np.pi)
    bins = np.arange(0.0, size, 1.0)

    for i, frequency in enumerate(frequencies):
        result += np.sin(frequency * bins -
                         i * (i+1) * np.pi / (len(frequencies)+1))

    return result * (0.5 / (max(max(result), abs(max(result)))))


