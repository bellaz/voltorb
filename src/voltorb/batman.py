'''
    Batman function http://mathworld.wolfram.com/BatmanCurve.html
'''

import numpy as np
import matplotlib.pyplot as plt

def batman_up(x):
    '''
        upper part of the batman function
    '''
    return ((batman_h(x)-batman_l(x))*np.heaviside(x+1,0)+
            (batman_r(x)-batman_h(x))*np.heaviside(x-1,0)+
            (batman_l(x)-batman_w(x))*np.heaviside(x+3,0)+
            (batman_w(x)-batman_r(x))*np.heaviside(x-3,0)
            +batman_w(x))

def batman_down(x):
    '''
        lower part of the batman function
    '''
    return  (1/2*(np.abs(1/2*x)+
            np.nan_to_num(np.sqrt(1-(np.abs(np.abs(x)-2)-1)**2))-
            1/112*(3*np.sqrt(33)-7)*x**2+
            3*np.nan_to_num(np.sqrt(1-(1/7*x)**2))-3)*
            (np.sign(x+4)-np.sign(x-4))-
            3*np.nan_to_num(np.sqrt(1-(1/7*x)**2)))

def batman_w(x):
    '''
        w term
    '''
    return np.nan_to_num(3*np.sqrt(1-(x/7)**2))

def batman_l(x):
    '''
        l term
    '''
    return (x+3)/2-3/7*np.nan_to_num(np.sqrt(10*(4-(x+1)**2)))+6/7*np.sqrt(10)

def batman_h(x):
    '''
        h term
    '''
    return 1/2*(3*(np.abs(x+1/2)+np.abs(x-1/2)+6)-11*(np.abs(x+3/4)+np.abs(x-3/4)))

def batman_r(x):
    '''
        r term
    '''
    return (3-x)/2-3/7*np.nan_to_num(np.sqrt(10*(4-(x-1)**2)))+6/7*np.sqrt(10)

if __name__ == "__main__" :
    # execute only if run as a script
    plt.figure()
    plt.plot(batman_up(np.linspace(-7,7,4096)))
    plt.plot(batman_down(np.linspace(-7,7,4096)))
    plt.show()
