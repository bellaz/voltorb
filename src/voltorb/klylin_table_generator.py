# pylint: disable=invalid-name
'''
    This script produces table needed from klylin to work
'''
import numpy as np
### PHASE IS IN RADIANS!!! ###

IQ_BIT_SIZE = 17
A2_BIT_SIZE = 25
UNITY_BIT_LEN = 15
TABLE_BIT_LEN = 10

class KlylinGenerator(object):
    '''
        generates klylin tables, given IQ correction tables
    '''
    class Klylin(object):
        '''
            Klylin object. it can map an input IQ signal to a output that
            compensates the distorsions of the amplifier
        '''
        def __init__(self, klylin_gen, corr_I, corr_Q):
            '''
                Initialize the klylin, corr_I and corr_Q are klylin.table_len long
                and represents the IQ correction at equidistant requested power point
            '''
            self.unity = klylin_gen.unity
            self.iq_value = klylin_gen.iq_value
            self.a2_value = klylin_gen.a2_value
            self.table_len = klylin_gen.table_len
            self.shift = klylin_gen.shift
            self.corr_I = corr_I
            self.corr_Q = corr_Q

            # writing base terms
            self.base_I_tables = (np.delete(corr_I, -1)*self.unity).astype(int)
            self.base_Q_tables = (np.delete(corr_Q, -1)*self.unity).astype(int)

            # calculate the differences between steps
            diff_correction_I = ((np.delete(corr_I, 0)*self.unity)
                                 -(np.delete(corr_I, -1)*self.unity))
            diff_correction_Q = ((np.delete(corr_Q, 0)*self.unity)
                                 -(np.delete(corr_Q, -1)*self.unity))

            # calculate the derivatives dY/dA^2. the distance between two samples is
            # the highest represented number inside the unit divided by the table length
            self.deriv_I_tables = diff_correction_I
            self.deriv_Q_tables = diff_correction_Q
            #self.deriv_I_tables = np.floor(self.deriv_I_tables*self.table_len/self.a2_value)
            #self.deriv_Q_tables = np.floor(self.deriv_Q_tables*self.table_len/self.a2_value)
            self.deriv_I_tables = np.floor(self.deriv_I_tables)
            self.deriv_Q_tables = np.floor(self.deriv_Q_tables)
            # integers
            self.deriv_I_tables = self.deriv_I_tables.astype(int)
            self.deriv_Q_tables = self.deriv_Q_tables.astype(int)

        def correction_fun(self, x):
            '''
                simulates correction of IQ channels. the input x are between 1 and
                MAX_VALUE
            '''
            # calculates the squared norm
            sq_x = x**2
            # resize the squared norm
            klylin_sq_x = np.floor(sq_x/self.shift)
            # take an index between 0 and KLYLIN_TABLE_LEN-1
            idx = np.floor(klylin_sq_x/np.around(self.a2_value/self.table_len))
            # read coefficients for base and derivative terms for I and Q
            idx_arr = np.linspace(0, self.table_len-1, self.table_len)
            base_I = np.interp(idx, idx_arr, self.base_I_tables)
            base_Q = np.interp(idx, idx_arr, self.base_Q_tables)
            deriv_I = np.interp(idx, idx_arr, self.deriv_I_tables)
            deriv_Q = np.interp(idx, idx_arr, self.deriv_Q_tables)
            dx = klylin_sq_x - idx*self.a2_value/self.table_len
            corr_I = base_I + np.floor(deriv_I*dx*self.table_len/self.a2_value)
            corr_Q = base_Q + np.floor(deriv_Q*dx*self.table_len/self.a2_value)
            return (corr_I, corr_Q)


        def correction_fun_base_only(self, x):
            '''
                simulates correction of IQ channels. the input x are between 1 and
                MAX_VALUE
            '''
            # calculates the squared norm
            sq_x = x**2
            # resize the squared norm
            klylin_sq_x = np.floor(sq_x/self.shift)
            # take an index between 0 and KLYLIN_TABLE_LEN-1
            idx = np.floor(klylin_sq_x/np.around(self.a2_value/self.table_len))
            # read coefficients for base terms for I and Q
            idx_arr = np.linspace(0, self.table_len-1, self.table_len)
            base_I = np.interp(idx, idx_arr, self.base_I_tables)
            base_Q = np.interp(idx, idx_arr, self.base_Q_tables)
            return (base_I, base_Q)


        def correct_IQ(self, i, q=0):
            '''
                correct I and Q value using the klylin tables
            '''
            a = np.sqrt(i**2 + q**2)
            (corr_I, corr_Q) = self.correction_fun(a)
            new_i = np.floor((corr_I*i - corr_Q*q)/self.unity)
            new_q = np.floor((corr_Q*i + corr_I*q)/self.unity)
            return(new_i, new_q)

        def correct_IQ_base(self, i, q=0):
            '''
                correct I and Q value using only base klylin tables
            '''
            a = np.sqrt(i**2 + q**2)
            (corr_I, corr_Q) = self.correction_fun_base_only(a)
            new_i = np.floor((corr_I*i - corr_Q*q)/self.unity)
            new_q = np.floor((corr_Q*i + corr_I*q)/self.unity)
            return(new_i, new_q)

    def __init__(self, unity_bit=UNITY_BIT_LEN,
                 iq_bit_size=IQ_BIT_SIZE,
                 a2_bit_size=A2_BIT_SIZE,
                 table_bit_len=TABLE_BIT_LEN,
                 interpolation="quadratic"):
        '''
            Initialize the generator with klylin values
        '''
        self.unity = 2**unity_bit # the output unity
        self.iq_value = 2**iq_bit_size # maximum value of input iq channels
        self.a2_value = 2**a2_bit_size # maximum rapresentable number in klylin
        self.table_len = 2**table_bit_len # the number of stored elements
        self.shift = 2**(iq_bit_size*2-a2_bit_size)
        self.interpolation = interpolation

    def generate_klylin_AP(self, amp, pha, vm_values):
        '''
            generate klylin from amplitude and phase measurements at VM output
            values on channel I. vm_values should be ordered from min to max
        '''
        amp_ = np.copy(amp)
        pha_ = np.copy(pha)
        vm_values_ = np.copy(vm_values)
        (amp_, pha_, vm_values_) = self.extend_measure(amp_, pha_, vm_values_)
        (corr_I, corr_Q) = self.generate_corr(amp_, pha_, vm_values_)
        return self.Klylin(self, corr_I, corr_Q)

    def extend_measure(self, amp, pha, vm_values):
        '''
            extend measure covering the full span of vm values
        '''
        idxmax = np.argmax(vm_values)
        vm_values = vm_values[:idxmax]
        amp = amp[:idxmax]
        pha = pha[:idxmax]

        ## extend begin
        amp = np.insert(amp, 0, 0)
        vm_values = np.insert(vm_values, 0, 0)
        pha = np.insert(pha, 0, pha[0])

        ## extend end
        amp = np.append(amp, amp[-1]*self.iq_value/vm_values[-1])
        vm_values = np.append(vm_values, self.iq_value)
        pha = np.append(pha, pha[-1])

        return (amp, pha, vm_values)

    def generate_corr(self, amp, pha, vm_values):
        '''
            generate correction tables from an amplitude
            and phase measurement
        '''
        ## normalize amplitude output and input values
        amp /= amp[-1]
        vm_values = vm_values / self.iq_value
        sq_amp = amp**2
        sq_vm_values = vm_values**2
        # array of equidistant output power points
        sq_amp_map = np.linspace(1.0/(self.table_len),
                                 1.0,
                                 self.table_len)

        # map vm values using linearized power output
        if self.interpolation == "quadratic":
            sq_vm_map = np.interp(sq_amp_map, sq_amp, sq_vm_values)
            # scaling factor
            scale_corr = np.sqrt(sq_vm_map/sq_amp_map)
            pha_corr = -np.interp(sq_vm_map, sq_vm_values, pha)
        elif self.interpolation == "linear":
            vm_map = np.interp(sq_amp_map, sq_amp, vm_values)
            # scaling factor
            scale_corr = vm_map/np.sqrt(sq_amp_map)
            pha_corr = -np.interp(vm_map, vm_values, pha)
#        elif self.interpolation == "lms_linear":
#            piecewise_vm = pwlf.PiecewiseLinFit(sq_amp, vm_values)
#            piecewise_vm.fit_with_breaks(sq_amp_map)
#            vm_map = piecewise_vm.predict(sq_amp_map)
#            #scaling factor
#            scale_corr = vm_map/np.sqrt(sq_amp_map)
#            piecewise_pha = pwlf.PiecewiseLinFit(vm_values, pha)
#            piecewise_pha.fit_with_breaks(vm_map)
#            pha_corr = -piecewise_pha.predict(vm_map)
#
#        elif self.interpolation == "lms_quadratic":
#            piecewise_vm = pwlf.PiecewiseLinFit(sq_amp, sq_vm_values)
#            piecewise_vm.fit_with_breaks(sq_amp_map)
#            sq_vm_map = piecewise_vm.predict(sq_amp_map)
#            #scaling factor
#            scale_corr = np.sqrt(sq_vm_map/sq_amp_map)
#            piecewise_pha = pwlf.PiecewiseLinFit(sq_vm_values, pha)
#            piecewise_pha.fit_with_breaks(sq_vm_map)
#            pha_corr = -piecewise_pha.predict(sq_vm_map)
#
        else:
            raise RuntimeError('Select a valid interpolation metod:'
                               '[linear, quadratic, lms_linear, lms_quadratic]')


        corr_I = scale_corr*np.cos(pha_corr)
        corr_Q = scale_corr*np.sin(pha_corr)
        corr_I = np.insert(corr_I, 0, corr_I[0])
        corr_Q = np.insert(corr_Q, 0, corr_Q[0])

        return (corr_I, corr_Q)

