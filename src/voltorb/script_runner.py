#/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
'''
    Runner of voltorb_scripts scripts
'''

import pkgutil
import argcomplete, argparse
import voltorb_scripts

def func_factory(parser):
    '''
        produce funcs when an incomplete script path is called
    '''
    def func(parsed):
        '''
            parsed element handler
        '''
        #pylint: disable=unused-argument
        parser.print_usage()
        exit(0)

    return func

def traverse_module(path, subparsers, modpath):
    '''
        Traverse voltorb_scripts to find all runnable modules
    '''
    for _, modname, ispkg in pkgutil.iter_modules([path]):
        if ispkg:
            module = __import__(modpath + '.' + modname)
            subparser = subparsers.add_parser(modname, description=module.__doc__)
            subparsers_p = subparser.add_subparsers()
            traverse_module(path +'/'+ modname,
                            subparsers_p,
                            modpath + '.' + modname)
            subparser.set_defaults(func=func_factory(subparser))

        else:
            module = __import__(modpath + '.' + modname, fromlist=['main'])
            if hasattr(module, 'main'):
                main_run = getattr(module, 'main')
                subparser = subparsers.add_parser(modname, description=main_run.desc)
                subparser.set_defaults(func=main_run.main)
                main_run.manager(subparser)

def main():
    '''
        Entry point for the scripts
    '''
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    traverse_module(list(voltorb_scripts.__path__)[0], subparsers, 'voltorb_scripts')
    parser.set_defaults(func=func_factory(parser))
    argcomplete.autocomplete(parser)
    parsed = parser.parse_args()
    parsed.func(parsed)
