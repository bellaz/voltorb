'''
    manager for TINE control system
'''

import os
import numpy as np
from .manager_abstract import ManagerAbstract

PyTine = None #pylint: disable=invalid-name

VECTOR_TYPES = ['A_INT',
                'A_LONG',
                'A_FLOAT',
                'A_DOUBLE',
                'A_BYTE',
                'SPECTRUM',
                'GSPECTRUM']

try:
    import PyTine
except ImportError:
    pass

pydoocs = None #pylint: disable=invalid-name

try:
    import pydoocs
except ImportError:
    pass

class ManagerTine(ManagerAbstract):
    '''
        TINE backend manager
    '''
    @staticmethod
    def read(addresses):
        '''
            read a value from the specified address
        '''
        if PyTine is not None:
            return ManagerTine.read_pytine(addresses)

        if pydoocs is not None:
            return ManagerTine.read_pydoocs(addresses)

        raise RuntimeError('No TINE backend library found')

    @staticmethod
    def write(address, value):
        '''
            write a value to the specified address
        '''
        if PyTine is not None:
            return ManagerTine.write_pytine(address, value)

        if pydoocs is not None:
            return ManagerTine.write_pydoocs(address, value)

        raise RuntimeError('No TINE backend library found')

    @staticmethod
    def read_pytine(addresses):
        '''
            read a value from the specified address with PyTine
        '''
        results = []
        for (address, query) in addresses:
            (dpath, pname) = ManagerTine.splitaddress(address)
            data = PyTine.get(dpath, pname)['data']

            if ('data' not in query) or query['data'] != 'raw':
                data = np.array(data)
            results.append(data)
        
        return results

    @staticmethod
    def write_pytine(address, value):
        '''
            write a value to the specified address with PyTine
        '''
        address, = address
        (dpath, pname) = ManagerTine.splitaddress(address)
        PyTine.set(dpath, pname, value)
        return value

    @staticmethod
    def splitaddress(address):
        '''
            split tine address in devicepath/property name
        '''
        return (os.path.dirname(address), os.path.basename(address))

    @staticmethod
    def read_pydoocs(addresses):
        '''
            read a value from the specified address
        '''
        results = []
        for (address, query) in addresses:
            data = pydoocs.read('tine:/' + address)
            datatype = data['type']
            if ('data' not in query) or query['data'] != 'raw':
                data = data['data']
                if datatype in VECTOR_TYPES:
                    data = np.array(data)

            results.append(data)

        return results

    @staticmethod
    def write_pydoocs(address, value):
        '''
            write a value to the specified address
        '''
        address = address[0]
        pydoocs.write('tine:/' + address, value)
        return value
