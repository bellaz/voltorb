'''
    manager for DOOCS control system
'''

import numpy as np
from .manager_abstract import ManagerAbstract

pydoocs = None #pylint: disable=invalid-name

VECTOR_TYPES = ['A_INT',
                'A_LONG',
                'A_FLOAT',
                'A_DOUBLE',
                'A_BYTE',
                'SPECTRUM',
                'GSPECTRUM']

try:
    import pydoocs
except ImportError:
    pass

class ManagerDoocs(ManagerAbstract):
    '''
        DOOCS backend manager
    '''
    @staticmethod
    def read(addresses):
        '''
            read a value from the specified address
        '''
        if pydoocs is not None:
            return ManagerDoocs.read_pydoocs(addresses)

        raise RuntimeError('No DOOCS backend library found')

    @staticmethod
    def write(address, value):
        '''
            write a value to the specified address
        '''

        if pydoocs is not None:
            return ManagerDoocs.write_pydoocs(address, value)

        raise RuntimeError('No DOOCS backend library found')

    @staticmethod
    def read_pydoocs(addresses):
        '''
            read a value from the specified address
        '''
        macropulse = None
        results = []

        for (addr, query) in addresses:

            data = None
            if macropulse is None:
                data = pydoocs.read(addr)
                macropulse = data['macropulse']
            else:
                data = pydoocs.read(addr, macropulse=macropulse)

            datatype = data['type']

            if ('data' not in query) or query['data'] != 'raw':

                if datatype in VECTOR_TYPES:
                    data = np.array(data['data'])
                else:
                    data = data['data']

                if (datatype == 'SPECTRUM') and 'spectrum' in query:
                    if query['spectrum'] == 'data':
                        data = data.transpose()[1]
                    elif query['spectrum'] == 'time':
                        data = data.transpose()[0]
                    elif query['spectrum'] == 'transpose':
                        data = data.transpose()
                    else:
                        pass

            results.append(data)

        return results

    @staticmethod
    def write_pydoocs(address, value):
        '''
            write a value to the specified address
        '''
        address = address[0]
        pydoocs.write(address, value)
