'''
    Abstract control system manager class
'''
from abc import ABC, abstractmethod

class ManagerAbstract(ABC):
    '''
        Abstract manager class that rapresents a control system address
        that can be used to communicate with.
    '''
    pass
