'''
    manager for EPICS control system
'''

import numpy as np
from .manager_abstract import ManagerAbstract

epics = None #pylint: disable=invalid-name

try:
    import epics
except ImportError:
    pass

pydoocs = None #pylint: disable=invalid-name

try:
    import pydoocs
except ImportError:
    pass

class ManagerEpics(ManagerAbstract):
    '''
        EPICS backend manager
    '''
    @staticmethod
    def read(addresses):
        '''
            read a value from the specified address
        '''
        if epics is not None:
            return ManagerEpics.read_pyepics(addresses)

        if pydoocs is not None:
            return ManagerEpics.read_pydoocs(addresses)

        raise RuntimeError('No EPICS backend library found')

    @staticmethod
    def write(address, value):
        '''
            write a value to the specified address
        '''
        if epics is not None:
            return ManagerEpics.write_pyepics(address, value)

        if pydoocs is not None:
            return ManagerEpics.write_pydoocs(address, value)

        raise RuntimeError('No EPICS backend library found')

    @staticmethod
    def read_pyepics(addresses):
        '''
            read a value from the specified address with epics
        '''

        address_strings = [el[0] for el in addresses]
        results = epics.caget_many(address_strings)

        return results

    @staticmethod
    def write_pyepics(address, value):
        '''
            write a value to the specified address with epics
        '''
        address, = address
        epics.caput(address, value)

    @staticmethod
    def read_pydoocs(addresses):
        '''
            read a value from the specified address
        '''
        macropulse = None

        for (addr, query) in addresses:

            data = None
            if macropulse is None:
                data = pydoocs.read(addr)
                macropulse = data['macropulse']
            else:
                data = pydoocs.read('epics://' + addr, macropulse=macropulse)

            if ('data' not in query) or query['data'] != 'raw':
                data = np.array(data)

            return data

    @staticmethod
    def write_pydoocs(address, value):
        '''
            write a value to the specified address
        '''
        address, = address
        pydoocs.write('epics://' + address, value)
