'''
    Default matplotlib parameters
'''

import matplotlib
from matplotlib import pyplot, font_manager

pyplot.ion()
matplotlib.style.use
matplotlib.rcParams["font.family"] = ["Computer Modern", "Times New Roman"]
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['figure.figsize'] = (3.54*2, 2.65*2)
matplotlib.rcParams['figure.dpi'] = 80
matplotlib.rcParams['font.size'] = 18
matplotlib.rcParams['legend.fontsize'] = 7*2
matplotlib.rcParams['figure.titlesize'] = 7*2
matplotlib.rcParams['lines.linewidth'] = 2

#pyplot.ticklabel_format(axis="both", style="sci", scilimits=(0,0))
